1. **Yêu cầu hệ thống:**

- Ubuntu 18.04
- Cấu hình tối thiểu 4gb Ram ( 8gb ram - cấu hình an toàn)

2. **Cài đặt**

Chạy file run.sh để bắt đầu cài đặt:
Để setup server và admin chạy file run.sh, lúc chạy sẽ yêu cầu nhập các thông tin như sau:

**Cấu hình Website**
- Nhập domain cho website. Lưu ý domain phải có định dạng (http/https):// ví dụ http://domain.vn hoạc https://domain.vn
- Nhập tên của website

**Cấu hình SSR**
- Nhập port cho ssr mặc đinh 3001
- Nhập tên website cho ssr

**Cấu hình API**

- Nhập port cho api: Lúc cấu hình domain api mình sẽ trỏ đến port này.

- Nhập domain api: Lưu ý domain phải có định dạng (http/https):// http://api.domain.vn hoạc https://api.domain.vn. 
Nếu nhập sai thì chương trình sẽ báo Domain không hợp lệ! và kết thúc chương trình.(1)

**Cấu hình mysql**: quá trình cài đặt cho phép chọn 2 chế độ: tạo mới mysql và sử dụng cấu hình mysql có sẵn
Tạo database y/n?
- (y) Chương trình sẽ yêu cầu nhập thêm password để tạo database. Lưu ý password không chứa ký tự đặc biệt, nếu nhập sai password chương trình sẽ kết thúc. Thông tin kết nối đến database là: username: root, password: input bạn vừa nhập.

- (n) Chương trình yêu cầu nhập các thông tin của database: username, password, host, port. Nếu không nhập username, password, host, port chương trình sẽ kết thúc. (Lưu ý: Chương trình sẽ tạo mới database: unlock_phone)

Toàn bộ quá trình setup sẽ thiết lập và khởi chạy các dịch vụ:

- Ssr : mặc định ở cổng 3001
- Api : mặc định ở cổng 9090
- Web Client: mặc định ở cổng 80
- Web admin: mặc định ở cổng 3000 - với tài khoản admin mặc định **admin/admin**

Lưu ý
- Trong quá trình setup hệ thống, chương tình sẽ tự động cài thêm docker, mysql-client, nhập Y để tiếp tục chương trình
- Sau khi cài đặt xong phải trỏ domain api ở (1) vào service api tương ứng

Sau khi cài đặt xong thì chương trình tự động sinh ra các file restart từng service. Khi chạy file restart cần nhập thông tin domain api
- restart_admin.sh: chạy file này để chạy lại con admin
- restart_api.sh: chạy file này để chạy lại con api
- restart_ssr.sh: chạy file này để chạy lại con ssr
- restart_web.sh: chạy file này để chạy lại con web