#!/bin/bash
# shellcheck disable=SC2164
cd script
chmod +x get_docker.sh
chmod +x service.sh
chmod +x start_with_createdb.sh
chmod +x start_without_createdb.sh
./get_docker.sh
./service.sh
# shellcheck disable=SC2103
cd ..
chmod +x restart_admin.sh
chmod +x restart_api.sh
chmod +x restart_ssr.sh
chmod +x restart_web.sh
