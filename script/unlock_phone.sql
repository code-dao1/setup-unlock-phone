-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: unlock_phone
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `checkout_config`
--

DROP TABLE IF EXISTS `checkout_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checkout_config` (
  `id` int NOT NULL AUTO_INCREMENT,
  `merchant_id` varchar(256) NOT NULL,
  `optional_1` varchar(256) NOT NULL,
  `tracking_url` varchar(256) NOT NULL,
  `key` varchar(256) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkout_config`
--

LOCK TABLES `checkout_config` WRITE;
/*!40000 ALTER TABLE `checkout_config` DISABLE KEYS */;
INSERT INTO `checkout_config` VALUES (1,'officialiphoneunlock_usd','cellunlocker','https://example.com/?strIMEI=','expressunlock-dot-com-2020',NULL,NULL,'2020-10-30 15:35:02',1,NULL,NULL);
/*!40000 ALTER TABLE `checkout_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manufacturer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `handle` varchar(128) NOT NULL,
  `title_detail` varchar(256) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `image_title` varchar(256) DEFAULT NULL,
  `image_src` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int NOT NULL,
  `updated_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `seo_title` text,
  `seo_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_deleted_at_UNIQUE` (`name`,(ifnull(`deleted_at`,_utf8mb4''))),
  UNIQUE KEY `handle_deleted_at_UNIQUE` (`handle`,(ifnull(`deleted_at`,_utf8mb4''))),
  KEY `handle_IDX` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer`
--

LOCK TABLES `manufacturer` WRITE;
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_legal`
--

DROP TABLE IF EXISTS `page_legal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `page_legal` (
  `id` int NOT NULL AUTO_INCREMENT,
  `handle` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `content` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int NOT NULL,
  `updated_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle_UNIQUE` (`handle`),
  KEY `handle_IDX` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_legal`
--

LOCK TABLES `page_legal` WRITE;
/*!40000 ALTER TABLE `page_legal` DISABLE KEYS */;
INSERT INTO `page_legal` VALUES (1,'privacy-policy','Privacy Policy','<p> This website is operated by Phone Approved Unlock. We take your privacy very seriously and want to explain to you how Phone Approved Unlock obtains and processes personal data to help you understand in a clear and transparent way how we collect, use, and share information about you, and give you the opportunity to decide how your data is handled. </p> <br /> <p><b>1. Who are we?</b></p> <p> Phone Approved Unlock is an Internet-based technology company that provides a wide range of services for the phone industry. We are responsible for handling the personal data transferred between our customers and Phone Approved Unlock. Personal details will only be kept for the purpose of processing your order. Payment details are not stored by Phone Approved Unlock, payment details are received securely by our payment processors to process payment only. </p> <br /> <p><b>2. For whom is this Privacy Policy intended?</b></p> <p>This Privacy Policy is intended for the following Phone Approved Unlock users:</p> <ul> <li> <p>Users of Phone Approved Unlock SERVICES, (for example, phone unlocking services and IMEI checks).</p> </li> <li> <p>Users browsing the Phone Approved Unlock website searching for products and services of interest to them, looking for the best deals or comparing current market prices.</p> </li> <li> <p>Phone Approved Unlock Users that provide personal information by completing an online form.</p> </li> </ul> <br /> <p><b>3. What data do we collect?</b></p> <p>Phone Approved Unlock can obtain data from its Users coming from:</p> <ul> <li> <p> Information requested using our online checkout form, basically, identifying data related to your phone (including, but not limited to, the IMEI number or original operator) and the SERVICE you are interested in purchasing. </p> </li> <li> <p>Information through interactions with the Phone Approved Unlock website (such as purchases, phone unlockings, phone checks, refunds, questions or comments) related to Phone Approved Unlock’s SERVICES.</p> </li> <li> <p>Contact information provided to Phone Approved Unlock through different communication channels, or information requests regarding a SERVICE.</p> </li> <li> <p>Browsing data obtained from our website which is used for technical purposes, performance enhancements and for analysing commercial transactions.</p> </li> </ul> <br /> <p><b>4. How do we use your personal information?</b></p> <p> We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways: </p> <ul> <li> <p>To personalise user\'s experience and to allow us to deliver the type of content and product offerings in which you are most interested.</p> </li> <li> <p>To improve our website in order to better serve you.</p> </li> <li> <p>To allow us to better service you in responding to your customer service requests.</p> </li> <li> <p>To quickly process your transactions.</p> </li> <li> <p>To follow up with them after correspondence (live chat, email or phone inquiries)</p> </li> </ul> <br /> <p><b>5. Do you share my personal data with third parties?</b></p> <p> We may share your personal data with specific providers when necessary for the fulfilment of the SERVICE, to unlock a device, to deliver items purchased through Phone Approved Unlock, or to process your complaints and disputes related to the service. </p> <p>Keeping in mind that Phone Approved Unlock offers a wide variety of online payment methods, operating at all times within a safe and secure environment to minimise possible fraud.</p> <br /> <p><b>6. How do we protect visitor information?</b></p> <p> Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. </p> <p>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</p> <p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p> <br /> <p><b>7. Can I receive promotional materials from Phone Approved Unlock?</b></p> <p> We would like to provide you with regular updates and inform you of any additional SERVICES, however, we guarantee you can opt out immediately from any e-mail you receive from Phone Approved Unlock by contacting support@phoneapprovedunlock.com. </p> <br /> <p><b>8. What are my rights regarding personal data?</b></p> <p>Under the general data protection regulation, you have a number of important rights.</p> <p>The rights you have with regards to your personal information are as follows:</p> <ul> <li> <p>The right to request access to and obtain free of charge any personal information we have stored on you and to use for your own purposes.</p> </li> <li> <p>The right to update any personal information which is incorrect or incomplete.</p> </li> <li> <p>The right to ask us to delete all personal information on you from our records.</p> </li> <li> <p>The right to object to the processing of your personal data in certain circumstances.</p> </li> <li> <p>You have the right to withdraw your consent at any time.</p> </li> <li> <p>You have the right to restrict the processing of your personal data in certain circumstances.</p> </li> <li> <p>The right to request that your personal information is sent to another company, institution, organization or yourself.</p> </li> </ul> <br /> <p> For further information on each of those rights, including the circumstances in which they apply, see the guidance from the UK Information Commissioner’s Office (ICO) on individuals rights under the General Data Protection Regulations. </p> <br /> <p><b>9. How can I exercise my rights?</b></p> <p>You can exercise each and every one of your rights by contacting us:</p> <ul> <li> <p>By sending an e-mail to support@phoneapprovedunlock.com with “Data Protection Rights” in the subject header.</p> </li> </ul> <br /> <p><b>10. Do we use \'cookies\'?</b></p> <p> Yes. Cookies are small files that a site or its service provider transfers to your computer\'s hard drive through your Web browser (if you allow) that enables the site\'s or service provider\'s systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. </p> <p>We use cookies to:</p> <ul> <li> <p>Help remember and process the items in the shopping cart.</p> </li> <li> <p>Understand and save user\'s preferences for future visits.</p> </li> </ul> <br /> <p> You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser\'s Help menu to learn the correct way to modify your cookies. </p> <p>If users disable cookies in their browser:</p> <ul> <li> <p>If you disable cookies off, some features will be disabled It will turn off some of the features that make your site experience more efficient and some of our services will not function properly.</p> </li> </ul> <br /> <p><b>11. Can the Privacy Policy change?</b></p> <p> The Privacy Policy may change in the future, for example, due to regulatory changes or changes to how Phone Approved Unlock operates its business. You should check this policy occasionally to ensure you are aware of the most recent version that will apply each time you access this website. </p>','2020-10-01 14:24:55','2020-10-10 18:23:01',NULL,0,2,NULL),(2,'terms-and-conditions','Terms and Conditions','<p>Our pre-order unlock service, is a service where you are placed on a mailing list which ensures keenest price of unlock from our service providers and allows us to check that the device has not been blacklisted. The pre-order charge will be offset against the final unlocking amount which may exceed the pre-order charge depending on the carrier/contract status of the device. If the device returns as blocked due to unpaid bills an additional fee will be required by the carrier to clear the balance in order to complete the unlock. An email notification will be sent to you as soon as the unlock is available to release. This is a specialist service, benefiting from discounted rates and faster unlocking times through submission of bulk unlocks.</p>\n<p>Our standard unlock service is for original device owners who are able to provide the original mobile number associated with the device, this is a one off payment with standard unlocking times set by the network provider.</p>\n<p><strong>1. iPhone Approved Unlock Service.</strong></p>\n<p>1.1 The services provided on the iPhone Approved Unlock website allow the user to remove the network lock or the iCloud lock from their mobile phone via IMEI number. All our services include customer support via email during office hours.</p>\n<p>1.2 Once full payment has been received from a customer our service of providing an unlock solution begins. At the point of order placement our timeframes are calculated and given as a guideline.</p>\n<p>1.3 Once the unlocking process is complete an email will be sent to the email address provided to notify the customer. The email will also provide the exact instructions for completing the unlocking process.</p>\n<p><strong>2. Refunds.</strong></p>\n<p>2.1 Refunds will be given if an unlock has not been delivered within 30 days and has not already been rejected for any reason. You cannot cancel prior to the 30 day window as our contract with our suppliers is 30 days and we incur costs as soon as the unlock is submitted.</p>\n<p>2.2 If you have placed a standard order with us and the device has contract issues then the unlock may not be released until the contract expires, generally after 6 months of network service. This is highlighted and agreed to along with our terms and conditions at the point of purchase. Please note that we incur considerable costs as soon as an order is submitted.</p>\n<p>2.3 iPhone Approved Unlock will not refund an order that has been submitted for unlocking and is being processed. We incur the cost of unlocking from the carrier immediately once we submit your unlock to them.</p>\n<p>2.4 Some devices will have a set amount of input attempts, we will not refund if this has been breeched.</p>\n<p>2.5 It is up to the customer to ensure the correct input of the phone\'s information and customer information when purchasing. The IMEI entered into the site must be found on the phone by dialing *#06#. If the IMEI, mobile number or network provided is later found to be submitted incorrectly by the customer then a refund will not be provided and the unlock request will come back “Blocked”.</p>\n<p>2.6 If an order is made with iPhone Approved Unlock and an unlock is sent to you; you the customer must provide us with proof that the unlock has failed to unlock your device, the device is still locked and provide full details of the procedure you took. Only then will a refund be issued.</p>\n<p>2.7 If your phone was already unlocked before you purchased an unlocking service, that will not be considered grounds for a refund.</p>\n<p>2.8 Refunds are not issued for a device that is locked to a corporate account as highlighted at the point of purchase.</p>\n<p>2.9 If your device has an iCloud activation lock and you can not activate it, you must remove the iCloud account by contacting the owner of the device or purchasing an iCloud removal from us or another provider. We will not provide a refund where we have completed a sim unlock which you are unable to use due to an Activation Lock.</p>\n<p>2.10 When purchasing an iCloud unlock, the IMEI should be clean and LOST MODE not activated. Refunds are not issued for an un-clean IMEI, if LOST MODE is activated as this service incurs considerable cost on our behalf. The original purchase receipt for the device will be required to prove ownership.</p>\n<p><strong>3. Buyer Requirements.</strong></p>\n<p>3.1 Customers must ensure that they check the compatibility of their handset with the network they intend to use once unlocked.</p>\n<p>3.2 It is the customer\'s responsibility to ensure that they have read the information on this website.</p>\n<p>3.3 Black listed / stolen / lost phones / contract issues / incorrect network provided (also known as Blocked) - The term black listed covers phones that are not paid off, lost, stolen, insurance claimed etc. It is the customer\'s responsibility to check whether the device is black listed. Lost, stolen and abused handsets are barred/blocked once reported and cannot be used inside the country it was blocked in on any major networks. If we undertake an unlock and find the phone has been marked Blocked and is in fact logged as either lost, stolen, abused, bad ESN and/or contract issues, we cannot refund your payment.</p>\n<p>3.4 Certain networks may be subject to an additional charge and or require the phone to be 6 months old before unlocking. As this is an automated process and the device has already been submitted for unlock, this will remain in queue and the unlock will be released for the device upon expiry of the 6 month contract.</p>\n<p>3.5 Prior to unlocking your phone, it is the customer\'s responsibility to make sure that their \'Contacts\', \'Messages\' and any other important data is properly backed up and synced prior to attempting any unlocking procedure. We will not accept responsibility for the loss of customer data for any reason.</p>\n<p>3.6 Unlocking your Phone is 100% legal in the Europe, America and many other regions. Some laws may vary and it is down to the buyer to comply with local laws when unlocking their device.</p>\n<p>3.7 In some circumstances we may require additional information about the device being unlocked.</p>\n<p>3.8 If an unlock is being processed by us the user may not attempt to obtain an unlock by any other method. This could lead to the device breaching the maximum number of unlock requests.</p>\n<p>3.9 Any issues with services provided by iPhone Approved Unlock MUST first be reported to us via the given support channels (email/helpdesk) We then request 7 days to satisfy any such issues.</p>\n<p><strong>4. Delivery Times and Service.</strong></p>\n<p>4.1 We reserve the right to change any information on the website including prices and delivery times at any time which are set by ourselves and the relative networks. If your unlock is in progress we will inform you immediately of any changes.</p>\n<p>4.2 All estimated delivery times are calculated using business days.</p>\n<p>4.3 We will endeavor to beat these delivery times but sometimes delays do occur with the network server. In the event of a significant delay, we would refund after 30 days.</p>\n<p><strong>5. Fraud Protection</strong></p>\n<p>5.1 We reserve the right to refuse any order that appears to be suspicious or that proves to be fraudulent.</p>\n<p>iPhone Approved Unlock reserve the right to amend these terms and conditions at any given time. All changes made will reload on the website at iPhone Approved Unlock. Therefore, the buyer is responsible to read the terms and conditions on every occasion you visit iPhone Approved Unlock. Your use of the website will signify your acceptance to be bound by the latest terms of use.</p>','2020-10-16 15:08:29','2020-10-16 15:15:30',NULL,0,1,NULL);
/*!40000 ALTER TABLE `page_legal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phone`
--

DROP TABLE IF EXISTS `phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phone` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(128) DEFAULT NULL,
  `handle` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` longtext,
  `how_description` longtext,
  `manufacturer_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image_src` varchar(200) DEFAULT NULL,
  `image_title` varchar(100) DEFAULT NULL,
  `created_by` int NOT NULL,
  `updated_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `seo_title` text,
  `seo_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle_deleted_at_UNIQUE` (`handle`,(ifnull(`deleted_at`,_utf8mb4''))),
  UNIQUE KEY `name_deleted_at_UNIQUE` (`name`,(ifnull(`deleted_at`,_utf8mb4''))),
  UNIQUE KEY `phone_code_deleted_at_UNIQUE` (`code`,(ifnull(`deleted_at`,_utf8mb4''))),
  KEY `manufacturer_id_IDX` (`manufacturer_id`),
  KEY `handle_IDX` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phone`
--

LOCK TABLES `phone` WRITE;
/*!40000 ALTER TABLE `phone` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phone_service`
--

DROP TABLE IF EXISTS `phone_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phone_service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `phone_id` int NOT NULL,
  `service_id` int NOT NULL,
  `reference_id` varchar(128) DEFAULT NULL,
  `price` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `created_by` int NOT NULL,
  `deleted_by` int DEFAULT NULL,
  `is_active` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `phone_id_IDX` (`phone_id`) /*!80000 INVISIBLE */,
  KEY `service_id_IDX` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phone_service`
--

LOCK TABLES `phone_service` WRITE;
/*!40000 ALTER TABLE `phone_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `phone_service_view`
--

DROP TABLE IF EXISTS `phone_service_view`;
/*!50001 DROP VIEW IF EXISTS `phone_service_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `phone_service_view` AS SELECT 
 1 AS `id`,
 1 AS `phone_id`,
 1 AS `service_id`,
 1 AS `reference_id`,
 1 AS `price`,
 1 AS `created_at`,
 1 AS `updated_at`,
 1 AS `deleted_at`,
 1 AS `updated_by`,
 1 AS `created_by`,
 1 AS `deleted_by`,
 1 AS `is_active`,
 1 AS `phone_name`,
 1 AS `service_name`,
 1 AS `user_created`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `handle` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` longtext NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `image_title` varchar(256) DEFAULT NULL,
  `image_src` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int NOT NULL,
  `updated_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `user_name` char(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle_deleted_at_UNIQUE` (`handle`,(ifnull(`deleted_at`,_utf8mb4'')))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script`
--

DROP TABLE IF EXISTS `script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `script` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `type` char(64) NOT NULL COMMENT 'global or payment_success',
  `is_active` bit(1) DEFAULT b'0',
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script`
--

LOCK TABLES `script` WRITE;
/*!40000 ALTER TABLE `script` DISABLE KEYS */;
/*!40000 ALTER TABLE `script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(128) DEFAULT NULL,
  `handle` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` longtext,
  `service_group_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int NOT NULL,
  `updated_by` int DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `image_src` varchar(256) DEFAULT NULL,
  `image_title` varchar(256) DEFAULT NULL,
  `from_price` float DEFAULT 0,
  `repair_time` varchar(128) DEFAULT NULL,
  `seo_title` text,
  `seo_description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_name_service_group_id_uindex` (`name`,`service_group_id`,(ifnull(`deleted_at`,_utf8mb4''))),
  UNIQUE KEY `service_handle_service_group_id_uindex` (`handle`,`service_group_id`,(ifnull(`deleted_at`,_utf8mb4''))),
  UNIQUE KEY `service_code_deleted_at_UNIQUE` (`code`,(ifnull(`deleted_at`,_utf8mb4''))),
  KEY `service_group_id_IDX` (`service_group_id`),
  KEY `handle_IDX` (`handle`),
  CONSTRAINT `service_service_group_id_fk` FOREIGN KEY (`service_group_id`) REFERENCES `service_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_group`
--

DROP TABLE IF EXISTS `service_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `manufacturer_id` int NOT NULL,
  `handle` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `created_by` int NOT NULL,
  `deleted_by` int DEFAULT NULL,
  `image_src` varchar(256) DEFAULT NULL,
  `image_title` varchar(256) DEFAULT NULL,
  `country_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle_deleted_at_UNIQUE` (`handle`,(ifnull(`deleted_at`,_utf8mb4''))),
  KEY `manufacturer_id_IDX` (`manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_group`
--

LOCK TABLES `service_group` WRITE;
/*!40000 ALTER TABLE `service_group` DISABLE KEYS */;
INSERT INTO `service_group` VALUES (26,2,'checking','Checking','<p>Checking</p>','2020-10-16 14:32:25','2020-10-16 14:38:32',NULL,2,2,NULL,'https://doctorunlock.net/images/resize/400/attachment/2014/10/24/0-iphone-imei-checker.png','0-iphone-imei-checker.png',NULL),(27,2,'austria-carriers','Austria carriers','<p><br></p>','2020-10-16 14:41:52',NULL,'2020-10-28 05:22:46',NULL,2,2,'https://doctorunlock.net/images/resize/35/attachment/2016/9/28/austria.png','austria.png','Austria '),(31,2,'usa-carriers','USA Carriers','<p><br></p>','2020-10-26 03:44:33','2020-10-28 05:00:22',NULL,2,2,NULL,'http://13.212.124.16:9090/2020/10/26/us.png','us.png','United States');
/*!40000 ALTER TABLE `service_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theme`
--

DROP TABLE IF EXISTS `theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `theme` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theme`
--

LOCK TABLES `theme` WRITE;
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` VALUES (1,'theme1','Light mode','theme1',_binary '\0',NULL);
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(200) NOT NULL,
  `full_name` varchar(128) DEFAULT NULL,
  `roles` varchar(30) DEFAULT 'admin',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','$2a$10$UDD0PTSD.mw4ZneuK/C3D.Sg1CMFJLq/IcO.wHCnNy5SWS3ZMnwmy','Admin','admin','2020-09-23 16:31:16',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `web_config`
--

DROP TABLE IF EXISTS `web_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `web_config` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` text,
  `description` text,
  `updated_by` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `url_hosting` text,
  `brand_name` varchar(128) DEFAULT NULL,
  `keyword` text,
  `logo` text,
  `favicon` text,
  `image_url` text,
  `image_type` varchar(64) DEFAULT NULL,
  `image_height` int DEFAULT NULL,
  `image_width` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_config`
--

LOCK TABLES `web_config` WRITE;
/*!40000 ALTER TABLE `web_config` DISABLE KEYS */;
INSERT INTO `web_config` VALUES (1,'Express Unlock, Unlock phone online - unlock phone by imei','Express Unlock, Unlock phone online by imei good quality and reasonable price',3,'2020-11-05 16:32:53',NULL,'https://expressunlock.com','Express Unlock','Express Unlock, unlock phone, crack phone, crack network, unlock network','https://expressunlock.com/media/images/system/logo-1x.png','https://expressunlock.com/favicon.png','https://expressunlock.com/media/images/background.jpg','image/jpeg',315,600);
/*!40000 ALTER TABLE `web_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `youtube_review`
--

DROP TABLE IF EXISTS `youtube_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `youtube_review` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `created_by` int NOT NULL,
  `deleted_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `youtube_review`
--

LOCK TABLES `youtube_review` WRITE;
/*!40000 ALTER TABLE `youtube_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `youtube_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `phone_service_view`
--

/*!50001 DROP VIEW IF EXISTS `phone_service_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `phone_service_view` AS select `phone_service`.`id` AS `id`,`phone_service`.`phone_id` AS `phone_id`,`phone_service`.`service_id` AS `service_id`,`phone_service`.`reference_id` AS `reference_id`,`phone_service`.`price` AS `price`,`phone_service`.`created_at` AS `created_at`,`phone_service`.`updated_at` AS `updated_at`,`phone_service`.`deleted_at` AS `deleted_at`,`phone_service`.`updated_by` AS `updated_by`,`phone_service`.`created_by` AS `created_by`,`phone_service`.`deleted_by` AS `deleted_by`,`phone_service`.`is_active` AS `is_active`,`p`.`name` AS `phone_name`,`s`.`name` AS `service_name`,`u`.`full_name` AS `user_created` from (((`phone_service` join `phone` `p` on((`phone_service`.`phone_id` = `p`.`id`))) join `service` `s` on((`phone_service`.`service_id` = `s`.`id`))) join `user` `u` on((`p`.`created_by` = `u`.`id`))) where ((`s`.`deleted_at` is null) and (`p`.`deleted_at` is null) and (`phone_service`.`deleted_at` is null)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-30  1:03:50
