var reviewInPage = [];
var page = 0;
var isImage = undefined;
const pageSize = 10;
var totalPage = 1;
var star = undefined;
var sort_field = undefined;
var sort_direction = undefined;
const WEBSITE_ID = getWebsiteId();

onClickFilterImage = function () {
  isImage = !isImage;
  if (isImage) {
    $("#option-filter-image").removeClass('no-active');
  } else {
    $("#option-filter-image").addClass('no-active');
  }

  fetchDataInPage({ isImage: isImage });
}

onClickFilterStar = function () {
  const value = parseInt($('#option-filter-star').val());
  if (!value || value === -1) {
    star = undefined;
  } else {
    star = value;
  }

  fetchDataInPage({ star });
}

onClickSortReview = function () {
  const value = parseInt($('#option-sort-review').val());
  if (value === 0) {
    sort_direction = undefined;
    fetchDataInPage();
    return;
  } else {
    if (value === 1) {
      sort_direction = 'asc';
    } else  {
      sort_direction = 'desc';
    }
  }

  fetchDataInPage({ sort_direction, sort_field: 'updated_at' });
}

fetchDataInPage = (option) => {
  let {
    page: page_input = (page + 1) || 1,
    sort_field: sort_field_input = sort_field,
    sort_direction: sort_direction_input = sort_direction,
    isImage: isImageInput = isImage,
    star: star_input = star
  } = option || {};
  if (!page_input) return;
  axios.get(
    pushParams({
      baseURI:`${CORE_API}/api/product/reviews`,
      params: {
        website_id: WEBSITE_ID,
        handle: getHandleProduct(),
        star: star_input,
        image: isImageInput || undefined,
        sort_field: sort_field_input,
        sort_direction: sort_direction_input,
        page: page_input,
        page_size: pageSize
      }
    })
  )
  .then(function (response) {
      // handle success
      if (response.status === 200) {
        const data = response.data;
        const {
          code,
          message,
          body
        } = data;
        if (code === 200 && isObject(body)) {
          let {
            items,
            page,
            total = 1
          } = body;
          reviewInPage = items;
          renderListReview(items);
          if (page === 1) {
            renderPagination({ totalPage: Math.ceil(total / pageSize) });
            renderTitleContainerReview(total)
          }
        }
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}

fetchSummaryReview = () => {
  axios.get(
    pushParams({
      baseURI:`${CORE_API}/api/product/review/summary`,
      params: {
        website_id: WEBSITE_ID,
        handle: getHandleProduct(),
      }
    })
  )
    .then(function (response) {
      // handle success
      if (response.status === 200) {
        const data = response.data;
        const {
          code,
          message,
          body
        } = data;
        if (code === 200 && isObject(body)) {
          renderSummaryReview(body);
        }
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}

renderListReview = function (reviews) {
  if (!reviews || reviews.length === 0) {
    $('#feedback-list-wrap').html(`<h6>Sản phảm chưa có đánh giá</h6>`);
    return;
  }

  const listItemReviewHTML = reviews
    .map(review => renderItemReview(review))
    .join('') || '';
  $('#feedback-list-wrap').html('');
  $('#feedback-list-wrap').html(listItemReviewHTML);
  setTimeout(function () {
    $('.materialboxed').materialbox();
  }, 100);
}

renderTitleContainerReview = function (total) {
  $('#title-review-container').html(`Đánh giá của khách hàng (${total})`);
}

renderSummaryReview = function (data) {
  const {
    average,
    five_start,
    four_start,
    num_review_image,
    one_start,
    three_start,
    total,
    two_start
  } = data;

  for(let i = 1; i <= 5; ++i) {
    let percent = 0;
    switch (i) {
      case 1:
        percent = one_start;
        break;
      case 2:
        percent = two_start;
        break;
      case 3:
        percent = three_start;
        break;
      case 4:
        percent = four_start;
        break;
      case 5:
      default:
        percent = five_start;
    }
    $(`#ratting-category-${i} .determinate`).css("width", `${percent}%`);
    $(`#ratting-category-${i} .num-percent`).html(`${percent}%`)
  }
  if (average) {
    $('#summary-score .rate-score-number b').html(average.toFixed(2))
  }
  $('#summary-score .star-view-big span').css("width", `${((average / 5).toFixed(2)) * 100}%`);
  $('#option-filter-image').html(`Hình ảnh (${num_review_image})`)

}

renderItemReview = function (data) {
  const {
    id,
    content,
    images,
    ratting = 5,
    status,
    time_review,
    username,
    uuid
  } = data;
  const currentUuid = getOrCreateUuid();
  const urlUser = `/display/detail.htm?ownerMemberId=${uuid}&amp;memberType=buyer`;
  const userInfo = `<div class="fb-user-info">
                <span class="user-name">
            <a href="${urlUser}" target="_blank" rel="nofollow" name="member_detail">${username}</a>
                </span>
<!--                <div class="user-country"><b class="css_flag css_ru">RU</b></div>-->
            </div>`;

  const rateHTML = `<div class="f-rate-info">
        <span class="star-view"><span style="width: ${(ratting / 5).toFixed(2) * 100}%;"></span></span>
    </div>`;

  const userOrderInfo = `<div class="user-order-info">
        <span class="first">
            <strong>Ring Size:</strong>
            7
        </span>
        <span>
            <strong>Logistics:</strong>
            AliExpress Standard Shipping
        </span>
    </div>`;

  const timeReview = time_review ? `<span class="r-time-new">${time_review}</span>` : '';

  let imagesHMTL = images.map((image, index) => {
    return `<li class="pic-view-item" data-index="${index}" data-src="${image}" data-eid="${index}">
            <img class="materialboxed" src="${image}" />
          </li>`;
  }).join('');
  if (imagesHMTL) {
    imagesHMTL = `<div class="r-photo-list" data-role="photo-list" data-image-num="4">
        <ul class="util-clearfix">
          ${imagesHMTL}
        </ul>
      </div>`;
  }

  const contentHTML = `<div class="f-content">
    <div class="buyer-review">
      <div class="buyer-feedback">
        <span>
          ${content}
        </span>
        ${timeReview}
      </div>
      ${imagesHMTL}
    </div>
  </div>`;

  let edit = '';
  if (uuid === currentUuid) {
    edit = `<a class="button-edit-review waves-effect btn-flat" onclick='showEditReview(${id})'><i class="material-icons">edit</i></a>`;
  }

  return `
    <div class="feedback-item clearfix">
        ${userInfo}
        <div id="review-item-${id}" class="fb-main">
            ${rateHTML}
            ${userOrderInfo}
            ${contentHTML}
            ${edit}
        </div>
    </div>
  `;
}

renderPagination = function ({ totalPage }) {
  $('#pagination-review').html('');
  $('#pagination-review').materializePagination({
    align: 'right',
    lastPage: totalPage || 1,
    firstPage: 1,
    useUrlParameter: false,
    onClickCallback: function(requestedPage) {
      fetchDataInPage({ page: requestedPage })
    }
  });
}

$( document ).ready(function() {
  fetchDataInPage();
  fetchSummaryReview();
});
