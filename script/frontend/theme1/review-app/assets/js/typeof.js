function isObject (value) {
  return value && typeof value === 'object' && value.constructor === Object;
}

function isArray (value) {
  return value && typeof value === 'object' && value.constructor === Array;
}

function isString (value) {
  return typeof value === 'string' || value instanceof String;
}

function isEmpty(value) {
  return value === '' || value === undefined || value === null;
}

function isEmptyObject(obj) {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
}

function isNumber (value) {
  // eslint-disable-next-line no-restricted-globals
  return typeof value === 'number' && isFinite(value);
}

function isFunction (value) {
  return typeof value === 'function';
}

// Returns if a value is null
function isNull (value) {
  return value === null;
}

// Returns if a value is undefined
function isUndefined (value) {
  return typeof value === 'undefined';
}

function isBoolean (value) {
  return typeof value === 'boolean';
}

function isRegExp (value) {
  return value && typeof value === 'object' && value.constructor === RegExp;
}

function isError (value) {
  return value instanceof Error && typeof value.message !== 'undefined';
}

function isDate (value) {
  return value instanceof Date;
}

function isSymbol (value) {
  return typeof value === 'symbol';
}

function isExistKey(object, key) {
  // eslint-disable-next-line no-prototype-builtins
  return isObject(object) && object.hasOwnProperty(key);
}

function isErrorResponse (response) {
  return !response || !(!isExistKey(response, 'code') || response.code === 200) || isError(response);
}

function isAsyncFunction (value) {
  return isFunction(value) && value.constructor.name === 'AsyncFunction';
}

function isPromise (value) {
  return value instanceof Promise;
}

function isUrl(str) {
  let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  return !!pattern.test(str);
}

const getResult = function (response) {
  if (isErrorResponse(response)) {
    return {};
  } else if (isExistKey(response, 'body')) {
    return response.body;
  } else {
    return response;
  }
};

const getCodeResponse = function (response) {
  if (isError(response)) {
    try {
      return JSON.parse(response.message).code;
    } catch (e) {
      console.log(e);
    }
  }
  if (response) {
    return response.code;
  }
  return '';
};

const getMessage = function (response) {
  if (isError(response)) {
    try {
      return JSON.parse(response.message).message;
    } catch (e) {
      console.log(e);
    }
  }
  if (response && (isErrorResponse(response) || isError(response))) {
    return response.message || '';
  } else {
    return '';
  }
};
