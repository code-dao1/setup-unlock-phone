function setItem({ name, value }) {
  localStorage.setItem(name, value);
};

function getItem(name) {
  return localStorage.getItem(name);
};

function removeItem(name) {
  localStorage.removeItem(name);
};
