const CORE_API = "https://api.minhtv.com";

function getWebsiteId () {
  const url_string = window.location.href;
  const url = new URL(url_string);
  return url.searchParams.get("website_id");
}

function getHandleProduct() {
  const url_string = window.location.href;
  const url = new URL(url_string);
  return url.searchParams.get("product_handle");
}

function validateEmail(username) {
  const emailValidate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (emailValidate.test(username)) {
    return true;
  }
  return false;
};
