const parseGETParams = function ({ baseURI, params }) {
  let resultURI = baseURI;
  let gotValidParam = false;
  if (params) {
    Object.keys(params).map(keyName => {
      if (params[keyName] !== undefined) {
        if (gotValidParam) {
          resultURI += `&${keyName}=${params[keyName]}`;
        } else {
          resultURI += `?${keyName}=${params[keyName]}`;
          gotValidParam = true;
        }
      }
    });
    return resultURI;
  }
  return resultURI;
};

const pushParams = function ({ baseURI, params }) {
  if (!baseURI || !params) return baseURI;
  let resultURI = baseURI;
  if (baseURI.indexOf('?') === -1) {
    return parseGETParams({ baseURI, params });
  }

  Object.keys(params).map(keyName => {
    if (params[keyName] !== undefined) {
      resultURI += `&${keyName}=${params[keyName]}`;
    }
  });
  return resultURI;
};
