let numStarFormReview = 5;
let numImageUpdate = [];
let dataReviewChange = {};

function uploadFileToCdn(file, callback) {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("website_id", getWebsiteId());
  axios({
    method: 'post',
    url: `${CORE_API}/api/upload`,
    data: formData,
    headers: {'Content-Type': 'multipart/form-data' }
  })
    .then(function (response) {
      // handle success
      if (response.status === 200) {
        const data = response.data;
        const {
          code,
          message,
          body
        } = data;
        if (code === 200) {
          if (!isErrorResponse(data) && !isError(data)) {
            callback(body);
          } else {
            alert(getMessage(response));
          }
        }
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    });
}

function renderStarActive(numStar, id) {
  let idUpdate = '';
  if (id && dataReviewChange[id]) {
    idUpdate = `#review-input-num-star-edit-${id}`;
    dataReviewChange[id].ratting = numStar;
  } else {
    numStarFormReview = numStar;
  }

  let numStarEl = [];
  for (let i = 1; i <= 5; ++i) {
    if (i <= numStar) {
      numStarEl.push(`<div class="star-view-big" onclick='renderStarActive(${i}, ${id})'><span class="star-view-big-active"></span></div>`);
    } else {
      numStarEl.push(`<div class="star-view-big" onclick='renderStarActive(${i}, ${id})'><span class="star-view-big-no-active"></span></div>`);
    }
  }

  $(idUpdate || '#review-input-num-star').html(numStarEl.join(''))
}

function insertImagePreviewReview(id) {
  let idUpdate = ''
  if (id) {
    idUpdate = `input-upload-file-review-edit-${id}`;
  }
  const input = document.getElementById(idUpdate || 'input-upload-file-review');
  if (input && input.files && input.files.length > 0) {
    const files = input.files;
    Array.from(files).forEach(file => {
      uploadFileToCdn(file, function (url) {
        if (id && dataReviewChange[id]) {
          let images = dataReviewChange[id].images || [];
          images.push(url);
          dataReviewChange[id].images = images;
        } else {
          numImageUpdate.push(url);
        }
        renderImageUpdateReview(id);
      })
    })
  }
  $(`#${idUpdate}` || '#input-upload-file-review').val('')
}

function removeImagePreviewReview(index, id) {
  if (id && dataReviewChange[id]) {
    const images = dataReviewChange[id].images;
    dataReviewChange[id].images = [
      ...images.slice(0, index),
      ...images.slice(index + 1)
    ]
  } else {
    if (index < numImageUpdate.length) {
      numImageUpdate = [
        ...numImageUpdate.slice(0, index),
        ...numImageUpdate.slice(index + 1)
      ]
    }
  }

  renderImageUpdateReview(id);
}

function renderImageUpdateReview(id) {
  let idUpdate = ''
  let listImage;
  if (id && dataReviewChange[id]) {
    idUpdate = `#show-image-form-review-edit-${id}`;
    listImage = dataReviewChange[id].images;
  }

  const imageShow = (listImage || numImageUpdate).map((image, index) => {
    return `<div class="pic-show-image-item">
            <img class="materialboxed" src="${image}" />
            <a onclick="removeImagePreviewReview(${index}, ${id})" class="waves-effect button-close"><i class="material-icons">close</i></a>
          </div>`;
  });
  $(idUpdate || '#show-image-form-review').html(imageShow.join(''))
  setTimeout(function () {
    $('.materialboxed').materialbox();
  }, 100);
}

function getDataAndValidateFormReview() {
  const name = $('#username').val();
  const email = $('#email').val();
  const content = $('#content-review').val();

  if (!content) {
    alert('Bạn chưa nhập nội dung đánh giá');
    return;
  }

  if (!name) {
    alert('Bạn chưa nhập tên người đánh giá');
    return;
  }

  if (!email) {
    alert('Bạn chưa nhập email người đánh giá');
    return;
  }

  if(!validateEmail(email)) {
    alert('Định dạng email không chính xác');
    return;
  }

  return {
    "uuid": getOrCreateUuid(),
    "username": name,
    "email": email,
    "ratting": numStarFormReview,
    "content": content,
    "images": numImageUpdate,
    "product_id": getHandleProduct(),
    "website_id": getWebsiteId()
  }
}

function clearReviewForm() {
  numStarFormReview = 5;
  numImageUpdate = [];
  $('#username').val('');
  $('#email').val('');
  $('#content-review').val('');
  renderImageUpdateReview();
  renderStarActive(numStarFormReview);
}

function hideEditReview(id) {
  if(dataReviewChange[id]) {
    delete dataReviewChange[id];
  }
  renderListReview(reviewInPage);
}

function showEditReview(id) {
  const data = reviewInPage.find(review => review.id === id);
  if (!data) return;
  dataReviewChange[id] = data;
  const {
    content,
    images,
    ratting,
    status,
    time_review,
    username,
    uuid
  } = data;
  const templateHTML = `
    <div class="content-form-review">
        <div id="review-input-num-star-edit-${id}" class="input-num-star">
        </div>
        <div class="content-review">
          <textarea
           id="content-review-edit-${id}"
            class="textarea"
             placeholder="Nội dung đánh giá của bạn"
             >${content}</textarea>
        </div>
        <div id="show-image-form-review-edit-${id}">
        </div>
        <div class="button-add-photo-wrapper">
          <a class="file-field input-field button-add-photo waves-effect waves-light btn">
            <input type="file" id="input-upload-file-review-edit-${id}" multiple onchange="insertImagePreviewReview(${id})">
            <span>
              <i class="material-icons left">add_a_photo</i>Thêm hình ảnh
            </span>
          </a>
        </div>
        <div class="action-form-review row">
            <div class="button-create-review">
            <a onclick="hideEditReview(${id})" class="waves-effect btn-flat">Hủy</a>
          </div>
          <div class="button-create-review">
            <a onclick="changeReview(${id})" class="waves-effect waves-light btn">Đăng</a>
          </div>
        </div>
      </div>
  `
  $(`#review-item-${id}`).html(templateHTML);
  setTimeout(() => {
    renderImageUpdateReview(id);
    renderStarActive(data.ratting, id)
  }, 100);
}

function changeReview(id) {
  const content = $(`#content-review-edit-${id}`).val();
  if (!content) {
    alert('Nội dung đánh giá không được để trống');
    return;
  }
  const data = dataReviewChange[id];
  if (!data) return;
  axios.put(
    `${CORE_API}/api/review/${id}`,
    {
      ...data,
      content,
      "product_id": getHandleProduct(),
      "website_id": getWebsiteId()
    }
  )
    .then(function (response) {
      // handle success
      if (response.status === 200) {
        const data = response.data;
        const {
          code,
          message,
          body
        } = data;
        if (code === 200 && isObject(body)) {
          if (!isErrorResponse(data) && !isError(data)) {
            delete dataReviewChange[id];
            const indexUpdate = reviewInPage.findIndex(review => review.id === id);
            if (indexUpdate > -1) {
              reviewInPage = [
                ...reviewInPage.slice(0, indexUpdate),
                body,
                ...reviewInPage.slice(indexUpdate + 1)
              ];
              renderListReview(reviewInPage);
            }

          } else {
            alert(getMessage(response));
          }
        }
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    });
}

function insertReview() {
  const data = getDataAndValidateFormReview();
  if (!data) return;
  axios.post(
    `${CORE_API}/api/review`,
    data
  )
    .then(function (response) {
      // handle success
      if (response.status === 200) {
        const data = response.data;
        const {
          code,
          message,
          body
        } = data;
        if (code === 200 && isObject(body)) {
          if (!isErrorResponse(data) && !isError(data)) {
            clearReviewForm();
            alert('Thêm mới thành công');
            reviewInPage = [
              body,
              ...reviewInPage
            ];
            renderListReview(reviewInPage);
          } else {
            alert(getMessage(response));
          }
        }
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    });
}

$( document ).ready(function() {
  renderStarActive(numStarFormReview)
});
