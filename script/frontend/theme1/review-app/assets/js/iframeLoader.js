function loadIframeAsync (idParentIframe, urlIframeReview, timeLoad = 1) {
  const rs = loadIframe(idParentIframe, urlIframeReview);

  if (!rs && timeLoad <= 30) {
    setTimeout(() => {
      loadIframeAsync(idParentIframe, urlIframeReview, timeLoad + 1);
    }, 300);
  }
}

function getHandleProduct() {
  const pathname = window.location.pathname;
  const pathnameArray = pathname.split('/');
  return pathnameArray[pathnameArray.length - 1];
}

function loadIframe (idParentIframe, urlIframeReview) {
  const iframeHTML = `<iframe width="100%" src="${urlIframeReview}" title="App review"></iframe>`;
  const elementParentIframe = document.getElementById(idParentIframe);
  if (elementParentIframe) {
    elementParentIframe.innerHTML = iframeHTML;
    return true;
  }
  return false;
}
const urlReview = `https://hiweb.minhtv.com/review-app/index.html?website_id={{website_id}}&product_handle=${getHandleProduct()}`;
const idParentIframe = 'panel-app-review';
loadIframeAsync(idParentIframe, urlReview);
