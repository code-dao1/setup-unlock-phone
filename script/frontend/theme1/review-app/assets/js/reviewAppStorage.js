var uuidCurrent;
function setFieldAndValue({ field, value }) {
  setCookie(field, value, 60);
  setItem({
    name: field,
    value: value
  });
};

function getOrCreateUuid() {
  if (uuidCurrent) {
    return uuidCurrent;
  } else {
    try {
      const uuid = getFieldInStorage('uuid');
      if (uuid) {
        return uuid;
      } else {
        const newUuid = uuidv4();
        setFieldAndValue({ field: 'uuid', value: newUuid });
        return newUuid;
      }
    } catch (e) {
      uuidCurrent = uuidv4();
      return uuidCurrent;
    }
  }
};

function getFieldInStorage(field) {
  return getItem(field) || getCookie(field);
};
