#!/bin/bash
# create a variable password for database
# shellcheck disable=SC2162
echo "Nhập password cho database chú ý password không chứa ký tự đặc biệt, username mặc định root"
read -p "Nhập password: " password

# Check if string is empty using -z. For more 'help test'
if [[ -z "$password" ]]; then
  printf '%s\n' "password không hợp lệ"
  exit 1
else
  # If userInput is not empty show what the user typed in and run ls -l
  printf "password %s \n" "$password"
fi

sudo mkdir -p /etc/mysql/unlock-phone/conf.d
sudo mkdir -p /var/lib/mysql/unlock-phone

sudo docker rm -f mysql-unlock-phone
sudo docker rm -f unlock-phone
sudo docker network rm unlock-phone-network

sleep 2
sudo docker network create unlock-phone-network
sleep 2
sudo docker run \
  --detach \
  --name=mysql-unlock-phone \
  --env="MYSQL_ROOT_PASSWORD=$password" \
  --publish 3306:3306 \
  --volume=/etc/mysql/unlock-phone/conf.d:/etc/mysql/conf.d \
  --volume=/var/lib/mysql/unlock-phone:/var/lib/mysql \
  --network unlock-phone-network \
  mysql

sudo apt-get install mysql-client

sleep 2
sudo docker build -t cmdd/unlock-phone .

echo 'sleep 10s'
sleep 10
mysql -h 127.0.0.1 --user=root --password="$password" -e " create database unlock_phone; "

echo 'init database'
mysql -h 127.0.0.1 --user=root --password="$password" unlock_phone <unlock_phone.sql

echo 'sleep 5s'
sleep 5

args="-Dspring.datasource.url=jdbc:mysql://mysql-unlock-phone:3306/unlock_phone -Dspring.datasource.jdbc-url=jdbc:mysql://mysql-unlock-phone:3306/unlock_phone -Dspring.datasource.username=root -Dspring.datasource.password=$password -Dserver.domain=$2"

sudo mkdir -p /tmp/projects/unlock_phone
sudo chmod a+rwx /tmp/projects/unlock_phone

sudo docker run -e JAVA_ARGS="$args" --network unlock-phone-network -p $1:9090 -v /tmp/projects/unlock_phone:/usr/app/upload-dir -v /usr/share/nginx/html/admin:/usr/app/ui-deploy --name unlock-phone -d cmdd/unlock-phone

sed -i "s/USERNAME_DB/root/g" ../restart_api.sh
sed -i "s/PASSWORD_DB/$password/g" ../restart_api.sh
sed -i "s/HOST_DB/mysql-unlock-phone/g" ../restart_api.sh
sed -i "s/PORT_DB/3306/g" ../restart_api.sh
