export function isObject (value) {
  return value && typeof value === 'object' && value.constructor === Object;
}

export function isArray (value) {
  return value && typeof value === 'object' && value.constructor === Array;
}

export function isString (value) {
  return typeof value === 'string' || value instanceof String;
}

export function isEmpty(value) {
  return value === '' || value === undefined || value === null;
}

export function isNumber (value) {
  // eslint-disable-next-line no-restricted-globals
  return typeof value === 'number' && isFinite(value);
}

export function isFunction (value) {
  return typeof value === 'function';
}

// Returns if a value is null
export function isNull (value) {
  return value === null;
}

// Returns if a value is undefined
export function isUndefined (value) {
  return typeof value === 'undefined';
}

export function isBoolean (value) {
  return typeof value === 'boolean';
}

export function isRegExp (value) {
  return value && typeof value === 'object' && value.constructor === RegExp;
}

export function isError (value) {
  return value instanceof Error && typeof value.message !== 'undefined';
}

export function isDate (value) {
  return value instanceof Date;
}

export function isSymbol (value) {
  return typeof value === 'symbol';
}

export function isExistKey(object, key) {
  // eslint-disable-next-line no-prototype-builtins
  return isObject(object) && object.hasOwnProperty(key);
}

export function isErrorResponse (response) {
  return !response || isError(response);
}

export function isAsyncFunction (value) {
  return isFunction(value) && value.constructor.name === 'AsyncFunction';
}

export function isPromise (value) {
  return value instanceof Promise;
}

export function isUrl(str) {
  let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  return !!pattern.test(str);
}

export const getResult = (response) => {
  if (isErrorResponse(response)) {
    return {};
  } else if (isExistKey(response, 'body')) {
    return response.body;
  } else {
    return response;
  }
};

export const getCodeResponse = (response) => {
  if (isError(response)) {
    try {
      return JSON.parse(response.message).code;
    } catch (e) {
      console.log(e);
    }
  }
  if (response) {
    return response.code;
  }
  return '';
};

export const getMessage = (response) => {
  if (isError(response)) {
    try {
      return JSON.parse(response.message).message;
    } catch (e) {
      console.log(e);
    }
  }
  return (response && response.message) || '';
};
