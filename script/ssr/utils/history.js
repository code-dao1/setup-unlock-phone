export const getParamByRouter = (props, field) => {
  const params = props && props.query || {};
  return params && params[field];
};

export const getParamBySearch = (props, field) => {
  try {
    const search = props && props.location && props.location.search || '';

    if (!search || search.indexOf('?') === -1) {
      return '';
    }
    const paramString = search.substr(1);
    const params = paramString.split('&');

    let valueField = '';

    params.forEach((param, index, arr) => {
      if (!!param && param.indexOf('=') !== -1) {
        const [key, value] = param.split('=');
        if (key === field) {
          if (valueField) {
            valueField = [valueField, value];
          } else {
            valueField = value;
          }
        }
      }
    });
    return valueField;
  } catch (e) {
    console.log(e);
  }

  return '';
};

export const getStateFieldByHistory = (props, field) => {
  const state = props && props.location && props.location.state || {};
  return state && state[field];
};
