import { isArray } from './typeof';

export const genLinkProduct = (item, { noCategory = false } = {}) => {
  const {
    handle,
    product_categories
  } = item;
  if (!isArray(product_categories) || product_categories.length === 0 || noCategory) {
    return `/products/${handle}`;
  } else {
    const product_category = product_categories[0];
    return `/collections/${product_category.path}/products/${handle}`;
  }
};
