export function youtubeUrlDetect(url) {
  if (!url) return "";

  try {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    const match = url.match(regExp);
    return match && match[7].length === 11
      ? `https://youtube.com/embed/${match[7]}?showinfo=0`
      : false;
  } catch (e) {
    return url;
  }
}

export function vimeoUrlDetect(url) {
  if (!url) return "";

  try {
    const vimeoRegex = /https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/i;
    const vimeoMatches = url.match(vimeoRegex);

    if (vimeoMatches && vimeoMatches[3]) {
      return `https://player.vimeo.com/video/${vimeoMatches[3]}`;
    }

    return false;
  } catch (e) {
    return url;
  }
}
