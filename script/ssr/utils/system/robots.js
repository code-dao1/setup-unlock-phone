import { publicRuntimeConfig } from '@/constants/system/serverConfig';

export const createRobots = (props) => {
  return (`User-agent: *\nSitemap: ${publicRuntimeConfig.urlProduct}/sitemap.xml`);
};
