import MobileDetect from 'mobile-detect';
import { window, document } from 'ssr-window';
import { TABLET_BREAKPOINT, SM_DESKTOP_BREAKPOINT } from '@/constants/system/ui';

export const updateDeviceWidth = () => {
  return window.innerWidth > 0 ? window.innerWidth : window.screen.width;
};

export const isMobileDevice = () => {
  const md = new MobileDetect(window.navigator.userAgent);
  return md.mobile() && md.phone();
};

export const isTabletDevice = () => {
  const md = new MobileDetect(window.navigator.userAgent);
  return md.tablet();
};

export const isMobileScreen = (deviceWidth = updateDeviceWidth()) => {
  return deviceWidth < TABLET_BREAKPOINT;
};

export const isTabletScreen = (deviceWidth = updateDeviceWidth()) => {
  return deviceWidth < SM_DESKTOP_BREAKPOINT && deviceWidth >= TABLET_BREAKPOINT;
};

export const isDesktopScreen = (deviceWidth = updateDeviceWidth()) => {
  return deviceWidth >= SM_DESKTOP_BREAKPOINT;
};

export const isTouchDevice = () => {
  return isMobileDevice() || isTabletDevice();
};

export function classList(...classes) {
  return classes
    .filter(item => !!item)
    .join(' ');
}
