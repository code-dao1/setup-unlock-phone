import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import moment from 'moment';

export const createSitemap = (props) => {
  const {
    phones,
    services,
    blogs,
    manufacturers
  } = props;

  return (`<urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
  >
<!--  created with Free Online Sitemap Generator www.xml-sitemaps.com  -->

        ${(phones.items || [])
    .map(({ handle, created_at, updated_at, }) => {
      return `
                <url>
                    <loc>${`${publicRuntimeConfig.urlProduct}/unlock/${handle}`}</loc>
                    <changefreq>daily</changefreq>
                    <priority>1</priority>
                    <lastmod>${moment().format()}</lastmod>
                </url>
            `;
    })
    .join('')}

        ${(services.items || [])
    .map(({ handle, created_at, updated_at, }) => {
      return `
                    <url>
                        <loc>${`${publicRuntimeConfig.urlProduct}/service/${handle}`}</loc>
                        <changefreq>daily</changefreq>
                        <priority>1</priority>
                        <lastmod>${moment().format()}</lastmod>
                    </url>
                `;
    })
    .join('')}
        
        ${(blogs.items || [])
    .map(({ handle, created_at, updated_at, }) => {
      return `
                        <url>
                            <loc>${`${publicRuntimeConfig.urlProduct}/blog/${handle}`}</loc>
                            <changefreq>daily</changefreq>
                            <priority>1</priority>
                            <lastmod>${moment().format()}</lastmod>
                        </url>
                    `;
    })
    .join('')}
        
        ${(manufacturers.items || [])
    .map(({ handle, created_at, updated_at, }) => {
      return `
                            <url>
                                <loc>${`${publicRuntimeConfig.urlProduct}/manufacturer/${handle}`}</loc>
                                <changefreq>daily</changefreq>
                                <priority>1</priority>
                                <lastmod>${moment().format()}</lastmod>
                            </url>
                        `;
    })
    .join('')}
    </urlset>
  `)
};
