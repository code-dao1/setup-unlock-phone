import { DOMAIN_CDN } from '../../constants/system/url';

export const parseGETParams = ({ baseURI, params }) => {
  let resultURI = baseURI;
  let gotValidParam = false;
  if (params) {
    Object.keys(params).map(keyName => {
      if (params[keyName] !== undefined) {
        if (gotValidParam) {
          resultURI += `&${keyName}=${params[keyName]}`;
        } else {
          resultURI += `?${keyName}=${params[keyName]}`;
          gotValidParam = true;
        }
      }
    });
    return resultURI;
  }
  return resultURI;
};

export const pushParams = ({ baseURI, params }) => {
  if (!baseURI || !params) return baseURI;
  let resultURI = baseURI;
  if (baseURI.indexOf('?') === -1) {
    return parseGETParams({ baseURI, params });
  }

  Object.keys(params).map(keyName => {
    if (params[keyName] !== undefined) {
      resultURI += `&${keyName}=${params[keyName]}`;
    }
  });
  return resultURI;
};

export const setSmallImage = (url, size) => {
  if (url && size && url.indexOf(DOMAIN_CDN) > -1) {
    try {
      let imageURL = new URL(url);
      imageURL.searchParams.set('w', size);
      return imageURL.toString();
    } catch (err) {
      console.log(err);
    }
  }
  return url;
};

export const checkUrlInDomain = (url, hostNameCheck = window.location.hostname) => {
  try {
    const urlObject = new URL(url);
    const hostName = urlObject.hostname;

    return hostName.lastIndexOf(hostNameCheck) === hostName.length - hostNameCheck.length;
  } catch (e) {
    return true;
  }
};

export const getParamByUrl = (url_string, field) => {
  try {
    const url = new URL(url_string);
    return url.searchParams.get(field);
  } catch (e) {
    console.log(e);
    return '';
  }
};
