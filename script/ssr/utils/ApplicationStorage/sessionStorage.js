export const setItem = ({ name, value }) => {
  sessionStorage.setItem(name, value);
};

export const getItem = (name) => {
  sessionStorage.getItem(name);
};

export const removeItem = (name) => {
  sessionStorage.removeItem(name);
};
