import uuidv4 from 'uuid/dist/v4';

import { CART_TOKEN_KEY } from '../../constants/system/storage';
import { setCookie, getCookie } from './cookieStorage';
import { setItem, getItem } from './localStorage';

export const createCart = () => {
  const cartToken = uuidv4();
  setCookie(CART_TOKEN_KEY, cartToken, 60);
  setItem({
    name: CART_TOKEN_KEY,
    value: cartToken
  });

  return cartToken;
};

export const getOrCreateCartToken = () => {
  let cartToken = getItem(CART_TOKEN_KEY) || getCookie(CART_TOKEN_KEY);
  if (cartToken) {
    return cartToken;
  }

  return createCart();
};
