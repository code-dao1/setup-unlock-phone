import { isArray } from './typeof';

export const getUnique = (arr, ...comps) => {
  if (arr && isArray(arr)) {
    if (isArray(comps) && comps.length > 0) {
      return arr
        .map(e => comps.reduce((key, comp) => `${key}-${e[comp]}`, ''))

        // store the keys of the unique objects
        .map((e, i, final) => final.indexOf(e) === i && i)

        // eliminate the dead keys & store unique objects
        .filter(e => arr[e]).map(e => arr[e]);
    } else {
      return arr
        // store the keys of the unique objects
        .map((e, i, final) => final.indexOf(e) === i && i)

        // eliminate the dead keys & store unique objects
        .filter(e => arr[e]).map(e => arr[e]);
    }
  }
  return [];
};

export const convertDataToObjects = (data, DataType) => {
  if (isArray(data) && DataType) {
    return data.map(item => new DataType(item));
  }

  return data;
};

export const addItemToFirst = (item, array) => {
  return ([
    ...array,
    item,
  ]);
};

export const addItemToLast = (item, array) => {
  return ([
    item,
    ...array,
  ]);
};

export const removeItemInArray = (item, array) => {
  return array.filter(itemOfArray => {
    return Object.keys(item).find(key => itemOfArray && item && item[key] !== itemOfArray[key]);
  });
};

export const findItemInArray = (item, array) => {
  return array.find(itemOfArray => {
    return !Object.keys(item).find(key => item[key] !== itemOfArray[key]);
  });
};

export const findItemIndexInArray = (item, array) => {
  return array.findIndex(itemOfArray => {
    return !Object.keys(item).find(key => item[key] !== itemOfArray[key]);
  });
};

export const updateItemInArray = (itemOld, itemNew, array) => {
  const index = findItemIndexInArray(itemOld, array);
  if (index !== -1) {
    return ([
      ...array.slice(0, index),
      {
        ...array[index],
        ...itemNew
      },
      ...array.slice(index + 1),
    ]);
  }

  return array;
};

export const updateOrAddItemInArray = (itemOld, itemNew, array) => {
  const index = findItemIndexInArray(itemOld, array);
  if (index !== -1) {
    return ([
      ...array.slice(0, index),
      {
        ...array[index],
        ...itemNew
      },
      ...array.slice(index + 1),
    ]);
  } else {
    return ([
      ...array,
      {
        ...itemOld,
        ...itemNew
      }
    ]);
  }
};
