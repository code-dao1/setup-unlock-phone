import moment from 'moment';
import { addZeroFontNumber } from './formatNumber';
import { isNumber, isString } from './typeof';

export const formatTime = (time, isSecondUnit = true) => {
  let creationTime = new Date();
  if (typeof time === 'number') {
    if (isSecondUnit) {
      creationTime = new Date(time * 1000);
    } else {
      creationTime = new Date(time);
    }
  }
  const now = new Date();
  const diff = now.getTime() - creationTime.getTime();
  let minuteDiff = diff / 1000 / 60;

  if (minuteDiff <= 30) {
    return 'Vừa xong';
  } else if (minuteDiff <= 60) {
    return `${Math.floor(minuteDiff)} phút trước`;
  } else if (minuteDiff / 60 <= 24) {
    return `${Math.floor(minuteDiff / 60)} giờ trước`;
  } else if (minuteDiff / 12 <= 48) {
    return 'Hôm qua';
  } else if (now.getFullYear() > creationTime.getFullYear()) {
    return `Tháng ${creationTime.getMonth() +
      1}, ${creationTime.getFullYear()}`;
  }

  // Default
  return `${creationTime.getDate()} Tháng ${creationTime.getMonth() + 1}`;
};

// 1540788440000
export const formatCoinHistoryTime = time => {
  let creationTime = new Date();
  if (typeof time === 'number') {
    creationTime = new Date(time * 1000);
  }
  const now = new Date();
  const diff = now.getTime() - creationTime.getTime();
  let minuteDiff = diff / 1000 / 60;

  if (minuteDiff <= 30) {
    return 'Vừa xong';
  } else if (minuteDiff <= 60) {
    return `${Math.floor(minuteDiff)} phút trước`;
  } else if (minuteDiff / 60 <= 24) {
    return `${Math.floor(minuteDiff / 60)} giờ trước`;
  } else if (minuteDiff / 12 <= 48) {
    return 'Hôm qua';
  } else if (now.getFullYear() > creationTime.getFullYear()) {
    return `${creationTime.getDate()}/${creationTime.getMonth() +
      1}/${creationTime.getFullYear()}`;
  }
  return `${creationTime.getDate()}/${creationTime.getMonth() +
    1}/${creationTime.getFullYear()}`;
};

export const getDateFromTime = time => {
  let creationTime = new Date();
  try {
    if (isNumber(time)) {
      creationTime = new Date(time);
    } else if (isString(time)) {
      creationTime = new Date(time);
    }

    creationTime = new Date(time);
  } catch (e) {
    console.log(e);
  }

  let day = creationTime.getDate();
  let month = creationTime.getMonth() + 1;

  if (day < 10) {
    day = `0${day}`;
  }

  if (month < 10) {
    month = `0${month}`;
  }

  return `${day}/${month}/${creationTime.getFullYear()}`;
};

export const getDateFromSecondTime = (timestamp) => {
  if (isNumber(timestamp)) {
    return getDateFromTime(timestamp * 1000);
  } else if (isString(timestamp)) {
    return getDateFromTime(parseInt(timestamp, 10) * 1000);
  }

  return '';
};

export const distinctWeekToString = times => {
  let weekString = '';
  let weekDistinctList = [];

  times.map((time, index) => {
    let week = moment(time).week();
    if (weekDistinctList.indexOf(week) < 0) {
      weekDistinctList.push(week);
      if (index === 0) {
        weekString += `${week}`;
      } else {
        weekString += `, ${week}`;
      }
    }
  });

  return weekString;
};

export const secondsToHMS = (duration, isString = true) => {
  if (!duration || duration < 0) {
    if (isString) {
      return '00:00:00';
    } else {
      return { hour: '00', minute: '00', second: '00' };
    }
  }

  if (duration >= 0) {
    duration = Number(duration);
    let hour = Math.floor(duration / 3600);
    let minute = Math.floor((duration % 3600) / 60);
    let second = Math.floor((duration % 3600) % 60);

    let hourLabel = hour;
    let miniteLabel = minute;
    let secondLabel = second;

    if (hour < 10) {
      hourLabel = '0' + hour;
    }

    if (minute < 10) {
      miniteLabel = '0' + minute;
    }

    if (second < 10) {
      secondLabel = '0' + second;
    }

    if (isString) {
      return `${hourLabel}:${miniteLabel}:${secondLabel}`;
    }

    return { hour: hourLabel, minute: miniteLabel, second: secondLabel };
  }
};

export const secondsToMS = (duration, isString = true) => {
  if (!duration || duration < 0) {
    if (isString) {
      return '00:00';
    } else {
      return { minute: '00', second: '00' };
    }
  }

  if (duration >= 0) {
    duration = Number(duration);
    let minute = Math.floor(duration / 60);
    let second = Math.ceil(duration % 60);

    let miniteLabel = minute;
    let secondLabel = second;

    if (minute < 10) {
      miniteLabel = '0' + minute;
    }

    if (second < 10) {
      secondLabel = '0' + second;
    }

    if (isString) {
      return `${miniteLabel}:${secondLabel}`;
    }

    return { minute: miniteLabel, second: secondLabel };
  }
};

export const getMomentFromWeek = week => {
  if (week) {
    let currentYear = moment().year();
    // week - 1 because when return moment it go to the next week
    return moment(currentYear, 'YYYY')
      .add(week - 1, 'week')
      .startOf('week');
  }
  return moment();
};

export const returnTwoCharacterFromNumber = number => {
  if (number < 10) {
    return `0${number}`;
  }
  return number;
};

export const reduceHourMinuteSeniorResponseTime = (hour, minute) => {
  if (hour && hour >= 1) {
    return `${hour} giờ`;
  }

  if (minute && minute >= 0) {
    return `${minute} phút`;
  } else {
    return '-- phút';
  }
};

export const getDayOfWeek = (timestamp) => {
  if (timestamp) {
    switch (new Date(timestamp).getDay()) {
      case 0:
        return 'CN';
      case 1:
        return 'T2';
      case 2:
        return 'T3';
      case 3:
        return 'T4';
      case 4:
        return 'T5';
      case 5:
        return 'T6';
      case 6:
        return 'T7';
      default:
        return '';
    }
  }
  return '';
};

export const getMonthAndYear = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `Tháng ${date.getMonth() + 1}, ${date.getFullYear()}`;
  }
  return '';
};

export const getMonthAndYearShort = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `${date.getMonth() + 1}, ${date.getFullYear()}`;
  }
  return '';
};

export const getDateAndMonth = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `${date.getDate()} Tháng ${date.getMonth() + 1}`;
  }
  return '';
};

export const getDateAndMonthMin = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `${date.getDate()} Th ${date.getMonth() + 1}`;
  }
  return '';
};

export const getDate = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return date.getDate();
  }
  return '';
};

export const getDateAndMonthAndYear = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `ngày ${date.getDate()} tháng ${date.getMonth() + 1}, ${date.getFullYear()}`;
  }
  return '';
};

export const getHoursAndMinutes = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `${returnTwoCharacterFromNumber(date.getHours())}:${returnTwoCharacterFromNumber(date.getMinutes())}`;
  }
  return '';
};

export const getDayAndDate = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `${getDayOfWeek(timestamp)}, ${date.getDate()}/${date.getMonth() + 1}`;
  }
  return '';
};

export const getDateAndMonthV2 = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    return `${date.getDate()}/${date.getMonth() + 1}`;
  }
  return '';
};

export const formatCountDowntime = (deltaTime) => {
  let seconds = Math.ceil(deltaTime / 1000);
  if (seconds < 60) {
    return `00 giờ 00 phút ${addZeroFontNumber(seconds)} giây`;
  } else {
    let minutes = Math.floor(seconds / 60);
    seconds %= 60;

    if (minutes < 60) {
      return `00giờ ${addZeroFontNumber(minutes)}phút ${addZeroFontNumber(seconds)}giây`;
    } else {
      let hours = Math.floor(minutes / 60);
      minutes %= 60;
      return `${addZeroFontNumber(hours)}giờ ${addZeroFontNumber(minutes)}phút ${addZeroFontNumber(seconds)}giây`;
    }
  }
};
