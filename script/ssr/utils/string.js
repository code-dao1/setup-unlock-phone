import he from 'he';

export function extractContent(s) {
  if (!s) return s;
  const stripedHtml = s.replace(/<[^>]+>/g, '');
  return he.decode(stripedHtml);
}
