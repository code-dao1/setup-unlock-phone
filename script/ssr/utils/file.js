export const convertImageSize = (url, size) => {
  const cdnMatched = url.match(
    /(https?:\/\/cdn\.noron\.vn.*\.(jpg|jpeg|png))(\?w=[0-9]+)?/
  );
  let replacedData = url;
  if (cdnMatched && cdnMatched[1]) {
    replacedData = cdnMatched[1] + `?w=${size}`;
  }
  return replacedData;
};

export const convertSizeToFixedSize = (size) => {
  if (size <= 39) {
    return 15;
  } else if (size <= 76) {
    return 64;
  } else if (size <= 300) {
    return 256;
  } else if (size <= 600) {
    return 512;
  } else {
    return 1024;
  }
};
