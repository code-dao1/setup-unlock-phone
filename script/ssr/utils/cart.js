import { isArray } from './typeof';

const copyArrayObject = (items) => {
  return items.map(item => ({ ...item }));
};

export const updateQuantity = (items, newItem) => {
  if (!isArray(items)) return [];
  let itemsCopy = copyArrayObject(items);

  const {
    product_id,
    variantId,
    quantity
  } = newItem;
  let itemIndex = itemsCopy.findIndex(item => item.product_id === product_id && item.variantId === variantId);
  let item = itemsCopy[itemIndex];
  if (!item) {
    return [
      ...itemsCopy,
      newItem
    ];
  } else if (quantity === 0 || !quantity) {
    return [
      ...itemsCopy.slice(0, itemIndex),
      ...itemsCopy.slice(itemIndex + 1)
    ];
  } else {
    item.quantity = quantity;
    return itemsCopy;
  }
};

export const addItem = (items, newItem) => {
  if (!isArray(items)) return [];
  let itemsCopy = copyArrayObject(items);

  const {
    product_id,
    variantId,
    quantity = 1
  } = newItem;

  let item = itemsCopy.find(item => item.product_id === product_id && item.variantId === variantId);
  if (!item) {
    return [
      ...itemsCopy,
      newItem
    ];
  } else {
    item.quantity += quantity;
    return itemsCopy;
  }
};

export const removeItem = (items, itemUpdate) => {
  if (!isArray(items)) return [];
  let itemsCopy = copyArrayObject(items);

  const {
    product_id,
    variantId,
    quantity = 1
  } = itemUpdate;

  let itemIndex = itemsCopy.findIndex(item => item.product_id === product_id && item.variantId === variantId);
  if (itemIndex > -1) {
    let item = itemsCopy[itemIndex];
    if (item.quantity <= quantity || !quantity) {
      return [
        ...itemsCopy.slice(0, itemIndex),
        ...itemsCopy.slice(itemIndex + 1)
      ];
    } else {
      item.quantity -= quantity;
      return itemsCopy;
    }
  }
};

export const convertItemsToUpdate = (items) => {
  return items
    .filter(item =>
      item.quantity > 0 && !!item.variantId && !!item.id)
    .map(item => ({
      product_id: item.product_id,
      product_variant_id: item.variantId,
      quantity: item.quantity
    }));
};

export const convertItemsToInit = (items) => {
  if (!isArray(items)) return [];

  return items
    .filter(item => !!item.product_variant)
    .map(item => ({
      ...item.product_variant,
      product_id: item.product_variant.product_id,
      variantId: item.product_variant.id,
      quantity: item.quantity
    }));
};
