import { isEmpty, isNumber, isString } from "./typeof";

export const formatBigNumber = number => {
  let res = Number.parseFloat(number);
  let curPos = 0;
  const cur = ['', ' N', ' Tr', ' T'];
  while (res >= 1000) {
    res =
      res % 100 === 0
        ? res / 1000
        : Math.floor(Number.parseFloat(res) / 100) / 10;
    curPos += 1;
  }
  return res.toString().replace('.', ',') + cur[curPos];
};

export function commarize(number, {
  fractionDigits = 1,
  units = ['k', 'M', 'B', 'T'],
  minBound = 0
} = {}) {
  try {
    const min = 1e3;
    const bound = 1e3;
    // Alter numbers larger than 1k
    if (number >= min) {
      let order = Math.floor(Math.log(number) / Math.log(bound));

      let unitname = units[(order - 1)] || '';

      let num = (((number / bound ** order) * (10 ** fractionDigits)).toFixed(0)) / (10 ** fractionDigits);
      if (minBound >= num && !!unitname && order > 0) {
        order -= 1;
        unitname = units[(order - 1)] || '';
        num = (((number / bound ** order) * (10 ** fractionDigits)).toFixed(0)) / (10 ** fractionDigits);
      }
      // output number remainder + unitname
      // eslint-disable-next-line no-use-before-define
      return addSeparateCharNumber(num) + unitname;
    }

    // return formatted original number
    return number.toLocaleString();
  } catch (e) {
    return number || 0;
  }
}

export const addSeparateCharNumber = (number, char = ',') => {
  if (isNumber(number)) {
    return addSeparateCharNumber(number.toString(), char);
  } else if (isString(number)) {
    let [prefix = '', suffix = ''] = number.split('.');
    prefix = prefix.replace(/(.)(?=(.{3})+$)/g, `$1${char}`);
    return `${prefix}${suffix ? `.${suffix}` : ''}`;
  } else if (isEmpty(number)) {
    return '0';
  }

  return number;
};

export const addZeroFontNumber = number => {
  if (number < 10) return `0${number}`;
  return number;
};
