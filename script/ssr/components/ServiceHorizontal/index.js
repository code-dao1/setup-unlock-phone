import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import Link from '@/components/common/Link';
import { closeModalOrderPhone, openModalOrderPhone } from '@/redux/actions/modalFormOrderPhone';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';
import classes from './style.module.scss';

const ServiceHorizontal = (props) => {
  const {
    className,
    data,
  } = props;
  const {
    id,
    description,
    image_src,
    handle,
    name,
  } = data || {};

  const dispatch = useDispatch();
  const openModalFormOrderPhone = () => openModalOrderPhone()(dispatch);

  return (
    <div className={classList(className, 'price-row', classes.serviceItem)}>
      <div className="row">
        <div className="col-sm-6 col-xs-7">
          <Link className={classes.serviceItemWrapper} to={`/service/${handle}`}>
            <Image
              src={image_src}
              alt={name}
              className={classList(classes.img)}
            />
            {name}
          </Link>
        </div>
        <div className="col-sm-3 col-xs-5 text-right">
          <strong>From £23.99</strong>
          <small>(1-24 hours Unlock)</small>
        </div>
        <div className="col-sm-3 col-xs-12">
          <a
            className="btn btn-primary btn-sm pull-right"
            href="#"
            data-target="requestform"
            data-type="Phone Unlock"
            data-network="227"
            data-network-name="EMEA"
            data-country=""
            onClick={openModalFormOrderPhone}
          >Buy now
          </a>
        </div>
        <div className="col-sm-offset-4 col-sm-5 hidden-xs">
          <small className="preorder">Pre-order unlock for fast delivery</small>
        </div>
        <div className="col-xs-12 col-xs-offset-0 visible-xs">
          <small className="preorder">Unlock - Pre-order</small>
        </div>
      </div>
    </div>
  );
};

ServiceHorizontal.propTypes = {};

ServiceHorizontal.defaultProps = {};

export default ServiceHorizontal;
