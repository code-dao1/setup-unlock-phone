import React from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router'
import Link from '@/components/common/Link';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';

import classes from './style.module.scss';

const DeviceItem = (props) => {
  const {
    className,
    data,
  } = props;

  const {
    id,
    handle,
    image_src,
    name,
  } = data || {};

  if (!id) {
    return (
      <div className={classList(className, 'text-center', classes.deviceItem, classes.loading)}>
        <div className={classes.contentWrapper}>
          <div className={classList('text-black', classes.content)}>
            <div className={classList(classes.phoneThumb, 'img-responsive center-block')}/>
            <br/>
            <strong className={classList('center-block', classes.phoneName)}>
              <br className="visible-xs hidden-sm hidden-md hidden-lg"/>
            </strong>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className={classList(className, 'text-center', classes.deviceItem)}>
      <div className={classes.contentWrapper}>
        <Link
          to={`/unlock/${handle}`}
          title={name}
          className={classList('text-black', classes.content)}
        >
          <Image
            className={classList(classes.phoneThumb, 'img-responsive center-block')}
            src={image_src}
            alt={name}
            fillMode={'contain'}
          />
          <br/>
          <strong className={classList('center-block', classes.phoneName)}>
            {name}
          </strong>
        </Link>
      </div>
    </div>
  );
};

DeviceItem.propTypes = {};

DeviceItem.defaultProps = {};

export default DeviceItem;
