import Product from '@/layout/Product';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useHistory } from 'react-router';
import validator from 'validator';
import Image from '../common/Image';
import IMEIInput from '../FormInputUnlockPhone/IMEIInput';

import classes from './style.module.scss';

const FormInputChecking = (props) => {
  const [imei, setIMEI] = useState('');
  const [errorMessage, setErrorMessage] = useState({});
  const changeImei = (e) => {
    const newIMEI = e.target.value;
    if (/^[0-9a-z]+$/.test(newIMEI) || !newIMEI) {
      setIMEI(e.target.value);
    }
  };
  const router = useRouter();

  const onSubmit = () => {
    let messageErrorNew = {};

    if (!imei || !imei.trim()) {
      messageErrorNew.imei = 'Please enter your IMEI number.';
    } else if (/^[0-9]+$/.test(imei) && !validator.isIMEI(imei)) {
      messageErrorNew.imei = 'IMEI invalid.';
    }

    if (Object.keys(messageErrorNew).length > 0) {
      setErrorMessage(messageErrorNew);
    } else {
      setErrorMessage({});
      router.push({
        pathname: '/imei-check/result',
        search: `?imei=${imei}`
      })
    }
  };

  return (
    <div className={classes.formChecking}>
      <div className="col-xs-12 text-center">
        <h1 className="text-center h1 heading-sm">Free IMEI Checker</h1>
        <br/>
      </div>
      <div id="formIMEIChecking" className="container">
        <Image
          src="/media/images/home-iphone.jpg"
          fillMode="contain"
          className="img-responsive"
          alt="image form checking"
        />
        <div className="row">
          <div className="col-md-6 col-md-offset-6 col-xs-12 col-xs-offset-0">
            <div className="row">
              <div className="col-md-11 col-xs-12">
                <br/>
                <p>
                  Imagine what it would be like if you could use a free IMEI checker to ascertain the unique identification number of your smartphone. It’ll be a fast and easily accessible tool, while also saving you the extra cash that would’ve been spent on a pro tool that does the same thing. Besides, this totally free online IMEI checker works with iPhone, LG, Samsung, Huawei, HTC, Nokia, Lenovo, etc. So, let's show you how to make the most of this tool.
                </p>
                <br/>
                <div id="PhoneUnlockForm">
                  <label className="control-label text-grey">Let&apos;s get started:</label>
                  <div
                    id="request"
                  >
                    <IMEIInput
                      value={imei}
                      messageError={errorMessage.imei}
                      changeValue={changeImei}
                    />
                    <div className="row">
                      <div className="col-sm-6 col-xs-12">
                        <br/>
                        <button
                          className="btn btn-primary btn-lg stretch-xs"
                          id="SubmitForm"
                          onClick={onSubmit}
                        >
                          <span className="notranslate">Check Now</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

FormInputChecking.propTypes = {};

FormInputChecking.defaultProps = {};

FormInputChecking.Layout = Product;

export default FormInputChecking;
