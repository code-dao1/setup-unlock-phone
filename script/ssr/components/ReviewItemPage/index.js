import React from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';
import HTMLContent from '../common/HTMLContent';

const ReviewItemPage = (props) => {
  const {
    item,
    className,
  } = props;

  const {
    user_name,
    content,
    title,
    rate,
  } = item;

  return (
    <div className={classList(className)}>
      <div className="testimonial text-center">
        <h3>{title}</h3>
        <p className="text-grey">{user_name}</p>
        <br/>
        <div className="rating small">
          {
            [...Array(5)].map((_, index) => {
              const isActive = index < rate;
              return (<i
                key={`star-review-item-${index}`}
                className={classList('fa fa-2x fa-star', !isActive && 'disabled')}
              />)
            })
          }
          <p>Rated {rate} out of 5 stars</p>
        </div>
        <br/>
        <div className="lead light">
          <HTMLContent content={content} />
        </div>
      </div>
    </div>
  );
};

ReviewItemPage.propTypes = {};

ReviewItemPage.defaultProps = {};

export default ReviewItemPage;
