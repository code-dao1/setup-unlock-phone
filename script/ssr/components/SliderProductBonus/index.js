import React from 'react';
import PropTypes from 'prop-types';
import ProductBonus from '../ProductBonus';
import ProductItem from '../ProductItem';

const SliderProductBonus = (props) => {
  return (
    <div className="shop-content__bestSale">
      <header>
        <div className="left">
          <span>ĐƯỢC XEM NHIỀU NHẤT</span>
        </div>
        <div className="right">
          <span>Xem tất cả</span>
          <img alt="" src="https://bibabo.vn/assets/shop/img/ic_see_all.svg"/>
        </div>
      </header>
      <section
        className="shop-content__bestSale-main swiper-container swiper-container-horizontal"
        id="js-swiper-product-bestSale-shop"
      >
        <div className="swiper-wrapper">
          {
            [...Array(7)].map((_, index) => {
              return (
                <div
                  key={`product-bonus-item-${index}`}
                  className="swiper-slide swiper-slide-active"
                  style={{ width: '236.8px' }}
                >
                  <ProductItem className={'slide-pitem'}/>
                  {/*<ProductBonus />*/}
                </div>
              );
            })
          }
        </div>
        <div className="swiper-button-next swiper-slide-button-next" />
        <div className="swiper-button-prev swiper-slide-button-prev swiper-button-disabled" />
      </section>
    </div>
  );
};

SliderProductBonus.propTypes = {};

SliderProductBonus.defaultProps = {};

export default SliderProductBonus;
