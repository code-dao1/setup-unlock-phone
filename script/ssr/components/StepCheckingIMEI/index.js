import React from 'react';
import PropTypes from 'prop-types';

const StepCheckingIMEI = (props) => {
  return (
    <section id="steps">
      <div className="container">
        <div className="row">
          <div className="col-xs-12 text-center">
            <p className="lead">Follow our simple steps on how to find the IMEI number on your phone:</p>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 col-sm-6 col-xs-12 text-center">
            <div className="num">1</div>
            <p className="lead light">Dial <strong>*#06#</strong> on the phone whose IMEI number you want to check.</p>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12 text-center">
            <div className="num">2</div>
            <p className="lead light">Copy the 15 digit IMEI number displayed on the device's screen.</p>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12 text-center">
            <div className="num">3</div>
            <p className="lead light">Next, access the online IMEI check tool on our webpage.</p>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12 text-center">
            <div className="num">4</div>
            <p className="lead light">Enter the IMEI number you copied and verify that it has been correctly entered.</p>
          </div>
        </div>
        <div className="col-xs-12 text-center">
          <br/>
          <p className="lead">Now that's all there is to it!</p>
        </div>
      </div>
    </section>
  );
};

StepCheckingIMEI.propTypes = {};

StepCheckingIMEI.defaultProps = {};

export default StepCheckingIMEI;
