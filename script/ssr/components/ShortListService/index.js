import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import phoneApi from '@/services/phoneApi';
import serviceApi from '@/services/serviceApi';
import { classList } from '@/utils/system/ui';
import { isErrorResponse } from '@/utils/typeof';
import ServiceItem from '../ServiceItem';

import classes from './style.module.scss';

const ShortListService = (props) => {
  const dataFromProps = props.data || {};
  const [services, setServices] = useState(dataFromProps.items || [...Array(6)]);
  const [total, setTotal] = useState(dataFromProps.total || 0);
  useEffect(() => {
    serviceApi.getListService({
      [PAGE]: 1,
      [PAGE_SIZE]: 6,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setServices(items);
        setTotal(total);
      } else {
        setServices([]);
        setTotal(0);
      }
    });
  }, []);

  return (
    <section className={classes.shortListService}>
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <div className="row">
              <div className="col-md-10 col-md-offset-1 col-xs-12">
                <div className={classList('row', classes.listService)}>
                  {
                    services.map((item, index, array) => {
                      const isLast = index === array.length - 1;
                      return <ServiceItem
                        key={`service-item-${index}`}
                        isLast={isLast}
                        numItemNoShow={total - array.length}
                        className="col-sm-2 col-xs-4"
                        data={item}
                      />
                    })
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

ShortListService.propTypes = {};

ShortListService.defaultProps = {};

export default ShortListService;
