import Link from '@/components/common/Link';
import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const ParallaxIntro = (props) => {
  const refShowMore = useRef();
  const refIntro = useRef();
  const [isShowButtonMore, setIsShowButtonMore] = useState(true);
  const scrollRectBody = useSelector(state => state.ui.scrollRectBody);
  const isMobileScreen = useSelector(state => state.ui.isMobileScreen);
  const isTabletScreen = useSelector(state => state.ui.isTabletScreen);
  const isDesktopScreen = useSelector(state => state.ui.isDesktopScreen);
  let strengthParallax = -400;
  if (isMobileScreen) {
    strengthParallax = -1000;
  } else if (isTabletScreen) {
    strengthParallax = -500;
  }

  useEffect(() => {
    window.addEventListener('scroll', checkShowMore, { passive: true });

    return () => window.removeEventListener('scroll', checkShowMore);
  }, [isShowButtonMore]);

  const checkShowMore = () => {
    if (refShowMore && refShowMore.current) {
      const rect = refShowMore.current.getBoundingClientRect();
      const {
        top
      } = rect;
      let newStatusButtonMore = true;
      if (window.innerHeight - top > 300) {
        newStatusButtonMore = false;
      }

      if (isShowButtonMore !== newStatusButtonMore) {
        setIsShowButtonMore(newStatusButtonMore);
      }
    }
  };

  const onClickShowMore = (e) => {
    e.stopPropagation();
    e.preventDefault();
    if (refIntro && refIntro.current) {
      const {
        top,
        bottom
      } = refIntro.current.getBoundingClientRect();
      window.scrollTo({
        top: bottom,
        behavior: 'smooth'
      });
    }
  };

  const appConfig = useSelector(state => state.config.appConfig);
  const topTitle = appConfig.top_title || `${publicRuntimeConfig.productBrand}\nby IMEI`;

  return (
    <div ref={refIntro} className={classes.parallaxIntro}>
      <div className={'container'} style={{ padding: 0, width: isMobileScreen ? '100%' : '' }}>
        <div
          style={{
            backgroundImage: 'url(/media/images/background.jpg)',
            backgroundSize: isMobileScreen ? 'cover' : 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center top'
          }}
        >
          <div className={classes.content}>
            <div style={{ position: 'relative' }}>
              <div className="inline-buttons">
                <div className="row">
                  <div className="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1" style={{ marginBottom: '24px' }}>
                    <h1 className="text-center h1 heading-sm" style={{ whiteSpace: 'pre-line' }}>
                      {topTitle}
                      <br/><br/>
                    </h1>
                    <div className="text-center h1 heading-sm">
                      <small>Unlocking Service</small>
                    </div>
                    <div className="hidden-xs">
                      <br/>
                    </div>
                    <div className="visible-xs">
                      <br/>
                      <br/>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="action text-center">
                      <Link
                        className="btn btn-lg btn-primary"
                        to={'/service/unlock-icloud-45'}
                      >
                        iCloud Unlock <i className="fa fa-cloud"/>
                      </Link>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="action text-center">
                      <Link
                        className="btn btn-lg btn-primary"
                        to={'/phones'}
                        data-target="requestform"
                        data-type="Phone Unlock"
                        data-country=""
                      >Phone Unlock <i className="fa fa-mobile"/>
                      </Link>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="action text-center">
                      <Link
                        className="btn btn-lg btn-primary"
                        to={'/manufacturers'}
                        data-target="requestform"
                        data-type="Cloud Activation"
                        data-country=""
                      >Unlock Service <i className="fa fa-signal"/>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
              {
                isMobileScreen &&
                (
                  <div style={{ height: '30vh' }}/>
                )
              }
              <div
                ref={refShowMore}
                className={classList('arrow', !isShowButtonMore && classes.hideButton)}
                onClick={onClickShowMore}
              >
                <a
                  title="Found out more"
                  className="bounce"
                >
                  <i className="fa fa-chevron-down"/>
                </a>
              </div>
            </div>
            {/*<IntroContent />*/}
          </div>
          {/*<Background className={classes.introBackground}>*/}
          {/*  <Image src={'/media/images/background.jpg'} alt={'back-ground-intro'} />*/}
          {/*</Background>*/}
        </div>
      </div>
    </div>
  );
};

ParallaxIntro.propTypes = {};

ParallaxIntro.defaultProps = {};

export default ParallaxIntro;
