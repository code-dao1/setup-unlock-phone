import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import React from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const ReasonInPhone = (props) => {
  const {
    id,
    description,
    handle,
    image_src,
    image_title,
    manufacturer_id,
    name,
  } = props.phone || {};

  return (
    <section className={classes.reasonInPhone}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 text-center">
            <h2>
              Why Unlock Phone {name} <br/>
              <small className="text-black">with {publicRuntimeConfig.productBrand}</small>
            </h2>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <div className="row">
              <div className="col-sm-6 col-xs-12">
                <ul className="list-unstyled">
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconLeft)}/> <strong>Internationally
                      Unlocked
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>Roam around the world and insert a local,
                      cheaper SIM card!
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconLeft)}/> <strong>Keep
                      your Phone {name} whilst it is Unlocked
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>We unlock your Phone {name}
                      remotely, you can still use it whilst it is being unlocked.
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconLeft)}/> <strong>Permanent
                      Unlock
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>Your Phone {name} will be
                      unlocked permanently, so updates will work as normal.
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconLeft)}/> <strong>Warranty
                      Remains Valid
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>Your Phone {name} warranty will
                      not be affected by our unlocking service!
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconLeft)}/> <strong>100%
                      Success Rate
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>Our Phone {name} unlocking
                      service has a 100% success rate!
                    </p>
                  </li>
                </ul>
              </div>
              <div className="col-sm-6 col-xs-12">
                <ul className="list-unstyled">
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconRight)}/> <strong>Recommended
                      Method
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>IMEI unlocking is the method Manufacturer and
                      Network use themselves.
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconRight)}/> <strong>Step-by-Step
                      Instructions
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>We do the hard work, then send you the
                      instructions with your unlock code to complete the phone 7 unlock.
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconRight)}/> <strong>Permanent
                      Unlock
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>Your Phone {name} will be
                      unlocked permanently, so updates will work as normal.
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconRight)}/> <strong>100%
                      Legal
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>IMEI unlocking your Phone {name}
                      is 100% legal in the US and around the world.
                    </p>
                  </li>
                  <li>
                    <h3><i className={classList('fa fa-check', classes.reasonIconRight)}/> <strong>Customer
                      Support
                    </strong>
                    </h3>
                    <p className={classes.reasonLabel}>We have a dedicated customer support team
                      to help you every step of the way.
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

ReasonInPhone.propTypes = {};

ReasonInPhone.defaultProps = {};

export default ReasonInPhone;
