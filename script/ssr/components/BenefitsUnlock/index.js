import React from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const BenefitsUnlock = (props) => {
  return (
    <section className={classList('content-section', classes.benefitsUnlock)}>
      <div className="container">
        <div className="row">
          <div className="col-md-5 col-xs-12">
            <div className="xs-text-center">
              <br/>
              <h2>The Advantages of Unlocking your Phone</h2>
              <br/>
              <br/>
              <p>
                Having an unlocked phone is great as you are free to use it with any SIM card. More importantly, you can easily switch to any cellular carrier with better rates or service. Here are some advantages of unlocking your phone with our service.
              </p>
            </div>
            <br/>
            <ul className="list-unstyled">
              <li>
                <p
                  className={classes.character}
                >
                  Eliminate roaming limits when out of your home country as you are able to use SIM cards from local network providers like EE, Vodafone, Three,O2, GiffGaff, Virgin and others.
                </p>
                <br/>
              </li>
              <li>
                <p className={classes.character}>
                  Get deals that work for your pocket or taste, perhaps that’s more data or better signal strength in your locality.
                </p>
                <br/>
              </li>
              <li>
                <p className={classes.character}>
                  Selling your device becomes easier and its value increases.
                </p>
                <br/>
              </li>
              <li>
                <p className={classes.character}>
                  You can just buy a locked device for cheap and increase its resale value by simply unlocking it with us swiftly.
                </p>
              </li>
            </ul>
            <br/>
            <br className="hidden-md hidden-lg"/>
            <br className="hidden-md hidden-lg"/>
          </div>
          <div className="col-md-7 col-xs-12">
            <div className="row">
              <div className="col-lg-11 col-xs-12 pull-right">
                <div className="row">
                  <div className="col-sm-6 col-xs-12" style={{ padding: 0 }}>
                    <div
                      className={classList('panel panel-default', classes.option)}
                    >
                      <div className={classList(classes.optionIconWrapper, 'panel-heading')}>
                        <i className={classList(classes.icon, 'fa fa-mobile fa-4x')}/>
                      </div>
                      <div className="panel-body" style={{ backgroundColor: '#fff' }}>
                        <div className="col-xs-12">
                          <h3>Standard Option</h3>
                          <br/>
                          <p className="text-grey">
                            Perfect choice if you are the first buyer of your device and you are aware of the current status of your network contract, that is you know if your Phone has not blacklisted or if you have no outstanding bills etc*
                          </p>
                          <hr/>
                          <ul className="list-unstyled">
                            <li className={classes.optionCharacter}>
                              <i
                                className={classList(classes.optionCharacterIcon, 'fa fa-check-circle text-blue')}
                              />
                              Straight Whitelisting on Database
                            </li>
                            <li
                              className={classes.optionCharacter}
                            >
                              <i
                                className={classList(classes.optionCharacterIcon, 'fa fa-check-circle text-blue')}
                              />
                              Eternal Factory Unlock
                            </li>
                            <li className={classes.optionCharacter}>
                              <i
                                className={classList(classes.optionCharacterIcon, 'fa fa-check-circle text-blue')}
                              />
                              One-time Payment
                              <br/>
                              <br/>
                              <br/>
                              &nbsp;
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-xs-12" style={{ padding: 0 }}>
                    <div
                      className={classList('panel panel-default', classes.option)}
                      style={{ zIndex: 'auto' }}
                    >
                      <div className={classList(classes.optionIconWrapper, 'panel-heading')}>
                        <i className={classList(classes.icon, 'fa fa-check-square-o fa-3x')} />
                      </div>
                      <div className="panel-body" style={{ backgroundColor: '#fff' }}>
                        <div className="col-xs-12">
                          <h3>Pre-Order Option</h3>
                          <br/>
                          <p className="text-grey">
                            If you are not the first buyer of your phone, it is possible that you would not be aware of the current status of your network. This is the ideal option for you as we will check out the status of your device and the go on to unlock it.
                          </p>
                          <hr/>
                          <ul className="list-unstyled">
                            <li
                              className={classes.optionCharacter}
                            >
                              <i
                                className={classList(classes.optionCharacterIcon, 'fa fa-check-circle text-blue')}
                              />
                              Immediate Whitelisting on Database
                            </li>
                            <li
                              className={classes.optionCharacter}
                            >
                              <i
                                className={classList(classes.optionCharacterIcon, 'fa fa-check-circle text-blue')}
                              />
                              Eternal Factory Unlock
                            </li>
                            <li className={classes.optionCharacter}>
                              <i
                                className={classList(classes.optionCharacterIcon, 'fa fa-check-circle text-blue')}
                              />
                              Could include extra payment that would depend on the status of your contract.
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <br/>
            <br/>
            <br/>
            <hr className="visible-lg visible-md visible-sm hidden-xs"/>
          </div>
        </div>
      </div>
    </section>
  );
};

BenefitsUnlock.propTypes = {};

BenefitsUnlock.defaultProps = {};

export default BenefitsUnlock;
