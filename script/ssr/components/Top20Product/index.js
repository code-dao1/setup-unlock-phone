import React from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router'

import Product from '../ProductTop';

const Top20Product = (props) => {
  let router = useRouter();
  let history = router.history;
  return (
    <div className="top20-shop">
      <header className="top20-shop__header">
        <p className="title" onClick={() => history.push('/product/top20')}>
          TOP 20 SẢN PHẨM NỔI BẬT
        </p>
      </header>
      <section className="top20-shop__content">
        <div className="tab-content">
          <div id="all" className="tab-pane fade in active clearfix show">
            {
              [...Array(9)].map((_, index) => {
                const isFirst = index === 0;
                return <Product key={`product-top-20-item-${index}`} className={isFirst && 'first'} isShowViewDetail={isFirst}/>
              })
            }
            <div className="clearfix" />
          </div>
        </div>
      </section>
      <footer className="top20-shop__footer" onClick={() => history.push('/product/top20')}>
        <p>Xem tất cả 20 sản phẩm của Top nổi bật</p>
      </footer>
    </div>
  );
};

Top20Product.propTypes = {};

Top20Product.defaultProps = {};

export default Top20Product;
