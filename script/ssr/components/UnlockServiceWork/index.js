import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import React from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const UnlockServiceWork = (props) => {
  return (
    <section className={classList('content-section', classes.unlockServiceWork)}>
      <div className="container text-center">
        <div className="row">
          <div className="col-md-8 col-md-offset-2 col-xs-12 col-xs-offset-0">
            <h2>How our unlock service works?</h2>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-10 col-md-offset-1">
            <p className="lead light">
              When the unlocking process begins, what happens is that your IMEI is placed in a Database whitelist via a SIM lock status update, thus giving off a perpetual unlock which is clean and completely devoid of legal issues. Interestingly, even your regular Network and Service providers recommend this unlocking method as it ensures that your Warranty remains intact!
            </p>
            <br/>
            <p className="lead light">
              How much you would spend to unlock your device would depend on the phone model you are using and the network that currently locks it. For us though, we only need your IMEI number, as our factory unlock is done virtually using WiFi/4G/3G. This method is particularly brilliant as it ensures no disruption to the use of your device or its service network. Immediately we are done, you will find that your device has been unlocked permanently for use with any service provider.
            </p>
            <br/>
            <p className="lead light">
              Our track record is impeccable and incredible too. We have a 0% failure rate. Don’t worry, we make sure that your device would never be damaged and the warranty on it will not be voided.
            </p>
            <br/>
            <p className="lead light">
              Many people unlock their devices for several reasons: Some, simply for the pleasure and others want to have access to service providers with a wider coverage, cheaper tariffs, multiple data bonuses & more. Whatever your reasons, you must ensure that you choose to unlock with the experts, so you don’t rule your actions in the end.
            </p>
            <p className="lead light">
              If you are still in doubt as to what options to choose, we would be glad to offer free advisory, kindly reach out to us. When you choose us to unlock your device, you are making a choice that allows your device to be unlocked without you ever stepping out of your home.
            </p>
            <br/>
            <br/>
          </div>
        </div>
      </div>
    </section>
  );
};

UnlockServiceWork.propTypes = {};

UnlockServiceWork.defaultProps = {};

export default UnlockServiceWork;
