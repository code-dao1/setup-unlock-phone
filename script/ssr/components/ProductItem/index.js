import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Link from '@/components/common/Link';
import { closeModal, openModal } from '../../actions/modal';
import ButtonAddToCart from '../../containers/ProductDetail/AddToCartForm/ButtonAddToCart';
import Product from '../../entities/Product';
import { genLinkProduct } from '@/utils/genLink';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';
import LoaderBalls from '../common/LoaderBalls';

// eslint-disable-next-line import/no-cycle
import QuickViewProduct from '../Modal/QuickViewProduct';

import classes from './style.module.scss';

const ProductItem = (props) => {
  const {
    index,
    item,
    className = '',
    isHoverShowAddCart = false,
    showAddCart = false,

    openModal
  } = props;

  if (!item) {
    return (
      <div className={classList(classes.productWrap, classes.loading, className, 'relative')}>
        <div
          className=":hover-no-underline d-block"
        >
          <div className="mb12">
            <div
              className={classList(classes.collectionImageContainer, 'hover-no-effect image-wrap text-align-center')}>
              <LoaderBalls/>
            </div>
          </div>
          <div className="text-align-center">
            <span itemProp="name" className={classList(classes.title, 'd-block')}/>
            <span className={classList(classes.price, 'd-block mt16')}/>
          </div>
        </div>
      </div>
    );
  }

  const {
    title,
    images = {},
    price,
    price_compare,
    product_categories
  } = item;
  const image = images[0];
  return (
    <div className={classList(classes.productWrap, className, 'relative', isHoverShowAddCart && classes.hoverShowAddCart, showAddCart && classes.showAddCart)}>
      <Link
        className=":hover-no-underline d-block"
        to={genLinkProduct(item)}
      >
        <div className="mb12 image-product">
          <div className={classList(classes.collectionImageContainer, 'hover-no-effect image-wrap text-align-center')}>
            <Image
              src={image.image_src}
              loadInWindow
            />
          </div>
        </div>
        <div className="text-align-center product-footer">
          <span itemProp="name" className={classList(classes.title, 'd-block', 'title')}>
            {title}
          </span>
          <span className={classList(classes.price, 'd-block mt16 price')}>
            <span className={classList(classes.money, 'has-text-weight-medium')}>
              ${price}
            </span>
            <span className={classList(classes.wasPrice, 'ml12')}>
              <span className={classList(classes.money, 'has-text-weight-medium')}>
                ${price_compare}
              </span>
            </span>
          </span>
          <ButtonAddToCart
            className={classes.buttonAddCartWrapper}
            classNameButton={classes.buttonAddCart}
            onAction={() => {
              openModal({
                className: '',
                content: <QuickViewProduct handle={item.handle}/>,
              });
            }}
          />
        </div>
      </Link>
    </div>
  );
};

ProductItem.propTypes = {
  index: PropTypes.number,
  item: Product,
  className: PropTypes.string
};

ProductItem.defaultProps = {
  index: 0,
  item: null,
  className: ''
};

const mapStateToProps = state => {
  return {
    isOpen: state.modal.isOpen,
  };
};

export default connect(
  mapStateToProps,
  {
    openModal
  }
)(ProductItem);
