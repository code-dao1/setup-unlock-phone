import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { components } from 'react-select';
import { PAGE, PAGE_SIZE, SEARCH } from '@/constants/system/service';
import serviceApi from '../../../services/serviceApi';
import { isErrorResponse, isObject } from '@/utils/typeof';
import ScrollView from '../../common/ScrollView';
import AsyncSelect from 'react-select/async';

const Control = (props) => {
  const selectProps = props.selectProps || {};

  useEffect(() => {
    if (isObject(selectProps.value)) {
      selectProps.onInputChange(selectProps.value.name)
    }
  }, [selectProps.value])

  return (
    <div ref={props.innerRef} className="input-group" style={{ position: 'relative', zIndex: 0 }}>
      <input
        type="text"
        autoFocus={props.menuIsOpen}
        {...props.innerProps}
        onFocus={selectProps.onMenuOpen}
        // onBlur={selectProps.onMenuClose}
        value={selectProps.inputValue}
        onChange={e => selectProps.onInputChange(e.target.value)}
        id="SearchNetworkList"
        className="form-control"
        placeholder="Search for a Service..."
      />
      <div className="input-group-addon">
        { !props.menuIsOpen && <i className="fa fa-search" /> }
        { props.menuIsOpen && <i className="fa fa-close" /> }
      </div>
    </div>
  );
};

const MenuList = (props) => {
  return (
    <div className="dropdown-menu" data-target="Model" style={{ display: 'block', transform: 'translateY(-8px)' }}>
      <ScrollView
        style={{
          maxHeight: '291px',
          minHeight: '291px',
        }}
      >
        <ul
          className="list-unstyled slimscroll"
          style={{ overflow: 'hidden', width: 'auto' }}
        >
          {
            props.options.map((service, index) => {
              const {
                id,
                name
              } = service;
              return (
                <li>
                  <a
                    style={{ height: '44px' }}
                    href="#"
                    data-value="4"
                    onClick={() => {
                      props.selectOption(service);
                    }}
                  >
                    {name}
                  </a>
                </li>
              );
            })
          }
        </ul>
      </ScrollView>
    </div>
  );
};

const SelectService = (props) => {
  const [isOpenMenu, setOpenMenu] = useState(false);
  const loadOptions = (inputValue, callback) => {
    serviceApi.getListService({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
      [SEARCH]: inputValue
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        callback(items);
      } else {
        callback([]);
      }
    });
  };
  return (
    <div className="ui-select" id="NetworkSelect">
      <AsyncSelect
        menuIsOpen={isOpenMenu}
        options={[{ value: 1, label: 'haha' }]}
        loadOptions={loadOptions}
        openMenuOnFocus
        cacheOptions
        defaultOptions
        components={{
          MenuList,
          Control
        }}
      />
    </div>
  );
};

SelectService.propTypes = {};

SelectService.defaultProps = {};

export default SelectService;
