import React from 'react';
import PropTypes from 'prop-types';

const EnterNumberIMEI = (props) => {
  return (
    <>
      <div className="form-group">
        <input
          className="form-control"
          data-val="true"
          data-val-required="Required"
          id="IMEI"
          name="IMEI"
          placeholder="Enter IMEI Number"
          type="text"
          value=""
        />
        <span className="field-validation-valid" data-valmsg-for="IMEI" data-valmsg-replace="true" />
        <small className="message" />
        <small className="visible-xs text-center text-grey">Dial *#06# to get your IMEI</small>
      </div>
      <div id="number-pad" data-helper="true" data-value="IMEI">
        <ul className="numpad list-unstyled">
          <li>
            <a href="#" data-value="1">1</a>
          </li>
          <li>
            <a href="#" data-value="2">2 <small>ABC</small></a>
          </li>
          <li>
            <a href="#" data-value="3">3 <small>DEF</small></a>
          </li>
          <li>
            <a href="#" data-value="4">4 <small>GHI</small></a>
          </li>
          <li>
            <a href="#" data-value="5">5 <small>JKL</small></a>
          </li>
          <li>
            <a href="#" data-value="6">6 <small>MNO</small></a>
          </li>
          <li>
            <a href="#" data-value="7">7 <small>PQRS</small></a>
          </li>
          <li>
            <a href="#" data-value="8">8 <small>TUV</small></a>
          </li>
          <li>
            <a href="#" data-value="9">9 <small>WXYZ</small></a>
          </li>
          <li>
            <a href="#" data-value="del">
              <i className="fa fa-chevron-left" />
              <small>DEL</small>
            </a>
          </li>
          <li>
            <a href="#" data-value="0">0 <small>+</small></a>
          </li>
          <li>
            <a href="#" data-value="clr">
              <i className="fa fa-times" />
              <small>CLR</small>
            </a>
          </li>
        </ul>
        <div
          className="help-tip left-align"
          data-value="IMEI_Helper"
          // style="opacity: 0; left: -300px; margin-top: -40px;"
        >
          <strong>Finding your IMEI Number</strong>
          <p>Your IMEI number can be found in the Settings &gt; General &gt; About screen, or by dialling
            *#06#
          </p>
          <hr/>
        </div>
      </div>
    </>
  );
};

EnterNumberIMEI.propTypes = {};

EnterNumberIMEI.defaultProps = {};

export default EnterNumberIMEI;
