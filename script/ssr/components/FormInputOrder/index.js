import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import Select, { components } from 'react-select';
import AsyncSelect from 'react-select/async';
import { closeModalOrderPhone } from '@/redux/actions/modalFormOrderPhone';
import { classList } from '@/utils/system/ui';
import ScrollView from '@/components/common/ScrollView';
import EnterNumberIMEI from './EnterNumberIMEI';

// // import './style.scss';
import SelectManufacturer from './SelectManufacturer';
import SelectPhone from './SelectPhone';
import SelectService from './SelectService';

const FormInputOrder = (props) => {
  const {
    isOpen
  } = props;

  const dispatch = useDispatch();
  const closeModalFormOrderPhone = () => closeModalOrderPhone()(dispatch);

  return (
    <form className={classList('form-input-order', isOpen && 'open-form-input-order')} action="/checkout/createorder" autoComplete="off" id="request" method="post" noValidate="novalidate">
      <div className="row">
        <div className="phone-ui" style={{ bottom: '50%' }}>
          <div className="screen">
            <div className="panel">
              <input value="Phone Unlock" id="Type" name="Type" type="hidden"/>
              <div className="step">
                <div className="panel-heading">
                  <label>Phone details</label>
                </div>
                <div className="panel-body">
                  <div className="dropdown-controls">
                    <div className="form-group">
                      <SelectManufacturer />
                    </div>
                    <div className="form-group" style={{ zIndex: 3 }}>
                      <SelectPhone />
                    </div>
                    <div className="form-group" style={{ zIndex: 2 }}>
                      <SelectService />
                    </div>
                  </div>
                   <EnterNumberIMEI />
                </div>
              </div>
              <div className="preloader" />
              <div className="button-controls">
                <div className="row">
                  <div className="col-xs-4">
                    <a href="#" className="btn-link pull-right" data-value="Cancel" onClick={closeModalFormOrderPhone}>
                      <i className="fa fa-times" />
                    </a>
                  </div>
                  <div className="col-xs-4">
                    <button type="submit" className="btn btn-primary btn-circle btn-invalid" id="SubmitForm">
                      <span className="notranslate">OK</span>
                    </button>
                  </div>
                  <div className="col-xs-4">
                    <a href="#" className="btn-link pull-left">
                      <i className="fa fa-info" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="phone-hud hidden-sm hidden-xs">
            <div id="clock"><span className="unit" id="hours">9</span>:<span className="unit" id="minutes">50</span>
            </div>
            <div className="signal">
              <ul className="list-unstyled list-inline">
                <li className="active">&nbsp;</li>
                <li className="active">&nbsp;</li>
                <li className="active">&nbsp;</li>
                <li>&nbsp;</li>
              </ul>
            </div>
            <div className="battery">&nbsp;</div>
          </div>
          <img src="/Images/Phone-X.jpg" alt="" className="hidden-xs"/>
        </div>
      </div>
    </form>
  );
};

FormInputOrder.propTypes = {};

FormInputOrder.defaultProps = {};

export default FormInputOrder;
