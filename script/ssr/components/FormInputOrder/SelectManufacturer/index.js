import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';
import ScrollView from '../../common/ScrollView';

const SelectManufacturer = (props) => {
  const [isOpenSelectPhone, setOpenSelectPhone] = useState(false);

  const toggleSelectPhone = () => setOpenSelectPhone(!isOpenSelectPhone);

  return (
    <div className="ui-select">
      <div className={classList('btn-group', isOpenSelectPhone && 'open')}>
        <button
          type="button"
          className="btn btn-default dropdown-toggle"
          data-helper="true"
          data-toggle="dropdown"
          data-value="Model"
          aria-haspopup="true"
          aria-expanded="true"
          onClick={toggleSelectPhone}
        >
          <span>Phone 5</span>
          <i className="fa fa-check" />
        </button>
        <div className="dropdown-menu" data-target="Model">
          <ScrollView
            style={{
              maxHeight: '374px',
              minHeight: '374px',
            }}
          >
            <ul
              className="list-unstyled slimscroll"
              style={{ overflow: 'hidden', width: 'auto' }}
            >
              <li><a
                style={{ height: '44px' }}
                href="#"
                data-value="63"
                onClick="Track('Selected Phone SE 2nd Gen');"
              >Phone SE 2nd Gen
              </a>
              </li>
              <li><a
                style={{ height: '44px' }}
                href="#"
                data-value="62"
                onClick="Track('Selected Phone 11 Pro Max');"
              >Phone 11 Pro Max
              </a>
              </li>
            </ul>
          </ScrollView>
        </div>
      </div>
    </div>
  );
};

SelectManufacturer.propTypes = {};

SelectManufacturer.defaultProps = {};

export default SelectManufacturer;
