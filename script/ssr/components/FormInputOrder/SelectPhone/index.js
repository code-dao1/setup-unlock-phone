import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ScrollView from '../../common/ScrollView';
import AsyncSelect from 'react-select/async';

const SelectPhone = (props) => {
  const [isOpenMenu, setOpenMenu] = useState(false);
  const loadOptions = (inputValue, callback) => {
    setTimeout(() => {
      callback([{ value: 1, label: 'haha' }]);
    }, 1000);
  };
  return (
    <div className="ui-select" id="NetworkSelect">
      <AsyncSelect
        menuIsOpen={isOpenMenu}
        options={[{ value: 1, label: 'haha' }]}
        loadOptions={loadOptions}
        openMenuOnFocus
        cacheOptions
        defaultOptions
        components={{ MenuList: () => {
            return (
              <div className="dropdown-menu" data-target="Model" style={{ display: 'block', transform: 'translateY(-8px)' }}>
                <ScrollView
                  style={{
                    maxHeight: '336px',
                    minHeight: '336px',
                  }}
                >
                  <ul
                    className="list-unstyled slimscroll"
                    style={{ overflow: 'hidden', width: 'auto' }}
                  >
                    <li>
                      <a
                        style={{ height: '44px' }}
                        href="#"
                        data-value="4"
                        onClick="Track('Vodafone UK');"
                      >
                        Vodafone UK
                      </a>
                    </li>
                  </ul>
                </ScrollView>
              </div>
            );
          },
          Control: (props) => {
            console.log(props, props.getValue());
            const selectProps = props.selectProps || {};
            return (
              <div ref={props.innerRef} className="input-group" style={{ position: 'relative', zIndex: 0 }}>
                <input
                  type="text"
                  autoFocus={props.menuIsOpen}
                  {...props.innerProps}
                  onFocus={() => setOpenMenu(true)}
                  onBlur={() => setOpenMenu(false)}
                  value={selectProps.value}
                  onChange={e => selectProps.onInputChange(e.target.value)}
                  id="SearchNetworkList"
                  className="form-control"
                  placeholder="Search for a Phone..."
                />
                <div className="input-group-addon">
                  { !props.menuIsOpen && <i className="fa fa-search" /> }
                  { props.menuIsOpen && <i className="fa fa-close" /> }
                </div>
              </div>
            );
          },
        }}
      />
    </div>
  );
};

SelectPhone.propTypes = {};

SelectPhone.defaultProps = {};

export default SelectPhone;
