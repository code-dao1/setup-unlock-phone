import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import InfiniteScroll from 'react-infinite-scroller';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import { getUnique, convertDataToObjects } from '@/utils/array';
import { isArray, isFunction } from '@/utils/typeof';
import Block from '../Block';

class BlockInfiniteLoad extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoading: false,
      load_more_able: true,
      page_num: 0,
      total: 0
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    if (!this.state.load_more_able || this.state.isLoading) {
      return;
    }

    this.setState({
      isLoading: true
    });

    const {
      DataType
    } = this.props;

    const {
      items,
      isLoading,
      page_num,
      total
    } = this.state;

    return this.props.apiFetchData({
      [PAGE]: page_num + 1,
      [PAGE_SIZE]: 10
    }).then(rs => {
      if (rs && isArray(rs.items)) {
        const {
          items: newItems,
          load_more_able,
          total: newTotal
        } = rs;
        this.setState({
          items: getUnique([...items, ...convertDataToObjects(newItems, DataType)], 'id'),
          total: newTotal,
          load_more_able,
          isLoading: false,
          page_num: page_num + 1
        });
      }
    });
  };

  render() {
    const {
      className,
      classNameContent,
      classNameItem,
      disableButtonShowMore,
      ComponentItem,
      children,
      fetchData: fetchDataByProps,

      onClickViewAll,
      enable
    } = this.props;

    let load_more_able;
    let itemsShow;
    let isLoading;
    let fetchData;

    if (isFunction(fetchDataByProps)) {
      isLoading = this.props.isLoading;
      load_more_able = this.props.load_more_able;
      itemsShow = this.props.items;
      fetchData = fetchDataByProps;
    } else {
      isLoading = this.state.isLoading;
      load_more_able = this.state.load_more_able;
      itemsShow = this.state.items;
      fetchData = this.fetchData;
    }

    if (isLoading) {
      itemsShow = [
        ...itemsShow,
        ...Array(6)
      ];
    }

    return (<InfiniteScroll
      pageStart={1}
      loadMore={fetchData}
      hasMore={load_more_able && enable}
      initialLoad
    >
      <div className={className}>
        <Block
          items={itemsShow}
          className={classNameContent}
          classNameItem={classNameItem}
          ComponentItem={ComponentItem}
        />
        {
          !disableButtonShowMore &&
          (<div className="flex justify-center mt24">
            <button
              className="view-more btn btn-outline"
              onClick={this.fetchData}
            >
              View more
            </button>
          </div>)
        }
        {children}
      </div>
    </InfiniteScroll>);
  }
}

BlockInfiniteLoad.propTypes = {
  enable: PropTypes.bool,
  className: PropTypes.string,
  classNameContent: PropTypes.string,
  classNameItem: PropTypes.string,
  disableButtonShowMore: PropTypes.bool,
  apiFetchData: PropTypes.func,
  DataType: PropTypes.any,
  ComponentItem: PropTypes.any.isRequired,

  // TH sử dụng render item ngoài
  fetchData: PropTypes.func,
  items: PropTypes.array,
  isLoading: PropTypes.bool,
  load_more_able: PropTypes.bool,

  onClickViewAll: PropTypes.func
};

BlockInfiniteLoad.defaultProps = {
  enable: true,
  className: '',
  disableButtonShowMore: false,
  DataType: null,
  classNameContent: 'col-6 col-md-4',
  classNameItem: '',
  apiFetchData: async () => null,

  fetchData: null,
  items: [],
  isLoading: false,
  load_more_able: true,

  onClickViewAll: () => null
};

export default BlockInfiniteLoad;
