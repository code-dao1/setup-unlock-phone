import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import { formatTime } from '@/utils/formatTime';
import { extractContent } from '@/utils/string';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';

import classes from './style.module.scss';

const FirstBlog = (props) => {
  const {
    data
  } = props;

  if (!data) return null;

  const {
    id,
    content,
    created_at,
    handle,
    image_src,
    image_title,
    title,
    user_name
  } = props.data || {};

  const contentText = extractContent(content);

  return (
    <div className={classes.fistBlogItem}>
      <div className="col-md-4 col-sm-6 col-xs-12 no-pad">
        <div className={classes.blogItemContent}>
          <p className="text-white text-uppercase" style={{ marginTop: '15px' }}>
            <small style={{ fontSize: '0.75em', fontWeight: '600' }}>Guides</small>
          </p>
          <h2 className="text-white">{title}</h2>
          <div className="row">
            <div className="col-md-3">
              <hr/>
            </div>
          </div>
          <p className={classList('text-white text-uppercase', classes.titleAuthor)}>
            <small>
              {user_name} &nbsp;•&nbsp; {formatTime(created_at, false)}
            </small>
          </p>
          <br/>
          <div className="text-white">
            <p className={classes.description}>
              {contentText}
            </p>
          </div>
          <br/>
          <br/>
          <div className="row">
            <div className="col-xs-6">
              <Link
                to={`/blog/${handle}`}
                title="Read more"
                className="btn btn-primary"
              >
                Read more &nbsp;<i className="fa fa-chevron-right" />
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-8 col-sm-6 col-xs-12 no-pad">
        <Image
          className={classes.coverImage}
          alt={title}
          src={image_src}
        />
        {/*<div*/}
        {/*  className={classes.coverImage}*/}
        {/*  style={{*/}
        {/*    backgroundImage: `url(${image_src})`*/}
        {/*  }}*/}
        {/*>&nbsp;*/}
        {/*</div>*/}
      </div>
    </div>
  );
};

FirstBlog.propTypes = {};

FirstBlog.defaultProps = {};

export default FirstBlog;
