import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import phoneApi from '@/services/phoneApi';
import { isErrorResponse } from '@/utils/typeof';
import DeviceItem from '../DeviceItem';

import classes from './style.module.scss';

const ListPhoneInfinity = (props) => {

  const [phones, setPhones] = useState([...Array(8)]);
  useEffect(() => {
    phoneApi.getListPhone({
      [PAGE]: 1,
      [PAGE_SIZE]: 16,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setPhones(items);
      } else {
        setPhones([]);
      }
    });
  }, []);

  return (
    <section id="deviceIndex" className={classes.listDevice}>
      <div className="row">
        <div className="container">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <h2 className="text-center">Phone Devices</h2>
            <br/>
            <p className="lead light text-center">
              We are able to unlock any model of Smartphones. Our affordable service and seamless process works well with all phone versions – if you still doubt, check for your device type here.
            </p>
            <br/>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {
            phones.map((item, index) => {
              return (<DeviceItem
                key={`device-item-${index}`}
                className={'col-md-3 col-xs-6 text-center'}
                data={item}
              />);
            })
          }
        </div>
      </div>
    </section>
  );
};

ListPhoneInfinity.propTypes = {};

ListPhoneInfinity.defaultProps = {};

export default ListPhoneInfinity;
