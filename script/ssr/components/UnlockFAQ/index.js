import React, { useState } from 'react';
import PropTypes from 'prop-types';
import FAQHome from '../../constants/FAQ/FAQHome';
import FAQItem from '../FAQItem';

import classes from './style.module.scss';

const UnlockFAQ = (props) => {
  const {
    data = FAQHome,
    title
  } = props;
  return (
    <section className={classes.listFaq}>
      <div className="container">
        {title}
        {
          data.map((item, index) => {
            return (
              <FAQItem
                key={`faq-item-${index}`}
                id={`faq-item-${index}`}
                item={item}
              />
            );
          })
        }
      </div>
    </section>
  );
};

UnlockFAQ.propTypes = {};

UnlockFAQ.defaultProps = {};

export default UnlockFAQ;
