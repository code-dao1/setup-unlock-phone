import React from 'react';
import PropTypes from 'prop-types';

const TitleFAQFeed = (props) => {
  return (
    <div className="row">
      <div className="col-xs-12 text-center">
        <h2>Phone Unlock FAQ</h2>
        <br/>
        <p className="lead">Answers to how to unlock your Phone</p>
        <br/>
        <br/>
      </div>
    </div>
  );
};

TitleFAQFeed.propTypes = {};

TitleFAQFeed.defaultProps = {};

export default TitleFAQFeed;
