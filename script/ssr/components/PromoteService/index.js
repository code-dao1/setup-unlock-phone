import React from 'react';
import PropTypes from 'prop-types';

import classes from './style.module.scss';

const PromoteService = (props) => {
  return (
    <section className={classes.promoteService}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 xs-text-center">
            <h2>Why we are preferred.</h2>
            <br/>
            <p className="lead">
              Our process is seamless, secure, reliable & endorsed by many as the finest and fastest way to unlock your device from home without infringing on the warranty.
            </p>
            <br/>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 col-xs-12 xs-text-center">
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-wifi" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>Remote Phone unlock service</h3>
                <br/>
                <p>
                  We unlock devices virtually; this means that while we work on your device, you can continue using it normally. Please note that no one on our team will ever request your personal login details.
                </p>
                <br/>
                <br/>
              </div>
            </div>
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-unlock" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>Safe & Eternal Unlock</h3>
                <br/>
                <p>
                  Once your device is whitelisted in the database, it will be perpetually unlocked for use on any service provider anywhere in the world. This process does not affect the device’s performance, security or warranty.
                </p>
                <br/>
                <br/>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-xs-12 xs-text-center">
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-clock-o" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>Round the clock Delivery & Committed Customer Care</h3>
                <br/>
                <p>
                  Our unlocking process takes less than 1 day! The live order tracking option and our customer support channel always willing to address your concerns provides absolute peace of mind and helps you look forward safely.
                </p>
                <br/>
                <br/>
              </div>
            </div>
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-tag" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>Incredible Value for Money</h3>
                <br/>
                <p>
                  Our device unlock service is perhaps the most affordable, yet reliable in the world.
                </p>
                <br/>
                <br/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

PromoteService.propTypes = {};

PromoteService.defaultProps = {};

export default PromoteService;
