import React from 'react';
import PropTypes from 'prop-types';
import Image from '../common/Image';

const CustomerOrder = (props) => {
  const {
    imei,
    orderInfo
  } = props;

  const {
    id,
    created_at,
    is_active,
    phone,
    price,
    reference_id,
    service,
  } = orderInfo;

  return (
    <div id="CustomerOrder">
      <div className="panel">
        <div className="panel-heading">
          <h4>Order Summary</h4>
        </div>
        <div className="panel-body">
          <div className="col-split">
            <div className="row">
              <div className="order-line">
                <div className="col-xs-3">
                  <Image
                    src={phone.image_src}
                    alt={phone.name}
                    className="img-responsive"
                  />
                </div>
                <div className="col-xs-6 no-pad">
                  <strong> Phone Unlock </strong>
                  {phone.name} - {service.name} <small className="IMEI">{imei}</small>
                </div>
                <div className="col-xs-3 text-right">
                  <span className="line-price"> ${price} </span>
                </div>
                <div className="col-xs-12">
                  <hr/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="panel-footer">
          <table style={{ width: '100%' }}>
            <tbody>
            <tr>
              <td align="left" valign="middle">
                Average Delivery Time:
              </td>
              <td align="right" valign="middle">
                1-24 hours
              </td>
            </tr>
            <tr>
              <td align="left" valign="top">
                <strong style={{ fontSize: '1.5em' }}>Total</strong>
              </td>
              <td align="right" valign="middle">
                <strong style={{ fontSize: '1.5em' }}>${price}</strong>
                <br/>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

CustomerOrder.propTypes = {};

CustomerOrder.defaultProps = {};

export default CustomerOrder;
