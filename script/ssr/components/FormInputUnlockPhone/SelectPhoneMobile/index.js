import React from 'react';
import PropTypes from 'prop-types';

const SelectPhoneMobile = (props) => {
  const {
    phones,
    phoneSelect,

    setPhoneSelect,
  } = props;

  const onChangePhone = (e) => {
    const phoneId = e.target.value;
    if (!phoneId) {
      setPhoneSelect({});
    } else {
      // eslint-disable-next-line eqeqeq
      const phoneSelect = phones.find(phone => phone.id == phoneId) || {};
      setPhoneSelect(phoneSelect);
    }
  }

  return (
    <select
      className="form-control"
      data-val="true"
      data-val-number="The field ModelId must be a number."
      data-val-required="Please select a model."
      id="ModelId"
      name="ModelId"
      onChange={onChangePhone}
      value={(phoneSelect && phoneSelect.id) || ''}
    >
      <option value="">Select Phone</option>
      {
        phones.map((phone, index) => {
          const {
            id,
            name
          } = phone || {};

          let isSelected = false;
          if (phoneSelect) {
            isSelected = (id === phoneSelect.id);
          }

          return (
            <option
              key={`phone-item-${index}-${id}`}
              value={id}
            >
              {name}
            </option>
          )
        })
      }
    </select>
  );
};

SelectPhoneMobile.propTypes = {};

SelectPhoneMobile.defaultProps = {};

export default SelectPhoneMobile;
