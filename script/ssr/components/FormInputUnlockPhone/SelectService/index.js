import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import NoSSR from 'react-no-ssr';
import SelectPhoneDesktop from '../SelectPhoneDesktop';
import SelectPhoneMobile from '../SelectPhoneMobile';
import SelectServiceDesktop from '../SelectServiceDesktop';
import SelectServiceMobile from '../SelectServiceMobile';

const SelectService = (props) => {
  const {
    isShowGroup,
    serviceSelect,
    services = [],
    setServiceSelect,
    messageError
  } = props;
  let inputSelect;
  const isDesktopScreen = useSelector(state => state.ui.isDesktopScreen);

  if (!isDesktopScreen) {
    inputSelect = (
      <SelectServiceMobile
        isShowGroup={isShowGroup}
        services={services}
        serviceSelect={serviceSelect}
        setServiceSelect={setServiceSelect}
      />
    );
  } else {
    inputSelect = (
      <SelectServiceDesktop
        isShowGroup={isShowGroup}
        serviceSelect={serviceSelect}
        services={services}
        setServiceSelect={setServiceSelect}
      />
    );
  }

  return (
    <div className="form-group">
      <div className="input-group" style={{ zIndex: 9 }}>
        <NoSSR>
          {inputSelect}
        </NoSSR>
      </div>
      {
        messageError &&
        (
          <div className="input-group">
            <span className="field-validation-error">
              <span
                className=""
              >
                {messageError}
              </span>
            </span>
          </div>
        )
      }
    </div>
  );
};

SelectService.propTypes = {};

SelectService.defaultProps = {};

export default SelectService;
