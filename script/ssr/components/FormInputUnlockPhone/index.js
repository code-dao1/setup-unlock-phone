import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import validator from 'validator';
import { useHistory } from 'react-router';
import { FILTER, PAGE, PAGE_SIZE } from '@/constants/system/service';
import groupServiceApi from '@/services/groupServiceApi';
import manufacturerApi from '@/services/manufacturerApi';
import phoneApi from '@/services/phoneApi';
import serviceApi from '@/services/serviceApi';
import { isErrorResponse } from '@/utils/typeof';
import IMEIInput from './IMEIInput';
import SelectPhone from './SelectPhone';
import SelectService from './SelectService';

const FormInputUnlockPhone = (props) => {
  const {
    defaultPhone,
    autoChangeUrlByPhone,
    manufacturerId,
  } = props;

  let phoneDefaultOption = null;
  if (defaultPhone) {
    phoneDefaultOption = {
      id: defaultPhone.id,
      value: defaultPhone.id,
      label: defaultPhone.name,
      handle: defaultPhone.handle,
    };
  }
  const router = useRouter();
  const [phones, setPhones] = useState(null);
  const [services, setServices] = useState(null);
  const [phoneSelect, setPhoneSelect] = useState(phoneDefaultOption);
  const [serviceSelect, setServiceSelect] = useState(null);
  const [imei, setIMEI] = useState('');
  const [errorMessage, setErrorMessage] = useState({});
  const changeImei = (e) => {
    const newIMEI = e.target.value;
    if (/^[0-9a-z]+$/.test(newIMEI) || !newIMEI) {
      setIMEI(e.target.value);
    }
  };

  useEffect(() => {
    if (defaultPhone) {
      setPhoneSelect({
        id: defaultPhone.id,
        value: defaultPhone.id,
        label: defaultPhone.name,
        handle: defaultPhone.handle,
      });
    } else {
      setPhoneSelect(null);
    }
  }, [defaultPhone]);

  useEffect(() => {
    let paramsUrl = {};

    if (manufacturerId) {
      paramsUrl.manufacturer_id = manufacturerId;
    }

    if (phoneSelect && phoneSelect.id) {
      paramsUrl.phone_id = phoneSelect.id;
    }

    groupServiceApi.getListGroupService(
      {
        [PAGE]: 1,
        [PAGE_SIZE]: -1,
      },
      paramsUrl,
    ).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setServices(items.filter(item => item && item.services && item.services.length > 0));
      }
    });
  }, [phoneSelect, manufacturerId]);

  useEffect(() => {
    if (phones && phoneSelect && phoneSelect.id) {
      const isSelectExist = phones.find(phone => phone.id === phoneSelect.id);
      if (!isSelectExist) {
        setPhoneSelect(null);
      }
    }
  }, [phones]);

  useEffect(() => {
    if (services && serviceSelect && serviceSelect.id) {
      const isSelectExist = services.find(group =>
        group.services.find(service =>
          service.id === serviceSelect.id));
      if (!isSelectExist) {
        setServiceSelect(null);
      }
    }
  }, [services]);

  useEffect(() => {
    let params = {
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    };

    if (serviceSelect && serviceSelect.id) {
      phoneApi.getListPhoneByServiceId(serviceSelect.id, params).then(rs => {
        if (!isErrorResponse(rs)) {
          let {
            items,
            load_more_able,
            page,
            pre_load_able,
            total,
          } = rs;
          if (manufacturerId) {
            items = items.filter(item => item.manufacturer_id === manufacturerId);
          }
          setPhones(items);
        }
      });
    } else {
      if (manufacturerId) {
        params = {
          ...params,
          [FILTER]: [{
            name: 'manufacturer_id',
            value: manufacturerId,
          }],
        };
      }

      phoneApi.getListPhone(params).then(rs => {
        if (!isErrorResponse(rs)) {
          const {
            items,
            load_more_able,
            page,
            pre_load_able,
            total,
          } = rs;
          setPhones(items);
        }
      });
    }
  }, [manufacturerId, serviceSelect]);

  if (autoChangeUrlByPhone) {
    useEffect(() => {
      if (phoneSelect && phoneSelect.handle) {
        router.push(`/unlock/${phoneSelect.handle}`);
      }
    }, [phoneSelect]);
  }

  const onSubmit = () => {
    let messageErrorNew = {}
    if (!phoneSelect || !phoneSelect.id) {
      messageErrorNew.phone = 'Please select a phone';
    }

    if (!serviceSelect || !serviceSelect.id) {
      messageErrorNew.service = 'Please select a service';
    }

    if (!imei || !imei.trim()) {
      messageErrorNew.imei = 'Please enter your IMEI number.';
    } else if (/^[0-9]+$/.test(imei) && !validator.isIMEI(imei)) {
      messageErrorNew.imei = 'IMEI invalid.';
    }

    if (Object.keys(messageErrorNew).length > 0) {
      setErrorMessage(messageErrorNew);
    } else {
      router.push({
        pathname: '/checkout/accountdetails',
        query: {
          phone_id: phoneSelect.id,
          service_id: serviceSelect.id,
          imei: imei,
        }
      })
    }
  }

  return (
    <div id="request">
      <div className="row">
        <div className="col-xs-12">
          <SelectPhone
            phoneSelect={phoneSelect}
            phones={phones || []}
            messageError={errorMessage.phone}
            setPhoneSelect={setPhoneSelect}
          />
        </div>
      </div>
      <div className="col-xs-12 no-pad">
        <SelectService
          isShowGroup
          messageError={errorMessage.service}
          serviceSelect={serviceSelect}
          services={services || []}
          setServiceSelect={setServiceSelect}
        />
      </div>
      <div className="row">
        <div className="col-xs-12">
          <IMEIInput
            value={imei}
            messageError={errorMessage.imei}
            changeValue={changeImei}
          />
        </div>
      </div>

      <div className="row">
        <div className="col-sm-6 col-xs-12">
          <br/>
          <button
            className="btn btn-primary btn-lg stretch-xs"
            id="SubmitForm"
            onClick={onSubmit}
          >
            <span className="notranslate">Unlock Now</span>
          </button>
        </div>
      </div>
    </div>
  );
};

FormInputUnlockPhone.propTypes = {};

FormInputUnlockPhone.defaultProps = {};

export default FormInputUnlockPhone;
