import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

// // import './style.scss';

const customStyles = {
  control: (styles) => ({
    ...styles,
    height: '40px',
    borderColor: '#c5c5c5',
    boxShadow: 'none',
    ':focus': {
      borderColor: '#66afe9!important',
      boxShadow: 'none!important'
    },
    ':hover': {
      borderColor: '#66afe9',
      boxShadow: 'none'
    },
    ':visited': {
      borderColor: '#66afe9',
      boxShadow: 'none'
    },
  })
}

const NoOptionsMessage = props => {
  return <div style={{ padding: '0.6rem 1.6rem' }}>Not found Service</div>
};

const SelectPhoneDesktop = (props) => {
  const {
    phoneSelect,
    phones,

    setPhoneSelect: select
  } = props;

  const options = useMemo(() => {
    return [
      {
        id: '',
        value: '',
        label: 'Select Phone'
      },
      ...phones.map(item => ({ id: item.id, value: item.id, label: item.name, handle: item.handle }))
    ]
  }, [phones]);

  const onChange = (newValue, reason) => {
    select(newValue);
  };

  return (
    <Select
      className={'select-service'}
      placeholder={'Select Phone ...'}
      styles={customStyles}
      options={options}
      components={{ NoOptionsMessage }}
      defaultOptions
      value={phoneSelect}
      onChange={onChange}
    />
  );
};

SelectPhoneDesktop.propTypes = {};

SelectPhoneDesktop.defaultProps = {};

export default SelectPhoneDesktop;
