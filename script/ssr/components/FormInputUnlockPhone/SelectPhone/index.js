import React from 'react';
import PropTypes from 'prop-types';
import NoSSR from 'react-no-ssr';
import { useSelector } from 'react-redux';
import SelectPhoneDesktop from '../SelectPhoneDesktop';
import SelectPhoneMobile from '../SelectPhoneMobile';

const SelectPhone = (props) => {
  const {
    phones = [],
    setPhoneSelect,
    phoneSelect,
    messageError
  } = props;
  let inputSelect;

  const isDesktopScreen = useSelector(state => state.ui.isDesktopScreen);

  if (!isDesktopScreen) {
    inputSelect = (
      <SelectPhoneMobile
        phones={phones}
        setPhoneSelect={setPhoneSelect}
        phoneSelect={phoneSelect}
      />
    );
  } else {
    inputSelect = (
      <SelectPhoneDesktop
        phones={phones}
        setPhoneSelect={setPhoneSelect}
        phoneSelect={phoneSelect}
      />
    );
  }

  return (
    <div className="form-group">
      <div className="input-group" style={{ zIndex: 10 }}>
        <NoSSR>
          {inputSelect}
        </NoSSR>
      </div>
      {
        messageError &&
        (
          <div className="input-group">
            <span className="field-validation-error">
              <span
                className=""
              >
                {messageError}
              </span>
            </span>
          </div>
        )
      }
    </div>
  );
};

SelectPhone.propTypes = {};

SelectPhone.defaultProps = {};

export default SelectPhone;
