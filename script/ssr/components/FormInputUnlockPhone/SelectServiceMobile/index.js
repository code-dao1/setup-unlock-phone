import React from 'react';
import PropTypes from 'prop-types';

const SelectServiceMobile = (props) => {
  const {
    isShowGroup,
    serviceSelect,
    services,

    setServiceSelect
  } = props;

  const onChangeService = (e) => {
    const serviceId = e.target.value;
    if (!serviceId) {
      setServiceSelect({});
    } else {
      const groupServiceSelectNew = services.find(group =>
        group.services.find(service => {
          // eslint-disable-next-line eqeqeq
          return serviceId == service.id
        })) || {};
      const serviceSelectNew = groupServiceSelectNew.services.find(service => {
        // eslint-disable-next-line eqeqeq
        return serviceId == service.id
      });
      setServiceSelect(serviceSelectNew);
    }
  }

  return (
    <select
      className="form-control"
      data-val="true"
      data-val-number="The field NetworkId must be a number."
      data-val-required="Please select a network"
      id="NetworkId"
      name="NetworkId"
      onChange={onChangeService}
      value={(serviceSelect && serviceSelect.id) || ''}
    >
      <option value="">Select Service</option>
      {
        isShowGroup && (
          services.map((group, indexGroup) => {
            return (
              <optgroup
                key={`opt-group-service-${indexGroup}`}
                label={group.name}
              >
                {
                  group.services.map((service, index) => {
                    const {
                      id,
                      name
                    } = service || {};
                    let isSelected = false;
                    if (serviceSelect) {
                      isSelected = (id === serviceSelect.id);
                    }
                    return (
                      <option
                        key={`service-item-${indexGroup}-${index}-${id}`}
                        value={id}
                      >
                        {name}
                      </option>
                    )
                  })
                }
              </optgroup>
            );
          })
        )
      }
      {
        !isShowGroup &&
        services.map((service, index) => {
          const {
            id,
            name
          } = service || {};
          return (
            <option
              key={`service-item-${index}-${id}`}
              value={id}
            >
              {name}
            </option>
          )
        })
      }
    </select>
  );
};

SelectServiceMobile.propTypes = {};

SelectServiceMobile.defaultProps = {};

export default SelectServiceMobile;
