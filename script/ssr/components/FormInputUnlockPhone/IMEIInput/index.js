import React, { useState } from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';

const IMEIInput = (props) => {
  const {
    value,
    messageError,

    changeValue
  } = props;

  const [isOpenPopup, setOpenPopup] = useState(false);

  const openPopup = () => setOpenPopup(true);
  const closePopup = () => setOpenPopup(false);

  return (
    <div className="form-group">
      <ReactModal
        isOpen={isOpenPopup}
        shouldCloseOnOverlayClick
        onRequestClose={closePopup}
        ariaHideApp={false}
        style={{
          overlay: {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(10,10,10,.86)',
            zIndex: 11
          },
          content: {
            top: '40px',
            left: '40px',
            right: '40px',
            bottom: '40px',
            margin: 'auto',
            border: '1px solid #ccc',
            background: '#fff',
            overflow: 'auto',
            WebkitOverflowScrolling: 'touch',
            borderRadius: '4px',
            outline: 'none',
            padding: '20px'
          }
        }}
      >
        <div className="box">
          <p className="title is-4 mb-1">How to get your phone IMEI number?</p>
          <small className="has-text-grey">IMEI Number is a 15-digit number unique to each device.</small>
          <hr />
          Simply dial <strong>*#06#</strong> to see your IMEI number.
          <hr />
          If it was an iPhone and wasn't activated, then simply tap the small "i" icon on the home screen to see the IMEI number.
        </div>
        <button aria-label="close" className="modal-close is-large" onClick={closePopup}/>
      </ReactModal>
      <div className="input-group">
        <input
          className="form-control"
          data-val="true"
          data-val-required="Please enter your IMEI number."
          id="IMEI"
          name="IMEI"
          placeholder="Enter IMEI Number"
          type="text"
          value={value}
          onChange={changeValue}
        />
        <div className="input-group-addon" onClick={openPopup}>
          <i
            className="fa fa-question-circle"
            data-toggle="popover"
            data-placement="top"
            data-trigger="hover"
            data-content="Your IMEI number can be found in the Settings > General > About screen, or by dialling *#06#"
            data-original-title=""
            title=""
          />
        </div>
      </div>
      {
        messageError &&
        (
          <span
            className="field-validation-error"
          >
            <span
              className=""
            >
              {messageError}
            </span>
          </span>
        )
      }

    </div>
  );
};

IMEIInput.propTypes = {};

IMEIInput.defaultProps = {};

export default IMEIInput;
