import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

// // import './style.scss';

const customStyles = {
  control: (styles) => ({
    ...styles,
    borderColor: '#c5c5c5',
    height: '40px',
    boxShadow: 'none',
    ':focus': {
      borderColor: '#66afe9!important',
      boxShadow: 'none!important'
    },
    ':hover': {
      borderColor: '#66afe9',
      boxShadow: 'none'
    },
    ':visited': {
      borderColor: '#66afe9',
      boxShadow: 'none'
    },
  })
}

const NoOptionsMessage = props => {
  return <div style={{ padding: '0.6rem 1.6rem' }}>Not found Service</div>
};

const SelectServiceDesktop = (props) => {
  const {
    isShowGroup,
    serviceSelect,
    services,

    setServiceSelect: select
  } = props;

  const options = useMemo(() => {
    if (isShowGroup) {
      return [
        {
          id: '',
          value: '',
          label: 'Select Service'
        },
        ...services.map(group => (
          {
            label: group.name,
            options: (group.services.map(item => ({ id: item.id, value: item.id, label: item.name })))
          }
        ))
      ];
    } else {
      return [
        {
          id: '',
          value: '',
          label: 'Select Service'
        },
        ...services.map(item => ({ id: item.id, value: item.id, label: item.name }))
      ];
    }

  }, [services]);

  const onChange = (newValue, reason) => {
    select(newValue);
  };

  return (
    <Select
      placeholder={'Select Service ...'}
      className={'select-service'}
      styles={customStyles}
      options={options}
      components={{ NoOptionsMessage }}
      defaultOptions
      value={serviceSelect}
      onChange={onChange}
    />
  );
};

SelectServiceDesktop.propTypes = {};

SelectServiceDesktop.defaultProps = {};

export default SelectServiceDesktop;
