import { MANUFACTURER_ID } from '@/constants/system/paramsRoute';
import manufacturerApi from '@/services/manufacturerApi';
import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import Link from '@/components/common/Link';
import { FILTER, PAGE, PAGE_SIZE, SEARCH } from '../../constants/system/service';
import phoneApi from '@/services/phoneApi';
import { getUnique, updateItemInArray } from '@/utils/array';
import { classList } from '@/utils/system/ui';
import { isErrorResponse } from '@/utils/typeof';
import ConditionalWrapper from '../common/ConditionalWrapper';
import DeviceItem from '../DeviceItem';
import Loading from '../common/Loading';

import classes from './style.module.scss';

const ListDevice = (props) => {
  const {
    isInfiniteScroll,
    manufacturerId,
    isShowSearch
  } = props;
  const refTimeout = useRef();
  const [phones, setPhones] = useState(props.data || []);
  const [page, setPage] = useState(1);
  const [isLoading, setLoading] = useState(true);
  const [loadMoreAble, setLoadMoreAble] = useState(false);
  const [keyword, setKeyword] = useState('');

  const handleLoadMore = () => {
    if (!isLoading && loadMoreAble) {
      setLoading(true);
      let params = {
        [PAGE]: page,
        [PAGE_SIZE]: 12,
      };

      if (manufacturerId) {
        params = {
          ...params,
          [FILTER]: [{
            name: 'manufacturer_id',
            value: manufacturerId
          }]
        }
      }

      if (keyword) {
        params = {
          ...params,
          [SEARCH]: keyword
        }
      }

      phoneApi.getListPhone(params).then(rs => {
        if (!isErrorResponse(rs)) {
          const {
            items,
            load_more_able,
            page: pageCurr,
            pre_load_able,
            total,
          } = rs;
          let itemsNew;
          if (pageCurr <= 1) {
            itemsNew = items
          } else {
            itemsNew = getUnique([
              ...phones,
              ...items
            ], 'id');
          }
          setPhones(itemsNew);
          setPage(pageCurr + 1);
          setLoadMoreAble(load_more_able)
        }

        setLoading(false);
      })
    }
  };

  const onChangeSearch = (event) => {
    setKeyword(event.target.value);
  };

  if (!isInfiniteScroll) {
    useEffect(() => {
      let params = {
        [PAGE]: 1,
        [PAGE_SIZE]: 12,
      };

      if (manufacturerId) {
        params = {
          ...params,
          [FILTER]: [{
            name: 'manufacturer_id',
            value: manufacturerId
          }]
        }
      }

      if (keyword) {
        params = {
          ...params,
          [SEARCH]: keyword
        }
      }
      setLoading(true);
      phoneApi.getListPhone(params).then(rs => {
        if (!isErrorResponse(rs)) {
          const {
            items,
            load_more_able,
            page: pageCurr,
            pre_load_able,
            total,
          } = rs;
          setPhones(items);
          setPage(pageCurr + 1);
          setLoadMoreAble(load_more_able);
        } else {
          setPhones([]);
          setLoadMoreAble(false);
        }
        setLoading(false);
      });
    }, [manufacturerId]);
  } else {
    useEffect(() => {
      clearTimeout(refTimeout.current);
      setLoading(true);
      refTimeout.current = setTimeout(() => {
        let params = {
          [PAGE]: 1,
          [PAGE_SIZE]: 12,
        };

        if (manufacturerId) {
          params = {
            ...params,
            [FILTER]: [{
              name: 'manufacturer_id',
              value: manufacturerId
            }]
          }
        }

        if (keyword) {
          params = {
            ...params,
            [SEARCH]: keyword
          }
        }
        phoneApi.getListPhone(params).then(rs => {
          if (!isErrorResponse(rs)) {
            const {
              items,
              load_more_able,
              page: pageCurr,
              pre_load_able,
              total,
            } = rs;
            setPhones(items);
            setLoadMoreAble(load_more_able);
            setPage(pageCurr + 1);
          } else {
            setPhones([]);
            setLoadMoreAble(false);
          }
          setLoading(false);
        });
      }, 300);
    }, [keyword, manufacturerId]);
  }

  return (
    <section id="deviceIndex" className={classes.listDevice}>
      <div className="row">
        <div className="container">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <h2 className="text-center">Phone Devices</h2>
            <br/>
            <p className="lead light text-center">
              We are able to unlock any model of Smartphones. Our affordable service and seamless process works well with all phone versions – if you still doubt, check for your device type here.
            </p>
            <br/>
          </div>
        </div>
      </div>
      {
        isShowSearch &&
        (
          <div className={'row'}>
            <div className={'container'}>
              <div className={classList('col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0', classes.searchPhone)}>
                <div className={"col-xs-9 no-pad"}>
                  <input id="country" name="country" type="hidden" value="{ Value =  }"/>
                  <input
                    className="form-control"
                    id="SearchTerm"
                    name="SearchTerm"
                    placeholder="Enter a keyword of phone"
                    style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0 }}
                    type="text"
                    onChange={onChangeSearch}
                    value={keyword}
                  />
                </div>
                <div className="no-pad">
                  <button
                    type="submit"
                    className="btn btn-primary btn-sm"
                    style={{ borderTopLeftRadius: '0', borderTopRightRadius: '5px', borderBottomRightRadius: '5px', borderBottomLeftRadius: '0', lineHeight: '34px', height: '34px' }}
                  >Search
                  </button>
                </div>
              </div>
            </div>
          </div>
        )
      }
      <div className="container">
        <div className="row">
          <ConditionalWrapper
            condition={isInfiniteScroll}
            wrapper={
              children =>
                <InfiniteScroll
                  pageStart={1}
                  loadMore={handleLoadMore}
                  hasMore={loadMoreAble}
                  threshold={100}
                  loader={
                    [...Array(8)].map((item, index) => {
                      return (<DeviceItem
                        key={`device-item-loading-${index}`}
                        className={'col-md-3 col-xs-6 text-center'}
                        data={item}
                      />);
                    })
                  }
                >
                  {children}
                </InfiniteScroll>
            }
          >
            <div className="row">
              {
                phones.map((item, index) => {
                  return (<DeviceItem
                    key={`device-item-${index}-${item && item.id}`}
                    className={'col-md-3 col-xs-6 text-center'}
                    data={item}
                  />);
                })
              }
              {
                (
                  !phones || phones.length === 0) &&
                !isLoading &&
                (<p>
                  Not found devices
                </p>)
              }
              {
                isLoading &&
                (<Loading isMessage={false} />)
              }
            </div>
          </ConditionalWrapper>
        </div>
        {
          !isInfiniteScroll &&
          loadMoreAble &&
          (
            <div>
              <Link
                style={{ minWidth: '170px', display: 'table', margin: 'auto' }}
                className="btn btn-primary"
                to={'/phones'}
              >
                View all
              </Link>
            </div>
          )
        }
      </div>
    </section>
  );
};

ListDevice.propTypes = {
  isInfiniteScroll: PropTypes.bool,
  manufacturerId: PropTypes.any,
  isShowSearch: PropTypes.bool
};

ListDevice.defaultProps = {
  isInfiniteScroll: false,
  manufacturerId: '',
  isShowSearch: false
};

export async function getStaticProps(context) {
  console.log(context)
  return {
    props: {}, // will be passed to the page component as props
  }
}

export default ListDevice;
