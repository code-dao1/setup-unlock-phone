import React from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const UnlockStep = (props) => {
  return (
    <section id="steps" className={classes.unlockStep}>
      <div className="container">
        <div className="row">
          <div className={classList('col-xs-12 text-center')}>
            <br/>
            <br/>
            <h2>
              How to unlock your phone in three simple steps with our unlocking service without losing your warranty
            </h2>
            <br/>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            <p className="lead">Hi there, here's a quick and easy guide on how you can completely and legally remove the network lock from your phone in only three simple steps:</p>
            <br/>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          <div className={classList('col-sm-4 col-xs-12', classes.stepItem)}>
            <div className="text-center">
              <div className="num">1</div>
              <h3>Input the details of your device</h3>
              <p className="lead light">
                Choose the phone model and the current network the Phone is currently locked to and input the IMEI or Serial number.
              </p>
              <p className="lead light">
                IMEI stands for (International Mobile Equipment Identity) and is a 15-digit code that identifies each specific mobile phone. When your cellphone is stolen, for example, you can use the IMEI code to remotely lock your phone so that the thief cannot use it. The same code is used by telephone companies to bind it to their SIM. (Dial *#06# to get your IMEI code)
              </p>
            </div>
          </div>
          <div className={classList('col-sm-4 col-xs-12', classes.stepItem)}>
            <div className="text-center">
              <div className="num">2</div>
              <h3>Processing your unlock</h3>
              <p className="lead light">
                Once your payment is confirmed and your order has been processed, we examine the device and immediately make appropriate changes on your order if we find that some of the information you had provided was incorrect.
              </p>
              <p className="lead light">
                At this point, you would have access to the live tracking login which comes together with the confirmation email; hence, you can see the status of your order any time.
              </p>
            </div>
          </div>
          <div className={classList('col-sm-4 col-xs-12', classes.stepItem)}>
            <div className="text-center">
              <div className="num">3</div>
              <h3>Device unlocked</h3>
              <p className="lead light">You have a phone that has been unlocked cleanly, completely and without any law or warranty issues.</p>
              <p className="lead light">
                Using 3G/4G/WIFI, your unlock would be delivered over the air and you will get confirmation through your email.
              </p>
              <br className="hidden-lg hidden-md hidden-sm"/>
              <br className="hidden-lg hidden-md hidden-sm"/>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

UnlockStep.propTypes = {};

UnlockStep.defaultProps = {};

export default UnlockStep;
