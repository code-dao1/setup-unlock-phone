import React from 'react';
import PropTypes from 'prop-types';
import IMEICheckAndUnlockService from './IMEICheckAndUnlockService';
import UnbeatableAndTrackUnlock from './UnbeatableAndTrackUnlock';

const PromoteBrand = (props) => {
  return (
    <div>
      <IMEICheckAndUnlockService />
      <UnbeatableAndTrackUnlock />
    </div>
  );
};

PromoteBrand.propTypes = {};

PromoteBrand.defaultProps = {};

export default PromoteBrand;
