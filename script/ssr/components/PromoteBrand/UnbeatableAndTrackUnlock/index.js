import React from 'react';
import PropTypes from 'prop-types';

const UnbeatableAndTrackUnlock = (props) => {
  return (
    <section className="split black-grey">
      <div className="container">
        <div className="row">
          <div className="col-md-6 no-pad in-view" id="Times">
            <div className="text-center">
              <div className="text-white">
                <h2 className="heading-lg">
                  <strong>
                    Unbeatable <br/>
                    Prices
                  </strong>
                </h2>
                <br/>
                <br/>
                <div className="row">
                  <div className="col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                    <p className="lead light">Quickest SIM unlock on the Internet (see our price list for full
                      details).
                    </p>
                    <br/>
                    <br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 no-pad in-view" id="Tracking">
            <div className="text-center">
              <div className="text-black">
                <h2 className="heading-lg">
                  <strong>
                    Track <br/>
                    your unlock
                  </strong>
                </h2>
                <br/>
                <br/>
                <div className="row">
                  <div className="col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                    <p className="lead light">Login below to view your Phone unlock progress at any point of the
                      process.
                    </p>
                    <br/>
                    <br/>
                    <p className="lead light">
                      <a href="/tracking" className="text-blue">Live tracking <i
                        className="fa fa-chevron-right"
                      />
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

UnbeatableAndTrackUnlock.propTypes = {};

UnbeatableAndTrackUnlock.defaultProps = {};

export default UnbeatableAndTrackUnlock;
