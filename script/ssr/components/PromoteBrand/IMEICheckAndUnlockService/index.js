import React from 'react';
import PropTypes from 'prop-types';

const IMEICheckAndUnlockService = (props) => {
  return (
    <section className="split grey-black">
      <div className="container">
        <div className="row">
          <div id="NetworkCheck" className="col-md-6 no-pad">
            <div className="text-center">
              <h2 className="heading-lg"><strong>IMEI Check</strong></h2>
              <p className="lead">&nbsp;</p>
              <br/>
              <div className="row">
                <div className="col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                  <p className="lead light">
                    Instantly find out what network your phone is locked to using the IMEI
                    number.<br/>
                    &nbsp;
                  </p>
                  <br/>
                  <br/>
                  <a href="/tracking" className="btn btn-primary">Order Tracking <i
                    className="fa fa-barcode"
                  />
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div id="CloudActivation" className="col-md-6 no-pad">
            <div className="text-center">
              <h2 className="text-white heading-lg"><strong>Unlock Service</strong></h2>
              <p className="lead text-white">&nbsp;</p>
              <br/>
              <div className="row">
                <div className="col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                  <p className="lead light text-white">Unlock EE, 02, Vodafone, Three and many more networks. Our remote unlocking service works on all Phone devices.
                  </p>
                  <br/>
                  <br/>
                  <a
                    href="/manufacturers"
                    title="Unlock Services"
                    className="btn btn-primary"
                  >Unlock Service <i className="fa fa-signal" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

IMEICheckAndUnlockService.propTypes = {};

IMEICheckAndUnlockService.defaultProps = {};

export default IMEICheckAndUnlockService;
