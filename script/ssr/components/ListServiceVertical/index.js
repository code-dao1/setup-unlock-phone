import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import manufacturerApi from '@/services/manufacturerApi';
import { isErrorResponse } from '@/utils/typeof';
import GroupServiceContent from '../GroupServiceContent';
import ServiceVertical from '../GroupServiceContent/ServiceVertical';
import GroupServiceItem from '../GroupServiceItem';

import classes from './style.module.scss';
// import './style.scss';

const ListServiceVertical = (props) => {
  const {
    manufacturerId
  } = props;

  const [serviceGroups, setServiceGroups] = useState([]);
  useEffect(() => {
    manufacturerApi.getListGroupServiceByManufacturerId(
      manufacturerId,
      {
        [PAGE]: 1,
        [PAGE_SIZE]: -1
      }
    ).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setServiceGroups(items);
      } else {
        setServiceGroups([]);
      }
    })
  }, [manufacturerId]);

  if (!serviceGroups || serviceGroups.length === 0) {
    return null;
  }

  return (
    <section className={classes.groupServiceContent}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 text-center">
            <h2>Standard Unlock Service<br/>
              <small className="text-black">
                (Prices vary depending on the device model and network/contract status)
              </small>
            </h2>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div className={classes.listService} id="Country-Networks">
          {
            serviceGroups.map((serviceGroup, index) => {
              return (
                <GroupServiceContent
                  data={serviceGroup}
                  key={`group-service-vertical-item-${index}`}
                />
              )
            })
          }
        </div>
      </div>
    </section>
  );
};

ListServiceVertical.propTypes = {};

ListServiceVertical.defaultProps = {};

export default ListServiceVertical;
