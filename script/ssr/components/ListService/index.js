import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import serviceApi from '@/services/serviceApi';
import { isErrorResponse } from '@/utils/typeof';
import ServiceHorizontal from '../ServiceHorizontal';

const ListService = (props) => {
  const [services, setServices] = useState([...Array(6)]);
  useEffect(() => {
    serviceApi.getListService({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setServices(items);
      } else {
        setServices([]);
      }
    });
  }, []);

  return (
    <div className="row">
      <div className="col-xs-12">
        <div className="network-prices">
          {
            services.map((item, index) => {
              const className = index % 2 === 0 ? 'old' : 'even';
              return (
                <ServiceHorizontal className={className} data={item}/>
              );
            })
          }
        </div>
      </div>
    </div>

  );
};

ListService.propTypes = {};

ListService.defaultProps = {};

export default ListService;
