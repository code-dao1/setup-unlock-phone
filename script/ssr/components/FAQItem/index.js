import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';
import HTMLContent from '../common/HTMLContent';

import classes from './style.module.scss';

const FAQItem = (props) => {
  const {
    id,
    item,
    className
  } = props;
  const {
    title,
    content
  } = item || {};

  const [isOpenContent, setOpenContent] = useState(false);

  const toggleContent = () => setOpenContent(!isOpenContent);

  return (
    <div className={classList(className, 'row', classes.faqItem)}>
      <div className="col-md-10 col-md-offset-1 col-xs-12">
        <div className={classes.faqItemWrapper}>
          <a
            data-toggle="collapse"
            data-parent="#accordion"
            onClick={toggleContent}
            className={classes.faqItemTitle}
          >
            <strong>{title}</strong>
            <i className={classList(classes.icon, 'fa fa-chevron-up', isOpenContent && classes.open)}/>
          </a>
          <div id={id} className={classList(classes.contentWrapper, !isOpenContent && classes.collapse)}>
            <div className={classes.content}>
              <HTMLContent content={`<p>${content}</p>`} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

FAQItem.propTypes = {};

FAQItem.defaultProps = {};

export default FAQItem;
