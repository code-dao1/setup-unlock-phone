import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Slider from 'react-slick';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import phoneApi from '@/services/phoneApi';
import { isErrorResponse } from '@/utils/typeof';
import Phone from './Phone';

import classes from './style.module.scss';

let settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  arrows: true,
};

const SliderPhone = (props) => {
  const phonesFromProps = props.data && props.data.items;
  const [phones, setPhones] = useState(phonesFromProps || [...Array(8)]);
  useEffect(() => {
    phoneApi.getListPhone({
      [PAGE]: 1,
      [PAGE_SIZE]: 20,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setPhones(items);
      } else {
        setPhones([]);
      }
    });
  }, []);

  const deviceWidth = useSelector(state => state.ui.deviceWidth);
  const containerWith = deviceWidth > 1170 ? 1170 : deviceWidth;
  const slidesToShow = Math.floor(containerWith / 80);
  settings.slidesToShow = slidesToShow;
  settings.slidesToScroll = slidesToShow;
  return (
    <div className={'container'}>
      <div className={classes.carouselPhones}>
        <Slider {...settings}>
          {
            phones.map((item, index) => <Phone key={`slider-phone-${index}`} data={item}/>)
          }
        </Slider>
      </div>
    </div>
  );
};

SliderPhone.propTypes = {};

SliderPhone.defaultProps = {};

export default SliderPhone;
