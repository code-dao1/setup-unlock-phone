import React from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router'
import Link from '@/components/common/Link';
import { classList } from '@/utils/system/ui';
import { useSelector } from 'react-redux';
import Image from '../../common/Image';

import classes from './style.module.scss';

const Phone = (props) => {

  const {
    id,
    handle,
    image_src,
    name,
  } = props.data || {};
  const router = useRouter();
  const isBrowser = useSelector(state => state.ui.isBrowser);

  const onClickPhone = () => {
    router.push(`/unlock/${handle}`);
  };

  if (!id) {
    return (
      <div
        className={classList(classes.phone, classes.loading)}
      >
        <div className={classes.phoneThumb} />
        <small className={classes.phoneName} />
      </div>
    );
  }

  if(!isBrowser) {
    return (
      <Link
        className={classes.phone}
        title={name}
        to={`/unlock/${handle}`}
      >
        <Image className={classes.phoneThumb} src={image_src} alt={name} small/>
        <small className={classes.phoneName}>{name}</small>
      </Link>
    );
  }

  return (
    <div
      className={classes.phone}
      title={name}
      onClick={onClickPhone}
    >
      <Image className={classes.phoneThumb} src={image_src} alt={name} small/>
      <small className={classes.phoneName}>{name}</small>
    </div>
  );
};

Phone.propTypes = {};

Phone.defaultProps = {};

export default Phone;
