import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';

const SuggestServiceCheck = (props) => {
  const {
    id,
    handle,
    name
  } = props.phone || {};
  return (
    <section style={{ backgroundColor: '#f7f7f7' }}>
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <div className="row">
              <div className="col-sm-6">
                <div className="row">
                  <div className="col-md-10 col-xs-12 pull-right">
                    <img src="/media/images/benefits-background.jpg" alt="" className="img-responsive pull-right"/>
                  </div>
                </div>
              </div>
              <div className="col-sm-6">
                <br/>
                <br/>
                <br className="hidden-xs"/>
                <h2>{name} Service Check</h2>
                <br/>
                <p>Don&apos;t know your Network? Instantly find out what network your {name} is locked
                  to using your IMEI number.</p>
                <br/>
                <Link
                  className="btn btn-primary"
                  to={`/unlock/${handle}`}
                >
                  Network Check
                </Link>
                <br className="hidden-lg hidden-md hidden-sm"/>
                <br className="hidden-lg hidden-md hidden-sm"/>
                <br className="hidden-lg hidden-md hidden-sm"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

SuggestServiceCheck.propTypes = {};

SuggestServiceCheck.defaultProps = {};

export default SuggestServiceCheck;
