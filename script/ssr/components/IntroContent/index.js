import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

const IntroContent = (props) => {

  const appConfig = useSelector(state => state.config.appConfig);
  const productBrand = appConfig.h1_title || publicRuntimeConfig.productBrand;

  return (
    <section className="content-section" style={{ backgroundColor: '#f8f8f8', paddingBottom: '0' }}>
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0 text-center">
            <h1 className="heading-sm">{productBrand}</h1>
            <br/>
            <br/>
            <p className="lead light">
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              If you bought your phone on a contract from a carrier, it's locked to that carrier indefinitely. If that's the case for you, you have come to the right place. We are here to offer you a reliable and safe phone unlocking service. Our unlocking service works with almost any Model of phone and your phone gets unlocked within 24 hours.
            </p>
            <br />
            <p className="lead light">
              <dfn>{publicRuntimeConfig.productBrand}</dfn> virtually by using only your IMEI number and any SIM card with a good network provider. Whether you are on Vodafone, EE, 02, or any other network, our virtual unlocking service affords you the chance to unlock all Phone devices.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

IntroContent.propTypes = {};

IntroContent.defaultProps = {};

export default IntroContent;
