import React, { useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import { FILTER, PAGE, PAGE_SIZE, SEARCH } from '../../constants/system/service';
import blogApi from '@/services/blogApi';
import { getUnique, removeItemInArray } from '@/utils/array';
import { isErrorResponse } from '@/utils/typeof';
import BlogHorizontal from '../BlogHorizontal';
import DeviceItem from '../DeviceItem';

const ListBlog = (props) => {
  const {
    ignoreItem,
    keyword
  } = props;
  const refTimeout = useRef();
  const refFetchIndex = useRef(0);
  const [blogs, setBlogs] = useState(props.data || [...Array(8)]);
  const [page, setPage] = useState(1);
  const [isLoading, setLoading] = useState(true);
  const [loadMoreAble, setLoadMoreAble] = useState(false);

  const handleLoadMore = () => {
    if (!isLoading) {
      setLoading(true);
      let params = {
        [PAGE]: page,
        [PAGE_SIZE]: 12,
      };

      if (keyword) {
        params = {
          ...params,
          [SEARCH]: keyword
        };
      }

      blogApi.getListBlog(params).then(rs => {
        if (!isErrorResponse(rs)) {
          const {
            items,
            load_more_able,
            page: pageCurr,
            pre_load_able,
            total,
          } = rs;
          let itemsNew;
          if (pageCurr <= 1) {
            itemsNew = items
          } else {
            itemsNew = getUnique([
              ...blogs,
              ...items
            ], 'id');
          }
          setBlogs(itemsNew);
          setPage(pageCurr + 1);
          setLoadMoreAble(load_more_able)
        } else {
          setLoadMoreAble(false);
        }

        setLoading(false);
      })
    }
  };

  useEffect(() => {
    if (ignoreItem) {
      const newBlogs = removeItemInArray({ id: ignoreItem.id }, blogs);
      if (newBlogs.length !== blogs.length) {
        setBlogs(newBlogs)
      }
    }
  }, [ignoreItem, blogs]);

  useEffect(() => {
    clearTimeout(refTimeout.current);
    refTimeout.current = setTimeout(() => {
      let params = {
        [PAGE]: 1,
        [PAGE_SIZE]: 12,
      };

      if (keyword) {
        params = {
          ...params,
          [SEARCH]: keyword
        }
      }

      blogApi.getListBlog(params).then(rs => {
        if (history.pushState) {
          const newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + (keyword ?'?q=' + keyword : '');
          window.history.pushState({path:newurl},'',newurl);
        }

        if (!isErrorResponse(rs)) {
          let {
            items,
            load_more_able,
            page: pageCurr,
            pre_load_able,
            total,
          } = rs;
          setBlogs(items);
          setPage(pageCurr + 1)
          setLoadMoreAble(load_more_able)
        } else {
          setBlogs([]);
          setLoadMoreAble(false);
        }
      });
    }, 300);
  }, [keyword]);

  return (
    <div className="col-md-8">
      <InfiniteScroll
        pageStart={1}
        loadMore={handleLoadMore}
        hasMore={loadMoreAble}
        threshold={100}
        loader={
          [...Array(8)].map((item, index) => {
            return null;
          })
        }
      >
        {
          (blogs.length === 0 || !blogs) &&
          <div>
            Not found blog
          </div>
        }
        {
          blogs.map((blog, index, arr) => {
            const isLastItem = arr.length - 1 === index;
            return (
              <div key={`blog-horizontal-item-${index}-${blog && blog.id}`}>
                <BlogHorizontal
                  data={blog}
                />
                {
                  !isLastItem && (<hr/>)
                }
              </div>
            )
          })
        }
      </InfiniteScroll>
    </div>
  );
};

ListBlog.propTypes = {};

ListBlog.defaultProps = {};

export default ListBlog;
