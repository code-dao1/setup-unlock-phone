import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';

// // import './style.scss';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';
import classes from './style.module.scss';

const ManufacturerItem = (props) => {
  const {
    className,
    data,
  } = props;

  if (!data) {
    return (
      <div className={classList(className, 'text-center', classes.deviceItem, classes.loading)}>
        <div className={classes.contentWrapper}>
          <div className={classList('text-black', classes.content)}>
            <div className={classList(classes.phoneThumb, 'img-responsive center-block')}/>
            <br/>
            <strong className={classList('center-block', classes.phoneName)}>
              <br className="visible-xs hidden-sm hidden-md hidden-lg"/>
            </strong>
          </div>
        </div>
      </div>
    );
  }

  const {
    created_at,
    created_by,
    handle,
    id,
    image_src,
    image_title,
    name,
    updated_at,
    updated_by,
  } = data;

  return (
    <div className={classList(className, 'text-center', classes.deviceItem)}>
      <div className={classes.contentWrapper}>
        <Link
          to={`/manufacturer/${handle}`}
          title={name}
          className={classList('text-black', classes.content)}
        >
          <Image
            className={classList(classes.phoneThumb, 'img-responsive center-block')}
            src={image_src}
            alt={name}
            fillMode={'contain'}
          />
          <br/>
          <strong className={classList('center-block', classes.phoneName)}>
            {name}
          </strong>
        </Link>
      </div>
    </div>
  );

  // return (
  //   <Link
  //     to={`/manufacturer/${handle}`}
  //     title="Networks in UNITED KINGDOM"
  //     className="text-center group-service-item"
  //   >
  //     <Image
  //       src={image_src}
  //       alt=""
  //       className="img-responsive center-block group-service-item-image"
  //     />
  //     <small>{name}</small>
  //   </Link>
  // );
};

ManufacturerItem.propTypes = {};

ManufacturerItem.defaultProps = {};

export default ManufacturerItem;
