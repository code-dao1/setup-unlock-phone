import { vimeoUrlDetect, youtubeUrlDetect } from '@/utils/url';
import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';

import classes from './style.module.scss';

const ReviewYoutubeItem = (props) => {
  const {
    className,
    data,
    isLast,
    numItemNoShow,
    autoPlay,
    controls = true
  } = props;
  const {
    id,
    url,
  } = data || {};

  if (!id) {
    return (
      <div className={classList(className, classes.reviewYoutubeItem, classes.loading)}>
        <div className={classes.reviewYoutubeItemWrapper}>
          <div
            className={classList('img-responsive', classes.img)}
          />
        </div>
      </div>
    );
  }

  const showLastItem = isLast && numItemNoShow > 0;

  const showUrl = useMemo(() => {
    return youtubeUrlDetect(url) || vimeoUrlDetect(url) || url
  }, [url]);

  const useIframe = showUrl && (showUrl.includes("youtube") || showUrl.includes("vimeo"));

  return (
    <div className={classList(className, classes.reviewYoutubeItem)}>
      <div className={classes.reviewYoutubeItemWrapper}>
        {
          useIframe &&
          (
            <iframe
              controls={controls}
              width="100%"
              title={showUrl}
              height="100%"
              src={showUrl}
              allowFullScreen
              autoPlay={autoPlay}
            />
          )
        }
        {
          !useIframe &&
          (
            <video
              width="100%"
              height="100%"
              controls={controls}
              title={showUrl}
              allowFullScreen
              autoPlay={autoPlay}
            >
              <source src={showLastItem} />
              Your browser does not support the video tag.
            </video>
          )
        }
      </div>
    </div>
  );
};

ReviewYoutubeItem.propTypes = {};

ReviewYoutubeItem.defaultProps = {};

export default ReviewYoutubeItem;
