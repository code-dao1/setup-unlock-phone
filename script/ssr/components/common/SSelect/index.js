import PropTypes from 'prop-types';
import React from 'react';

// import './style.scss';

const SSelect = (props) => {
  const {
    isActive,
    value: valueSelect,
    options,
    onChange,
    label,
    placeholder,
  } = props;

  const isShowFloatingLabel = !!valueSelect;
  return (
    <div className="s-select-wrapper">
      <div
        className={`is-medium s-select is-fullwidth floating-label ${isShowFloatingLabel ? 'is-show-floating-label' : ''} ${isActive ? 'is-active' : ''}`}>
        <label className="s-label">
          {label}
        </label>
        <select
          className={`s-select-inner ${!valueSelect ? 'placeholder' : ''}`}
          onChange={onChange}
          placeholder={placeholder}
          value={valueSelect || ''}
        >
          <option value="" disabled>{placeholder}</option>
          {
            options.map(({ value, label, isSelected }, index) => {
              return (
                <option
                  key={`s-select-option-${index}`}
                  value={value}
                >
                  {label}
                </option>
              )
            })
          }
        </select>
      </div>
    </div>
  );
};

SSelect.propTypes = {
  isActive: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  errorMessage: PropTypes.string,
};

SSelect.defaultProps = {
  isActive: false,
  type: 'text',
  value: '',
  label: '',
  placeholder: '',
  errorMessage: '',
};

export default SSelect;
