import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { STUCK_BOTTOM, STUCK_LEFT, STUCK_RIGHT, STUCK_TOP } from '../../../constants/sticky';

class Sticky extends Component {
  static baseClass = 'tk-sticky';

  constructor(props) {
    super(props);

    this.state = {
      height: 0,
      width: 0,
      stuckBottom: false,
      stuckLeft: false,
      stuckRight: false,
      stuckTop: false,
    };
  }

  componentDidMount() {
    this.handleScroll();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.scrollRectBody.top !== this.props.scrollRectBody.top) {
      this.handleScroll();
    }
  }

  stickyDiv = React.createRef();

  handleScroll = () => {
    const { sides, cross } = this.props;
    const stickyDiv = this.stickyDiv.current || null;

    if (!stickyDiv) {
      return;
    }

    const scrollRect = this.props.scrollRectBody;

    let stickyRect = stickyDiv.getBoundingClientRect();

    if (!this.state.height || !this.state.width) {
      this.setState({
        height: stickyRect.height,
        width: stickyRect.width,
      });
    }

    stickyRect = { // Apparently you can't spread the results of a bounding client rectangle
      height: this.state.height || stickyRect.height,
      width: this.state.width || stickyRect.width,
      x: stickyRect.x,
      y: stickyRect.y,
    };
    // TODO khi nào cần dùng vượt qua ở các vị trí khác nhau tự config vào, đã xử lý top
    if (typeof sides.bottom === 'number') {
      const stuckBottom = stickyRect.y + stickyRect.height > (scrollRect.height + scrollRect.top) - sides.bottom;
      this.setState({ stuckBottom });
    }

    if (typeof sides.top === 'number') {
      const stuckTop = stickyRect.y + (cross ? stickyRect.height : 0) < 0 + sides.top;
      this.setState({ stuckTop });
    }

    if (typeof sides.left === 'number') {
      const stuckLeft = stickyRect.x < scrollRect.left + sides.left;
      this.setState({ stuckLeft });
    }

    if (typeof sides.right === 'number') {
      const stuckRight = stickyRect.x + stickyRect.width > (scrollRect.width + scrollRect.left) - sides.right;
      this.setState({ stuckRight });
    }
  };

  render() {
    const { children } = this.props;
    const { stuckBottom, stuckLeft, stuckRight, stuckTop } = this.state;

    const stickyModifiers = [];

    if (stuckBottom) {
      stickyModifiers.push(STUCK_BOTTOM);
    }

    if (stuckLeft) {
      stickyModifiers.push(STUCK_LEFT);
    }

    if (stuckRight) {
      stickyModifiers.push(STUCK_RIGHT);
    }

    if (stuckTop) {
      stickyModifiers.push(STUCK_TOP);
    }

    const childrenWithStuckProps = React.Children.map(children, (child) => {
      const childModifiers = (child.props && child.props.modifiers) || [];
      return React.cloneElement(child, { modifiers: [...childModifiers, ...stickyModifiers] });
    });

    return (
      <div
        className={Sticky.baseClass}
        ref={this.stickyDiv}
      >
        {childrenWithStuckProps}
      </div>
    );
  }
}

Sticky.propTypes = {
  /** Pass in a React component, and it will receive `stuckBottom`, `stuckLeft`, `stuckRight`, and/or `stuckTop` modifiers */
  children: PropTypes.node.isRequired,
  /** If you have an internally scrolling component, pass its ref callback to watch for scroll events */
  scrollTarget: PropTypes.object,
  /** These offsets determine how far from the edge of the page an element must be to count as 'stuck' */
  sides: PropTypes.shape({
    bottom: PropTypes.number,
    left: PropTypes.number,
    right: PropTypes.number,
    top: PropTypes.number,
  }),
  cross: PropTypes.bool
};

Sticky.defaultProps = {
  scrollTarget: null,
  sides: {
    top: 0,
  },
  cross: false
};

export default connect(
  state => ({
    scrollRectBody: state.ui.scrollRectBody,
  }),
)(Sticky);
