import React from 'react';
import ImageGallery from 'react-image-gallery';

import 'react-image-gallery/styles/scss/image-gallery.scss';
import { setSmallImage } from '@/utils/system/apiURI';
import { classList } from '@/utils/system/ui';

// import './style.scss';

const ReactImageGallery = (props) => {
  const {
    media = [],
    className,
    option = {}
  } = props;

  const showN = media.length > 1;

  const convertMedia = media.map(({ type, url }) => ({
    original: setSmallImage(url, 800),
    thumbnail: setSmallImage(url, 320)
  }));

  return (
    <div className={classList(className, 'react-image-gallery')}>
      <ImageGallery
        items={convertMedia}
        thumbnailPosition={'bottom'}
        showPlayButton={false}
        showFullscreenButton={false}
        showNav={showN}
        {...option}
      />
    </div>
  );
};

export default ReactImageGallery;
