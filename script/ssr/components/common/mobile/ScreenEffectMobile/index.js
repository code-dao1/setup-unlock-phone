import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
// Style
// // import './style.scss';
import { clearAllBodyScrollLocks, disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import ScrollView from '../../ScrollView';
import ConditionalWrapper from '../../ConditionalWrapper';

export const APPEAR = {
  TOP_TO_BOTTOM: 'top_to_bottom',
  BOTTOM_TO_TOP: 'bottom_to_top',
  LEFT_TO_RIGHT: 'left_to_right',
  RIGHT_TO_LEFT: 'right_to_left',
};

class ScreenEffectMobile extends PureComponent {
  componentDidMount() {
    this.targetElement = document.querySelector('body');
    if (this.props.isActive) {
      disableBodyScroll(this.targetElement);
    }
  }

  componentWillUnmount() {
    clearAllBodyScrollLocks();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.targetElement) {
      if (this.props.isActive !== prevProps.isActive) {
        if (this.props.isActive) {
          disableBodyScroll(this.targetElement);
        } else {
          enableBodyScroll(this.targetElement);
        }
      }
    }
  }

  render() {
    const {
      isActive,
      className,
      classNameContent,
      appear,
      clickOutside,
      children
    } = this.props;

    return (
      <div className={`screen-effect-mobile ${className}`}>
        <div
          className={`screen-mobile-content ${appear} ${classNameContent} ${isActive ? 'opening' : ''}`}
          ref={r => this.wrapperOutsideRef = r}
        >
          <ConditionalWrapper
            condition={this.props.useScroll}
            wrapper={
              children =>
                <ScrollView
                  width="100wh"
                >
                  {children}
                </ScrollView>
            }
          >
            {children}
          </ConditionalWrapper>
        </div>
        <div
          className="screen-effect-mobile-overlay"
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            this.props.clickOutside();
          }}
        />
      </div>
    );
  }
}

ScreenEffectMobile.propTypes = {
  className: PropTypes.string,
  classNameContent: PropTypes.string,
  isActive: PropTypes.bool,
  appear: PropTypes.string.isRequired,
  clickOutside: PropTypes.func,
  useScroll: PropTypes.bool
};

ScreenEffectMobile.defaultProps = {
  className: '',
  classNameContent: '',
  isActive: false,
  clickOutside: () => null,
  useScroll: true
};

export default ScreenEffectMobile;
