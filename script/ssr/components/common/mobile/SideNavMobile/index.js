import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ScrollAnimation from 'react-animate-on-scroll';
import { closeSideNavMobile, changeSearch } from '@/redux/actions/mainHeader/sideNav';
import { clearAllBodyScrollLocks, disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

import { SITE_NAV } from '@/constants/system/siteNav';
import { classList } from '@/utils/system/ui';
import SiteNavItem from '../../../SiteNavItem';

import classes from './style.module.scss';

class SideNavMobile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      searchTextLocal: props.searchText || '',
    };
    this.targetElement = document.querySelector('body');
  }

  componentDidMount() {
    if (this.props.isOpenSideNavMobile) {
      disableBodyScroll(this.targetElement)
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!this.timeoutChangeSearch && this.props.searchText !== this.state.searchTextLocal) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        searchTextLocal: this.props.searchText
      });
    }

    if (prevProps.isOpenSideNavMobile !== this.props.isOpenSideNavMobile) {
      if (this.props.isOpenSideNavMobile) {
        disableBodyScroll(this.targetElement)
      } else {
        enableBodyScroll(this.targetElement)
      }
    }
  }

  changeSearchText = (e) => {
    const text = e.target.value || '';

    this.setState({
      searchTextLocal: text
    });

    if (this.timeoutChangeSearch) {
      clearTimeout(this.timeoutChangeSearch);
    }

    this.timeoutChangeSearch = setTimeout(() => {
      this.props.changeSearch(text);
      this.timeoutChangeSearch = null;
    }, 200);
  };

  render() {
    const {
      isOpenSideNavMobile,
      searchText,

      closeSideNavMobile,
      changeSearch
    } = this.props;

    const {
      searchTextLocal
    } = this.state;

    return (
      <div className={classList(classes.sidebarMenuWrap, isOpenSideNavMobile && classes.isOpen)}>
        <div className={classes.sidebarMenu}>
          <div className={classes.catTitleWrap}>
            <div className={classList(classes.mobileNav, 'p0 m0')}>
              {
                isOpenSideNavMobile &&
                (
                  SITE_NAV.map(({ link, name }, index) => {
                    return (
                      <ScrollAnimation
                        key={`site-nav-item-${index}`}
                        className={classes.mobileNavItem}
                        animateIn="fadeInLeft"
                        duration={(0.25 * index)}
                        initiallyVisible
                      >
                        <SiteNavItem
                          className={classList(classes.mobileNavHasSublist, 'p0 m0')}
                          classNameLink={classes.mobileNavLink}
                          name={name}
                          link={link}
                          onClick={closeSideNavMobile}
                        />
                      </ScrollAnimation>
                    );
                  })
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SideNavMobile.propTypes = {};

SideNavMobile.defaultProps = {};

export default connect(
  state => ({
    isOpenSideNavMobile: state.sideNav.isOpenSideNavMobile,
    searchText: state.sideNav.searchText,
  }), {
    closeSideNavMobile,
    changeSearch
  }
)(SideNavMobile);
