import React, { Component, Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

// Components
import Popup from '../../Popup';

// Style
// import './style.scss';

const MenuMobile = (props) => {
  const {
    isOpen,
    className,
    children,
    close,
    clickOutside
  } = props;

  return (
    <Popup
      className={`menu-mobile-popup ${className}`}
      isActive={isOpen}
      clickOutside={clickOutside}
    >
      <div className="menu-mobile-content">
        <div className="dialog-content">
          {children}
        </div>
        <div className="dialog-footer">
          <div
            className="cancel"
            onClick={() => {
              close();
            }}
          >
            Hủy
          </div>
        </div>
      </div>
    </Popup>
  );
};

MenuMobile.propTypes = {
  className: PropTypes.string,
  isOpen: PropTypes.bool,
  clickOutside: PropTypes.func,
  close: PropTypes.func
};

MenuMobile.defaultProps = {
  className: '',
  isOpen: false,
  clickOutside: () => null,
  close: () => null
};

export default MenuMobile;
