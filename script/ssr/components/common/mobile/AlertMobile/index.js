import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../Loading';

import Popup from '../../Popup';

// import './style.scss';

class AlertMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRunningLeft: false,
      isRunningRight: false
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isOpen && !this.props.isOpen) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        isRunningLeft: false,
        isRunningRight: false
      });
    }
  }

  renderActionFlexRow = () => {
    const {
      onButtonLeft,
      onButtonRight,
      buttonLeftLabel,
      buttonRightLabel
    } = this.props;

    return (
      <>
        {buttonLeftLabel && (
          <div
            className="button-left"
            onClick={(e) => {
              if (!this.state.isRunningLeft) {
                this.setState({
                  isRunningLeft: true
                });
                onButtonLeft(e);
              }
            }}
          >
            {this.state.isRunningLeft && (<Loading isMessage={false}/>)}
            {buttonLeftLabel}
          </div>
        )}

        {buttonRightLabel && (
          <div
            className="button-right"
            onClick={(e) => {
              if (!this.state.isRunningRight) {
                this.setState({
                  isRunningRight: true
                });
                onButtonRight(e);
              }
            }}
          >
            {this.state.isRunningRight && (<Loading isMessage={false}/>)}
            {buttonRightLabel}
          </div>
        )}
      </>
    );
  };

  renderActionFlexColumn = () => {
    const {
      onButtonLeft,
      onButtonRight,
      buttonLeftLabel,
      buttonRightLabel
    } = this.props;

    // Đảo thứ tự trên dưới
    return (
      <>
        {buttonRightLabel && (
          <div
            className="button-first"
            onClick={(e) => {
              if (!this.state.isRunningLeft) {
                this.setState({
                  isRunningLeft: true
                });
                onButtonRight(e);
              }
            }}
          >
            {this.state.isRunningLeft && (<Loading isMessage={false}/>)}
            {buttonRightLabel}
          </div>
        )}

        {buttonLeftLabel && (
          <div
            className="button-second"
            onClick={(e) => {
              if (!this.state.isRunningRight) {
                this.setState({
                  isRunningRight: true
                });
                onButtonLeft(e);
              }
            }}
          >
            {this.state.isRunningRight && (<Loading isMessage={false}/>)}
            {buttonLeftLabel}
          </div>
        )}
      </>
    );
  };

  render() {
    const {
      isOpen,
      className,
      title = '',
      content = '',
      buttonLeftLabel,
      buttonRightLabel,
      clickOutside
    } = this.props;

    let direction;
    if (buttonLeftLabel.length > 12 || buttonRightLabel.length > 12) {
      direction = 'column';
    } else {
      direction = 'row';
    }

    return (
      <Popup
        className={`popup-mobile-alert ${className}`}
        isActive={isOpen}
        clickOutside={clickOutside}
      >
        <div className="alert-mobile-content">
          {title && (
            <div className="dialog-header">
              <p className="title">{title}</p>
            </div>
          )}

          {content && (
            <div className="dialog-body">
              <div className="message-content">{content}</div>
            </div>
          )}
        </div>
        <div className={`dialog-footer ${direction}`}>
          {
            direction === 'column' &&
            this.renderActionFlexColumn()
          }
          {
            direction === 'row' &&
            this.renderActionFlexRow()
          }
        </div>
      </Popup>
    );
  }
}

AlertMobile.propTypes = {
  className: PropTypes.string,
  isOpen: PropTypes.bool,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.func
  ]),
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.func
  ]),
  onButtonLeft: PropTypes.func,
  onButtonRight: PropTypes.func,
  buttonLeftLabel: PropTypes.string,
  buttonRightLabel: PropTypes.string,
  clickOutside: PropTypes.func
};

AlertMobile.defaultProps = {
  className: null,
  isOpen: false,
  title: null,
  content: null,
  onButtonLeft: () => null,
  buttonLeftLabel: 'Đồng ý',
  onButtonRight: () => null,
  buttonRightLabel: 'Hủy bỏ',
  clickOutside: () => null
};

export default AlertMobile;
