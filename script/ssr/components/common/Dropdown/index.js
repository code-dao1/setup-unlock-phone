import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

class Dropdown extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
    };
    this.wrapperOutsideRef = null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.isActive !== this.props.isActive) {
      if (this.props.isActive) {
        document.addEventListener('mousedown', this.handleClickOutside);
      } else {
        document.removeEventListener('mousedown', this.handleClickOutside);
      }
    }
  }

  handleClickOutside = event => {
    const {
      closeOnClickOutSide,
      isActive,
      targetButton
    } = this.props;
    if (
      closeOnClickOutSide &&
      isActive &&
      this.wrapperOutsideRef &&
      !this.wrapperOutsideRef.contains(event.target) &&
      (!targetButton || !targetButton.contains(event.target))
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.props.close();
    }
  };

  render() {
    let { className, children, isActive } = this.props;

    return (
      <div
        ref={c => {
          this.wrapperOutsideRef = c;
        }}
        className={classList(className, classes.dropdown, isActive ? classes.active : '')}
      >
        {children}
      </div>
    );
  }
}

Dropdown.propTypes = {
  targetButton: PropTypes.any,
  isActive: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  closeOnClickOutSide: PropTypes.bool,
  className: PropTypes.string,
};

Dropdown.defaultProps = {
  targetButton: null,
  className: '',
  closeOnClickOutSide: false
};

export default Dropdown;
