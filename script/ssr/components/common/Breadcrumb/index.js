import React from 'react';
import Link from '@/components/common/Link';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const Breadcrumb = (props) => {
  const {
    className = '',
    paths = []
  } = props;
  return (
    <div className={classList(classes.breadcrumb, className, 'text-align-center text-align-left-md mt40')}>
      <div className="breadcrumb-text">
        {
          paths.map(({ to, name }, index) => {
            if (paths.length - 1 === index) {
              return (
                <span
                  key={`breadcrumb-item-${index}`}
                  className={classList(classes.breadcrumbLink, 'breadcrumb_link--current')}
                >
                {name}
              </span>);
            }

            return (<span
              key={`breadcrumb-item-${index}`}
              className={classes.breadcrumbLink}
            >
              <Link to={to} className={'router-link-active'}>
                {name}
              </Link>
              &nbsp;/&nbsp;
            </span>);
          })
        }
      </div>
    </div>
  );
};

export default Breadcrumb;
