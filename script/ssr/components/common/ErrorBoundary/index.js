import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, errorInfo) {
    const data = this.props.data || {};
    this.setState({
      hasError: true,
      error,
      errorInfo
    });

    this.props.callback(error, errorInfo);
  }
  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      if (this.props.renderError) {
        return this.props.renderError(this.state.error, this.state.errorInfo);
      } else {
        return <div>Something broke</div>;
      }
    }
    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  renderError: PropTypes.func,
  callback: PropTypes.func,
  data: PropTypes.object
};

ErrorBoundary.defaultProps = {
  renderError: null,
  callback: () => null,
  data: {}
};

export default withRouter(
  connect(
    state => ({
    }),
    {}
  )(ErrorBoundary)
);
