import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { classList } from '@/utils/system/ui';
import { isFunction } from '@/utils/typeof';

class LoaderInWindow extends Component {
  static baseClass = 'tk-sticky';

  constructor (props) {
    super(props);

    this.state = {
      height: 0,
      width: 0,
      isActiveSticky: false,
    };
    this.init = true;
  }

  componentDidMount() {
    this.handleScroll();
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.props.enable && this.state.isActiveSticky !== prevState.isActiveSticky) || this.init) {
      const stickyDiv = this.stickyDiv.current || null;
      if (!stickyDiv) {
        return;
      }
      let stickyRect = stickyDiv.getBoundingClientRect();
      this.init = false;
      this.props.setInWindow(this.state.isActiveSticky, {
        height: stickyRect.height,
        width: stickyRect.width,
      });
    }

    if (prevProps.scrollRectBody.top !== this.props.scrollRectBody.top) {
      this.handleScroll();
    }
  }

  stickyDiv = React.createRef();

  handleScroll = () => {
    const stickyDiv = this.stickyDiv.current || null;

    this.frameId = 0;

    if (!stickyDiv) {
      return;
    }

    const scrollRect = this.props.scrollRectBody;
    const slide = this.props.slide || {};
    const preloadX = slide.x || 0;
    const preloadY = slide.y || 0;
    let stickyRect = stickyDiv.getBoundingClientRect();

    stickyRect = { // Apparently you can't spread the results of a bounding client rectangle
      height: stickyRect.height,
      width: stickyRect.width,
      top: stickyRect.top,
      left: stickyRect.left,
    };

    const heightScroll = scrollRect.height || document.documentElement.clientHeight;
    const widthScroll = scrollRect.width || document.documentElement.clientWidth;

    const X1 = {
      x: stickyRect.left,
      y: stickyRect.top,
    };

    const X3 = {
      x: stickyRect.left + stickyRect.width,
      y: stickyRect.top + stickyRect.height,
    };

    const X4 = {
      x: stickyRect.left,
      y: stickyRect.top + stickyRect.height,
    };

    const Y1 = {
      x: 0,
      y: 0
    };

    const Y3 = {
      x: widthScroll,
      y: heightScroll
    };

    const Y4 = {
      x: 0,
      y: heightScroll
    };

    let dimensionalX = true;
    let dimensionalY = true;
    if (this.props.dimensionalX) {
      dimensionalX = (X4.x <= Y4.x && Y4.x <= X3.x) || (X4.x >= Y4.x && X4.x <= Y3.x);
    }

    if (this.props.dimensionalY) {
      dimensionalY = (X4.y <= Y4.y && Y1.y <= X4.y) ||
        (X4.y >= Y4.y && Y4.y >= X1.y) ||
        (X4.y - preloadY <= Y4.y && Y1.y <= X4.y + preloadY) ||
        (X4.y + preloadY >= Y4.y && Y4.y + preloadY >= X1.y);
    }

    this.setState({
      isActiveSticky: dimensionalY
    });
  };

  render() {
    const { children, className, renderContent, style } = this.props;
    const { isActiveSticky } = this.state;

    if (isFunction(renderContent)) {
      return (
        <div
          className={classList(LoaderInWindow.baseClass, className)}
          style={style}
          ref={this.stickyDiv}
        >
          {renderContent({ isActiveSticky })}
        </div>
      );
    }

    const childrenWithStuckProps = React.Children.map(children, (child) => {
      return React.cloneElement(child, { isActiveSticky });
    });

    return (
      <div
        className={LoaderInWindow.baseClass}
        style={style}
        ref={this.stickyDiv}
      >
        {childrenWithStuckProps}
      </div>
    );
  }
}

LoaderInWindow.propTypes = {
  className: PropTypes.string,
  enable: PropTypes.bool,
  style: PropTypes.object,
  renderContent: PropTypes.func,
  setInWindow: PropTypes.func,
  slide: PropTypes.object,
  /** If you have an internally scrolling component, pass its ref callback to watch for scroll events */
  scrollTarget: PropTypes.object,
  dimensionalX: PropTypes.bool,
  dimensionalY: PropTypes.bool,
};

LoaderInWindow.defaultProps = {
  className: '',
  enable: true,
  style: {},
  slide: {
    x: 0,
    y: 0
  },
  renderContent: null,
  scrollTarget: null,
  dimensionalX: true,
  dimensionalY: true,
  setInWindow: () => null
};

export default connect(
  state => ({
    scrollRectBody: state.ui.scrollRectBody,
  }),
)(LoaderInWindow);
