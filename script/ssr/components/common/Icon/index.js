import React from 'react';
import PropTypes from 'prop-types';

import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const Icon = (props) => {
  const {
    onClick,
    title,
    onMouseEnter,
    onMouseLeave,
    iconName,
    className
  } = props;
  return (<span
    className={classList(className, iconName, classes.icon)}
    onClick={onClick}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
    title={title}
  />);
};

Icon.propTypes = {
  iconName: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  title: PropTypes.string
};

Icon.defaultProps = {
  className: '',
  title: '',
  onClick: () => null,
  onMouseEnter: () => null,
  onMouseLeave: () => null
};

export default Icon;
