/*
 * File: components/elements/ScrollView/index.js
 * Desc: Scrollable view with customize scrollbar
 * Author: Nguyễn Nhật Phương (phuongnn10)
 * Created: 08/08/2018
 */

import PerfectScrollbar from 'perfect-scrollbar';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { classList } from '@/utils/system/ui';

// // import './style.scss';

/*
  * DOMSubtreeModified bị lỗi trên safari dó bộ gõ mặc định
  * Bỏ modified và chủ động call event đối với scroll view cho edit text
  * TODO tìm phương án thay thế cho DOMSubtreeModified
*/

class ScrollView extends PureComponent {

  constructor(props) {
    super(props);
    this._calledEnd = false;
    this._calledStart = false;
    this._isWaitingCallEnd = false;
    this._isWaitingCallStart = false;
    this._timeoutJobScroll = null;
    this.handleContentChange = this.handleContentChange.bind(this);
    this.update = this.update.bind(this);
  }

  componentDidMount() {
    this._ps = new PerfectScrollbar(
      this._wrapper,
      {
        suppressScrollX: true
      }
    );
    this.props.getPs(this._ps);

    if (this._wrapper) {
      this._wrapper.addEventListener('ps-scroll-y', this._handleScroll);
      this._wrapper.addEventListener('ps-scroll-up', this._handleScrollUp);
      this._wrapper.addEventListener('ps-scroll-down', this._handleScrollDown);
      this._wrapper.addEventListener('ps-y-reach-start', this._handleScrollUp);
      this._wrapper.addEventListener('ps-y-reach-end', this._handleScrollDown);

      if (!this.props.isOffDOMSubtreeModified) {
        this._wrapper.addEventListener(
          'DOMSubtreeModified',
          this.handleContentChange
        );
      }
      this._jobScroll();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.hasMore && this._timeoutJobScroll) {
      clearTimeout(this._timeoutJobScroll);
      this._timeoutJobScroll = null;
    }
  }

  componentWillUnmount() {
    if (this._wrapper) {
      this._wrapper.removeEventListener('ps-scroll-y', this._handleScroll);
      this._wrapper.removeEventListener('ps-scroll-up', this._handleScrollUp);
      this._wrapper.removeEventListener(
        'ps-scroll-down',
        this._handleScrollDown
      );
      this._wrapper.removeEventListener(
        'ps-y-reach-start',
        this._handleScrollUp
      );
      this._wrapper.removeEventListener(
        'ps-y-reach-end',
        this._handleScrollDown
      );
      if (!this.props.isOffDOMSubtreeModified) {
        this._wrapper.removeEventListener(
          'DOMSubtreeModified',
          this.handleContentChange
        );
      }
    }
    if (this._ps) {
      this._ps.destroy();
    }

    if (this._timeoutJobScroll) {
      clearTimeout(this._timeoutJobScroll);
      this._timeoutJobScroll = null;
    }
  }

  update = function () {
    this._ps.update();
  };

  showScrollbar = () => {
    this._ps.lastScrollTop = -1;
    this._ps.onScroll();
  };

  scrollToTop = () => {
    if (this._ps) {
      if (this._ps.element) {
        this._ps.element.scrollTop = 0;
        this._ps.update();
      }
    }
  };

  _handleScroll = () => {
    if (this.didRenderInit) {
      this.scrollChange = true;
    }

    const lastScrollTop = this._ps.lastScrollTop;
    const containerHeight = this._ps.containerHeight;
    const contentHeight = this._ps.contentHeight;
    const scrollRest = contentHeight - (lastScrollTop + containerHeight);
    this._calledEnd = this.props.endThreshold < scrollRest;
    this._calledStart = lastScrollTop >= this.props.startThreshold;
    if (typeof this.props.onScroll === 'function') {
      this.props.onScroll();
    }
    if (this._calledEnd && this._calledStart && this._timeoutJobScroll) {
      clearTimeout(this._timeoutJobScroll);
      this._timeoutJobScroll = null;
    } else if (!this._timeoutJobScroll && this.props.hasMore) {
      this._jobScroll();
    }
  };

  _jobScroll = (timeout = 500) => {
    this._timeoutJobScroll = setTimeout(() => {
      if (this.props.hasMore) {
        if (this._ps && this._ps.element) {
          this._ps.update();

          const lastScrollTop = this._ps.lastScrollTop;
          const containerHeight = this._ps.containerHeight;
          const contentHeight = this._ps.contentHeight;
          const scrollRest = contentHeight - (lastScrollTop + containerHeight);
          this._calledEnd = this.props.endThreshold < scrollRest;
          this._calledStart = lastScrollTop >= this.props.startThreshold;

          this._handleScrollUp();
          this._handleScrollDown();
          this._jobScroll(timeout);
        }
      }
    }, timeout);
  };

  handleContentChange = function () {
    this._calledEnd = false;
    this._calledStart = false;
  };

  _handleScrollUp = () => {
    const lastScrollTop = this._ps.lastScrollTop;

    if (
      !this._calledStart &&
      lastScrollTop < this.props.startThreshold &&
      typeof this.props.onStartReach === 'function' &&
      this.props.hasMore &&
      !this.props.isLoading &&
      !this._isWaitingCallStart
    ) {
      this._calledStart = true;
      const rsStartReach = this.props.onStartReach();
      if (rsStartReach && rsStartReach instanceof Promise && typeof rsStartReach.then === 'function') {
        this._isWaitingCallStart = true;
        rsStartReach.then(() => {
          this._isWaitingCallStart = false;
        });
      } else {
        this._isWaitingCallStart = true;
        setTimeout(() => {
          this._isWaitingCallStart = false;
        }, 200);
      }
    }
  };

  _handleScrollDown = () => {
    const lastScrollTop = this._ps.lastScrollTop;
    const containerHeight = this._ps.containerHeight;
    const contentHeight = this._ps.contentHeight;
    const scrollRest = contentHeight - (lastScrollTop + containerHeight);
    if (
      !this._calledEnd &&
      scrollRest < this.props.endThreshold &&
      typeof this.props.onEndReach === 'function' &&
      this.props.hasMore &&
      !this.props.isLoading &&
      !this._isWaitingCallEnd
    ) {
      this._calledEnd = true;
      const rsEndReach = this.props.onEndReach();
      if (rsEndReach && rsEndReach instanceof Promise && typeof rsEndReach.then === 'function') {
        this._isWaitingCallEnd = true;
        rsEndReach.then(() => {
          this._isWaitingCallEnd = false;
        });
      } else {
        this._isWaitingCallEnd = true;
        setTimeout(() => {
          this._isWaitingCallEnd = false;
        }, 200);
      }
    }
  };

  render() {
    const { children, style, id } = this.props;
    let { className, height } = this.props;

    let heightStyle = {};
    className = classList(
      'e-scrollview',
      className
    );

    // NOTE: default value sẽ control bằng CSS
    if (height) {
      heightStyle = {
        maxHeight: typeof height === 'string' ? height : `${height}rem`
      };
    }

    let scrollViewStyle = { ...style, ...heightStyle };

    return (
      <div
        className={className}
        id={id}
        style={scrollViewStyle}
        ref={r => {
          this._wrapper = r;
        }}
      >
        <div className="e-scrollview-content">{children}</div>
      </div>
    );
  }
}

ScrollView.propsType = {
  className: PropTypes.string,
  style: PropTypes.object,
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.func
  ]),
  startThreshold: PropTypes.number,
  endThreshold: PropTypes.number,
  onStartReach: PropTypes.func,
  onEndReach: PropTypes.func,
  hasMore: PropTypes.bool,
  isLoading: PropTypes.bool,
  initToBottom: PropTypes.bool,
  getPs: PropTypes.func,
  isOffDOMSubtreeModified: PropTypes.bool
};

ScrollView.defaultProps = {
  className: null,
  style: {},
  height: null,
  startThreshold: 10,
  endThreshold: 10,
  hasMore: false,
  isLoading: false,
  initToBottom: false,
  getPs: () => null,
  isOffDOMSubtreeModified: false
};

export default ScrollView;
