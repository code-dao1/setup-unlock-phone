import React from 'react';
import Icon from '@/components/common/Icon';
import { classList } from '@/utils/system/ui';

// import './style.scss';

const Select = (props) => {
  const {
    className,
    items,
    onChange,
    valueSelect,
    autoComplete = true
  } = props;
  return (
    <div className={classList(className, 'base-select')}>
      <select
        onChange={onChange}
        autoComplete={'true'}
      >
        {
          items.map(({ name, value }, index) => {
            return (
              <option
                selected={valueSelect === value}
                key={`select-option-${index}`}
                value={value}
              >
                {name}
              </option>
            );
          })
        }
      </select>
      <Icon iconName={'angle-down'} className={'base-select-icon'} />
    </div>
  );
};

export default Select;
