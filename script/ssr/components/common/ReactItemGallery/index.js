import React from 'react';
import ImageGallery from 'react-image-gallery';

import 'react-image-gallery/styles/scss/image-gallery.scss';
import { classList } from '@/utils/system/ui';

// import './style.scss';

const ReactItemGallery = (props) => {
  const {
    items = [],
    option,
    className
  } = props;

  return (
    <div className={classList(className, 'react-image-gallery')}>
      <ImageGallery
        items={items}
        thumbnailPosition={'bottom'}
        showPlayButton={false}
        showFullscreenButton={false}
        {...option}
      />
    </div>
  );
};

export default ReactItemGallery;
