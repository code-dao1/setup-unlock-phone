import React from 'react';

const LoadingV2 = (props) => {
  return (
    <div>
      <svg width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
        <g fill="currentColor" transform="rotate(71.8013 12 12)">
          <path d="M20 12h-2c0-3.309-2.691-6-6-6V4c4.411 0 8 3.589 8 8z"/>
          <path
            d="M12 20c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8zm0-14c-3.309 0-6 2.691-6 6s2.691 6 6 6 6-2.691 6-6-2.691-6-6-6z"
            opacity=".242"
          />
          <animateTransform
            attributeName="transform"
            attributeType="XML"
            type="rotate"
            from="0 12 12"
            to="360 12 12"
            dur=".5s"
            repeatCount="indefinite"/>
        </g>
      </svg>
    </div>
  );
};

export default LoadingV2;
