import React from 'react';
import PropTypes from 'prop-types';
import LinkCustom from 'next/link'

const Link = (props) => {
  const {
    to,
    children
  } = props;

  return (
    <LinkCustom href={to}>
      <a {...props}>
        {children}
      </a>
    </LinkCustom>
  );
};

Link.propTypes = {};

Link.defaultProps = {};

export default Link;
