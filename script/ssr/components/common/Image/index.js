import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { classList } from '@/utils/system/ui';
import { connect } from 'react-redux';
import LoaderBalls from '../LoaderBalls';
import LoaderInWindow from '../LoaderInWindow';
// Styles
// // import './style.scss';

class Image extends PureComponent {
  constructor(props) {
    super(props);

    let { src, isFetchImage, isLoading } = this._initialVariables(props);

    this.state = {
      currentSrc: src,
      isFetchImage: isFetchImage,
      isLoading: isLoading,
      isAccessLoad: !props.loadInWindow
    };
    this.init = true;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.src !== this.props.src) {
      let { src, isFetchImage, isLoading } = this._initialVariables(this.props);
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        currentSrc: src,
        isLoading,
        isFetchImage,
        isAccessLoad: !this.props.loadInWindow
      });
    }

    if ((this.state.isAccessLoad && !prevState.isAccessLoad) || (this.state.isFetchImage && !this.props.loadInWindow)) {
      this.fetchImage(this.props.src);
    }
  }

  componentWillUnmount() {
    if (this.loadingImage) {
      this.loadingImage.onload = null;
    }
  }

  fetchImage = src => {
    this.setState({
      isFetchImage: false
    });
    let imgTag = new window.Image();
    imgTag.onload = () => {
      this.setState({ currentSrc: this.loadingImage.src, isLoading: false, isAccessLoad: true });
    };

    imgTag.onError = (e => {
      const DEFAULT_IMG = `/media/image/default/${
        this.props.default ? this.props.default : 'not-found.svg'
      }`;
      this.setState({ isFetchImage: true });
      const src = e.target.src;
      if ((src || '').includes(DEFAULT_IMG)) {
        return;
      }

      if (!this.props.onError) {
        this.setState({ currentSrc: DEFAULT_IMG, isLoading: false, isAccessLoad: true });
      } else {
        this.props.onError(e);
      }
    });

    imgTag.src = src;

    this.loadingImage = imgTag;
  };

  setSmallImage = props => {
    const { src, isLazyLoad, isFullImage } = props;
    return { imageSrc: src, isFetchImage: false };
  };

  _initialVariables = props => {
    let { imageSrc, isFetchImage } = props && this.setSmallImage(props);

    if (imageSrc) {
      return {
        src: imageSrc,
        isFetchImage: isFetchImage,
        isLoading: true
      };
    }

    if (props && props.src && props.src.match(/data:image\/(?:jpg|jpeg|png)/)) {
      return { src: props.src, isFetchImage: false, isLoading: false };
    }

    imageSrc = props && `/media/image/${props.path}.${props.extension}`;
    if (imageSrc) {
      return { src: imageSrc, isFetchImage: false, isLoading: true };
    }
    return { src: '', isFetchImage: false, isLoading: true };
  };

  _onLoad = (e) => {
    const { onLoad } = this.props;
    this.setState({ isLoading: false });
    if (typeof onLoad === 'function') {
      if ((e && e.target && e.target.src === this.props.src) || this.props.path) {
        this.props.onLoad(e);
      }
    }
  };

  _onError = (e) => {
    const DEFAULT_IMG = `/media/image/default/${
      this.props.default ? this.props.default : 'not-found.svg'
    }`;
    this.setState({ isLoading: false });
    const src = e.target.src;
    if ((src || '').includes(DEFAULT_IMG)) {
      return;
    }

    if (!this.props.onError) {
      e.target.src = DEFAULT_IMG;
    } else {
      this.props.onError(e);
    }
  }

  _renderImage = () => {
    const { isLoading, currentSrc, isFetchImage } = this.state;
    const {
      path,
      className,
      alt,
      style,
      onError,
      imgRef,
      align,
      title
    } = this.props;
    const DEFAULT_IMG = `/media/image/default/${
      this.props.default ? this.props.default : 'not-found.svg'
    }`;

    let { fillMode } = this.props;
    let modeList = ['fill', 'contain', 'cover', 'none', 'scale-down'];

    if (modeList.indexOf(fillMode) > -1) {
      fillMode = ' obj-' + fillMode;
    } else {
      fillMode = '';
    }

    if (!currentSrc && !path) {
      return null;
    }

    return (
      <img
        ref={imgRef}
        onLoad={this._onLoad}
        className={`image ${fillMode} ${className}`}
        src={currentSrc}
        onError={this._onError}
        style={!isLoading ? style : { display: 'none' }}
        alt={alt || 'media-object'}
        align={align}
        title={title || ''}
      />
    );
  };

  renderLoading = () => {
    const {
      isLoading,
      isAccessLoad
    } = this.state;

    const {
      small,
      className,
      style
    } = this.props;

    return (
      <>
        {
          (isLoading) &&
          !small &&
          (<div style={style} className={classList(className, 'loading-image-lazy')}><LoaderBalls className={className} style={style}/></div>)
        }
        {
          (isLoading) &&
          small &&
          (<div style={style} className={classList(className, 'loading-small-lazy')}/>)
        }
      </>
    );
  };

  renderLazyImage = () => {
    const {
      isAccessLoad
    } = this.state;

    return (<>
      {isAccessLoad && this._renderImage()}
      {this.renderLoading()}
    </>);
  };

  setInWindow = (isActiveSticky) => {
    if (this.init) {
      this.init = false;
      if (isActiveSticky) {
        this.fetchImage(this.props.src);
      }
    }
    if (isActiveSticky && !this.state.isAccessLoad) {
      this.setState({
        isAccessLoad: true
      });
    }
  };

  render() {
    const {
      loadInWindow,
      style,
      small,
      isBrowser,
      className,
      src,
      alt,
      title,
      align
    } = this.props;

    const {
      isLoading,
      isAccessLoad
    } = this.state;

    if (!isBrowser) {
      let { fillMode } = this.props;
      let modeList = ['fill', 'contain', 'cover', 'none', 'scale-down'];

      if (modeList.indexOf(fillMode) > -1) {
        fillMode = ' obj-' + fillMode;
      } else {
        fillMode = '';
      }

      return (
        <img
          className={`image ${fillMode} ${className}`}
          src={src}
          alt={alt || 'media-object'}
          align={align}
          title={title || ''}
        />
      );
    }

    if (!loadInWindow) {
      return this.renderLazyImage();
    }

    return (<LoaderInWindow
      src={this.state.currentSrc}
      className={classList((!isAccessLoad || isLoading) ? 'loading-image' : '', 'sticky-image')}
      style={style}
      enable={!isAccessLoad}
      renderContent={this.renderLazyImage}
      setInWindow={this.setInWindow}
    />);
  }
}

Image.propTypes = {
  src: PropTypes.string,
  path: PropTypes.string,
  default: PropTypes.string,
  small: PropTypes.bool,
  extension: PropTypes.string,
  className: PropTypes.string,
  alt: PropTypes.string,
  style: PropTypes.object,
  fillMode: PropTypes.string,
  imgRef: PropTypes.func,
  align: PropTypes.string,
  isLazyLoad: PropTypes.bool,
  loadInWindow: PropTypes.bool,
  isFullImage: PropTypes.bool
};

Image.defaultProps = {
  src: null,
  path: null,
  default: null,
  small: false,
  extension: 'svg',
  className: '',
  alt: '',
  style: {},
  fillMode: 'cover',
  imgRef: () => {
  },
  align: null,
  isLazyLoad: true,
  loadInWindow: false,
  isFullImage: false
};

const mapStateToProps = state => ({
  isBrowser: state.ui.isBrowser
});

export default connect(
  mapStateToProps,
)(Image);
