import PropTypes from 'prop-types';
import React from 'react';

// import './style.scss';

const SInput = (props) => {
  const {
    isActive,
    type,
    value,
    onChange,
    label,
    placeholder,
    errorMessage,
  } = props;

  const isShowFloatingLabel = !!value;
  const isInvalid = !!errorMessage;
  return (
    <div className="s-input-wrapper">
      <div
        className={`is-medium s-input floating-label ${isShowFloatingLabel ? 'is-show-floating-label' : ''} ${isActive ? 'is-active' : ''}`}>
        <label className="s-label">
          {label}
        </label>
        <input
          type={type}
          className={`s-input-inner ${isInvalid ? 'is-invalid' : ''}`}
          placeholder={placeholder}
          value={value || ''}
          onChange={onChange}
        />
      </div>
      <div>
        {
          errorMessage &&
          (<p className="field-message field-message-error">
            {errorMessage}
          </p>)
        }
      </div>
    </div>
  );
};

SInput.propTypes = {
  isActive: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  errorMessage: PropTypes.string,
};

SInput.defaultProps = {
  isActive: false,
  type: 'text',
  value: '',
  label: '',
  placeholder: '',
  errorMessage: '',
};

export default SInput;
