import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

class Popup extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.wrapperOutsideRef = null;
  }

  componentDidMount() {
    if (this.props.isActive) {
      document.addEventListener('click', this.handleClickOutside);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isActive !== prevProps.isActive) {
      if (this.props.isActive) {
        document.addEventListener('click', this.handleClickOutside);
      } else {
        document.removeEventListener('click', this.handleClickOutside);
      }
    }
  }

  handleClickOutside = (event) => {
    if (
      this.props.isActive &&
      this.wrapperOutsideRef &&
      this.props.clickOutside &&
      !this.wrapperOutsideRef.contains(event.target)
    ) {
      this.props.clickOutside();
    }
  };

  render() {

    const {
      isActive,
      className,
      children
    } = this.props;

    return (
      <div
        className={classList(classes.popup, isActive && 'opening', className)}
      >
        <div
          className={`content ${isActive ? 'opening' : ''}`}
          ref={r => this.wrapperOutsideRef = r}
        >
          {children}
        </div>
      </div>
    );
  }
}

Popup.propTypes = {
  isActive: PropTypes.bool,
  className: PropTypes.string,
  clickOutside: PropTypes.func,
};

Popup.defaultProps = {
  isActive: false,
  className: '',
  clickOutside: () => null
};

export default Popup;
