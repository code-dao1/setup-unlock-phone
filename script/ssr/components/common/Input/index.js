import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';

class Input extends PureComponent {
  focus = () => {
    if (this.ref) {
      this.ref.focus();
    }
  };

  render() {
    const {
      id,
      className,
      name,
      value,
      placeholder,
      style,
      required,
      checked,
      disabled,
      autoComplete,
      readOnly,
      maxLength,
      onKeyUp,
      onFocus,
      onKeyDown,
      autoFocus,
      onClick,
      reFocus,
      defaultValue,
      min,
      step
    } = this.props;

    let { type, accept, alt, pattern } = this.props;

    let isFireFox = false;

    // Đúng type thì mới return input tương ứng
    let types = [
      'button',
      'checkbox',
      'color',
      'date',
      'datetime-local',
      'email',
      'file',
      'hidden',
      'image',
      'month',
      'number',
      'password',
      'radio',
      'range',
      'reset',
      'search',
      'submit',
      'tel',
      'text',
      'time',
      'url',
      'week'
    ];
    if (types.indexOf(type) <= -1) {
      return null;
    }

    // Chỉ với 'type' tương ứng mới sử dụng được 1 số props đặc biệt
    switch (type) {
      case 'image':
        if (!alt) {
          alt = 'media-object';
        }
        accept = null;
        break;
      case 'file':
        alt = null;
        break;
      case 'number':
        // NOTE: input type 'number' và 'pattern' attribute không hỗ trợ trên Firefox Desktop, cần xử lý regex riêng bằng js
        if (navigator.userAgent.indexOf('Chrome') === -1) {
          isFireFox = true;
        }
        pattern = '[0-9]*';
        break;
      default:
        accept = null;
        alt = null;
    }

    let valueOrDefaultValue;

    if (value === undefined && defaultValue !== undefined) {
      valueOrDefaultValue = {
        defaultValue
      };
    } else {
      valueOrDefaultValue = {
        value
      };
    }

    return (
      <input
        id={id}
        className={classList(className, 'input-field')}
        type={type}
        name={name}
        min={min}
        step={step}
        {...valueOrDefaultValue}
        placeholder={placeholder}
        style={style}
        maxLength={maxLength}
        pattern={pattern}
        required={required}
        checked={checked}
        accept={accept}
        disabled={disabled}
        autoComplete={autoComplete ? 'on' : 'off'}
        readOnly={readOnly}
        alt={alt}
        ref={c => {
          this.ref = c;
        }}
        onChange={e => {
          this.ref.focus();
          this.props.onChange(e);
        }}
        onKeyPress={e => {
          // If: nếu input là OTP, chặn khi đã nhập 6 kí tự (tránh input tự làm tròn số nguyên)
          // Else: if Firefox, chặn input chữ
          if (id === 'otp' && this.ref.textLength > 5) {
            e.preventDefault();
          } else if (isFireFox) {
            // charcode của 0 - 9 từ 48 - 57
            if (e.charCode < 48 || e.charCode > 57) {
              e.preventDefault();
            }
          }
        }}
        onClick={onClick}
        onKeyUp={onKeyUp}
        onBlur={() => {
          if (reFocus) {
            this.ref.focus();
          }

          this.props.onBlur();
        }}
        onFocus={onFocus}
        onKeyDown={onKeyDown}
        autoFocus={autoFocus}
      />
    );
  }
}

Input.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.func
  ]),
  placeholder: PropTypes.string,
  style: PropTypes.object,
  accept: PropTypes.string,
  maxLength: PropTypes.number,
  pattern: PropTypes.string,
  required: PropTypes.bool,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  autoComplete: PropTypes.string,
  readOnly: PropTypes.bool,
  alt: PropTypes.string,
  // ref: PropTypes.oneOfType([
  //   PropTypes.string,
  //   PropTypes.func,
  //   PropTypes.func
  // ]),
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  onKeyUp: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onKeyDown: PropTypes.func,
  autoFocus: PropTypes.bool,
  iconClick: PropTypes.func,
  reFocus: PropTypes.bool,
  defaultValue: PropTypes.string,
  responseType: PropTypes.string
};

Input.defaultProps = {
  label: null,
  id: null,
  className: '',
  type: null,
  name: null,
  value: undefined,
  placeholder: null,
  style: {},
  accept: null,
  maxLength: null,
  pattern: null,
  required: false,
  checked: false,
  disabled: false,
  autoComplete: '',
  readOnly: null,
  alt: null,
  // ref: () => null,
  onChange: () => null,
  onClick: () => null,
  onKeyUp: () => null,
  onBlur: () => null,
  onFocus: () => null,
  onKeyDown: () => null,
  autoFocus: false,
  iconClick: () => null,
  reFocus: false,
  defaultValue: '',
  responseType: 'error'
};

export default Input;
