import PropTypes from 'prop-types';
import React from 'react';

// Style
// import './style.scss';

const Loading = (props) => {
  const { message, className } = props;

  return (
    <div className={`loading-state ${className}`}>
      <div className="loading-spinner" />

      {
        !!message &&
        (<p className="loading-message">{message}</p>)
      }
    </div>
  );
}

Loading.propTypes = {
  message: PropTypes.node,
  className: PropTypes.string,
  isMessage: PropTypes.bool
};

Loading.defaultProps = {
  message: '',
  className: '',
  isMessage: true
};

export default Loading;
