import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { changeDimension, focusWindow, blurWindow } from '../../../actions/system/ui';

class CronJob extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    window.addEventListener('focus', this.onFocusWindow);
    window.addEventListener('blur', this.onBlurWindow);
  }

  onFocusWindow = () => {
    if (this.setTimeoutChangeActive) {
      clearTimeout(this.setTimeoutChangeActive);
    }
    this.setTimeoutChangeActive = setTimeout(() => {
      this.props.focusWindow();
      this.setTimeoutChangeActive = null;
    }, 50);
  };

  onBlurWindow = () => {
    if (this.setTimeoutChangeActive) {
      clearTimeout(this.setTimeoutChangeActive);
    }
    this.setTimeoutChangeActive = setTimeout(() => {
      this.props.blurWindow();
      this.setTimeoutChangeActive = null;
    }, 50);
  };

  updateDimensions = () => {
    if (this.setTimeoutChangeDimension) {
      clearTimeout(this.setTimeoutChangeDimension);
    }
    this.setTimeoutChangeDimension = setTimeout(() => {
      this.props.changeDimension();
      this.setTimeoutChangeDimension = null;
    }, 50);
  };

  render() {
    return null;
  }
}

CronJob.propTypes = {};

CronJob.defaultProps = {};

export default withRouter(connect(
  state => ({
  }), {
    changeDimension,
    focusWindow,
    blurWindow
  }
)(CronJob));
