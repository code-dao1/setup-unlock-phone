import React from 'react';
import PropTypes from 'prop-types';

const Video = (props) => {
  const {
    src,
    className,
    defaultPoster,
    poster,
    autoPlay,
    controls,
    getRef
  } = props;
  return (
    <video
      ref={r => getRef(r)}
      className={className}
      controls={controls}
      title={src}
      allowFullScreen
      autoPlay={autoPlay}
      poster={src ? poster : defaultPoster}
    >
      {src && <source src={src} />}
    </video>
  );
}

Video.propTypes = {
  src: PropTypes.string,
  className: PropTypes.string,
  defaultPoster: PropTypes.string,
  poster: PropTypes.string,
  autoPlay: PropTypes.bool,
  controls: PropTypes.bool,
  onLoad: PropTypes.func,
  getRef: PropTypes.func
};

Video.defaultProps = {
  src: null,
  className: '',
  defaultPoster: '/media/image/default/not-found.svg',
  poster: '',
  autoPlay: false,
  controls: true,
  onLoad: () => null,
  getRef: () => null
};

export default Video;
