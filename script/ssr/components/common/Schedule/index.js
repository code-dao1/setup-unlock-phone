import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class Schedule extends PureComponent {
  componentDidMount() {
    if (this.props.isActive) {
      this.startSchedule();
      this.mount = true;
    }
  }

  componentWillUnmount() {
    this.mount = false;
    this.stopSchedule();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.isActive && this.props.isActive && this.mount) {
      this.startSchedule();
    } else if (prevProps.isActive && !this.props.isActive) {
      this.stopSchedule();
    }
  }

  startSchedule = () => {
    const {
      isActive,
      timeout
    } = this.props;
    if (isActive && timeout > 100) {
      this.runSchedule();
    }
  };

  runSchedule = () => {
    const {
      data,
      callback,
      timeout,
      isActive
    } = this.props;
    if (this.timeoutSchedule) {
      clearTimeout(this.timeoutSchedule);
    }

    this.timeoutSchedule = setTimeout(() => {
      callback(data);
      this.timeoutSchedule = null;
      if (isActive && this.mount) {
        this.runSchedule();
      }
    }, timeout);
  };

  stopSchedule = () => {
    if (this.timeoutSchedule) {
      clearTimeout(this.timeoutSchedule);
      this.timeoutSchedule = null;
    }
  };

  render() {
    return null;
  }
}

Schedule.propTypes = {
  data: PropTypes.any,
  isActive: PropTypes.bool,
  callback: PropTypes.func,
  timeout: PropTypes.number
};

Schedule.defaultProps = {
  data: null,
  isActive: false,
  callback: () => null,
  timeout: 5000
};

export default Schedule;
