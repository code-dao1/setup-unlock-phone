import React, { PureComponent } from 'react';

import { connect } from 'react-redux';

class HTMLContent extends PureComponent {
  constructor(props) {
    super(props);
    this.contentRef = null;
  }
  componentDidMount() {
    // if (this.contentRef) {
    // }
  }
  componentWillUnmount() {
  }

  render() {
    const {
      className,
      content,
    } = this.props;

    return (
      <div
        className={`image-previewer-wrapper${className ? ` ${className}` : ''}`}
        ref={c => {
          this.contentRef = c;
        }}
      >
        <div
          className={`html-content${className ? ` ${className}` : ''}`}
          dangerouslySetInnerHTML={{ __html: content }}
        />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  ...state.image
});
export default connect(
  mapStateToProps,
)(HTMLContent);
