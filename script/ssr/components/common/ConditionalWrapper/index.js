import React from 'react';
import * as PropTypes from 'prop-types';

const ConditionalWrapper = (props) => {
  const {
    condition,
    wrapper,
    children
  } = props;
  return condition ? wrapper(children) : children;
};

ConditionalWrapper.propTypes = {
  condition: PropTypes.bool,
  wrapper: PropTypes.func
};

ConditionalWrapper.defaultProps = {
  condition: false,
  wrapper: () => null
};

export default ConditionalWrapper;
