import ListVideoReview from '@/components/ListVideoReview';
import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import React from 'react';
import PropTypes from 'prop-types';
import reviewInHome from '../../constants/review/reviewInHome';
import ReviewItemPage from '../ReviewItemPage';

const ListReviewPage = (props) => {
  return (
    <section className="reviews">
      <div className="container">
        <div className="col-md-10 col-md-offset-1 text-center">
          <div>
            <h2 className="heading-lg">Excellent <strong>4.8</strong> <small>/ 5</small></h2>
            <br/>
            <div className="rating">
              <i className="fa fa-2x fa-star" />
              <i className="fa fa-2x fa-star" />
              <i className="fa fa-2x fa-star" />
              <i className="fa fa-2x fa-star" />
              <i className="fa fa-2x fa-star" />
              <div className="row">
                <div className="col-md-8 col-md-offset-2 col-xs-12 col-xs-offset-0">
                  <p className="text-grey" style={{ margin: '10px 0 0', lineHeight: '22px' }}>
                    {publicRuntimeConfig.productBrand}, Phone Unlocking Service is rated 4.8 out of 5 based on 528 reviews
                  </p>
                </div>
              </div>
            </div>
            <br/>
          </div>
        </div>
        <hr/>
      </div>
      <div id="reviewCarousel" className="carousel slide" data-ride="carousel">
        <div className="carousel-inner">
          <div className="item active">
            <div className="container">
              <div className="row">
                {
                  reviewInHome.map((item, index) => {
                    return <ReviewItemPage
                      className={'col-md-4'}
                      item={item}
                      key={`review-item-page-${index}`}
                    />;
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
      <ListVideoReview />
    </section>
  );
};

ListReviewPage.propTypes = {};

ListReviewPage.defaultProps = {};

export default ListReviewPage;
