import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { copyStringToClipboard } from '@/utils/clipboard';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';
// import './style.scss';

const ShareButton = (props) => {

  const {
    id,
    content,
    created_at,
    handle,
    image_src,
    image_title,
    title,
    user_name,
  } = props.data || {};

  const [isCopied, setIsCopied] = useState(false);

  const fullUrl = typeof window === 'undefined' ? '' : window.location.href;

  useEffect(() => {
    setIsCopied(false);
  }, [id]);

  const onCopy = () => {
    setIsCopied(true);
    copyStringToClipboard(fullUrl);
  };

  return (
    <div
      id="atstbx"
      className={classList('at-resp-share-element at-style-responsive addthis-smartlayers addthis-animated at4-show', classes.shareButton)}
      role="region"
    >
      <span className="at4-visually-hidden">AddThis Sharing Buttons</span>
      <div className="at-share-btn-elements">
        <a
          role="button"
          tabIndex="0"
          target={'_blank'}
          href={`https://www.facebook.com/sharer/sharer.php?u=${fullUrl}`}
          className={classList('at-icon-wrapper at-share-btn at-svc-facebook', classes.facebookButton)}
        >
          <span className="at4-visually-hidden">Share to Facebook</span>
          <span className={classList('at-icon-wrapper', classes.iconWrapper)}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              viewBox="0 0 32 32"
              version="1.1"
              role="img"
              aria-labelledby="at-svg-facebook-1"
              style={{ fill: 'rgb(255, 255, 255)', width: '20px', height: '20px' }}
              className="at-icon at-icon-facebook"
            >
              <title id="at-svg-facebook-1">Facebook</title>
              <g><path
                d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z"
                fillRule="evenodd"
              />
              </g>
            </svg>
          </span>
          <span
            className={classList('at-label', classes.buttonLabel)}
          >Facebook
          </span>
        </a>
        <a
          role="button"
          tabIndex="0"
          target={'_blank'}
          className={classList('at-icon-wrapper at-share-btn at-svc-twitter', classes.twitterButton)}
          href={encodeURI(`https://twitter.com/intent/tweet?text=${title}&url=${fullUrl}`)}
        >
          <span className="at4-visually-hidden">Share to Twitter</span>
          <span className={classList('at-icon-wrapper', classes.iconWrapper)}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              viewBox="0 0 32 32"
              version="1.1"
              role="img"
              aria-labelledby="at-svg-twitter-2"
              style={{ fill: 'rgb(255, 255, 255)', width: '20px', height: '20px' }}
              className="at-icon at-icon-twitter"
            >
              <title id="at-svg-twitter-2">Twitter</title>
              <g>
                <path
                  d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336"
                  fillRule="evenodd"
                />
              </g>
            </svg>
          </span>
          <span
            className={classList('at-label', classes.buttonLabel)}
          >Twitter
          </span>
        </a>
        <a
          role="button"
          tabIndex="0"
          className={classList('at-icon-wrapper at-share-btn at-svc-link', classes.copyLinkButton)}
          onClick={onCopy}
        >
          <span className="at4-visually-hidden">Share to Copy Link</span>
          <span className={classList('at-icon-wrapper', classes.iconWrapper)}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              viewBox="0 0 32 32"
              version="1.1"
              role="img"
              aria-labelledby="at-svg-link-3"
              className="at-icon at-icon-link"
              style={{ fill: 'rgb(255, 255, 255)', width: '20px', height: '20px' }}
            >
              <title id="at-svg-link-3">
                {!isCopied ? 'Copy Link' : 'Copied Link'}
              </title>
              <g>
                <path
                  d="M23.476 20.663c0-.324-.114-.6-.34-.825l-2.524-2.524a1.124 1.124 0 0 0-.826-.34c-.34 0-.63.13-.873.388.024.024.1.1.23.225s.217.212.26.26c.046.05.106.126.183.23a.976.976 0 0 1 .2.644c0 .325-.113.6-.34.827-.226.226-.5.34-.825.34-.12 0-.23-.015-.332-.043a.976.976 0 0 1-.31-.158 2.89 2.89 0 0 1-.23-.182 7.506 7.506 0 0 1-.26-.26l-.226-.23c-.267.25-.4.545-.4.885 0 .322.113.597.34.824l2.5 2.512c.218.218.493.328.825.328.323 0 .598-.106.825-.316l1.784-1.772a1.11 1.11 0 0 0 .34-.813zm-8.532-8.556c0-.323-.113-.598-.34-.825l-2.5-2.512a1.124 1.124 0 0 0-.825-.34c-.316 0-.59.11-.826.328L8.67 10.53a1.11 1.11 0 0 0-.34.813c0 .323.113.598.34.825l2.524 2.524c.22.22.494.328.825.328.34 0 .63-.126.873-.376-.024-.025-.1-.1-.23-.225a7.506 7.506 0 0 1-.26-.262 2.89 2.89 0 0 1-.183-.23.976.976 0 0 1-.2-.644c0-.323.113-.598.34-.825.226-.227.5-.34.824-.34a.976.976 0 0 1 .643.2c.106.077.183.137.23.182.05.044.137.13.262.26s.2.207.224.23c.267-.25.4-.545.4-.885zm10.862 8.556c0 .97-.344 1.792-1.032 2.464L22.99 24.9c-.67.67-1.492 1.006-2.463 1.006-.98 0-1.805-.344-2.476-1.032l-2.5-2.512c-.67-.67-1.006-1.493-1.006-2.463 0-.997.356-1.842 1.068-2.538l-1.068-1.068c-.696.712-1.538 1.068-2.525 1.068-.97 0-1.797-.34-2.476-1.02L7.02 13.82C6.34 13.138 6 12.314 6 11.343c0-.97.344-1.792 1.032-2.464l1.784-1.773c.67-.67 1.492-1.007 2.463-1.007.978 0 1.803.344 2.475 1.032l2.5 2.512c.67.67 1.007 1.492 1.007 2.463 0 .995-.356 1.84-1.068 2.537l1.068 1.068c.696-.712 1.537-1.068 2.524-1.068.97 0 1.797.34 2.476 1.02l2.524 2.523c.68.68 1.02 1.505 1.02 2.476z"
                  fillRule="evenodd"
                />
              </g>
            </svg>
          </span>
          <span
            className={classList('at-label', classes.buttonLabel)}
          >
            {!isCopied ? 'Copy Link' : 'Copied Link'}
          </span>
        </a>
      </div>
    </div>
  );
};

ShareButton.propTypes = {};

ShareButton.defaultProps = {};

export default ShareButton;
