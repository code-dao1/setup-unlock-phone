import React from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';
import HTMLContent from '../common/HTMLContent';
import Image from '../common/Image';

import classes from './style.module.scss';

const ServiceDescription = (props) => {
  const {
    id,
    created_at,
    created_by,
    handle,
    description,
    image_src,
    image_title,
    name,
  } = props.data || {};

  return (
    <section className={classes.serviceDescription}>
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <Image
              src={image_src}
              alt={name}
              className={classList("center-block", classes.image)}
            />
            <HTMLContent content={description} />
          </div>
        </div>
      </div>
    </section>
  );
};

ServiceDescription.propTypes = {};

ServiceDescription.defaultProps = {};

export default ServiceDescription;
