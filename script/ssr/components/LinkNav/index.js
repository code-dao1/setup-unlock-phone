import React from 'react';
import PropTypes from 'prop-types';

import classes from './style.module.scss';

const LinkNav = (props) => {
  return (
    <div className={classes.shopLinks}>
      <div className={classes.shopLinksInner}>
        <div className={classes.shopLinksItem}>
          <a>
            Danh mục sản phẩm
          </a>
        </div>
      </div>
    </div>

  );
};

LinkNav.propTypes = {};

LinkNav.defaultProps = {};

export default LinkNav;
