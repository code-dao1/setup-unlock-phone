import React from 'react';
import PropTypes from 'prop-types';
import HTMLContent from '../common/HTMLContent';
import ServiceVertical from './ServiceVertical';
import Image from '../common/Image';

import classes from './style.module.scss'

const GroupServiceContent = (props) => {
  const {
    data
  } = props;

  if (!data) return null;

  const {
    id,
    description,
    handle,
    manufacturer,
    manufacturer_id,
    name,
    image_src,
    image_title,
    services,
  } = data;

  if (!services || services.length === 0) return null;

  return (
    <section className={classes.groupServiceContent} id="GB">
      <div className="container">
        <div className="row">
          <div className="col-xs-9">
            <div className="row" style={{ display: 'flex', alignItems: 'center' }}>
              <div className="col-md-1 col-sm-2 col-xs-3">
                <Image src={image_src} alt="" className="img-responsive" />
              </div>
              <div className="col-md-11 col-sm-10 col-xs-9 no-pad">
                <h3 style={{ margin: 0 }}>{name}</h3>
              </div>
            </div>
            <br/>
          </div>
        </div>
        <div className={classes.listService} id="Country-Networks">
          {
            services.map((service, index) => {
              return (
                <ServiceVertical
                  key={`service-vertical-item-${index}`}
                  data={service}
                  className={'col-md-3 col-sm-6 col-xs-6 no-pad'}
                />
              )
            })
          }
        </div>
      </div>
    </section>
  );
};

GroupServiceContent.propTypes = {};

GroupServiceContent.defaultProps = {};

export default GroupServiceContent;
