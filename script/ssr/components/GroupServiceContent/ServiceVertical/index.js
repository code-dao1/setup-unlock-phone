import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import { extractContent } from '@/utils/string';
import { classList } from '@/utils/system/ui';
import HTMLContent from '../../common/HTMLContent';
import Image from '../../common/Image';

import classes from './style.module.scss';

const ServiceVertical = (props) => {
  const {
    data,
    className
  } = props;

  if (!data) return null;

  const {
    id,
    handle,
    description,
    image_src,
    image_title,
    name,
    repair_time,
    from_price
  } = data;

  return (
    <div className={className}>
      <div className={classes.serviceItemVerticalWrapper}>
        <div className="row">
          <Link
            to={`/service/${handle}`}
            className={'text-black'}
          >
            <div className="col-xs-8 col-xs-offset-2">
              <Image
                src={image_src}
                alt={name}
                className={classList('center-block img-responsive', classes.image)}
              />
            </div>
            <div className="col-xs-12 text-center">
              <strong style={{ height: '4rem', display: 'block' }}>{name}</strong>
              {
                from_price !== undefined &&
                from_price !== null &&
                (<p>From ${from_price}</p>)
              }
              {
                repair_time &&
                (<small>{repair_time}</small>)
              }
              <hr className={classes.hr}/>
            </div>
          </Link>
          <div className="col-xs-12">
            <Link
              className="btn btn-primary"
              to={`/service/${handle}`}
            >
              Unlock
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

ServiceVertical.propTypes = {};

ServiceVertical.defaultProps = {};

export default ServiceVertical;
