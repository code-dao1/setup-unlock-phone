import { clearAllBodyScrollLocks, disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { closeModal, openModal } from '../../actions/modal';
import { classList } from '@/utils/system/ui';
import Icon from '../common/Icon';

import classes from './style.module.scss';

class Modal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.targetElement = document.querySelector('body');
    if (this.props.isOpen) {
      disableBodyScroll(this.targetElement);
    }
  }

  componentWillUnmount() {
    clearAllBodyScrollLocks();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.targetElement) {
      if (this.props.isOpen !== prevProps.isOpen) {
        if (this.props.isOpen) {
          disableBodyScroll(this.targetElement);
        } else {
          enableBodyScroll(this.targetElement);
        }
      }
    }
  }

  render() {
    const {
      isOpen,
      content,
      className,
      clickOutSide,
      closeModal,
    } = this.props;
    if (!isOpen) return null;

    return (
      <div className={classList(classes.modal, className)}>
        <div
          className={classes.modalBackground}
          onClick={() => {
            if (clickOutSide) {
              closeModal();
            }
          }}
        />
        <div className={classList(classes.modalContent, 'modal-content')}>
          {content}
          <Icon
            className={classes.modalClose}
            iconName={'close-icon'}
            onClick={() => {
              closeModal();
            }}
          />
        </div>
      </div>
    );
  }
}

Modal.propTypes = {};

Modal.defaultProps = {};

const mapStateToProps = state => {
  return {
    isOpen: state.modal.isOpen,
    content: state.modal.content,
    className: state.modal.className,
    clickOutSide: state.modal.clickOutSide,
  };
};

export default connect(
  mapStateToProps,
  {
    closeModal
  }
)(Modal);
