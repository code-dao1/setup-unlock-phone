import React from 'react';
import PropTypes from 'prop-types';

const Breadcrumb = (props) => {
  return (
    <div className="product-detail__category">
      <a className="cate-name" href="https://bibabo.vn/ec/product/list/all--1">
        Trang chủ
      </a>
      <img
        className="icon-right lazy loaded"
        data-src="https://bibabo.vn/assets/web/img/product_detail/arrow-right.svg"
        src="https://bibabo.vn/assets/web/img/product_detail/arrow-right.svg"
        data-was-processed="true"
        alt={''}
      />
      <a className="cate-name" href="https://bibabo.vn/mua-sam/danh-cho-me">
        Sách tiếng Việt
      </a>
      <img
        className="icon-right lazy loaded"
        data-src="https://bibabo.vn/assets/web/img/product_detail/arrow-right.svg"
        src="https://bibabo.vn/assets/web/img/product_detail/arrow-right.svg"
        data-was-processed="true"
        alt={''}
      />
      <a className="cate-name" href="https://bibabo.vn/mua-sam/thuc-pham-chuc-nang-cho-me">
        Lịch sử - Địa lý - Tôn giáo
      </a>
      <img
        className="icon-right lazy loaded"
        data-src="https://bibabo.vn/assets/web/img/product_detail/arrow-right.svg"
        src="https://bibabo.vn/assets/web/img/product_detail/arrow-right.svg"
        data-was-processed="true"
        alt={''}
      />
      <a className="cate-name" href="https://bibabo.vn/mua-sam/dha-cho-me">
        Tôn giáo
      </a>
    </div>
  );
};

Breadcrumb.propTypes = {};

Breadcrumb.defaultProps = {};

export default Breadcrumb;
