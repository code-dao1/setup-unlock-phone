import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import { getDateFromTime } from '@/utils/formatTime';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';

import classes from './style.module.scss';

const BlogItem = (props) => {
  const {
    data,
    className,
  } = props;

  const {
    content,
    created_at,
    handle,
    id,
    image_src,
    image_title,
    title,
  } = data || {};

  return (
    <div className={classList(className, classes.blogItem)}>
      <Link
        to={`/blog/${handle}`}
        title={title}
      >
        <Image
          className={classList('img-responsive', classes.image)}
          src={image_src}
          alt={title}
        />
      </Link>
      <br/>
      <h3>
        <Link
          to={`/blog/${handle}`}
          title={title}
          className={classList('text-black', classes.title)}
        >
          {title}
        </Link>
      </h3>
      <p className="text-grey text-uppercase" style={{ marginBottom: '15px' }}>
        <small>
          Guides • {getDateFromTime(created_at)}
        </small>
      </p>
    </div>
  );
};

BlogItem.propTypes = {};

BlogItem.defaultProps = {};

export default BlogItem;
