import React from 'react';
import { classList } from '@/utils/system/ui';

const Block = (props) => {
  const {
    className = '',
    classNameItem = '',
    items = [],
    ComponentItem,
  } = props;
  return (
    <div className="row">
      {items.map((item, index) => {
        return (
          <div className={classList(className, 'col-wrap')} key={`block-item-${index}`}>
            <ComponentItem
              index={index}
              item={item}
              className={classNameItem}
            />
          </div>
        );
      })}
    </div>
  );
};

export default Block;
