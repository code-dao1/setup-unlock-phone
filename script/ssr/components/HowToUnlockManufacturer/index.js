import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import React from 'react';
import PropTypes from 'prop-types';

const HowToUnlockManufacturer = (props) => {
  const {
    id,
    created_at,
    created_by,
    handle,
    image_src,
    image_title,
    name,
  } = props.manufacturer;

  return (
    <section style={{ padding: '80px 0 0', background: '#fff' }}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 text-center">
            <h2>
              How to unlock your phone of {name} <br/>
              <small className="text-black">with {publicRuntimeConfig.productBrand}</small>
            </h2>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <div className="row">
              <div className="col-sm-4 col-xs-12 text-center">
                <div className="row">
                  <div className="col-xs-12">
                    <div className="icon-lg">
                      <i className="fa fa-file-text" />
                    </div>
                  </div>
                </div>
                <h3>Get the Ball Rolling</h3>
                <br/>
                <p>Give us some basic details like your IMEI number so we can start processing your unlock.</p>
              </div>
              <div className="col-sm-4 col-xs-12 text-center">
                <div className="row">
                  <div className="col-xs-12">
                    <div className="icon-lg">
                      <i className="fa fa-code" />
                    </div>
                  </div>
                </div>
                <h3>We Process your Unlock</h3>
                <br/>
                <p>We whitelist your phone in database, ensuring a permanent and approved
                  unlock.
                </p>
              </div>
              <div className="col-sm-4 col-xs-12 text-center">
                <div className="row">
                  <div className="col-xs-12">
                    <div className="icon-lg">
                      <i className="fa fa-unlock" />
                    </div>
                  </div>
                </div>
                <h3>Your Phone is Unlocked</h3>
                <br/>
                <p>Confirmation will be sent to you via email and your unlock is delivered over the air using
                  3G/4G/WIFI.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

HowToUnlockManufacturer.propTypes = {};

HowToUnlockManufacturer.defaultProps = {};

export default HowToUnlockManufacturer;
