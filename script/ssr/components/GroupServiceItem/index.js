import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';

// import './style.scss';

const GroupServiceItem = (props) => {
  return (
    <Link
      href="#GB"
      title="Networks in UNITED KINGDOM"
      className="text-center group-service-item"
    >
      <img
        src="/Images/flags/united-kingdom.png"
        alt=""
        className="img-responsive center-block group-service-item-image"
      />
      <small>UK</small>
    </Link>
  );
};

GroupServiceItem.propTypes = {};

GroupServiceItem.defaultProps = {};

export default GroupServiceItem;
