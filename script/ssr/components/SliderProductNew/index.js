import React from 'react';
import PropTypes from 'prop-types';
import ProductBonus from '../ProductBonus';
import ProductItem from '../ProductItem';

const SliderProductNew = (props) => {
  return (
    <div className="shop-content__newProduct">
      <header>
        <div className="left">
          <span>SÁCH MỚI CẬP NHẬT</span>
        </div>
        <div className="right">
          <span>Xem tất cả</span>
          <img alt="" src="https://bibabo.vn/assets/shop/img/ic_see_all.svg"/>
        </div>
      </header>
      <section
        className="shop-content__newProduct-main swiper-container swiper-container-horizontal"
        id="js-swiper-product-newProduct-shop"
      >
        <div className="swiper-wrapper">
          {
            [...Array(7)].map((_, index) => {
              return (
                <div
                  key={`product-bonus-item-${index}`}
                  className="swiper-slide swiper-slide-active"
                  style={{ width: '236.8px' }}
                >
                  <ProductItem className={'slide-pitem'} isShowRating={false}/>
                </div>
              );
            })
          }
        </div>
        <div className="swiper-button-next swiper-slide-button-next" />
        <div className="swiper-button-prev swiper-slide-button-prev swiper-button-disabled" />
      </section>
    </div>
  );
};

SliderProductNew.propTypes = {};

SliderProductNew.defaultProps = {};

export default SliderProductNew;
