import React from 'react';
import PropTypes from 'prop-types';

// // import './style.scss';
import Link from 'next/link';
import { classList } from '@/utils/system/ui';

const Product = (props) => {
  const {
    isShowViewDetail,
    className
  } = props;
  return (
    <Link
      href={`/product/muon-kiep-nhan-sinh`}
    >
      <a className={classList('top20-shop__content-item', className)}>
        <div className="left">
          <img alt="" src="https://cdn0.fahasa.com/media/personalization/images/28/287377.jpg"/>
          <div className="shop-discount">
            -4%
          </div>
        </div>
        <div className="right">
          <p className="name">
            Muôn Kiếp Nhân Sinh - Many Times, Many Lives
          </p>
          <div className="price">
          <span className="origin">
            114.240đ
          </span>
            <span className="maket">168.000đ</span>
            <div className="vote">
            <span className="larger_star" score="5">
              <i className="fa fa-star star_checked" />
              <i className="fa fa-star star_checked" />
              <i className="fa fa-star star_checked" />
              <i className="fa fa-star star_checked" />
              <i className="fa fa-star star_checked" />
            </span>
              <span className="vote-count">(797)</span>
            </div>
          </div>
        </div>
        <div className="clearfix" />
        {isShowViewDetail && <button>Xem chi tiết</button>}
        <div className="rank">
          <p className="rank-number">1</p>
        </div>
      </a>
    </Link>
  );
};

Product.propTypes = {};

Product.defaultProps = {};

export default Product;
