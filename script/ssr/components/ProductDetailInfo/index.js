import React from 'react';
import PropTypes from 'prop-types';

// import './style.scss';
import Policy from './Policy';
import ProductDetailInfoBasic from './ProductDetailInfoBasic';
import ProductImages from './ProductImages';

const ProductDetailInfo = (props) => {
  return (
    <div className="product-detail__info">
      <ProductImages />
      <ProductDetailInfoBasic />
      <div className="product-detail__info-right">
        <div id="arrilot-widget-container-3" data-7aymmq0hozodb2eb8gsd="" />
        <Policy />
      </div>
    </div>
  );
};

ProductDetailInfo.propTypes = {};

ProductDetailInfo.defaultProps = {};

export default ProductDetailInfo;
