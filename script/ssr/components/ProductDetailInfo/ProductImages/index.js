import React from 'react';
import PropTypes from 'prop-types';

const ProductImages = (props) => {
  return (
    <div className="product-detail__info-image">
      <div className="swiper-container gallery-top swiper-container-initialized swiper-container-horizontal">
        <div className="swiper-wrapper" style={{ transform: 'translate3d(0px, 0px, 0px)' }}>
          <a
            className="swiper-slide swiper-slide-active"
            data-fancybox="image-product-list"
            data-type="iframe"
            href="https://www.youtube.com/embed/C3WkjF-SpU?autoplay=1&amp;rel=0"
            style={{ width: '450px' }}
          >
            <img
              className="lazy image loaded"
              alt="Viên uống bổ sung DHA cho bà bầu nội địa Úc Bio Island 60 viên"
              data-src="https://cdn.bibabo.vn/uploads/bo/vi/pr/2/2f/2fkbnejl78hyc2aaaaa-1500x_-resize.jpeg"
              src="https://cdn0.fahasa.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/u/muonkiepnhansinh.jpg"
              data-was-processed="true"
            />
          </a>
          <a
            href="https://cdn.bibabo.vn/uploads/bo/vi/pr/b/bg/bg5s02bb50a7030aaaa-1500x_-resize.jpeg"
            data-fancybox="image-product-list"
            className="swiper-slide swiper-slide-next"
            data-caption=""
            style={{ width: '450px' }}
          >
            <img
              className="lazy loaded"
              alt="Viên uống bổ sung DHA cho bà bầu nội địa Úc Bio Island 60 viên"
              data-src="https://cdn.bibabo.vn/uploads/bo/vi/pr/b/bg/bg5s02bb50a7030aaaa-1500x_-resize.jpeg"
              src="https://cdn0.fahasa.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/k/mkns_den.jpg"
              data-was-processed="true"
            />
          </a>
          <a
            href="https://cdn.bibabo.vn/uploads/bo/vi/pr/7/7q/7q2hvg03f03dgl9aaaa-1500x_-resize.jpeg"
            data-fancybox="image-product-list"
            className="swiper-slide"
            data-caption=""
            style={{ width: '450px' }}
          >
            <img
              className="lazy"
              alt="Viên uống bổ sung DHA cho bà bầu nội địa Úc Bio Island 60 viên"
              data-src="https://cdn0.fahasa.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/3/d/3d_muon-kiep-nhan-sinh.jpg"
            />
          </a>
          <a
            href="https://cdn.bibabo.vn/uploads/bo/vi/pr/7/7q/7q2hvg03f03dgkvaaaa-1500x_-resize.jpeg"
            data-fancybox="image-product-list"
            className="swiper-slide"
            data-caption=""
            style={{ width: '450px' }}
          >
            <img
              className="lazy"
              alt="Viên uống bổ sung DHA cho bà bầu nội địa Úc Bio Island 60 viên"
              data-src="https://cdn0.fahasa.com/media/flashmagazine/images/page_images/muon_kiep_nhan_sinh___many_times__many_lives/2020_05_14_15_05_28_5.png"
            />
          </a>
          <div
            id="js-slide-user-satisfaction"
            style={{ display: 'none', width: '450px', height: '450px', margin: 0, padding: '0 60px' }}
          >
            <div style={{ display: 'flex', alignItems: 'center', width: '100%', height: '100%' }}>
              <div className="user-satisfaction-widget js-user-satisfaction-widget">
                <img
                  alt=""
                  className="user-satisfaction-widget__triangle lazy"
                  data-src="/assets/wap/images/icons/widgets/user-satisfaction/triangle.svg"
                  width="50"
                />
                <div
                  className="user-satisfaction-widget__container js-user-satisfaction-container"
                  data-user-satisfaction-user-id="5"
                  data-user-satisfaction-user-full-name=""
                  data-user-satisfaction-received-id="999"
                  data-user-satisfaction-received-name="Bibabo"
                  data-user-satisfaction-object="40"
                  data-user-satisfaction-object-id="100115866"
                >
                  <div className="user-satisfaction-widget__text">
                    Bạn có hài lòng với bộ hình sản phẩm trên không?
                  </div>
                  <div
                    className="user-satisfaction-widget__tick -satisfied js-user-satisfaction-tick"
                    data-user-satisfaction-satisfied="1"
                    data-placeholder="Điều gì khiến bạn hài lòng ? (không bắt buộc)"
                  >
                    <img
                      alt=""
                      className="user-satisfaction-widget__icon lazy"
                      data-src="/assets/wap/images/icons/widgets/user-satisfaction/satisfied-v2.svg"
                    />
                    <span>Hài lòng</span>
                    <div className="user-satisfaction-widget__arrow-box hidden js-user-satisfaction-arrow-box" />
                  </div>
                  <div
                    className="user-satisfaction-widget__tick -unsatisfied js-user-satisfaction-tick"
                    data-user-satisfaction-satisfied="0"
                    data-placeholder="Điều gì khiến bạn không hài lòng ? (không bắt buộc)"
                  >
                    <img
                      alt=""
                      className="user-satisfaction-widget__icon lazy"
                      data-src="/assets/wap/images/icons/widgets/user-satisfaction/unsatisfied-v2.svg"
                    />
                    <span>Không hài lòng</span>
                    <div className="user-satisfaction-widget__arrow-box hidden js-user-satisfaction-arrow-box" />
                  </div>
                </div>
                <div className="user-satisfaction-widget__form hidden js-user-satisfaction-form">
                    <textarea
                      className="user-satisfaction-widget__textarea js-feedback-content"
                      spellCheck="false"
                    />
                  <button className="user-satisfaction-widget__btn js-user-satisfaction-btn-send">Gửi góp ý</button>
                </div>
                <div className="user-satisfaction-widget__message hidden js-user-satisfaction-message">
                  Cảm ơn bạn đã đánh giá!
                  <br/>
                  <button
                    className="user-satisfaction-widget__btn -btn-close"
                    onClick="$(this).closest('.js-user-satisfaction-widget').remove();"
                  >Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="swiper-button-next swiper-button-white"
          tabIndex="0"
          role="button"
          aria-label="Next slide"
          aria-disabled="false"
        />
        <div
          className="swiper-button-prev swiper-button-white swiper-button-disabled"
          tabIndex="0"
          role="button"
          aria-label="Previous slide"
          aria-disabled="true"
        />
        <span className="swiper-notification" aria-live="assertive" aria-atomic="true" />
      </div>
      <div
        className="swiper-container gallery-thumbs swiper-container-initialized swiper-container-horizontal swiper-container-free-mode swiper-container-thumbs"
      >
        <div className="swiper-wrapper" style={{ transform: 'translate3d(0px, 0px, 0px)' }}>
          <a
            className="swiper-slide swiper-slide-visible swiper-slide-active swiper-slide-thumb-active"
            style={{ width: '75px' }}
          >
            <img
              className="lazy image loaded"
              alt="Viên uống bổ sung DHA cho bà bầu nội địa Úc Bio Island 60 viên"
              data-src="https://cdn.bibabo.vn/uploads/bo/vi/pr/2/2f/2fkbnejl78hyc2aaaaa-1500x_-resize.jpeg"
              src="https://cdn0.fahasa.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/u/muonkiepnhansinh.jpg"
              data-was-processed="true"
            />
          </a>
          <img
            alt=""
            className="swiper-slide lazy swiper-slide-visible loaded swiper-slide-next"
            data-src="https://cdn.bibabo.vn/uploads/bo/vi/pr/b/bg/bg5s02bb50a7030aaaa-70x_-resize.jpeg"
            src="https://cdn0.fahasa.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/k/mkns_den.jpg"
            data-was-processed="true"
            style={{ width: '75px' }}
          />
          <img
            alt=""
            className="swiper-slide lazy swiper-slide-visible loaded"
            data-src="https://cdn.bibabo.vn/uploads/bo/vi/pr/7/7q/7q2hvg03f03dgl9aaaa-70x_-resize.jpeg"
            src="https://cdn0.fahasa.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/3/d/3d_muon-kiep-nhan-sinh.jpg"
            data-was-processed="true"
            style={{ width: '75px' }}
          />
          <img
            alt=""
            className="swiper-slide lazy swiper-slide-visible loaded"
            data-src="https://cdn.bibabo.vn/uploads/bo/vi/pr/7/7q/7q2hvg03f03dgkvaaaa-70x_-resize.jpeg"
            src="https://cdn0.fahasa.com/media/flashmagazine/images/page_images/muon_kiep_nhan_sinh___many_times__many_lives/2020_05_14_15_05_28_5.png"
            data-was-processed="true"
            style={{ width: '75px' }}
          />
        </div>
        <span className="swiper-notification" aria-live="assertive" aria-atomic="true" />
      </div>
    </div>
  );
};

ProductImages.propTypes = {};

ProductImages.defaultProps = {};

export default ProductImages;
