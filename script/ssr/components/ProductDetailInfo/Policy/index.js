import React from 'react';
import PropTypes from 'prop-types';

const Policy = (props) => {
  return (
    <div className="product-detail__info-policy">
      <p className="slogan">AN TÂM MUA SẮM TẠI BIBABO</p>
      <div className="policy-list">
        <div className="policy-item">
          <img
            alt=""
            className="icon-image lazy loaded"
            data-src="https://bibabo.vn/assets/web/img/product_detail/policy-icon-1.svg"
            src="https://bibabo.vn/assets/web/img/product_detail/policy-icon-1.svg"
            data-was-processed="true"
          />
          <span>Rõ nguồn gốc, xuất xứ</span>
        </div>
        <div className="policy-item">
          <img
            alt=""
            className="icon-image lazy loaded"
            data-src="https://bibabo.vn/assets/web/img/product_detail/policy-icon-2.svg"
            src="https://bibabo.vn/assets/web/img/product_detail/policy-icon-2.svg"
            data-was-processed="true"
          />
          <span>Hoàn tiền gấp 10 lần nếu phát hiện hàng giả.</span>
        </div>
        <div className="policy-item">
          <img
            alt=""
            className="icon-image lazy loaded"
            data-src="https://bibabo.vn/assets/web/img/product_detail/policy-icon-3.svg"
            src="https://bibabo.vn/assets/web/img/product_detail/policy-icon-3.svg"
            data-was-processed="true"
          />
          <span>Đổi trả trong 7 ngày.</span>
        </div>
      </div>
      <div className="policy-shipping">
        <p><b>Miễn phí giao</b> cho đơn hàng:</p>
        <p>
          - Trên <b>500.000đ</b> khi giao nội thành Hà Nội, TP. Hồ Chí Minh
          <img
            alt=""
            src="https://bibabo.vn/assets/web/img/product_detail/note.svg"
            data-toggle="modal"
            data-target="#modal-urban-region"
          />
        </p>
        <p>
          - Trên <b>1.000.000đ</b> khi giao ngoại thành, tỉnh
          <img
            alt=""
            src="https://bibabo.vn/assets/web/img/product_detail/note.svg"
            data-toggle="modal"
            data-target="#modal-suburban-region"
          />
        </p>
      </div>
    </div>
  );
};

Policy.propTypes = {};

Policy.defaultProps = {};

export default Policy;
