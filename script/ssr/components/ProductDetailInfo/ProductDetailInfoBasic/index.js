import React from 'react';
import PropTypes from 'prop-types';
import HTMLContent from '../../common/HTMLContent';

const shortDescription = '“Muôn kiếp nhân sinh” là tác phẩm do Giáo sư John Vũ - Nguyên Phong viết từ năm 2017 và hoàn tất đầu năm 2020 ghi lại những câu chuyện, trải nghiệm tiền kiếp kỳ lạ từ nhiều kiếp sống của người bạn tâm giao lâu năm, ông Thomas – một nhà kinh doanh tài chính nổi tiếng...'

const ProductDetailInfoBasic = (props) => {
  return (
    <div className="product-detail__info-basic">
      <h1 className="name">
        Muôn Kiếp Nhân Sinh - Many Times, Many Lives
      </h1>
      <div className="wrapper-extra">
        <p className="brand">
          <span className="brand-title">Nhà cung cấp:</span>
          <span className="brand-name">FIRST NEWS</span>
        </p>
        <p className="brand">
          <span className="brand-title">Nhà xuất bản:</span>
          <span className="brand-name">NXB Tổng Hợp TPHCM</span>
        </p>
        <p className="brand">
          <span className="brand-title">Tác giả:</span>
          <span className="brand-name">Nguyên Phong</span>
        </p>
        <p className="brand">
          <span className="brand-title">Hình thức bìa:</span>
          <span className="brand-name">Bìa Mềm</span>
        </p>
        <div className="rate">
          <div className="number-rate rate-item" onClick="Bm.goTopDiv('review')">
              <span className="larger_star" score="5" style={{ fontSize: '14px' }}>
                <i className="fa fa-star star_checked" />
                <i className="fa fa-star star_checked" />
                <i className="fa fa-star star_checked" />
                <i className="fa fa-star star_checked" />
                <i className="fa fa-star star_checked" />
              </span>
            <span><b>4.8</b></span>
            <span>798 đánh giá</span>
          </div>
          <div className="number-qa rate-item" onClick="Bm.goTopDiv('qa')">
            1.808 hỏi đáp
          </div>

          <div className="number-buy rate-item">
            25.228 bạn đọc đã mua
          </div>
        </div>
      </div>

      <div className="price">
        <p className="price-sale">
          555.000đ
        </p>
        <p className="price-market">
          580.000 đ
        </p>
        <p className="percent-discount">
          -4%
        </p>
      </div>
      <p className="status active" />
      <div style={{ margin: '16px 0' }}>
        <HTMLContent content={shortDescription}/>
      </div>

      {/*<ul className="attribute">*/}
      {/*  <li className="attribute-item"><p>Sản xuất tại Úc</p></li>*/}
      {/*  <li className="attribute-item"><p>Giúp bổ sung đầy đủ lượng DHA cần thiết cho con ngay từ khi trong bụng*/}
      {/*    mẹ*/}
      {/*  </p>*/}
      {/*  </li>*/}
      {/*  <li className="attribute-item"><p>Sử dụng được cho mẹ chuẩn bị mang thai, đang mang thai và sau sinh</p></li>*/}
      {/*</ul>*/}

      <div className="shipping-info">
        <img
          alt=""
          className="lazy loaded"
          data-src="https://bibabo.vn/assets/web/img/product_detail/location.svg"
          src="https://bibabo.vn/assets/web/img/product_detail/location.svg"
          data-was-processed="true"
        />
        <div className="text">
          <div className="wrapper-address" href="#">
            <div className="address">
              <span>Giao hàng tới:</span>
              <div className="select-address" onClick="productDetail.chooseAddressShip()">
                <span>Chọn địa chỉ nhận hàng</span>
                <img alt="" src="https://bibabo.vn/assets/web/img/product_detail/arrow-down.svg"/>
              </div>
            </div>
            <div className="dropdown-address" style={{ display: 'none' }}>
              <select
                className="form-control chosen-select"
                id="geo_province_id"
                name="geo_province_id"
                data-placeholder="Chọn tỉnh, thành"
                onChange="productDetail.onChangeProvince(this)"
                style={{ display: 'none' }}
              >
                <option value="101">
                  Hà Nội
                </option>
                <option value="179">
                  Hồ Chí Minh
                </option>
                <option value="174">
                  Bình Dương
                </option>
                <option value="175">
                  Đồng Nai
                </option>
                <option value="172">
                  Tây Ninh
                </option>
                <option value="170">
                  Bình Phước
                </option>
                <option value="168">
                  Lâm Đồng
                </option>
                <option value="167">
                  Đắk Nông
                </option>
                <option value="166">
                  Đắk Lắk
                </option>
                <option value="164">
                  Gia Lai
                </option>
                <option value="162">
                  Kon Tum
                </option>
                <option value="160">
                  Bình Thuận
                </option>
                <option value="158">
                  Ninh Thuận
                </option>
                <option value="156">
                  Khánh Hòa
                </option>
                <option value="154">
                  Phú Yên
                </option>
                <option value="152">
                  Bình Định
                </option>
                <option value="151">
                  Quảng Ngãi
                </option>
                <option value="148">
                  Đà Nẵng
                </option>
                <option value="177">
                  Bà Rịa - Vũng Tàu
                </option>
                <option value="180">
                  Long An
                </option>
                <option value="182">
                  Tiền Giang
                </option>
                <option value="183">
                  Bến Tre
                </option>
                <option value="184">
                  Trà Vinh
                </option>
                <option value="186">
                  Vĩnh Long
                </option>
                <option value="187">
                  Đồng Tháp
                </option>
                <option value="189">
                  An Giang
                </option>
                <option value="191">
                  Kiên Giang
                </option>
                <option value="192">
                  Cần Thơ
                </option>
                <option value="193">
                  Hậu Giang
                </option>
                <option value="194">
                  Sóc Trăng
                </option>
                <option value="195">
                  Bạc Liêu
                </option>
                <option value="196">
                  Cà Mau
                </option>
                <option value="126">
                  Vĩnh Phúc
                </option>
                <option value="102">
                  Hà Giang
                </option>
                <option value="104">
                  Cao Bằng
                </option>
                <option value="106">
                  Bắc Kạn
                </option>
                <option value="108">
                  Tuyên Quang
                </option>
                <option value="110">
                  Lào Cai
                </option>
                <option value="111">
                  Điện Biên
                </option>
                <option value="112">
                  Lai Châu
                </option>
                <option value="114">
                  Sơn La
                </option>
                <option value="115">
                  Yên Bái
                </option>
                <option value="117">
                  Hòa Bình
                </option>
                <option value="119">
                  Thái Nguyên
                </option>
                <option value="120">
                  Lạng Sơn
                </option>
                <option value="122">
                  Quảng Ninh
                </option>
                <option value="124">
                  Bắc Giang
                </option>
                <option value="125">
                  Phú Thọ
                </option>
                <option value="149">
                  Quảng Nam
                </option>
                <option value="127">
                  Bắc Ninh
                </option>
                <option value="130">
                  Hải Dương
                </option>
                <option value="131">
                  Hải Phòng
                </option>
                <option value="133">
                  Hưng Yên
                </option>
                <option value="134">
                  Thái Bình
                </option>
                <option value="135">
                  Hà Nam
                </option>
                <option value="136">
                  Nam Định
                </option>
                <option value="137">
                  Ninh Bình
                </option>
                <option value="138">
                  Thanh Hóa
                </option>
                <option value="140">
                  Nghệ An
                </option>
                <option value="142">
                  Hà Tĩnh
                </option>
                <option value="144">
                  Quảng Bình
                </option>
                <option value="145">
                  Quảng Trị
                </option>
                <option value="146">
                  Thừa Thiên Huế
                </option>
              </select>
              <div
                className="chosen-container chosen-container-single"
                style={{ width: '276px' }}
                title=""
                id="geo_province_id_chosen"
              >
                <a className="chosen-single" tabIndex="-1">
                  <span>Hà Nội</span>
                  <div><b /></div>
                </a>
                <div className="chosen-drop">
                  <div className="chosen-search"><input type="text" autoComplete="off"/></div>
                  <ul className="chosen-results" />
                </div>
              </div>
              <div id="wrapper-district">
                <select
                  className="form-control chosen-select"
                  id="geo_district_id"
                  name="geo_district_id"
                  data-placeholder="Chọn quận, huyện"
                  style={{ display: 'none' }}
                >
                  <option value="1001">
                    Ba Đình
                  </option>
                  <option value="1271">
                    Ba Vì
                  </option>
                  <option value="1019">
                    Bắc Từ Liêm
                  </option>
                  <option value="1005">
                    Cầu Giấy
                  </option>
                  <option value="1277">
                    Chương Mỹ
                  </option>
                  <option value="1273">
                    Đan Phượng
                  </option>
                  <option value="1017">
                    Đông Anh
                  </option>
                  <option value="1006">
                    Đống Đa
                  </option>
                  <option value="1018">
                    Gia Lâm
                  </option>
                  <option value="1268">
                    Hà Đông
                  </option>
                  <option value="1007">
                    Hai Bà Trưng
                  </option>
                  <option value="1274">
                    Hoài Đức
                  </option>
                  <option value="1002">
                    Hoàn Kiếm
                  </option>
                  <option value="1008">
                    Hoàng Mai
                  </option>
                  <option value="1004">
                    Long Biên
                  </option>
                  <option value="1250">
                    Mê Linh
                  </option>
                  <option value="1282">
                    Mỹ Đức
                  </option>
                  <option value="1974">
                    Nam Từ Liêm
                  </option>
                  <option value="1280">
                    Phú Xuyên
                  </option>
                  <option value="1272">
                    Phúc Thọ
                  </option>
                  <option value="1275">
                    Quốc Oai
                  </option>
                  <option value="1016">
                    Sóc Sơn
                  </option>
                  <option value="1269">
                    Sơn Tây
                  </option>
                  <option value="1003">
                    Tây Hồ
                  </option>
                  <option value="1276">
                    Thạch Thất
                  </option>
                  <option value="1278">
                    Thanh Oai
                  </option>
                  <option value="1020">
                    Thanh Trì
                  </option>
                  <option value="1009">
                    Thanh Xuân
                  </option>
                  <option value="1279">
                    Thường Tín
                  </option>
                  <option value="1281">
                    Ứng Hòa
                  </option>
                </select>
                <div
                  className="chosen-container chosen-container-single"
                  style={{ width: '275px' }}
                  title=""
                  id="geo_district_id_chosen"
                >
                  <a className="chosen-single" tabIndex="-1">
                    <span>Ba Đình</span>
                    <div><b /></div>
                  </a>
                  <div className="chosen-drop">
                    <div className="chosen-search"><input type="text" autoComplete="off"/></div>
                    <ul className="chosen-results" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <p className="date-fee">
            Chọn địa chỉ để biết thời gian giao
          </p>
        </div>
      </div>

      <div className="buy-now">
        <div className="main-box">
          <div
            className="btn-buy-now active"
            data-product-id="100115866"
            data-product-name="Viên uống bổ sung DHA cho bà bầu nội địa Úc Bio Island 60 viên"
            data-product-category="Sắt, Canxi, DHA"
            data-product-price="555000"
            onClick="productDetail.buyNow('https://bibabo.vn/ec/cart/buy/100115866', this)"
          >
            <p>Mua ngay</p>
            <p>Xem hàng trước khi thanh toán</p>
          </div>
        </div>
        <div className="hotline">
          <span>Gọi đặt mua miễn phí</span>
          <a href="tel:18006290"><img alt="" src="https://bibabo.vn/assets/web/img/product_detail/phone.svg"/> 1800.6290</a>
          <span>(8:00 - 21:30)</span>
        </div>
      </div>
    </div>
  );
};

ProductDetailInfoBasic.propTypes = {};

ProductDetailInfoBasic.defaultProps = {};

export default ProductDetailInfoBasic;
