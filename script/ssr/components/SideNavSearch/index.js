import React from 'react';
import PropTypes from 'prop-types';

const SideNavSearch = (props) => {
  return (
    <div id="nav_h">
      <div id="facets">
        <h5>Nhóm sản sản phẩm</h5>
        <ul>
          <li style={{ fontWeight: 'normal' }}><a
            className="hrc"
            data-attribute="categories"
            data-value="Dành cho mẹ"
            href="#"
          >Sách ngoại ngữ (59)
          </a>
          </li>
          <li style={{ fontWeight: 'normal' }}><a
            className="hrc"
            data-attribute="categories"
            data-value="Dành cho bé"
            href="#"
          >Sách Tiếng Việt (47)
          </a>
          </li>
        </ul>
        <div className="facet">
          <h5>Thương hiệu</h5>
          <ul>
            <li>
              <a
                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="A-Derma"
              > A-Derma<span className="facet-count">6</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="Chicco"
              > Chicco<span className="facet-count">2</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="Doppelherz"
              > Doppelherz<span className="facet-count">2</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="Farlin"
              > Farlin<span className="facet-count">3</span>
              </a>
            </li>
            <li>
              <a
                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="Guo"
              > Guo<span className="facet-count">2</span>
              </a>
            </li>
            <li>
              <a
                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="KUKU"
              > KUKU<span className="facet-count">4</span>
              </a>
            </li>
            <li>
              <a
                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="Mộc An"
              > Mộc An<span className="facet-count">13</span>
              </a>
            </li>
            <li>
              <a
                className="facet-link toggle-refine facet-disjunctive"
                data-facet="brand"
                data-value="Upass"
              > Upass<span className="facet-count">4</span>
              </a>
            </li>
          </ul>
        </div>

        <div className="facet">
          <h5>Price</h5>
          <span className="irs js-irs-0 irs-with-grid">
            <span className="irs">
              <span className="irs-line" tabIndex="-1"><span className="irs-line-left" /><span
                className="irs-line-mid"
              /><span className="irs-line-right" />
              </span><span
                className="irs-min"
                style={{ visibility: 'hidden' }}
              >41,000 đ
                     </span>
              <span className="irs-max" style={{ visibility: 'hidden' }}>6,000,000 đ</span><span
                className="irs-from"
                style={{ visibility: 'visible', left: '-12.5%' }}
              >41,000 đ
              </span>
              <span className="irs-to" style={{ visibility: 'visible', left: '70.0758%' }}>6,000,000 đ</span><span
                className="irs-single"
                style={{ visibility: 'hidden', left: '7.95455%' }}
              >41,000 đ — 6,000,000 đ
                                                                                                             </span>
            </span>
            <span className="irs-grid" style={{ width: '87.8788%', left: '5.96061%' }}>
              <span className="irs-grid-pol" style={{ left: '0%' }} />
              <span
                className="irs-grid-text js-grid-text-0"
                style={{ left: '0%', marginLeft: '-15.5303%' }}
              >
                41,000 đ
              </span>
              <span
                className="irs-grid-pol small"
                style={{ left: '20%' }}
              />
              <span className="irs-grid-pol small" style={{ left: '15%' }} /><span
                className="irs-grid-pol small"
                style={{ left: '10%' }}
              /><span
                className="irs-grid-pol small"
                style={{ left: '5%' }}
              />
              <span className="irs-grid-pol" style={{ left: '25%' }} /><span
                className="irs-grid-text js-grid-text-1"
                style={{ left: '25%', visibility: 'hidden', marginLeft: '-20.4545%' }}
              >1,530,750 đ
              </span>
              <span className="irs-grid-pol small" style={{ left: '45%' }} /><span
                className="irs-grid-pol small"
                style={{ left: '40%' }}
              /><span
                className="irs-grid-pol small"
                style={{ left: '35%' }}
              />
              <span className="irs-grid-pol small" style={{ left: '30%' }} /><span
                className="irs-grid-pol"
                style={{ left: '50%' }}
              />
              <span
                className="irs-grid-text js-grid-text-2"
                style={{ left: '50%', visibility: 'visible', marginLeft: '-20.4545%' }}
              >3,020,500 đ
              </span><span
                className="irs-grid-pol small"
                style={{ left: '70%' }}
              />
              <span className="irs-grid-pol small" style={{ left: '65%' }} /><span
                className="irs-grid-pol small"
                style={{ left: '60%' }}
              /><span
                className="irs-grid-pol small"
                style={{ left: '55%' }}
              />
              <span className="irs-grid-pol" style={{ left: '75%' }} /><span
                className="irs-grid-text js-grid-text-3"
                style={{ left: '75%', visibility: 'hidden', marginLeft: '-20.4545%' }}
              >4,510,250 đ
              </span>
              <span className="irs-grid-pol small" style={{ left: '95%' }} /><span
                className="irs-grid-pol small"
                style={{ left: '90%' }}
              /><span
                className="irs-grid-pol small"
                style={{ left: '85%' }}
              />
              <span className="irs-grid-pol small" style={{ left: '80%' }} /><span
                className="irs-grid-pol"
                style={{ left: '100%' }}
              /><span
                className="irs-grid-text js-grid-text-4"
                style={{ left: '100%', marginLeft: '-20.4545%' }}
              >6,000,000 đ
              </span>
            </span>
            <span className="irs-bar" style={{ left: '6.06061%', width: '87.8788%' }} /><span
              className="irs-shadow shadow-from"
              style={{ display: 'none' }}
            /><span
              className="irs-shadow shadow-to"
              style={{ display: 'none' }}
            />
            <span className="irs-slider from" style={{ left: '0%' }} /><span
              className="irs-slider to"
              style={{ left: '87.8788%' }}
            />
          </span>
          <input
            type="text"
            id="price-slider"
            data-min="41000"
            data-max="6000000"
            data-from="41000"
            data-to="6000000"
            className="irs-hidden-input"
            readOnly=""
          />
        </div>

        <div className="facet">
          <h5>Khoảng giá</h5>
          <ul>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="price_range"
                data-value="0-500000"
              > 0-500000<span className="facet-count">105</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="price_range"
                data-value="500000-1000000"
              > 500000-1000000<span
                className="facet-count"
              >8
              </span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="price_range"
                data-value="1000000-1500000"
              > 1000000-1500000<span
                className="facet-count"
              >3
              </span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="price_range"
                data-value=">2000000"
              > &gt;2000000<span className="facet-count">2</span>
              </a>
            </li>
          </ul>
        </div>

        <div className="facet">
          <h5>Hình thức</h5>
          <ul>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="kich_co"
                data-value="1 tháng"
              >Bìa cứng<span className="facet-count">1</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="kich_co"
                data-value="3 tháng"
              >Bìa mềm<span className="facet-count">1</span>
              </a>
            </li>
          </ul>
        </div>

        <div className="facet">
          <h5>Nhà cung cấp</h5>
          <ul>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="mau_sac"
                data-value="Hồng"
              >Nhà xuất bản Kim Đồng<span className="facet-count">8</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="mau_sac"
                data-value="Mèo mây hồng"
              >Minh Long<span className="facet-count">3</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="mau_sac"
                data-value="Mèo mây xanh"
              >MC Book<span className="facet-count">3</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="mau_sac"
                data-value="Sọc hồng"
              >PHANBOOK<span className="facet-count">3</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="mau_sac"
                data-value="Sọc xanh"
              >First News<span className="facet-count">3</span>
              </a>
            </li>
          </ul>
        </div>

        <div className="facet">
          <h5>Ngôn ngữ</h5>
          <ul>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="kieu_dang"
                data-value="Da dầu"
              >Tiếng Việt<span className="facet-count">1</span>
              </a>
            </li>
            <li>
              <a

                className="facet-link toggle-refine facet-disjunctive"
                data-facet="kieu_dang"
                data-value="Da khô"
              >Tiếng Anh<span className="facet-count">1</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

SideNavSearch.propTypes = {};

SideNavSearch.defaultProps = {};

export default SideNavSearch;
