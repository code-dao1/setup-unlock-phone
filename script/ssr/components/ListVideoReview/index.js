import ReviewYoutubeItem from '@/components/ReviewYoutubeItem';
import reviewApi from '@/services/reviewApi';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { PAGE, PAGE_SIZE } from '@/constants/system/service';
import { classList } from '@/utils/system/ui';
import { isErrorResponse } from '@/utils/typeof';

import classes from './style.module.scss';

const ListVideoReview = (props) => {
  const dataFromProps = props.data || {};
  const [reviews, setReviews] = useState(dataFromProps.items || [...Array(6)]);
  const [total, setTotal] = useState(dataFromProps.total || 0);
  useEffect(() => {
    reviewApi.getListYoutube({
      [PAGE]: 1,
      [PAGE_SIZE]: 4,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setReviews(items);
        setTotal(total);
      } else {
        setReviews([]);
        setTotal(0);
      }
    });
  }, []);

  return (
    <section className={classes.listVideoReview}>
      <div className="container">
        <div className="row">
          <div className={classList("col-xs-12 text-center", classes.title)}>
            <h2>What do our customers say?</h2>
            <br />
          </div>
          <div className="col-md-12 col-xs-12">
            <div className="row">
              <div className="col-md-12 col-xs-12">
                <div className={classList('row', classes.listService)}>
                  {
                    reviews.map((item, index, array) => {
                      const isLast = index === array.length - 1;
                      return <ReviewYoutubeItem
                        key={`service-item-${index}`}
                        isLast={isLast}
                        numItemNoShow={total - array.length}
                        className="col-md-3 col-sm-6 col-xs-12"
                        data={item}
                      />
                    })
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

ListVideoReview.propTypes = {};

ListVideoReview.defaultProps = {};

export default ListVideoReview;
