import React from 'react';
import PropTypes from 'prop-types';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const ListBlogRight = (props) => {
  const {
    className,
    keyword,
    changeKeyword,
    onSearch
  } = props;
  return (
    <div className={classList("col-md-3 col-md-offset-1", classes.listBlogRight, className)}>
      <h3>Search Blog</h3>
      <hr/>
      <div>
        <div className="col-xs-9 no-pad">
          <input id="country" name="country" type="hidden" value="{ Value =  }"/>
          <input
            className={classList("form-control", classes.inputSearch)}
            id="SearchTerm"
            name="SearchTerm"
            placeholder="Enter a keyword..."
            type="text"
            value={keyword}
            onChange={changeKeyword}
          />
        </div>
        <div className="col-xs-3 no-pad">
          <button
            className={classList("btn btn-primary btn-sm", classes.buttonSearch)}
            onClick={onSearch}
          >
            Search
          </button>
        </div>
      </div>
      <br/>
      <br/>
      <br/>
      <br/>
      <div
        className={classList("text-center text-white", classes.suggestBox)}
      >
        <h2>Unbeatable Prices</h2>
        <br/>
        <br/>
        <p className="lead light">Unlock your phone from any carrier by IMEI</p>
        <br/>
        <br/>
        <a href="/" className="btn btn-primary">Unlock Now <i className="fa fa-chevron-right" /></a>
      </div>
    </div>
  );
};

ListBlogRight.propTypes = {
  onSearch: PropTypes.func,
  className: PropTypes.string,
  keyword: PropTypes.string,
  changeKeyword: PropTypes.func,
};

ListBlogRight.defaultProps = {
  onSearch: () => null,
  changeKeyword: () => null,
};

export default ListBlogRight;
