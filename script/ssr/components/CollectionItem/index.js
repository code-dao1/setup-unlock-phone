import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import Collection from '../../entities/Collection';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';
import LoaderBalls from '../common/LoaderBalls';

import classes from './style.module.scss';

const CollectionItem = (props) => {
  const {
    index,
    item,
    className = '',
    hoverImage
  } = props;

  if (!item) {
    return (
      <div className={classList(classes.collectionProductWrap, classes.loading, 'text-align-center')}>
        <div
          className=":hover-no-underline d-block"
        >
          <div className={classList(classes.collectionImageContainer, 'image-wrap mb16')}>
            <LoaderBalls/>
          </div>
          <div className={classList(classes.title, 'is-uppercase display-8 has-text-weight-medium mb0')} />
        </div>
      </div>
    );
  }

  return (<div className={classList(classes.collectionProductWrap, 'text-align-center')}>
    <Link
      to={`/collections/${item.path}`}
      className={classList(hoverImage && classes.hoverImage, ':hover-no-underline d-block')}
    >
      <div className={classList(classes.collectionImageContainer, 'image-wrap mb16')}>
        <Image
          src={item.image}
          loadInWindow
        />
      </div>
      <h5 className={classList(classes.title, 'is-uppercase display-8 has-text-weight-medium mb0')}>
        {item.name}
      </h5>
    </Link>
  </div>);
};

CollectionItem.propTypes = {
  index: PropTypes.number,
  item: Collection,
  className: PropTypes.string,
  hoverImage: PropTypes.bool
};

CollectionItem.defaultProps = {
  index: 0,
  item: null,
  className: '',
  hoverImage: false
};

export default CollectionItem;
