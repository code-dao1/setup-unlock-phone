import React from 'react';
import PropTypes from 'prop-types';

import classes from './style.module.scss';

const SupportService = (props) => {
  return (
    <section className={classes.supportService}>
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-xs-12 xs-text-center">
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-wifi" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>Remote Phone unlock service</h3>
                <br/>
                <p>We unlock Phones remotely, this means you can continue using your Phone as normal whilst we
                  complete the unlock in the background. We’ll never ask for personal login details.
                </p>
                <br/>
                <br/>
              </div>
            </div>
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-question-circle" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>
                  <a href="/frequestly-asked-questions">Frequently Asked Questions</a>
                </h3>
                <br/>
                <p>
                  For all general questions about our service and unlocking process please visit our&nbsp;
                  <a
                    href="/en-gb/support/frequestly-asked-questions"
                    style={{ display: 'inline', textDecoration: 'underline' }}
                  >frequently asked questions
                  </a> page.
                </p>
                <br/>
                <br/>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-xs-12 xs-text-center">
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-signal" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>
                  <a href="/tracking">Order Support</a>
                </h3>
                <br/>
                <p>
                  If you do not know your network then please use our <a
                  href="/tracking"
                  style={{ display: 'inline', textDecoration: 'underline' }}
                >network
                  check service
                </a> before proceeding with your
                  Phone Unlock.
                </p>
                <br/>
                <br/>
              </div>
            </div>
            <div className="row">
              <div className="col-md-2 col-sm-3 col-xs-12">
                <div className="icon-lg text-center">
                  <i className="fa fa-mobile" />
                </div>
              </div>
              <div className="col-md-10 col-sm-9 col-xs-12">
                <h3>
                  <a href="#">Phone Unlock Support</a>
                </h3>
                <br/>
                <p>
                  For more information about your Phone and to get started with our Phone unlocking service
                  please visit our&nbsp;
                  <a href="/" style={{ display: 'inline', textDecoration: 'underline' }}>Phone Unlock</a> page.
                </p>
                <br/>
                <br/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

SupportService.propTypes = {};

SupportService.defaultProps = {};

export default SupportService;
