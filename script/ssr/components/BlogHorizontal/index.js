import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import { formatTime } from '@/utils/formatTime';
import { extractContent } from '@/utils/string';
import { classList } from '@/utils/system/ui';
import classes from './style.module.scss';

const BlogHorizontal = (props) => {
  const {
    data
  } = props;

  if (!data) return null;

  const {
    id,
    content,
    created_at,
    handle,
    image_src,
    image_title,
    title,
    user_create,
    user_name
  } = props.data || {};

  const contentText = extractContent(content);

  if (!id) return null;

  return (
    <div className={classList("row", classes.blogItem)}>
      <div className="col-sm-4 col-xs-12">
        <Link
          to={`/blog/${handle}`}
          className={classes.readMoreButton}
          style={{
            backgroundImage: `url(${image_src})`
          }}
        >
          Read more
        </Link>
      </div>
      <div className="col-sm-8 col-xs-12">
        <label className={classList('label label-info', classes.labelInfo)}>Guides</label>
        <br/>
        <br/>
        <h3>
          <Link
            to={`/blog/${handle}`}
            title=" How to Use Find My Phone When It’s Lost or Stolen"
            className="text-black"
          >
            {title}
          </Link>
        </h3>
        <p className={classList("text-grey text-uppercase", classes.authorTitle)}>
          <small>
            {user_name} &nbsp;•&nbsp; {formatTime(created_at, false)}
          </small>
        </p>
        <p dir="ltr"><span className={classes.contentBlog}>
          {contentText}
        </span>
        </p>
      </div>
    </div>
  );
};

BlogHorizontal.propTypes = {};

BlogHorizontal.defaultProps = {};

export default BlogHorizontal;
