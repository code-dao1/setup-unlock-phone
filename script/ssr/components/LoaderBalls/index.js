import React from 'react';

// // import './style.scss';

const LoaderBalls = (props) => {
  const {
    className = '',
    style = {}
  } = props;

  return (<div style={style} className={`loader-balls w-100 h-100 ${className}`}>
    <div className="loader-balls-wrapper flex justify-space-between items-center">
      <span className="loader-balls-item"/>
      <span className="loader-balls-item" />
      <span className="loader-balls-item" />
    </div>
  </div>);
};

export default LoaderBalls;
