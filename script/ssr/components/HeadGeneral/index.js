import Head from 'next/head';
import React from 'react';
import PropTypes from 'prop-types';
import { publicRuntimeConfig } from '@/constants/system/serverConfig';

const HeadGeneral = (props) => {
  const {
    code,
    asPath,
    webConfig
  } = props;

  const {
    id,
    title = publicRuntimeConfig.productBrand,
    description = `${publicRuntimeConfig.productBrand}, Unlock phone online by imei good quality and reasonable price`,
    url_hosting = publicRuntimeConfig.urlProduct,
    brand_name = publicRuntimeConfig.productBrand,
    keyword = `${publicRuntimeConfig.productBrand}, unlock phone, crack phone, crack network, unlock network`,
    logo,
    favicon = '/favicon.png',
    image_url = `${publicRuntimeConfig.urlProduct}/media/images/background.jpg`,
    image_type = 'image/jpeg',
    image_height = 315,
    image_width = 600
  } = webConfig || {};

  return (
    <Head>
      <title>{title}</title>
      <meta name="keywords"
            content={keyword} />
      <meta name="description" content={description} />
      <meta charSet="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta content="INDEX,FOLLOW" name="robots"/>
      <meta name="copyright" content={brand_name}/>
      <meta name="author" content={brand_name}/>
      <meta name="GENERATOR" content={brand_name}/>
      <meta httpEquiv="audience" content="General"/>
      <meta name="resource-type" content="Document"/>
      <meta name="distribution" content="Global"/>
      <meta name="revisit-after" content="1 days"/>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
      <meta httpEquiv="content-language" content="en"/>
      <meta key="og:site_name" property="og:site_name" content={brand_name}/>
      <meta key="og:type" property="og:type" content="product"/>
      <meta key="og:url" property="og:url" content={`${url_hosting}${asPath}`}/>
      <meta key="og:title" property="og:title" content={title}/>
      <meta key="og:description" property="og:description" content={description}/>
      <meta key="og:image" property="og:image" content={image_url}/>
      <meta key="og:image:type" property="og:image:type" content={image_type}/>
      <meta key="og:image:width" property="og:image:width" content={image_width} />
      <meta key="og:image:height" property="og:image:height" content={image_height}/>

      <link rel="icon" href={favicon} />
      <link href={favicon} rel="shortcut icon" />

      <link rel="canonical" href={`${url_hosting}${asPath}`}/>
      {
        code && (<head dangerouslySetInnerHTML={{__html: code}} />)
      }
    </Head>
  );
};

HeadGeneral.propTypes = {};

HeadGeneral.defaultProps = {};

export default HeadGeneral;
