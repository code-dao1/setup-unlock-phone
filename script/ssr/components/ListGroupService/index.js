import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import groupServiceApi from '@/services/groupServiceApi';
import serviceApi from '@/services/serviceApi';
import { isErrorResponse } from '@/utils/typeof';

import GroupServiceItem from '../GroupServiceItem';

// import './style.scss';

const ListGroupService = (props) => {
  const [groupServices, setGroupServices] = useState([...Array(6)]);
  useEffect(() => {
    groupServiceApi.getListGroupService({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setGroupServices(items);
      } else {
        setGroupServices([]);
      }
    });
  }, []);

  return (
    <section className={'group-list-service'} >
      <div className="container">
        <div className="row">
          <div className="col-xs-12 text-center">
            <h2>Select the country of your network to unlock</h2>
            <p className="lead light">Don&apos;t know the Network? <a href="#" title="Network Check">Click here</a></p>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row text-center" id="country-badges">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            {
              [...Array(5)].map((_, index) => {
                return (
                  <GroupServiceItem
                    key={`group-service-item-${index}`}
                  />
                );
              })
            }
          </div>
        </div>
      </div>
    </section>
  );
};

ListGroupService.propTypes = {};

ListGroupService.defaultProps = {};

export default ListGroupService;
