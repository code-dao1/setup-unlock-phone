import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { clearAllBodyScrollLocks, disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import Modal from 'react-modal';
import { useSelector } from 'react-redux';
import FormInputOrder from '../FormInputOrder';

const ModalFormInputOrder = (props) => {
  const {
    isOpen,

    onClose,
    onOpen
  } = props;
  const isMDScreen = typeof window === 'undefined' ? false : window.innerWidth <= 992;
  useEffect(() => {
    const targetElement = document.querySelector('body');
    if (targetElement) {
      if (isOpen) {
        disableBodyScroll(targetElement);
      } else {
        enableBodyScroll(targetElement);
      }
    }
  }, [isOpen]);

  let styleContentDialog = {
    width: 0,
    height: 0,
    padding: 0,
    backgroundColor: 'transparent',
  };
  let styleOverlayDialog = {
    zIndex: 99,
  };

  if (isMDScreen) {
    styleContentDialog = {
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: '#fff',
    };

    styleOverlayDialog = {
      zIndex: 99,
      top: 44
    }
  }

  return (
    <Modal
      isOpen={isOpen}
      contentLabel="Minimal Modal Example"
      onRequestClose={onClose}
      style={{
        overlay: styleOverlayDialog,
        content: styleContentDialog
      }}
    >
      <FormInputOrder isOpen={isOpen}/>
    </Modal>
  );
};

ModalFormInputOrder.propTypes = {};

ModalFormInputOrder.defaultProps = {};

export default ModalFormInputOrder;
