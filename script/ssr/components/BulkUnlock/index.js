import React from 'react';
import PropTypes from 'prop-types';

const BulkUnlock = (props) => {
  return (
    <section className="banner" id="bulkunlock">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-md-offset-3 col-xs-12 col-xs-offset-0">
            <div className="text-center">
              <h2 className="text-white heading-lg">Bulk Unlock</h2>
              <br/>
              <p className="lead text-white">Ideal for businesses or families with 3 or more devices to unlock.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

BulkUnlock.propTypes = {};

BulkUnlock.defaultProps = {};

export default BulkUnlock;
