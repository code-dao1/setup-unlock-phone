import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import React from 'react';
import PropTypes from 'prop-types';
import HTMLContent from '../common/HTMLContent';

const HowToUnlockPhone = (props) => {
  const {
    id,
    how_description,
    handle,
    image_src,
    image_title,
    manufacturer_id,
    name,
  } = props.phone || {};
  return (
    <section style={{ padding: '80px 0 0', background: '#fff' }}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 text-center">
            <h2>
              How to {name} <br/>
              <small className="text-black">with {publicRuntimeConfig.productBrand}</small>
            </h2>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <div className="row">
              <div className="col-sm-4 col-xs-12 text-center">
                <div className="row">
                  <div className="col-xs-12">
                    <div className="icon-lg">
                      <i className="fa fa-file-text" />
                    </div>
                  </div>
                </div>
                <h3>Get the Ball Rolling</h3>
                <br/>
                <p>Give us some basic details like your IMEI number so we can start processing your unlock.</p>
              </div>
              <div className="col-sm-4 col-xs-12 text-center">
                <div className="row">
                  <div className="col-xs-12">
                    <div className="icon-lg">
                      <i className="fa fa-code" />
                    </div>
                  </div>
                </div>
                <h3>We Process your Unlock</h3>
                <br/>
                <p>We whitelist your phone in manufacturer database, ensuring a permanent and approved
                  unlock.</p>
              </div>
              <div className="col-sm-4 col-xs-12 text-center">
                <div className="row">
                  <div className="col-xs-12">
                    <div className="icon-lg">
                      <i className="fa fa-unlock" />
                    </div>
                  </div>
                </div>
                <h3>Your Phone is Unlocked</h3>
                <br/>
                <p>Confirmation will be sent to you via email and your unlock is delivered over the air using
                  3G/4G/WIFI.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12">
                <br/>
                <hr/>
                <br/>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12" style={{ fontSize: '16px', lineHeight: '24px' }}>
                <h2>How to Unlock Phone by IMEI</h2>
                <br/>
                <p className="lead">IMEI unlocking is the simplest and most trusted way to unlock the Phone</p>
                <br/>
                <br/>
                <HTMLContent content={how_description} />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4 col-sm-offset-4 col-xs-12 col-xs-offset-0">
                <div className="row">
                  <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
                    <br/>
                    <br/>
                    <br/>
                    <a href="#" className="btn btn-primary btn-lg stretch-xs">Unlock Phone</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

HowToUnlockPhone.propTypes = {};

HowToUnlockPhone.defaultProps = {};

export default HowToUnlockPhone;

