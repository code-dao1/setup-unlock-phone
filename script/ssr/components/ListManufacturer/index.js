import React, { useEffect, useState } from 'react';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import manufacturerApi from '@/services/manufacturerApi';
import { isErrorResponse } from '@/utils/typeof';
import ManufacturerItem from '../ManufacturerItem';

// import './style.scss';

const ListManufacturer = (props) => {
  const [listManufacturer, setListManufacturer] = useState(props.data || [...Array(6)]);
  useEffect(() => {
    manufacturerApi.getListManufacturer({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setListManufacturer(items);
      } else {
        setListManufacturer([]);
      }
    });
  }, []);

  return (
    <section className={'list-manufacturer'}>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 text-center">
            <h1>Select the manufacturer of phone to unlock</h1>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
        <div id="Country-Networks">
          <section id="deviceIndex">
            <div className="container">
              <div className="row">
                {
                  listManufacturer.map((item, index) => {
                    return (
                      <ManufacturerItem
                        data={item}
                        className={'col-md-3 col-xs-6 text-center'}
                        key={`group-service-item-${index}`}
                      />
                    );
                  })
                }
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  );
};

ListManufacturer.propTypes = {};

ListManufacturer.defaultProps = {};

export default ListManufacturer;
