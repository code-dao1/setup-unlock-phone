import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { PAGE, PAGE_SIZE } from '../../constants/system/service';
import postApi from '@/services/postApi';
import serviceApi from '@/services/serviceApi';
import { isErrorResponse } from '@/utils/typeof';
import BlogItem from '../BlogItem';

import classes from './style.module.scss';

const ListBlogInHome = (props) => {
  const dataFromProps = props.data || {};
  const [posts, setPosts] = useState(dataFromProps.items || [...Array(6)]);
  useEffect(() => {
    postApi.getListPost({
      [PAGE]: 1,
      [PAGE_SIZE]: 4,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setPosts(items);
      } else {
        setPosts([]);
      }
    });
  }, []);

  return (
    <section className={classes.listBlogHome}>
      <div className="container" style={{ backgroundColor: '#fff' }}>
        <div className="row">
          <div className="col-xs-12">
            <hr/>
            <h2>Latest Articles</h2>
            <br/>
            <br/>
          </div>
        </div>
        <div className="row">
          {
            posts.map((item, index) => {
              return (<BlogItem
                key={`blog-item-home-${index}`}
                className={'col-sm-3'}
                data={item}
              />)
            })
          }
        </div>
      </div>
    </section>
  );
};

ListBlogInHome.propTypes = {};

ListBlogInHome.defaultProps = {};

export default ListBlogInHome;
