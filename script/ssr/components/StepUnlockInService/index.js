import React from 'react';
import PropTypes from 'prop-types';

const StepUnlockInService = (props) => {
  return (
    <section id="usp">
      <div className="container">
        <div className="row">
          <div className="col-sm-4 col-xs-12">
            <div className="text-center">
              <div className="icon">
                <i className="fa fa-clock-o fa-2x" />
              </div>
              <h3>24 hour unlock delivery</h3>
              <p className="lead light">Instant unlocking for many mobile networks.</p>
            </div>
          </div>
          <div className="col-sm-4 col-xs-12">
            <div className="text-center">
              <div className="icon">
                <i className="fa fa-wrench fa-2x" />
              </div>
              <h3>Highly Skilled Tech Team</h3>
              <p className="lead light">
                Experienced in all Apple<sup>TM</sup> Phone <br/>
                products.
              </p>
            </div>
          </div>
          <div className="col-sm-4 col-xs-12">
            <div className="text-center">
              <div className="icon">
                <i className="fa fa-mobile-phone fa-2x" />
              </div>
              <h3>Your Phone is Safe</h3>
              <p className="lead light">You keep your device throughout the entire process.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

StepUnlockInService.propTypes = {};

StepUnlockInService.defaultProps = {};

export default StepUnlockInService;
