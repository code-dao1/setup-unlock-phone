import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import { FILTER, PAGE, PAGE_SIZE } from '../../constants/system/service';
import manufacturerApi from '@/services/manufacturerApi';
import phoneApi from '@/services/phoneApi';
import { updateItemInArray } from '@/utils/array';
import { isErrorResponse } from '@/utils/typeof';
import Image from '../common/Image';
import DeviceItem from '../DeviceItem';
import ListDevice from '../ListDevice';
import Phone from '../SliderPhone/Phone';

import classes from './style.module.scss'

const ListPhoneOfManufacturer = (props) => {

  const [listManufacturer, setListManufacturer] = useState([]);
  useEffect(() => {
    manufacturerApi.getListManufacturer({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setListManufacturer(items);
      } else {
        setListManufacturer([]);
      }
    });
  }, []);
  const [loading, setLoading] = useState(false);
  const hasMore = !!listManufacturer.find(manufacturer => !manufacturer.phones);
  const handleLoadMore = () => {
    const manufacturerNoLoad = listManufacturer.find(manufacturer => !manufacturer.phones);
    if (manufacturerNoLoad && !loading) {
      setLoading(true);
      phoneApi.getListPhone({
        [PAGE]: 1,
        [PAGE_SIZE]: -1,
        [FILTER]: [{
          name: 'manufacturer_id',
          value: manufacturerNoLoad.id
        }]
      }).then(rs => {
        if (!isErrorResponse(rs)) {
          const {
            items,
            load_more_able,
            page,
            pre_load_able,
            total,
          } = rs;
          const newList = updateItemInArray(
            {
              id: manufacturerNoLoad.id
            },
            {
              ...manufacturerNoLoad,
              phones: items
            },
            listManufacturer
          );
          setListManufacturer(newList);
        } else {
          setListManufacturer(updateItemInArray(
            {
              id: manufacturerNoLoad.id
            },
            {
              phones: []
            },
            listManufacturer
          ));
        }
        setLoading(false);
      })
    }
  };

  return (
    <InfiniteScroll
      pageStart={1}
      loadMore={handleLoadMore}
      hasMore={hasMore}
      threshold={300}
      // initialLoad
      loader={
        <div className="container" key={'device-item-loader'}>
          {
            [...Array(12)].map((item, index) => {
              return (<DeviceItem
                key={`device-item-loader-${index}`}
                className={'col-md-3 col-xs-6 text-center'}
                data={item}
              />);
            })
          }
        </div>
      }
    >
      {
        listManufacturer.map((manufacturer, index) => {
          if (manufacturer && manufacturer.phones && manufacturer.phones.length > 0) {
            const {
              handle,
              id,
              image_src,
              image_title,
              name,
              phones
            } = manufacturer;
            return (
              <section
                key={`manufacturer-item-${index}`}
                className={classes.groupServiceContent}
              >
                <div className="container">
                  <div className="row">
                    <div className="col-xs-9">
                      <div className="row">
                        <div className="col-md-1 col-sm-2 col-xs-3">
                          <Image
                            src={image_src}
                            fillMode="contain"
                            alt=""
                            className="img-responsive center-block group-service-item-image"
                          />
                        </div>
                        <div className="col-md-11 col-sm-10 col-xs-9">
                          <h3>{name}</h3>
                          <p>Unlock Phone from {name}</p>
                        </div>
                      </div>
                      <br/>
                    </div>
                  </div>
                  <div className={classes.listService} id="Country-Networks">
                    <section id="deviceIndex" className={classes.listDevice}>
                      <div className="container">
                        <div className="row">
                          {
                            phones.map((item, indexPhone) => {
                              return (<DeviceItem
                                key={`device-item-${index}-${indexPhone}`}
                                className={'col-md-3 col-xs-6 text-center'}
                                data={item}
                              />);
                            })
                          }
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </section>
            );
          }
          return null;
        })
      }
    </InfiniteScroll>
  );
};

ListPhoneOfManufacturer.propTypes = {};

ListPhoneOfManufacturer.defaultProps = {};

export default ListPhoneOfManufacturer;
