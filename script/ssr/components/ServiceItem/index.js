import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import { classList } from '@/utils/system/ui';
import Image from '../common/Image';

import classes from './style.module.scss';

const ServiceItem = (props) => {
  const {
    className,
    data,
    isLast,
    numItemNoShow,
  } = props;
  const {
    id,
    description,
    image_src,
    handle,
    name,
  } = data || {};

  if (!id) {
    return (
      <div className={classList(className, classes.serviceItem, classes.loading)}>
        <div className={classes.serviceItemWrapper}>
          <div
            className={classList('img-responsive', classes.img)}
          />
        </div>
      </div>
    );
  }

  const showLastItem = isLast && numItemNoShow > 0;

  return (
    <div className={classList(className, classes.serviceItem)}>
      <Link className={classes.serviceItemWrapper} to={showLastItem ? '/manufacturers' : `/service/${handle}`}>
        <Image
          src={image_src}
          alt={name}
          className={classList('img-responsive', classes.img)}
        />
        {
          showLastItem &&
          (
            <div className={classes.wrapperNumNoShow}>
              +{numItemNoShow}
            </div>
          )
        }
      </Link>
    </div>
  );
};

ServiceItem.propTypes = {};

ServiceItem.defaultProps = {};

export default ServiceItem;
