import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import validator from 'validator';
import { FILTER, PAGE, PAGE_SIZE } from '@/constants/system/service';
import phoneApi from '@/services/phoneApi';
import serviceApi from '@/services/serviceApi';
import { isErrorResponse } from '@/utils/typeof';
import IMEIInput from '../FormInputUnlockPhone/IMEIInput';
import SelectPhone from '../FormInputUnlockPhone/SelectPhone';

const FormInputUnlockPhoneInService = (props) => {
  const {
    serviceId
  } = props;
  const router = useRouter();
  const [phones, setPhones] = useState([]);
  const [phoneSelect, setPhoneSelect] = useState(null);
  const [imei, setIMEI] = useState('');
  const [errorMessage, setErrorMessage] = useState({});
  const changeImei = (e) => {
    const newIMEI = e.target.value;
    if (/^[0-9a-z]+$/.test(newIMEI) || !newIMEI) {
      setIMEI(e.target.value);
    }
  };
  if (!serviceId) return null;

  useEffect(() => {
    let params = {
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    };

    phoneApi.getListPhoneByServiceId(serviceId, params).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page,
          pre_load_able,
          total,
        } = rs;
        setPhones(items);
      }
    })
  }, [serviceId]);

  const onSubmit = () => {
    let messageErrorNew = {}
    if (!phoneSelect || !phoneSelect.id) {
      messageErrorNew.phone = 'Please select a phone';
    }

    if (!imei || !imei.trim()) {
      messageErrorNew.imei = 'Please enter your IMEI number.';
    } else if (/^[0-9]+$/.test(imei) && !validator.isIMEI(imei)) {
      messageErrorNew.imei = 'IMEI invalid.';
    }

    if (Object.keys(messageErrorNew).length > 0) {
      setErrorMessage(messageErrorNew);
    } else {
      router.push({
        pathname: '/checkout/accountdetails',
        query: {
          phone_id: phoneSelect.id,
          service_id: serviceId,
          imei: imei,
        }
      })
    }
  }

  return (
    <div id="PhoneUnlockForm" style={{ position: 'relative', zIndex: '2' }}>
      <div
        id="request"
      >
        <div className="step">
          <div className="dropdown-controls">
            <div className="col-md-6 col-xs-12">
              <SelectPhone
                phones={phones || []}
                phoneSelect={phoneSelect}
                setPhoneSelect={setPhoneSelect}
                messageError={errorMessage.phone}
              />
            </div>
            <div className="col-md-6 col-xs-12">
              <IMEIInput
                value={imei}
                messageError={errorMessage.imei}
                changeValue={changeImei}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 col-md-offset-4 col-xs-8 col-xs-offset-2">
            <button
              type="submit"
              className="btn btn-primary btn-lg"
              style={{ width: '100%', marginTop: '20px' }}
              id="SubmitForm"
              onClick={onSubmit}
            >
              <span>Continue <i className="fa fa-arrow-circle-right" /></span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

FormInputUnlockPhoneInService.propTypes = {};

FormInputUnlockPhoneInService.defaultProps = {};

export default FormInputUnlockPhoneInService;
