import React from 'react';
import PropTypes from 'prop-types';
import HTMLContent from '../common/HTMLContent';
import Image from '../common/Image';

const ServiceInformation = (props) => {
  const {
    id,
    created_at,
    created_by,
    handle,
    description,
    image_src,
    image_title,
    name,
  } = props.data || {};

  return (
    <>
      <Image
        src={image_src}
        alt={name}
        className="center-block"
        style={{ width: '120px', height: 'auto', borderRadius: '3px', marginBottom: '30px' }}
      />
      <h1 className="text-center">{name}</h1>
      <div className="row">
        <div className="col-md-12">
          <p className="text-center" style={{ margin: '30px 15px' }}>
            Unlock your {name} Phone for use on any network worldwide. Permanent factory Phone unlocking
            service, safe and reliable process, guaranteed to quickly unlock your Phone from the {name}
            network.
          </p>
        </div>
      </div>
    </>
  );
};

ServiceInformation.propTypes = {};

ServiceInformation.defaultProps = {};

export default ServiceInformation;
