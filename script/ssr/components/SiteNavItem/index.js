import React from 'react';
import Link from '@/components/common/Link';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const SiteNavItem = React.forwardRef(({ className, classNameLink, name, link = '', onClick, children }, ref) => {
  return (
    <div ref={ref} className={classList(classes.siteNavItem, 'm0', className)} onClick={onClick}>
      {
        link &&
        (<Link to={link} className={classList(classNameLink, 'flex items-center')}>
          {name}
        </Link>)
      }
      {children}
    </div>
  );
});

export default SiteNavItem;
