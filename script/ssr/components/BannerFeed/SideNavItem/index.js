import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { classList } from '@/utils/system/ui';
import Image from '../../common/Image';
 import SubSideNavItem from '../SubSideNavItem';

import classes from './style.module.scss';

const SideNavItem = (props) => {
  const {
    category_id,
    handle,
    category_name,
    category_icon,
    sub_categories = []
  } = props.data || {};
  const isMore = true;
  return (
    <div className={classList(classes.shopBannerDropdownSubmenu, classes.shopBannerDropdownNewSubmenu)}>
      <Link href={`/${handle}`} rel={handle}>
        <div>
          <div className={classes.shopBannerDropdownSubmenuImg}>
            <Image
              alt={category_name}
              src={category_icon}
            />
          </div>
          <span>{category_name}</span>
        </div>
      </Link>
      <div className={classes.shopDropdownMenu} id={handle}>
        <ul>
          {
            sub_categories.map((sub_category, index) => {
              return <SubSideNavItem
                key={`sub-side-nav-item-${index}-${category_id}`}
                data={sub_category}
                handle={handle}
              />;
            })
          }
          {
            isMore &&
            (
              <li>
                <Link tabIndex="-1" href={`/mua-sam/${handle}`} className={'active'}>
                  Xem thêm
                </Link>
              </li>
            )
          }
        </ul>
      </div>
    </div>
  );
};

SideNavItem.propTypes = {};

SideNavItem.defaultProps = {};

export default SideNavItem;
