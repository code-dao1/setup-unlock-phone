import React from 'react';
import Slider from 'react-slick';

import { classList } from '@/utils/system/ui';
import Image from '../../common/Image';
import classes from './style.module.scss';

const BannerSlider = (props) => {
  const {
    className
  } = props;

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    <Slider
      {...settings}
      className={classList(
        className,
        'carousel slide',
        classes.carouselBannerGeneric
      )}
    >
      {
        [...Array(20)].map((_, key) => {
          return (
              <div
                className="swiper-slide swiper-slide-ads jsReachableAdvertisement"
                data-reach="ad:4924"
              >
                <a
                  href="https://bibabo.vn/chuong-trinh/THOI-TRANG-ME-BE--BUNG-SANG-NGAY-HE-185?itm_campaign=thoi-trang-me-be&amp;itm_source=ecom_banner1_web&amp;itm_medium=banner"
                  // className="js-gaee-banner-data"
                  data-banner-id="ad-4924"
                  data-banner-name="ecom_banner1_web: Thời trang mẹ bé"
                  data-banner-pos="Banner trang chủ mua sắm Website"
                  data-banner-creative=""
                  data-banner-list="Banners"
                >
                  <div>
                    <Image
                      alt=""
                      src="https://cdn0.fahasa.com/media/magentothem/banner7/Banner-chinh_920x420.jpg"
                      style={{
                        width: '100%',
                        height: '100%'
                      }}
                    />
                    {/*<div className="number-slide-active">*/}
                    {/*  1/5*/}
                    {/*</div>*/}
                  </div>
                </a>
              </div>
            );
            // return (
            //   <div className="ad-banner-wrapper">
            //     <div id="js-ad-banner-12" className="swiper-container swiper-container-horizontal">
            //       <div
            //         className="swiper-wrapper"
            //       >
            //         <div
            //           className="swiper-slide swiper-slide-ads jsReachableAdvertisement"
            //           data-reach="ad:4924"
            //         >
            //           <a
            //             href="https://bibabo.vn/chuong-trinh/THOI-TRANG-ME-BE--BUNG-SANG-NGAY-HE-185?itm_campaign=thoi-trang-me-be&amp;itm_source=ecom_banner1_web&amp;itm_medium=banner"
            //             className="js-gaee-banner-data"
            //             data-banner-id="ad-4924"
            //             data-banner-name="ecom_banner1_web: Thời trang mẹ bé"
            //             data-banner-pos="Banner trang chủ mua sắm Website"
            //             data-banner-creative=""
            //             data-banner-list="Banners"
            //           >
            //             <div className="ad-banner-img-default">
            //               <img
            //                 alt=""
            //                 src="https://cdn0.fahasa.com/media/magentothem/banner7/Banner-chinh_920x420.jpg"
            //               />
            //               <div className="number-slide-active">
            //                 1/5
            //               </div>
            //             </div>
            //           </a>
            //         </div>
            //       </div>
            //       <div className="swiper-button-next swiper-slide-button"/>
            //       <div className="swiper-button-prev swiper-slide-button"/>
            //       <div className="swiper-pagination">
            //         <span className="swiper-pagination-bullet"/><span className="swiper-pagination-bullet"/><span
            //         className="swiper-pagination-bullet"
            //       />
            //         <span className="swiper-pagination-bullet swiper-pagination-bullet-active"/><span
            //         className="swiper-pagination-bullet"
            //       />
            //       </div>
            //     </div>
            //     <div className="clearfix"/>
            //   </div>
            // )
          }
        )
      }
      {/*<div*/}
      {/*  id="carousel-example-generic"*/}
      {/*  className={classList(*/}
      {/*    className,*/}
      {/*    'carousel slide',*/}
      {/*    classes.carouselBannerGeneric*/}
      {/*  )}*/}
      {/*  data-ride="carousel"*/}
      {/*>*/}
      {/*  */}
      {/*</div>*/}
    </Slider>
  );
};

BannerSlider.propTypes = {};

BannerSlider.defaultProps = {};

export default BannerSlider;
