import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

const SubSideNavItem = (props) => {
  const {
    handle: handleParent,
    data
  } = props;
  const {
    category_id,
    handle,
    category_name,
  } = data;
  return (
    <li>
      <Link tabIndex="-1" href={`${handleParent}/${handle}`}>
        {category_name}
      </Link>
    </li>
  );
};

SubSideNavItem.propTypes = {};

SubSideNavItem.defaultProps = {};

export default SubSideNavItem;
