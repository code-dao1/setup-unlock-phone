import PropTypes from 'prop-types';

import LinkNav from '../LinkNav';
import BannerSlider from './BannerSlider';
import SideNavItem from './SideNavItem';

import classes from './style.module.scss';
// import './style.scss'

const sidenav = [
  {
    category_id: 1,
    handle: 'van-hoc',
    category_name: 'Văn học',
    category_icon: 'https://bibabo.vn/assets/bec/img/category_img/for_mother.svg?v=1',
    sub_categories: [
      {
        category_id: 2,
        handle: 'tieu-thuyet',
        category_name: 'Tiểu Thuyết',
      },
      {
        category_id: 2,
        handle: 'truyen-ngan-tan-van',
        category_name: 'Truyện Ngắn - Tản Văn',
      },
      {
        category_id: 2,
        handle: 'light-novel',
        category_name: 'Light Novel',
      },
      {
        category_id: 2,
        handle: 'ngon-tinh',
        category_name: 'Ngôn Tình',
      },
      {
        category_id: 2,
        handle: 'truyen-trinh-tham-tieng-viet',
        category_name: 'Truyện Trinh Thám - Kiếm Hiệp',
      },
    ]
  }
];

const BannerFeed = (props) => {
  return (
    <div className={classes.shopBannerContainer}>
      <div className={classes.shopBannerInner}>
        <div className={classes.mainBanner}>
          <div className={classes.shopBannerLeft}>
            <div id="nav_h" className={classes.shopBannerContainerLeft}>
              <div id="shop_menu">
                <LinkNav />
                {
                  sidenav.map((item, index) => {
                    return (
                      <SideNavItem
                        key={`side-nav-item-${index}-${item.category_id}`}
                        data={item}
                      />
                    );
                  })
                }
              </div>
            </div>
          </div>
          <div className={classes.shopBannerRight}>
            <BannerSlider className={classes.shopBannerRightLeft} />
            <div className="shop-banner__right-right">
              <div onClick="location.href='https://bibabo.vn/mua-sam/hang-noi-dia'">
                <img alt="" src="https://bibabo.vn/assets/shop/img/banner2-2.png" />
              </div>
            </div>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
        </div>
      </div>
    </div>
  );
};

BannerFeed.propTypes = {};

BannerFeed.defaultProps = {};

export default BannerFeed;
