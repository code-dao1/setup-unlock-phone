export const HOME = {
  to: '/',
  name: 'Home'
};

export const COLLECTIONS = {
  to: '/collections',
  name: 'Collection List'
};

export const BREADCRUMB_ITEM_CUSTOM = (name, to) => ({
  to: to,
  name: name
});

export const BREADCRUMB_COLLECTIONS = [HOME, COLLECTIONS];
export const BREADCRUMB_COLLECTION = (name) => [HOME, BREADCRUMB_ITEM_CUSTOM(name)];
