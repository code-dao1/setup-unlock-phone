// Collection

export const ID = 'id';
export const NAME = 'name';
export const DESCRIPTION = 'description';
export const POSITION = 'position';
export const CHILDREN_COUNT = 'children_count';
export const IMAGE = 'image';
export const ALT_TEXT = 'alt_text';
export const INCLUDE_IN_MENU = 'include_in_menu';
export const IS_ACTIVE = 'is_active';
export const IS_ANCHOR = 'is_anchor';
export const LEVEL = 'level';
export const PATH = 'path';
export const SEO_TITLE = 'seo_title';
export const SEO_DESCRIPTION = 'seo_description';
export const META_FIELDS = 'meta_fields';
export const KEY = 'key';
export const NAMESPACE = 'namespace';
export const VALUE = 'value';
export const VALUE_TYPE = 'value_type';
export const SORT_ORDER = 'sort_order';

export const COLLECTION_FIELDS = [ID, NAME, DESCRIPTION, POSITION, CHILDREN_COUNT, IMAGE, ALT_TEXT, INCLUDE_IN_MENU, IS_ACTIVE, IS_ANCHOR, LEVEL, PATH, SEO_TITLE, SEO_DESCRIPTION, SORT_ORDER];
