export const DESKTOP_BREAKPOINT = 1136; /* Desktop: > 1136 */
export const SM_DESKTOP_BREAKPOINT = 1024; /* Small desktop: 1024 - 1135 */
export const TABLET_BREAKPOINT = 768; /* Tablet: 768 - 1023 */
export const MOBILE_BREAKPOINT = 360; /* Medium mobile: 360 - 767 */
export const SM_MOBILE_BREAKPOINT = 359; /* Small Mobile: < 360 */
