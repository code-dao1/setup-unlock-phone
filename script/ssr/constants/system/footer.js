import React from 'react';

export const FOOTER_ITEMS_COL_1 = [
  {
    name: 'Collections',
    link: '/collections'
  },
  {
    name: 'Quilt Blanket',
    link: '/collections/quilt-blanket'
  },
  {
    name: 'Area Rug',
    link: '/collections/area-rug'
  },
  {
    name: 'Round Carpet',
    link: '/collections/round-carpet'
  },
  {
    name: 'Bedding Sets',
    link: '/collections/bedding-sets'
  },
];

export const FOOTER_ITEMS_COL_2 = [
  {
    content: <>Email: <strong>support@fabricaly.com</strong></>
  },
  {
    content: <>Address: Houston, Texas 77043, USA</>
  }
];

export const FOOTER_ITEMS_COL_3 = [
  {
    name: 'Privacy policy',
    link: '/policies/privacy-policy'
  },
  {
    name: 'Terms of Service',
    link: '/policies/terms-of-service'
  },
  {
    name: 'Shipping policy',
    link: '/policies/shipping-policy'
  },
  {
    name: 'Refund policy',
    link: '/policies/refund-policy'
  }
];

export const FOOTER_COL_1 = {
  title: 'SHOP',
  items: FOOTER_ITEMS_COL_1
};

export const FOOTER_COL_2 = {
  title: 'CONTACT US',
  items: FOOTER_ITEMS_COL_2
};

export const FOOTER_COL_3 = {
  title: 'Policies',
  items: FOOTER_ITEMS_COL_3
};

export const CREDITS = [
  {
    iconName: 'credit-visa-icon',
    title: 'Visa Card',
  },
  {
    iconName: 'credit-master-icon',
    title: 'Master Card',
  },
  {
    iconName: 'credit-amex-icon',
    title: 'Amex Card',
  },
  {
    iconName: 'credit-paypal-icon',
    title: 'Paypal',
  },
];
