import getConfig from "next/config";

// Check Enviroment to get service Config
// const BUILD_ENV = process.env.REACT_APP_SERVER_CONFIG;
// const configFile = require(`../../../config/env/${BUILD_ENV || 'dev'}.json`);

// export const CURRENT_ENV = BUILD_ENV === 'production' || false;
export const { publicRuntimeConfig } = getConfig();
export const CORE_API = getConfig().publicRuntimeConfig.baseUrl;
export const ORDER_API = 'https://secure.nochex.com';
