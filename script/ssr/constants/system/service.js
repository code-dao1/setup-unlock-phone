export const PRODUCT_CATEGORY_ID = 'product_category_id';
export const PAGE = 'page';
export const PAGE_SIZE = 'page_size';
export const FILTER = 'filters';
export const SEARCH = 'keyword';
export const SORT_FIELD = 'sort_field';
export const SORT_DIRECTION = 'sort_direction';
