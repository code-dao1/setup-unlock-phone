export const SITE_NAV = [
  {
    name: 'Home',
    link: '/'
  },
  {
    name: 'Manufacturers',
    link: '/manufacturers'
  },
  {
    name: 'Phones',
    link: '/phones'
  },
  {
    name: 'IMEI Check',
    link: '/imei-check'
  },
  {
    name: 'iCloud Unlock',
    link: '/service/unlock-icloud-45'
  },
  {
    name: 'Support',
    link: '/support'
  },
  {
    name: 'Order Tracking',
    link: '/tracking'
  },
];
