export default [
  {
    title: 'How do I unlock my Phone?',
    content: 'IMEI whitelisting is by far the easiest way known to unlock any phone! You only have to choose your Phone model and network from the dropdown, then input your IMEI number. Once your payment is confirmed, you will get an email confirming your order while providing tracking information. Your unlock starts and is completed shortly as your IMEI is whitelisted in the database. Immediately the process is successfully completed, you will get a confirmation email.',
  },
  {
    title: 'How do I get my IMEI number?',
    content: 'Every device has an IMEI number. <b>You will usually find this number on the box of the device. If you have thrown it away, do not despair. Enter the string *#06# (asterisk, hash zero six asterisk) on your mobile phone and the code will appear on your screen: take a pen and paper and write it down.</b>',
  },
  {
    title: 'What do I need to unlock my Phone?',
    content: 'The only thing you need is a WIFI or 3G/4G connection and of course, the new SIM card you are looking to use with the phone.<ul><li>\tFor iPhones: Simply connect your phone to iTunes with a USB cable and your phone should be unlocked automatically.</li><li>\tFor Android: Insert the SIM card for a different carrier into your device and enter the unlock code.</li></ul>',
  },
  {
    title: 'Why should I use your service to unlock my Phone?',
    content: 'We offer a Phone unlock technique that is safe and recognized. Our process ensures that the unlock is perpetual, such that no future updates would tamper with it, neither would you lose your warranty. Also, unlike many other unlock service providers, we are able to complete your device unlock without you even stepping out of your home with the device.',
  },
  {
    title: 'How do I know if my Phone has been blacklisted?',
    content: 'It is likely that your phone has been blacklisted if has once been reported lost or stolen. If you do not know or you are not sure, kindly contact our customer care department or your service provider for more information.',
  },
  {
    title: 'How do I know what network my Phone is locked to?',
    content: 'Before we are able to process your unlock request, it is important for us to know what network your Phone is locked to. If you are not sure, you can contract us and request a free IMEI checker.',
  },
  {
    title: 'What if I am under contract with my network?',
    content: 'Even if your Phone is still under legal bond with your current service provider, it does not mean you cannot unlock your Phone, we will use an alternative network for you, however, you must ensure to be religious with your monthly payments.',
  },
  {
    title: 'Does your unlocking service remove the iCloud Activation Lock?',
    content: 'Our process will only remove the network restriction from your device, thus, allowing you to use any service provider of your choice. If your device has an iCloud Activation Lock however, you can avail yourself of our iCloud unlock service which can help you remove the Activation Lock, this way you can have full access to your device.',
  },
];
