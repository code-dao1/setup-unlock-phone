import { publicRuntimeConfig } from '@/constants/system/serverConfig';

export default [
  {
    user_name: 'Russell Leach',
    content: 'Placed my order for my AT&T iPhone on Friday, and they just emailed me and my phone is now unlocked. Amazing service! Will definitely use again!!',
    title: 'Great Experience',
    rate: 5,
  },
  {
    user_name: 'Miss Lorraine Gower',
    content: 'Simply the best service ever. I had my Samsung Galaxy S8 unlocked within 3 hours after purchase. Previously, I’d been trying to contact my carrier and asking them to unlock my phone for my UK trip, but they declined my request as my phone was still on a payment plan. This actually worked at an affordable price.',
    title: 'Professional, fast, and well-priced',
    rate: 4,
  },
  {
    user_name: 'Concerned Consumer',
    content: 'Looking for trusted unlock company? Look no further, you are finally here, 100% recommend to anyone.',
    title: 'Reliable Unlock Company',
    rate: 5,
  }
];
