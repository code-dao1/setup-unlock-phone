import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/phones`;

export default {
  getListPhone: (params) => {
    return requests.post(
      URL_DEFAULT,
      params,
    );
  },
  getPhoneInfo: (handle) => {
    return requests.get(`${URL_DEFAULT}/${handle}`);
  },
  getListPhoneByServiceId: (serviceId, params) => {
    return requests.get(
      parseGETParams({
        baseURI: `${CORE_API}/api/services/${serviceId}/phones`,
        params
      })
    );
  },
};
