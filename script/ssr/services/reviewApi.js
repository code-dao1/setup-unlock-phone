import { CORE_API } from '@/constants/system/serverConfig';
import { parseGETParams, pushParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/youtube-reviews`;

export default {
  getListYoutube: (params) => {
    return requests.post(
      URL_DEFAULT,
      params,
    );
  },
};
