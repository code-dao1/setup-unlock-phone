import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/service-groups`;

export default {
  getListGroupService: (params, paramsUrl = {}) => {
    return requests.post(
      parseGETParams({
        baseURI: URL_DEFAULT,
        params: paramsUrl
      }),
      params,
    );
  },
};
