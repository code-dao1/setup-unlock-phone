import { CORE_API } from '@/constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/script`;

export default {
  getScriptByType: (type) => {
    return requests.get(`${URL_DEFAULT}/${type}`);
  },
  getConfig: () => {
    return requests.get(`${CORE_API}/api/checkout-config`);
  },
  getConfigWebsite: () => {
    return requests.get(`${CORE_API}/api/website/config`);
  },
  checkingPhoneByImei: (imei) => {
    return requests.get(`${CORE_API}/api/imei/${imei}`);
  },
};
