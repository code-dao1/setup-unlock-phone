import { CORE_API } from '../constants/system/serverConfig';
import requests from './core/base';

export default {
  getOrderInfo: (phoneId, serviceId) => {
    return requests.get(
      `${CORE_API}/api/phones/${phoneId}/services/${serviceId}`
    );
  },
};
