import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams, pushParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/services`;

export default {
  getListService: (params) => {
    return requests.post(
      URL_DEFAULT,
      params,
    );
  },
  getServiceInfo: (handle) => {
    return requests.get(`${URL_DEFAULT}/${handle}`);
  },
  getListServiceByPhoneId: (phoneId, params) => {
    return requests.get(
      parseGETParams({
        baseURI: `${CORE_API}/api/phones/${phoneId}/services`,
        params
      })
    );
  },
};
