import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

export default {
  submit: (data, { onSuccess, onError }) => {
    return requests.submitOrder('https://secure.nochex.com', { data, onSuccess, onError })
  },
};
