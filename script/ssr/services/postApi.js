import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/posts`;

export default {
  getListPost: (params) => {
    return requests.post(
      URL_DEFAULT,
      params,
    );
  },
};
