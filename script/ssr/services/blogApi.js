import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/posts`;

export default {
  getListBlog: (params) => {
    return requests.post(
      URL_DEFAULT,
      params,
    );
  },
  getBlogInfo: (handle) => {
    return requests.get(`${URL_DEFAULT}/${handle}`);
  },
};
