import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);
const TIMEOUT_SECONDS = 15000;
const RETRY_TIME = 5;

let csrfToken = null;

const checkDataResponse = data => {
  if (data) {
    if (data.code && (data.code !== 0 && data.code !== 200)) {
      if (data.code === 500) {
        // window.location = routesConfig.error.path;
      }
      return data.body ? data.body : data;
    }
    return data.body ? data.body : data;
  }
  return {};
};

function getCookieValue(a) {
  // let b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
  // return b ? b.pop() : '';
}

function getToken() {
  let token = null;
  try {
    token = window.localStorage.getItem('jwt');
  } catch (err) {
    console.log('...');
  }
  if (!token) token = getCookieValue('jwt');
  return `Bearer ${token}`;
}

const tokenPlugin = req => {
  const bearer = getToken();
  if (bearer) {
    // req.set('authorization', bearer);
  }
};

// Example
const errorHandle = () => {
  // window.location = routesConfig.error.path;
};

const uploadFile = (uploadURL, { data, onSuccess, onError, onProgress }) => {
  if (data) {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', uploadURL);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Authorization', getToken());

    let percent = 0;
    xhr.onload = () => {
      percent = 100;
      if (typeof onProgress === 'function') onProgress(percent);
    };

    xhr.upload.onprogress = event => {
      const { loaded, total } = event;
      percent = (loaded / total).toFixed(2) * 100;
      if (typeof onProgress === 'function') {
        onProgress(percent);
      }
    };

    xhr.onerror = event => {
      if (typeof onError === 'function') onError(event);
    };

    xhr.onreadystatechange = () => {
      if (
        xhr.readyState === 4 &&
        xhr.status === 200 &&
        typeof onSuccess === 'function'
      ) {
        try {
          const response = JSON.parse(xhr.responseText);
          if (response && response.code === 0) {
            onSuccess(response.result);
          } else {
            onError(response);
          }
        } catch (error) {
          onError(error);
        }
      }
    };

    // send file in body
    const index = data.indexOf(';base64,') + 7;
    xhr.send(JSON.stringify({ data: data.substr(index) }));
  } else {
    if (typeof onError === 'function') {
      onError({
        code: 500,
        message:
          'To upload image, you have pass token and image data as base 64 string'
      });
    } else {
      console.error(
        'To upload image, you have pass token and image data as base 64 string'
      );
    }
  }
};

// upload video
const uploadVideo = (uploadURL, { data, onSuccess, onError, onProgress }) => {
  if (data) {
    let formData = new FormData();
    formData.append('file_upload', data);
    let xhr = new XMLHttpRequest();
    xhr.open('POST', uploadURL);
    // xhr.setRequestHeader(
    //   'Content-Type',
    //   'multipart/form-data;boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    // );
    xhr.setRequestHeader('Authorization', getToken());

    let percent = 0;
    xhr.onload = () => {
      percent = 100;
      if (typeof onProgress === 'function') onProgress(percent);
    };

    xhr.upload.onprogress = event => {
      const { loaded, total } = event;
      percent = (loaded / total).toFixed(2) * 100;
      if (typeof onProgress === 'function') {
        onProgress(percent);
      }
    };

    xhr.onerror = event => {
      if (typeof onError === 'function') onError(event);
    };

    xhr.onreadystatechange = () => {
      if (
        xhr.readyState === 4 &&
        xhr.status === 200 &&
        typeof onSuccess === 'function'
      ) {
        try {
          const response = JSON.parse(xhr.responseText);
          if (response && response.code === 0) {
            onSuccess(response.result);
          } else {
            onError(response);
          }
        } catch (error) {
          onError(error);
        }
      }
    };
    // send file in body
    xhr.send(formData);
  } else {
    if (typeof onError === 'function') {
      onError({
        code: 500,
        message: ''
      });
    } else {
      console.error('');
    }
  }
};

// upload video
const submitOrder = (orderApi, { data, onSuccess, onError, onProgress }) => {
  if (data) {
    let formData = new FormData();
    Object.keys(data).forEach((key) => {
      formData.append(key, data[key]);
    });
    let xhr = new XMLHttpRequest();
    xhr.open('POST', orderApi);

    let percent = 0;
    xhr.onload = () => {
      percent = 100;
      if (typeof onProgress === 'function') onProgress(percent);
    };

    xhr.upload.onprogress = event => {
      const { loaded, total } = event;
      percent = (loaded / total).toFixed(2) * 100;
      if (typeof onProgress === 'function') {
        onProgress(percent);
      }
    };

    xhr.onerror = event => {
      if (typeof onError === 'function') onError(event);
    };

    xhr.onreadystatechange = () => {
      if (
        xhr.readyState === 4 &&
        xhr.status === 200 &&
        typeof onSuccess === 'function'
      ) {
        try {
          onSuccess(xhr);
        } catch (error) {
          onError(error);
        }
      }
    };
    // send file in body
    xhr.send(formData);
  } else {
    if (typeof onError === 'function') {
      onError({
        code: 500,
        message: ''
      });
    } else {
      console.error('');
    }
  }
};

const setCsrfToken = headers => {
  if (headers && headers['csrf-token']) {
    csrfToken = headers['csrf-token'];
  }
};

const getCsrfToken = () => {
  return csrfToken;
};

const clearCsrfToken = () => {
  csrfToken = null;
};

const generateClientInfoHeader = () => ('{}');

export default {
  get: (url, cb, nonToken = false, returnToResponse = null) => {
    const req = superagent.get(url);

    // req.set('Client-Info', generateClientInfoHeader());
    return req
      .timeout({ response: TIMEOUT_SECONDS })
      .retry(RETRY_TIME)
      .then(
        function onSuccess(res) {
          setCsrfToken((res && res.headers) || null);

          return checkDataResponse(res.body);
        },
        function onError(err) {
          return typeof cb === 'function' ? cb(err) : errorHandle(err);
        }
      );
  },
  put: (url, body) =>
    superagent
      .put(url, body)
      .use(tokenPlugin)
      .timeout({ response: TIMEOUT_SECONDS })
      .retry(RETRY_TIME)
      // .set('Client-Info', generateClientInfoHeader())
      .set('Csrf-Token', getCsrfToken())
      .then(
        function onSuccess(res) {
          setCsrfToken((res && res.headers) || null);
          return checkDataResponse(res);
        },
        function onError(err) {
          return err;
        }
      ),
  post: (url, body, cb, useCSRF = true) => {
    let req = superagent.post(url, body).use(tokenPlugin);
    if (useCSRF) {
      req.set('Csrf-Token', getCsrfToken());
    }
    return req
      .timeout({ response: TIMEOUT_SECONDS })
      .retry(RETRY_TIME)
      // .set('Client-Info', generateClientInfoHeader())
      .then(
        function onSuccess(res) {
          setCsrfToken((res && res.headers) || null);

          return checkDataResponse(res.body);
        },
        function onError(err) {
          return checkDataResponse(err);
        }
      );
  },
  del: (url, body) => {
    let agent = body ? _superagent : superagent;

    return agent
      .del(url, body)
      .use(tokenPlugin)
      .timeout({ response: TIMEOUT_SECONDS })
      .retry(RETRY_TIME)
      .set('Client-Info', generateClientInfoHeader())
      .set('Csrf-Token', getCsrfToken())
      .then(
        function onSuccess(res) {
          setCsrfToken((res && res.headers) || null);
          return checkDataResponse(res.body);
        },
        function onError(err) {
          console.log(err);
        }
      );
  },
  uploadFile,
  uploadVideo,
  submitOrder
};
