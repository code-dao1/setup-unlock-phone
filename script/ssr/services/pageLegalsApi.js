import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/page-legals`;

export default {
  getLegalPageDetail: (handle) => {
    return requests.get(`${URL_DEFAULT}/${handle}`);
  },
};
