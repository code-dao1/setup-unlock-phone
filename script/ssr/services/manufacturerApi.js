import { CORE_API } from '../constants/system/serverConfig';
import { parseGETParams } from '@/utils/system/apiURI';
import requests from './core/base';

const URL_DEFAULT = `${CORE_API}/api/manufacturers`;

export default {
  getListManufacturer: (params) => {
    return requests.post(
      URL_DEFAULT,
      params,
    );
  },
  getManufacturerInfo: (handle) => {
    return requests.get(`${URL_DEFAULT}/${handle}`);
  },
  getListGroupServiceByManufacturerId: (manufacturerId, params) => {
    return requests.get(
      parseGETParams({
        baseURI: `${URL_DEFAULT}/${manufacturerId}/service-groups`,
        params
      })
    );
  },
};
