import { changeDimension, changeScrollBody, initSystem } from '@/redux/actions/system/ui';
import { useRouter } from 'next/router';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import ProgressBar from 'react-topbar-progress-indicator';
import { closeModalOrderPhone } from '@/redux/actions/modalFormOrderPhone';
import ModalFormInputOrder from '@/components/ModalFormInputOrder';

import { TopBar } from './components';
import Footer from './components/Footer';
import ImplicitLogic from './components/ImplicitLogic';

import classes from './style.module.scss';

ProgressBar.config({
  barColors: {
    0: 'rgba(0,98,237,1)',
    1: 'rgba(0,98,237,1)'
  },
  shadowBlur: '5px',
  barThickness: '2'
});

const Product = props => {
  const { route, children, webConfig = {} } = props;
  const router = useRouter();

  const [openNavBarMobile, setOpenNavBarMobile] = useState(false);
  const isOpenModalFormOrderPhone = useSelector(state => state.modalFormOrder.isOpen);
  const dispatch = useDispatch();
  const closeModalFormOrderPhone = () => closeModalOrderPhone()(dispatch);
  const onChangeDimension = () => changeDimension()(dispatch);
  const onInitSystem = () => {
    initSystem()(dispatch);
  };
  const handleNavBarMobileOpen = () => {
    setOpenNavBarMobile(true);
  };

  const handleNavBarMobileClose = () => {
    setOpenNavBarMobile(false);
  };

  useEffect(() => {
    window.addEventListener('resize', onChangeDimension);

    return () => {
      window.removeEventListener('resize', onChangeDimension);
    }
  }, []);

  useEffect(() => {
    onInitSystem();
  }, [router]);

  return (
    <div className={classes.root}>
      <TopBar
        webConfig={webConfig}
        className={classes.topBar}
        onOpenNavBarMobile={handleNavBarMobileOpen}
      />
      <ImplicitLogic />
      <ModalFormInputOrder
        isOpen={isOpenModalFormOrderPhone}
        onClose={closeModalFormOrderPhone}
      />
      <div className={classes.container}>
        <main className={classes.content}>
          {children}
        </main>
        <Footer />
      </div>
    </div>
  );
};

Product.propTypes = {
  route: PropTypes.object
};

export default Product;
