import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { changeAff, initSystem } from '@/redux/actions/system/ui';
import { getParamByUrl } from '@/utils/system/apiURI';

const ImplicitLogic = (props) => {
  const affCurrent = useSelector(state => state.ui.aff);
  const dispatch = useDispatch();
  const changeAffToRedux = () => changeAff()(dispatch);
  useEffect(() => {
    initSystem()(dispatch);
    const affParam = getParamByUrl(window.location.href, 'aff');
    if (!affParam && affCurrent !== affParam) {
      changeAffToRedux(affParam);
    }
  }, []);

  return null;
};

ImplicitLogic.propTypes = {};

ImplicitLogic.defaultProps = {};

export default ImplicitLogic;
