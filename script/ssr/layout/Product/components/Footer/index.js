// import { faEnvelope, faTimes } from '@fortawesome/free-solid-svg-icons';
import { publicRuntimeConfig } from '@/constants/system/serverConfig';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import PropTypes from 'prop-types';

// import './style.scss';
import { classList } from '@/utils/system/ui';
import MenuItem from './MenuItem';

const LIST_MENU = [
  {
    nameGroup: 'Browse',
    items: [
      {
        name: 'Helpful Links',
      },
      {
        name: 'Home',
        url: '/'
      },
      {
        name: 'Phones',
        url: '/phones'
      },
      {
        name: 'Blog',
        url: '/blog'
      },
    ]
  },
  {
    items: [
      {
        name: 'Services',
      },
      {
        name: 'Order-Tracking',
        url: '/tracking'
      },
      {
        name: 'Phone Unlock',
        url: '/phones'
      },
      {
        name: 'iCloud Unlock',
        url: '/service/unlock-icloud-45'
      },
    ]
  },
  {
    items: [
      {
        name: 'Support',
      },
      {
        name: 'Search Support',
        url: '/support'
      },
      {
        name: 'Order Tracking',
        url: '/tracking'
      },
      {
        name: 'Frequently Asked Questions',
        url: '/frequestly-asked-questions'
      },
    ]
  },
]

const Footer = (props) => {
  const [indexOpenMenu, setIndexOpenMenu] = useState(-1);

  return (
    <footer id="footer">
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-sm-12 col-xs-12">
            <h4 className="hidden-xs">Contact</h4>
            <ul className="list-unstyled">
              <li className="list-title hidden-xs">Enquires</li>
              <li>
                <span style={{ width: '20px', display: 'inline-block' }}>
                  {/*<FontAwesomeIcon*/}
                  {/*  icon={faEnvelope}*/}
                  {/*/>*/}
                  <i className="fa fa-envelope"/>
                </span>
                <span>support@expressunlock.com</span>
              </li>
            </ul>
          </div>
          <div className="col-md-8 col-sm-12 col-xs-12">
            <div className="row">
              {
                LIST_MENU.map((menu, index) => {
                  return <MenuItem
                    key={`menu-footer-${index}`}
                    index={index}
                    setIndexOpenMenu={setIndexOpenMenu}
                    isOpen={index === indexOpenMenu}
                    nameGroup={menu.nameGroup}
                    className={'col-sm-4 col-xs-12'}
                    items={menu.items}
                  />
                })
              }
            </div>
          </div>
        </div>
      </div>
      <div className="footer-copy">
        <div className="container">
          <hr/>
          <ul className="payment-methods list-unstyled list-inline">
            <li>
              <img
                data-target="/Images/Payment-Cards/visa.jpg"
                src="/media/images/visa.jpg"
                alt="Visa"
                className="img-responsive"
              />
            </li>
            <li>
              <img
                src="/media/images/mastercard-dark.jpg"
                alt="Mastercard"
                className="img-responsive"
              />
            </li>
            <li>
              <img
                src="/media/images/amex.jpg"
                alt="AMEX"
                className="img-responsive"
              />
            </li>
            <li>
              <img
                src="/media/images/paypal-logo.jpg"
                alt="Paypal"
                className="img-responsive"
              />
            </li>
          </ul>
          <div className="row">
            <div className="col-md-8 col-md-offset-2">
              <ul className="list-unstyled list-inline text-center">
                <li>{publicRuntimeConfig.productBrand} operates in:</li>
                <li><a href="" title="United Kingdom">United Kingdom</a></li>
                <li><a href="" title="Canada">Canada</a></li>
                <li><a href="" title="United States">United States</a></li>
                <li><a href="" title="Ireland">Ireland</a></li>
                <li><a href="" title="Australia">Australia</a></li>
                <li><a href="" title="Philippines">Philippines</a></li>
                <li><a href="" title="India">India</a></li>
                <li><a href="" title="Spain">Spain</a></li>
                <li><a href="" title="Japan">Japan</a></li>
                <li><a href="" title="Mexico">Mexico</a></li>
                <li><a href="" title="Portugal">Portugal</a></li>
                <li><a href="" title="Brazil">Brazil</a></li>
                <li><a href="" title="Italy">Italy</a></li>
                <li><a href="" title="Germany">Germany</a></li>
                <li><a href="" title="France">France</a></li>
              </ul>
              <p><small>All third party brands and logos are the registered trademarks of their respected owners. This
                website is neither affiliated nor part of any of the network operators / handset manufacturers
                detailed on our website.
              </small>
              </p>
            </div>
          </div>
          <hr/>
        </div>
      </div>
      <div className="footer-end">
        <div className="container">
          <div className="row">
            <div className="col-sm-3 col-xs-12"/>
            <div className="col-sm-6 col-xs-12">
              <ul className="list-inline text-center center-block">
                <li>
                  <a href="/terms-and-conditions" title="Terms and Conditions">Terms &amp; Conditions</a>
                </li>
                <li>
                  <a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a>
                </li>
                <li>
                  <a href="/support" title="Site Map">Support</a>
                </li>
              </ul>
            </div>
            <div className="col-sm-3 col-xs-12">
              {/*<ul className="list-inline pull-right hidden">*/}
              {/*  <li>*/}
              {/*    <a href="" title="" target="_blank">*/}
              {/*      <i className="fa fa-facebook"/>*/}
              {/*    </a>*/}
              {/*  </li>*/}
              {/*  <li>*/}
              {/*    <a href="" title="" target="_blank">*/}
              {/*      <i className="fa fa-twitter"/>*/}
              {/*    </a>*/}
              {/*  </li>*/}
              {/*  <li>*/}
              {/*    <a href="" title="" target="_blank">*/}
              {/*      <i className="fa fa-google-plus"/>*/}
              {/*    </a>*/}
              {/*  </li>*/}
              {/*</ul>*/}
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

Footer.propTypes = {};

Footer.defaultProps = {};

export default Footer;
