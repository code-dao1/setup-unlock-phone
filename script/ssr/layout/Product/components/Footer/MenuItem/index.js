import React from 'react';
import PropTypes from 'prop-types';
import Link from '@/components/common/Link';
import { classList } from '@/utils/system/ui';

import classes from './style.module.scss';

const MenuItem = (props) => {
  const {
    index,
    isOpen,
    nameGroup,
    className,
    items,

    setIndexOpenMenu,
  } = props;

  const toggleMenu = () => {
    if (isOpen) {
      setIndexOpenMenu(-1);
    } else {
      setIndexOpenMenu(index);
    }
  };

  return (
    <div className={classList(className, classes.menuItem)}>
      {
        nameGroup && <br className="hidden-lg hidden-md hidden-xs"/>
      }
      <h4 className="hidden-xs">{nameGroup || ' '}</h4>
      <ul className={classList(isOpen && classes.isOpen, 'list-unstyled', classes.menuContent)}>
        {
          items.map((item, index) => {
            const {
              name,
              url,
            } = item;
            const key = `item-footer-${index}-${url}-${name}`;
            if (url) {
              return (
                <li key={key}>
                  <Link to={url} title={name}>{name}</Link>
                </li>
              );
            }
            return (
              // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
              <li
                key={key}
                className={classList(isOpen && 'active', 'list-title')}
                onClick={toggleMenu}
              >
                {name}
              </li>);
          })
        }
      </ul>
    </div>
  );
};

MenuItem.propTypes = {};

MenuItem.defaultProps = {};

export default MenuItem;
