import React, { useState, useRef } from 'react';
import { connect } from 'react-redux';
import Dropdown from '@/components/common/Dropdown';
import Icon from '@/components/common/Icon';
import { classList } from '@/utils/system/ui';
import SiteNavItem from './SiteNavItem';

import classes from './style.module.scss';

const SiteNav = ({ items = [], deviceWidth }) => {
  const widthPercentShow = 0.7;
  const widthInit = 110;
  const numShowItem = Math.floor((deviceWidth || 1440) * widthPercentShow / widthInit);
  const numDropdown = items.length > numShowItem ? items.length - numShowItem : 0;
  const refButtonDropdown = useRef(null);

  const [isShowDropDown, setShowDropDown] = useState(false);
  return (
    <div className={classList(classes.siteNav, 'm0 p0 flex items-center')}>
      {
        items.slice(0, numShowItem).map(({ name, link }, index) => {
          return (<SiteNavItem
            key={`site-nav-item-${index}`}
            classNameLink={classes.siteNavLink}
            name={name}
            link={link}
          />);
        })
      }
      {
        numDropdown > 0 &&
        (<SiteNavItem
          ref={refButtonDropdown}
          className={classList(classes.siteNavHasDropdown, classes.siteNavMoreSection, 'm0 flex items-center')}
        >
          <Icon
            className={classes.siteNavMore}
            iconName={'menu-more-icon'}
            onClick={() => {
              setShowDropDown(!isShowDropDown);
            }}
          />

          <Dropdown
            targetButton={refButtonDropdown && refButtonDropdown.current}
            isActive={isShowDropDown}
            closeOnClickOutSide
            close={(status) => setShowDropDown(status)}
            className={classList(classes.siteNavDropdown, 'm0 px0 py10')}
          >
            {
              items.slice(numShowItem).map(({ name, link }, index) => {
                return (<SiteNavItem
                  key={`site-nav-item-${index + numShowItem}`}
                  className={classes.siteNavItem}
                  classNameLink={classList(classes.siteNavLink, classes.siteNavDropdownLink)}
                  name={name}
                  link={link}
                />);
              })
            }
          </Dropdown>
        </SiteNavItem>)
      }
    </div>
  );
};

export default connect(
  state => ({
    deviceWidth: state.ui.deviceWidth,
  })
)(SiteNav);
