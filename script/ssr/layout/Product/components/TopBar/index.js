import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Link from '@/components/common/Link';
import { closeSideNavMobile, openSideNavMobile } from '@/redux/actions/mainHeader/sideNav';
import { blurWindow, changeDimension, focusWindow } from '@/redux/actions/system/ui';
import Icon from '@/components/common/Icon';
import Image from '@/components/common/Image';
import SideNavMobile from '@/components/common/mobile/SideNavMobile';
import { SITE_NAV } from '@/constants/system/siteNav';
import { addZeroFontNumber } from '@/utils/formatNumber';
import { classList } from '@/utils/system/ui';
import { publicRuntimeConfig } from '@/constants/system/serverConfig'
import SiteNav from './SideNav';

import classes from './style.module.scss';

class MainHeader extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isShowDropdownCart: false
    };
  }

  componentDidMount() {
    if (!this.props.isInitCart && this.props.cartToken) {
      this.props.initCart(this.props.cartToken);
    }
  }

  render() {
    const {
      isShowDropdownCart
    } = this.state;

    const {
      isMobileScreen,
      isTabletScreen,
      isDesktopScreen,
      webConfig,

      isOpenSideNavMobile,
      openSideNavMobile,
      closeSideNavMobile,

      numItemInCart
    } = this.props;

    const {
      logo = publicRuntimeConfig.logo
    } = webConfig || {}

    return (<section className={classList(classes.header, classes.responsive, isOpenSideNavMobile && classes.isOpenSideNav)}>
      <div className="container">
        <div className={classList('row relative', classes.headerWrapper)}>
          <div className={classList(classes.logoCol, 'col-8 col-sm-2 flex items-center justify-center')}>
            <div className={classes.logo}>
              <Link to={'/'} className={classList(classes.logoText, 'items-center flex')}>
                {
                  !isDesktopScreen &&
                  (<Image
                    src={logo}
                    alt="Unlock phone"
                    className={classes.fitLogo}
                  />)
                }
                {
                  isDesktopScreen &&
                  (<Image
                    src={logo}
                    alt="Unlock phone"
                    className={classes.fitLogo}
                  />)
                }
              </Link>
            </div>
          </div>
          <div className={classList(classes.menuContentCol, 'flex items-center')}>
            <div className={classList(classes.menuContentDesk, 'text-align-center flex justify-center items-center w-100')}>
              <SiteNav
                items={SITE_NAV}
              />
            </div>
            <div className={classes.menuContentMobile}>
              {
                isOpenSideNavMobile &&
                (<i
                  className="fa fa-times"
                  style={{ color: '#fff' }}
                  onClick={closeSideNavMobile}
                />)
              }
              {
                !isOpenSideNavMobile &&
                (
                  <Icon
                    iconName={'menu-icon'}
                    className={classes.buttonMenu}
                    onClick={openSideNavMobile}
                  />
                )
              }
            </div>
          </div>
        </div>
      </div>
      {
        isMobileScreen && (
        <SideNavMobile/>
        )
      }
    </section>);
  }
}

MainHeader.propTypes = {};

MainHeader.defaultProps = {};

export default connect(
  state => ({
    isMobileScreen: state.ui.isMobileScreen,
    isTabletScreen: state.ui.isTabletScreen,
    isDesktopScreen: state.ui.isDesktopScreen,
    isOpenSideNavMobile: state.sideNav.isOpenSideNavMobile,
    appConfig: state.config.appConfig,
  }), {
    openSideNavMobile,
    closeSideNavMobile,
    changeDimension,
    focusWindow,
    blurWindow,
  }
)(MainHeader);
