import { PHONE_ID } from '@/constants/system/paramsRoute';
import Product from '@/layout/Product';
import phoneApi from '@/services/phoneApi';
import { isErrorResponse } from '@/utils/typeof';
import Head from 'next/head';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import scriptApi from '@/services/systemApi';

import classes from './style.module.scss';

const CheckoutPaymentFailed = (props) => {

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  const code = props.code || '';
  return (
    <div id="wrapper">
      <Head>
        <title>Checkout payment failed - {productBrand}</title>
        <meta key="og:title" property="og:title" content={`Checkout payment failed - ${productBrand}`}/>
        { code && <head dangerouslySetInnerHTML={{__html: code}} /> }
      </Head>
      <div className={classes.paymentFailed}>
        <section className="section has-background is-large has-background-danger-light">
          <div className="container has-text-centered">
            <h1 className="title mb-6">We were unable to process your payment!</h1>
            <p className="subtitle">
              Something went wrong and we were unable to process your payment <br/>
              Your PayPal / Credit card hasn't been charged.
            </p>
            <p className="subtitle is-6">If you think this is an error, please don't hesitate to contact us.</p>
            <p><a href="/" className="button btn btn-primary is-primary is-medium router-link-active">Back to home page</a></p>
          </div>
        </section>
      </div>
    </div>
  );
};

CheckoutPaymentFailed.propTypes = {};

CheckoutPaymentFailed.defaultProps = {};

CheckoutPaymentFailed.Layout = Product;

export async function getServerSideProps(context) {
  const rs = await scriptApi.getScriptByType('payment_success')
  let code = '';
  if (rs && rs.id && rs.code) {
    code = rs.code;
  }
  console.log(code);
  return {
    props: {
      code,
      productBrand: process.env.PRODUCT_BRAND,
    }, // will be passed to the page component as props
  }
}

export default CheckoutPaymentFailed;
