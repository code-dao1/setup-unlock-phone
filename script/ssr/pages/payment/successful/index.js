import Product from '@/layout/Product';
import CheckoutPaymentFailed from '@/pages/payment/failed';
import Head from 'next/head';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import scriptApi from '@/services/systemApi';

const CheckoutPaymentSuccess = (props) => {

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  const code = props.code || '';
  return (
    <div id="wrapper">
      <Head>
        <title>Checkout payment successful - {productBrand}</title>
        <meta key="og:title" property="og:title" content={`Checkout payment successful - ${productBrand}`}/>
        { code && <head dangerouslySetInnerHTML={{__html: code}} /> }
      </Head>
      <div id="background-img">
        <div
          id="background-img"
          style={{
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center center',
            backgroundImage: 'url("/media/images/thank-you.png")',
            width: '60vh',
            height: '70vh',
            margin: 'auto'
          }}
        >
          <div className={'content'}/>
        </div>
      </div>
    </div>
  );
};

CheckoutPaymentSuccess.propTypes = {};

CheckoutPaymentSuccess.defaultProps = {};

CheckoutPaymentSuccess.Layout = Product;

export async function getServerSideProps(context) {
  const rs = await scriptApi.getScriptByType('payment_success')
  let code = '';
  if (rs && rs.id && rs.code) {
    code = rs.code;
  }
  console.log(code);
  return {
    props: {
      code,
      productBrand: process.env.PRODUCT_BRAND,
    }, // will be passed to the page component as props
  }
}

export default CheckoutPaymentSuccess;
