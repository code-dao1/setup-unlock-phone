import { PAGE, PAGE_SIZE } from '@/constants/system/service';
import Product from '@/layout/Product';
import phoneApi from '@/services/phoneApi';
import Head from 'next/head';
import React from 'react';
import PropTypes from 'prop-types';
import ListDevice from '@/components/ListDevice';

const Phones = (props) => {
  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  return (
    <div
      id="wrapper"
    >
      <Head>
        <title>Phones - {productBrand}</title>
        <meta name="description"
              content="We offer an unlocking service for all Smart phone models, our easy and affordable phone unlocking service is compatible with all versions – find your device here." />
        <meta key="og:title" property="og:title" content={`Phones - ${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content="We offer an unlocking service for all Smart phone models, our easy and affordable phone unlocking service is compatible with all versions – find your device here."/>
      </Head>
      <div id="list-phone">
        <div className="body-content">
          <ListDevice
            isInfiniteScroll
            isShowSearch
            data={props.phones && props.phones.items}
          />
        </div>
      </div>
    </div>
  );
};

Phones.propTypes = {};

Phones.defaultProps = {};

Phones.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  const phones = await phoneApi.getListPhone({
    [PAGE]: 1,
    [PAGE_SIZE]: 20,
  });

  return {
    props: {
      phones: phones,
      productBrand: process.env.PRODUCT_BRAND
    }, // will be passed to the page component as props
  }
}

export default Phones;
