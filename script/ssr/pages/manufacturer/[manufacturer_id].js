import Image from '@/components/common/Image';
import Loading from '@/components/common/Loading';
import FormInputUnlockPhone from '@/components/FormInputUnlockPhone';
import HowToUnlockManufacturer from '@/components/HowToUnlockManufacturer';
import ListDevice from '@/components/ListDevice';
import ListReviewPage from '@/components/ListReviewPage';
import ListServiceVertical from '@/components/ListServiceVertical';
import UnbeatableAndTrackUnlock from '@/components/PromoteBrand/UnbeatableAndTrackUnlock';
import StepUnlockInPhone from '@/components/StepUnlockInPhone';
import UnlockFAQ from '@/components/UnlockFAQ';

// import './style.scss';
import { MANUFACTURER_ID, SERVICE_ID } from '@/constants/system/paramsRoute';
import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import Product from '@/layout/Product';
import manufacturerApi from '@/services/manufacturerApi';
import serviceApi from '@/services/serviceApi';
import { getParamByRouter } from '@/utils/history';
import { isErrorResponse } from '@/utils/typeof';
import Head from 'next/head';
import { useRouter } from 'next/router'
import React, { useEffect, useMemo, useState } from 'react';
import { Redirect } from 'react-router';

const TitleFAQ = (props) => {
  const {
    id,
    created_at,
    created_by,
    handle,
    image_src,
    image_title,
    name,
  } = props.manufacturer;

  return (<div className="row">
    <div className="col-xs-12 text-center">
      <h2>Phone of {name} Unlock FAQ</h2>
      <br/>
      <p className="lead">How to unlock Phone of {name} questions answered.</p>
      <br/>
      <br/>
    </div>
  </div>);
}

const ManufacturerDetail = (props) => {
  const router = useRouter();
  const manufacturerHandle = props.manufacturerHandle;
  const [loading, setLoading] = useState(true);
  const [manufacturer, setManufacturer] = useState(props.data);

  useEffect(() => {
    setLoading(true);
    manufacturerApi.getManufacturerInfo(manufacturerHandle)
      .then(rs => {
        if (!isErrorResponse(rs) && rs.id) {
          setManufacturer(rs);
        }
        setLoading(false);
      });
  }, [manufacturerHandle]);

  if (!manufacturer && loading) {
    return (
      <Loading isMessage={false}/>
    );
  }

  if (!manufacturer) {
    router.push('/');
  }

  const {
    id,
    created_at,
    created_by,
    handle,
    seo_description,
    seo_title,
    image_src,
    image_title,
    title_detail,
    name,
  } = manufacturer;

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  const titleSEOShow = seo_title || `${name} - ${productBrand}`;
  const descriptionSEOShow = seo_description || `Unlock your ${name} quickly and easily with ${productBrand}. Using our secure online unlocking service, all our unlocks have a success rate of 100% without affecting performance or any outstanding warranty on your phone, enjoy peace of mind with a hassle-free service.`;
  return (
    <div id="wrapper">
      <Head>
        <title>{titleSEOShow}</title>
        <meta name="description"
              content={descriptionSEOShow} />
        <meta key="og:title" property="og:title" content={titleSEOShow}/>
        <meta key="og:description" property="og:description"
              content={descriptionSEOShow}/>
        {
          image_src &&
          (<meta key="og:image" property="og:image" content={image_src}/>)
        }
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section style={{ padding: '5vh 0', backgroundColor: '#f8f8f8' }}>
            <div className="container">
              <div className="row">
                <div className="col-xs-12 text-center">
                  <h1>{title_detail || name}</h1>
                  <br/>
                  <br/>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6 col-xs-12">
                  <div className="row">
                    <div className="col-md-10 col-md-offset-1 col-xs-8 col-xs-offset-2">
                      <Image
                        src={image_src}
                        alt={name}
                        className="img-responsive"
                        style={{ position: 'relative' }}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-xs-12">
                  <div className="row">
                    <div className="col-md-11 col-xs-12">
                      <br/>
                      <br/>
                      <p>
                        Unlock your <b>{name}</b> quickly and easily with <span className="notranslate">{publicRuntimeConfig.productBrand}</span>.
                        Using our secure online unlocking service, all our unlocks have a success rate of
                        100% without affecting performance or any outstanding warranty on your phone, enjoy peace of
                        mind with a hassle-free service.
                      </p>
                      <br/>
                      <div id="OIPUForm">
                        <label className="control-label text-grey">Let&apos;s get started:</label>
                        <FormInputUnlockPhone manufacturerId={id}/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <ListServiceVertical manufacturerId={manufacturer.id}/>
          <HowToUnlockManufacturer manufacturer={manufacturer}/>
          <ListDevice
            manufacturerId={id}
          />
          <UnlockFAQ
            title={<TitleFAQ manufacturer={manufacturer}/>}
          />
          <UnbeatableAndTrackUnlock/>
          <StepUnlockInPhone/>
          <ListReviewPage/>
        </div>
      </div>
    </div>
  );
};

ManufacturerDetail.propTypes = {};

ManufacturerDetail.defaultProps = {};

ManufacturerDetail.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;
  const manufacturerHandle = params[MANUFACTURER_ID];

  const rs = await manufacturerApi.getManufacturerInfo(manufacturerHandle);

  if (context.res && isErrorResponse(rs)) {
    context.res.writeHead(302, { Location: '/' });
    context.res.end();
  }

  return {
    props: {
      data: rs,
      productBrand: process.env.PRODUCT_BRAND,
      manufacturerHandle
    }, // will be passed to the page component as props
  }
}

export default ManufacturerDetail;
