import { IMEI, PHONE_ID, SERVICE_ID } from '@/constants/system/paramsRoute';
import Product from '@/layout/Product';
import ResultChecking from '@/pages/imei-check/result';
import systemApi from '@/services/systemApi';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Loading from '@/components/common/Loading';
import CustomerOrder from '@/components/CustomerOrder';
import ListReviewPage from '@/components/ListReviewPage';
import { ORDER_API } from '@/constants/system/serverConfig';
import checkoutApi from '@/services/checkoutApi';
import { getParamByRouter } from '@/utils/history';
import { validateEmail } from '@/utils/inputValidate';
import { getMessage, isErrorResponse } from '@/utils/typeof';
import { Redirect } from 'react-router';

const CheckoutAccountDetail = (props) => {
  const router = useRouter();
  const phoneIdFromProps = props.phoneId;
  const serviceIdFromProps = props.serviceId;
  const imeiFromProps = props.imei;
  const affCurrent = useSelector(state => state.ui.aff);
  const refForm = useRef();
  const [orderInfo, setOrderInfo] = useState();
  const [email, setEmail] = useState('');
  const [confirmEmail, setConfirmEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState({});
  const [receiveNewsLetter, setReceiveNewsLetter] = useState(true);

  if (!phoneIdFromProps || !serviceIdFromProps || !imeiFromProps) {
    console.log(phoneIdFromProps, serviceIdFromProps, imeiFromProps)
    router.push('/');
  }

  useEffect(() => {
    checkoutApi.getOrderInfo(phoneIdFromProps, serviceIdFromProps).then(rs => {
      if (!isErrorResponse(rs)) {
        setOrderInfo(rs);
      } else {
        setOrderInfo(null);
      }
    });
  }, [phoneIdFromProps, serviceIdFromProps]);

  if (!orderInfo) {
    return <Loading isMessage={false}/>;
  }

  const {
    created_at,
    id,
    is_active,
    phone,
    phone_id,
    price,
    reference_id,
    service,
    service_id,
    checkout_config = {}
  } = orderInfo;
  const {
    merchant_id,
    optional_1
  } = checkout_config;

  const cancel = () => router.history.goBack();
  const next = () => {
    let errorMessage = {};
    if (!email) {
      errorMessage.email = 'Please enter your email.';
    } else if (!validateEmail(email)) {
      errorMessage.email = 'Email invalid.';
    }

    if (!confirmEmail) {
      errorMessage.confirmEmail = 'Please confirm your email.';
    } else if (confirmEmail !== email) {
      errorMessage.confirmEmail = 'Confirm email and email must be equal';
    }
    setErrorMessage(errorMessage);

    if (Object.keys(errorMessage).length === 0) {
      if (refForm.current) {
        refForm.current.submit()
      }
    }
  };

  const changeEmail = e => setEmail(e.target.value);
  const changeConfirmEmail = e => setConfirmEmail(e.target.value);
  const changeReceiveNewsLetter = e => setReceiveNewsLetter(!receiveNewsLetter);

  return (
    <div id="wrapper">
      <Head>
        <title>Checkout phone</title>
        <meta
          name="description"
          content={`Checkout phone`}
        />
        <meta key="og:title" property="og:title" content="Checkout phone"/>
        <meta
          key="og:description"
          property="og:description"
          content="Checkout phone"
        />
      </Head>
      <div id="background-img">
        <div className="body-content">

          <form ref={refForm} id="frmLogin" action={ORDER_API} method="post" style={{ display: 'none' }}>
            <input name="amount" value={price} />
            <input name="description" value={`${phone.name} / (IMEI: ${imeiFromProps}) / ${service.name}`} />
            <input name="email_address" value={email} />
            <input name="merchant_id" value={merchant_id} />
            <input name="optional_1" value={optional_1} />
            <input name="optional_2" value={phone.code} />
            <input name="optional_3" value={service.code} />
            <input name="optional_4" value="lfp=5fb3eeb1f1d8d" />
            {/*<input name="optional_5" value={`${price || ''}`} />*/}
            <input name="order_id" value={`${imeiFromProps}`} />
          </form>

          <section id="checkout">
            <div className="container">
              <div className="panel page-panel">
                <div className="panel-heading">
                  <h3>Create an Order</h3>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-4 col-md-push-8 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                      <CustomerOrder
                        imei={imeiFromProps}
                        orderInfo={orderInfo}
                      />
                    </div>
                    <div className="col-md-8 col-md-pull-4 col-sm-12 col-sm-pull-0 col-xs-12 col-xs-pull-0">
                      <div noValidate="novalidate">
                        <div className="form-block">
                          <div className="row">
                            <div className="col-sm-8 col-xs-10">
                              <strong>Please enter a preferred email address:</strong>
                              <br/>
                              <br/>
                            </div>
                            <div className="col-xs-2" />
                          </div>
                          <div className="row">
                            <div className="col-md-10 col-sm-12 col-xs-12">
                              <div className="form-group">
                                <div className="input-group">
                                  <input
                                    className="form-control"
                                    data-val="true"
                                    data-val-email="Invalid email address."
                                    id="EmailAddress"
                                    name="EmailAddress"
                                    placeholder="Email address"
                                    type="text"
                                    value={email}
                                    onChange={changeEmail}
                                  />
                                  <span className="input-group-addon">
                                    <i className="fa fa-envelope" />
                                  </span>
                                </div>
                                {
                                  errorMessage.email &&
                                  (
                                    <span className="field-validation-error">
                                      <span
                                        className=""
                                      >
                                        {errorMessage.email}
                                      </span>
                                    </span>
                                  )
                                }
                              </div>
                              <div className="form-group">
                                <input
                                  autoComplete="off"
                                  autofill="off"
                                  className="form-control"
                                  data-val="true"
                                  data-val-equalto="Emails must match."
                                  data-val-equalto-other="EmailAddress"
                                  id="EmailAddressConfirmation"
                                  name="EmailAddressConfirmation"
                                  placeholder="Confirm email"
                                  type="text"
                                  value={confirmEmail}
                                  onChange={changeConfirmEmail}
                                />
                                {
                                  errorMessage.confirmEmail &&
                                  (
                                    <span className="field-validation-error">
                                      <span
                                        className=""
                                      >
                                        {errorMessage.confirmEmail}
                                      </span>
                                    </span>
                                  )
                                }
                              </div>
                              <div className="info" id="account-help">
                                <small>For confirmation of service only, we will not send junk mail, share or keep your
                                  email address once the service has been provided.
                                </small>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-10 col-sm-12 col-xs-12">
                              <div className="panel info-panel">
                                <div className="panel-body">
                                  <div className="checkbox">
                                    <label>
                                      <input
                                        data-val="true"
                                        data-val-required="The Newsletter field is required."
                                        id="Newsletter"
                                        name="Newsletter"
                                        type="checkbox"
                                        checked={receiveNewsLetter}
                                        onChange={changeReceiveNewsLetter}
                                      />
                                      <input name="Newsletter" type="hidden" value="false"/> I would like to receive
                                      newsletter and promotional emails.
                                    </label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <br/>
                          <div className="row hidden">
                            <div className="col-md-10 col-sm-12 col-xs-12">
                              <br/>
                              <p className="text-grey">Did you know that 1% of people enter their details in wrong?</p>
                              <p className="text-grey">
                                Please ensure you have checked your email, device model, network and IMEI number
                                provided. If your device information is incorrect then we will be unable to complete
                                your unlock and a
                                refund can not be provided.
                              </p>
                              <br/>
                            </div>
                          </div>
                        </div>
                        <div className="form-block">
                          <div className="button-controls">
                            <div className="row">
                              <div className="col-xs-6">
                                <div className="row">
                                  <div className="col-sm-6 col-xs-12">
                                    <button
                                      type="button"
                                      className="btn btn-default btn-sm"
                                      onClick={cancel}
                                    >Cancel
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div className="col-xs-6">
                                <div className="row">
                                  <div className="col-sm-6 col-sm-offset-6 col-xs-12 col-xs-offset-0">
                                    <button
                                      className="btn btn-primary btn-sm pull-right"
                                      onClick={next}
                                    >
                                      Next <i className="fa fa-chevron-circle-right" />
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <ListReviewPage />
        </div>
      </div>
    </div>
  );
};

CheckoutAccountDetail.propTypes = {};

CheckoutAccountDetail.defaultProps = {};

CheckoutAccountDetail.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params,
    query
  } = context;
  const phoneId = query[PHONE_ID];
  const serviceId = query[SERVICE_ID];
  const imei = query[IMEI];

  return {
    props: {
      phoneId,
      serviceId,
      imei
    }, // will be passed to the page component as props
  }
}

export default CheckoutAccountDetail;

