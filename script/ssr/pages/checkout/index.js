import Product from '@/layout/Product';
import Head from 'next/head';
import React from 'react';
import PropTypes from 'prop-types';

// import './style.scss';
import ListReviewPage from '@/components/ListReviewPage';

const Checkout = (props) => {
  return (
    <div id="wrapper">
      <Head>
        <title>Checkout phone</title>
        <meta
          name="description"
          content={`Checkout phone`}
        />
        <meta key="og:title" property="og:title" content="Checkout phone"/>
        <meta
          key="og:description"
          property="og:description"
          content="Checkout phone"
        />
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section id="checkout">
            <div className="container">
              <div className="panel page-panel">
                <div className="panel-heading">
                  <i className="fa fa-check-square-o fa-2x pull-left" style={{ position: 'relative', top: '3px' }}/>
                  <h3>Verification Required</h3>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-4 col-md-push-8 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                      <div id="CustomerOrder">
                        <div className="panel">
                          <div className="panel-heading">
                            <h4>Order Summary</h4>
                          </div>
                          <div className="panel-body">
                            <div className="col-split">
                              <div className="row">
                                <div className="order-line">
                                  <div className="col-xs-3">
                                    <img
                                      src="/Images/Devices/Phone-XS-Max-Thumb.jpg"
                                      alt=""
                                      className="img-responsive"
                                    />
                                  </div>
                                  <div className="col-xs-6 no-pad">
                                    <strong> Phone Unlock </strong>
                                    Phone XS Max - EE UK <small className="IMEI">357291099801817</small>
                                    <small style={{ margin: '-10px 0 15px 0' }} className="ordertype">&nbsp;</small>
                                  </div>
                                  <div className="col-xs-3 text-right">
                                    <span className="line-price">&nbsp;</span>
                                  </div>
                                  <div className="col-xs-12">
                                    <hr/>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="panel-footer" style={{ display: 'none' }}>
                            <table style={{ width: '100%' }}>
                              <tbody>
                                <tr>
                                  <td align="left" valign="middle">
                                    Average Delivery Time:
                                  </td>
                                  <td align="right" valign="middle">
                                    1-24 hours
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top">
                                    <strong style={{ fontSize: '1.5em' }}>Total</strong>
                                  </td>
                                  <td align="right" valign="middle">
                                    <strong style={{ fontSize: '1.5em' }}>£19.99</strong>
                                    <br/>
                                    <small>(GBP)</small>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-8 col-md-pull-4 col-sm-12 col-sm-pull-0 col-xs-12 col-xs-pull-0">
                      <form id="VerificationForm" action="/checkout/Verification" method="post" noValidate="novalidate">
                        <style type="text/css" />
                        <div className="form-block">
                          <div className="row">
                            <div className="col-sm-8 col-xs-10">
                              <strong>Are you the original owner of the Apple device?</strong>
                              <br/>
                              <br/>
                              <div id="DeviceOwner">
                                <p>
                                  <input
                                    id="Owner_Yes"
                                    data-val="true"
                                    data-val-required="The Owner field is required."
                                    name="Owner"
                                    type="radio"
                                    value="True"
                                  />
                                  <label
                                    htmlFor="Owner_Yes"
                                    style={{ cursor: 'pointer', fontWeight: 'normal' }}
                                  >&nbsp;Yes
                                  </label>
                                </p>
                                <p><input id="Owner_No" checked="checked" name="Owner" type="radio" value="False"/>
                                  <label
                                    htmlFor="Owner_No"
                                    style={{ cursor: 'pointer', fontWeight: 'normal' }}
                                  >&nbsp;No
                                  </label>
                                </p>
                              </div>
                            </div>
                            <div className="col-xs-2" />
                          </div>
                        </div>
                        <div className="form-block" id="OriginalNumber" style={{ display: 'none' }}>
                          <div className="row">
                            <div className="col-md-10 col-sm-12 col-xs-12">
                              <strong>Please enter the original mobile number: <span
                                className="text-red"
                              >*
                              </span>
                              </strong>
                              <br/>
                              <br/>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-10 col-sm-12 col-xs-12">
                              <div className="form-group">
                                <div className="input-group">
                                  <input
                                    type="text"
                                    className="form-control ignore"
                                    data-val="true"
                                    data-val-required="Required"
                                    id="OrigMobNumber"
                                    name="OrigMobNumber"
                                    placeholder="Mobile Number"
                                    value=""
                                  />
                                  <span className="input-group-addon">
                                    <i className="fa fa-mobile"/>
                                  </span>
                                </div>
                                <span
                                  className="field-validation-valid"
                                  data-valmsg-for="OrigMobNumber"
                                  data-valmsg-replace="true"
                                />
                              </div>
                              <div className="info" style={{ display: 'block', height: 'auto' }}>
                                <small style={{ marginTop: 0 }}>* Please note if the wrong number is submitted this order
                                  will be rejected.
                                </small>
                                <br/>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-block">
                          <div className="row">
                            <div className="col-xs-12">
                              <strong>Please select an order type:</strong>
                              <br/>
                              <br/>
                              <div
                                className="info"
                                style={{ height: 'auto', clear: 'both', float: 'none', border: '1px solid #111', padding: '12px 15px', marginBottom: '30px', textAlign: 'center' }}
                              >
                                <strong>
                                  <small style={{ color: '#111' }}>
                                    If you are not the original owner of the apple device please use the Pre-Order
                                    service below. Or if you are the original owner but would like to take advantage of
                                    our fast bulk
                                    unlocking process and benefit from a 30% saving on your unlock, please use this
                                    service.
                                  </small>
                                </strong>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-xs-12">
                              <div className="row">
                                <div className="col-sm-6 col-xs-12">
                                  <div className="product-select disabled" id="Standard">
                                    <h3>Standard Service</h3>
                                    <hr/>
                                    <ul className="list-unstyled">
                                      <li>
                                        <i className="fa fa-clock-o"/>
                                        <span>3-10 Days Delivery</span>
                                      </li>
                                      <li>
                                        <i className="fa fa-check"/>
                                        <span>Direct Whitelisting on Apple&apos;s Database</span>
                                      </li>
                                      <li>
                                        <i className="fa fa-unlock"/>
                                        <span>Permanent Factory Unlock</span>
                                      </li>
                                      <li>
                                        <strong>&nbsp;</strong>
                                      </li>
                                    </ul>
                                    <div className="triangle-top-right">
                                      <i className="fa fa-circle"/>
                                    </div>
                                  </div>
                                  <br/>
                                </div>
                                <div className="col-sm-6 col-xs-12">
                                  <div className="product-select selected" id="Preorder">
                                    <h3>Pre-Order Service</h3>
                                    <hr/>
                                    <ul className="list-unstyled">
                                      <li>
                                        <i className="fa fa-clock-o"/>
                                        <span>24-72 hrs Delivery</span>
                                      </li>
                                      <li>
                                        <i className="fa fa-check"/>
                                        <span>Direct Whitelisting on Apple&apos;s Database</span>
                                      </li>
                                      <li>
                                        <i className="fa fa-unlock"/>
                                        <span>Permanent Factory Unlock</span>
                                      </li>
                                      <li>
                                        <i className="fa fa-certificate"/>
                                        <strong>Up to 30% Saving</strong>
                                      </li>
                                    </ul>
                                    <div className="triangle-top-right">
                                      <i className="fa fa-check"/>
                                    </div>
                                  </div>
                                  <br/>
                                </div>
                                <input value="Preorder" id="OrderType" name="OrderType" type="hidden"/>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-block">
                          <div className="button-controls">
                            <div className="row">
                              <div className="col-xs-6">
                                <div className="row">
                                  <div className="col-sm-6 col-xs-12">
                                    <button
                                      type="button"
                                      className="btn btn-default btn-sm"
                                      onClick="window.history.back()"
                                    >Cancel
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div className="col-xs-6">
                                <div className="row">
                                  <div className="col-sm-6 col-sm-offset-6 col-xs-12 col-xs-offset-0">
                                    <button type="submit" className="btn btn-primary btn-sm pull-right">Next <i
                                      className="fa fa-chevron-circle-right"
                                    />
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <ListReviewPage />
        </div>
      </div>
    </div>
  );
};

Checkout.propTypes = {};

Checkout.defaultProps = {};

Checkout.Layout = Product;

export default Checkout;
