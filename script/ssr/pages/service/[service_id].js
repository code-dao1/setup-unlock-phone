import Product from '@/layout/Product';
import Head from 'next/head';
import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import { useRouter } from 'next/router'
import Loading from '@/components/common/Loading';
import FormInputUnlockPhoneInService from '@/components/FormInputUnlockPhoneInService';
import ListReviewPage from '@/components/ListReviewPage';
import UnbeatableAndTrackUnlock from '@/components/PromoteBrand/UnbeatableAndTrackUnlock';
import ServiceDescription from '@/components/ServiceDescription';
import ServiceInformation from '@/components/ServiceInformation';
import StepUnlockInService from '@/components/StepUnlockInService';
import UnlockStep from '@/components/UnlockStep';

// import './style.scss';
import { BLOG_ID, MANUFACTURER_ID, SERVICE_ID } from '@/constants/system/paramsRoute';
import manufacturerApi from '@/services/manufacturerApi';
import serviceApi from '@/services/serviceApi';
import { getParamByRouter } from '@/utils/history';
import { isErrorResponse } from '@/utils/typeof';

const ServiceDetail = (props) => {
  const router = useRouter();
  const serviceHandle = useMemo(() => getParamByRouter(router, SERVICE_ID), [router]);
  const [loading, setLoading] = useState(true);
  const [service, setService] = useState(props.data);

  useEffect(() => {
    setLoading(true);
    serviceApi.getServiceInfo(serviceHandle)
      .then(rs => {
        if (!isErrorResponse(rs) && rs.id) {
          setService(rs);
        }
        setLoading(false);
      });
  }, [serviceHandle]);

  if (!service && loading) {
    return (
      <Loading isMessage={false}/>
    );
  }

  if (!service) {
    router.push('/');
  }

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  const {
    id,
    created_at,
    created_by,
    handle,
    seo_description,
    seo_title,
    image_src,
    image_title,
    name,
  } = service;

  const titleSeoShow = seo_title || `${name} - ${productBrand}`;
  const descriptionSeoShow = seo_description || `Unlock your ${name} Phone for use on any network worldwide. Permanent factory Phone unlocking service, safe and reliable process, guaranteed to quickly unlock your Phone from the ${name} network.`;

  return (
    <div id="wrapper">
      <Head>
        <title>{titleSeoShow}</title>
        <meta
          name="description"
          content={descriptionSeoShow} />
        <meta key="og:title" property="og:title" content={`${titleSeoShow}`}/>
        <meta key="og:description" property="og:description"
              content={descriptionSeoShow}/>
        {
          image_src &&
          (<meta key="og:image" property="og:image" content={image_src}/>)
        }
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section id="model-hero">
            <div className="container" style={{ position: 'relative' }}>
              <div className="row">
                <div className="col-md-8 col-md-offset-2 col-xs-12 col-xs-offset-0">
                  <ServiceInformation data={service}/>
                  <FormInputUnlockPhoneInService serviceId={id}/>
                </div>
              </div>
            </div>
          </section>
          <ServiceDescription data={service}/>
          <UnlockStep />
          <UnbeatableAndTrackUnlock />
          <StepUnlockInService />
          <ListReviewPage />
        </div>
      </div>
    </div>
  );
};

ServiceDetail.propTypes = {};

ServiceDetail.defaultProps = {};

ServiceDetail.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;
  const serviceHandle = params[SERVICE_ID];

  const rs = await serviceApi.getServiceInfo(serviceHandle);

  if (context.res && isErrorResponse(rs)) {
    context.res.writeHead(302, { Location: '/' });
    context.res.end();
  }

  return {
    props: {
      data: rs,
      productBrand: process.env.PRODUCT_BRAND
    }, // will be passed to the page component as props
  }
}

export default ServiceDetail;
