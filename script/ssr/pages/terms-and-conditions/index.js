import Product from '@/layout/Product';
import { extractContent } from '@/utils/string';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import HTMLContent from '@/components/common/HTMLContent';
import Loading from '@/components/common/Loading';
import { TERMS_AND_CONDITIONS } from '@/constants/legalPage';
import pageLegalsApi from '@/services/pageLegalsApi';
import { isErrorResponse } from '@/utils/typeof';

const TermsAndConditions = (props) => {
  const [data, setData] = useState(props.data);

  useEffect(() => {
    pageLegalsApi.getLegalPageDetail('terms-and-conditions')
      .then(rs => {
        if (!isErrorResponse(rs)) {
          setData(rs);
        }
      })
  }, []);

  if (!data) {
    return <Loading isMessage={false}/>;
  }

  const {
    id,
    content,
    handle,
    name,
    updated_at,
  } = data;

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  const contentText = extractContent(content) || '';
  const shortText = contentText.substr(0, 200);

  return (
    <div id="wrapper">
      <Head>
        <title>{name} - {productBrand}</title>
        <meta name="description"
              content={shortText} />
        <meta key="og:title" property="og:title" content={`${name} - ${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content={shortText}/>
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section id="terms-conditions" className="content-page">
            <div className="page-nav affix">
              <div className="container">
                <div className="row">
                  <div className="col-xs-6">
                    <h3>Legal</h3>
                  </div>
                  <div className="col-xs-6">
                    <a href="/privacy-policy" title="Privacy Policy" className="pull-right text-black">
                      <small>Privacy Poilcy</small>
                    </a>
                    <a href="/support" title="Support" className="pull-right text-black">
                      <small>Support</small>
                    </a>
                  </div>
                </div>
                <hr/>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-8">
                  <h1 className="heading-sm">{name}</h1>
                  <br/>
                  <HTMLContent content={content} />
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

TermsAndConditions.propTypes = {};

TermsAndConditions.defaultProps = {};

TermsAndConditions.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  const rs = await pageLegalsApi.getLegalPageDetail('terms-and-conditions');

  return {
    props: {
      data: rs,
      productBrand: process.env.PRODUCT_BRAND
    }, // will be passed to the page component as props
  }
}

export default TermsAndConditions;
