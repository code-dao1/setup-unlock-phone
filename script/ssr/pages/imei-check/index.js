import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import Product from '@/layout/Product';
import systemApi from '@/services/systemApi';
import { isErrorResponse } from '@/utils/typeof';
import React from 'react';
import Head from 'next/head';
import FormInputChecking from '../../components/FormInputChecking';
import ListReviewPage from '../../components/ListReviewPage';
import StepCheckingIMEI from '../../components/StepCheckingIMEI';

import classes from './style.module.scss';

const IMEICheck = (props) => {

  const {
    webConfig = {},
    config
  } = props;

  const productBrand = webConfig.brand_name || publicRuntimeConfig.productBrand;
  const {
    page_imei_title = `Phone IMEI Check Service - ${productBrand}`,
    seo_description = 'All mobile phone devices have a unique IMEI number (short for International Mobile Equipment Identity). This important piece of information is useful for a number of reasons, such as unlocking or blacklisting a phone if it has been stolen.',
    seo_title = `Phone IMEI Check Service - ${productBrand}`
  } = config;
  const imeiSeoTitle = webConfig.imei_seo_title || page_imei_title;
  const imeiSeoDescription = webConfig.imei_seo_description || seo_description;
  return (
    <div id="wrapper" className={classes.imeiCheck}>
      <Head>
        <title>{imeiSeoTitle}</title>
        <meta
          name="description"
          content={imeiSeoDescription}
        />
        <meta key="og:title" property="og:title" content={imeiSeoTitle}/>
        <meta
          key="og:description"
          property="og:description"
          content={imeiSeoDescription}
        />
      </Head>
      <div id="background-img">
        <div className="body-content">
          <FormInputChecking />
          <section className="content-section" style={{ paddingBottom: 0 }}>
            <div className="container">
              <div className="row">
                <div className="col-xs-10 col-xs-offset-1 text-center">
                  <h2>How to Get Your Phone’s IMEI Number</h2>
                  <br/>
                  <p className="lead light">
                    IMEI denotes International Mobile Equipment Identity. It is a 17 digit or 15 digit sequence of numbers that uniquely identifies your phone. Here's how to how to get your phone’s IMEI number
                  </p>
                </div>
              </div>
            </div>
            <StepCheckingIMEI />
          </section>
          <section id="IMEIContent">
            <div className="container">
              <div className="row">
                <div className="col-md-5 col-xs-12">
                  <h2>What Information Can I Find out From My IMEI number?</h2>
                  <br/>
                  <p>Your IMEI number holds the following information:</p>
                  <strong>1. Details About Your Device</strong>
                  <p>
                    An IMEI number holds details such as the phone warranty, manufacture date, specification, and system version of your device. Thus, an IMEI carrier lookup can help in unlocking or blacklisting your phone if it has been stolen. This means that this tool serves as a stolen phone checker since you can easily tell if the second-hand phone you purchased or were gifted was legitimately sourced.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-5 col-md-offset-7 col-xs-12 col-xs-offset-0">
                  <strong>2. Network Your Device is Locked to:</strong>
                  <p>
                    Free IMEI checker and unlocking services often go hand-in-hand. The reason lies in the fact that an IMEI number helps you discover the network your phone is locked to. You'll also be able to determine the country such as the U.S., the U.K, from which your device originated. Once you've detected your network, you can then get a paid unlock service.
                  </p>
                  <strong>3. Express Unlock: </strong>
                  <p>
                    IMEI check services span across Find my iPhone, SimLock Check, Carrier Check, Warranty Check, iCloud Status, Blacklist Check, Purchase Country, etc. Thus, you can verify that the used phone you're about to buy is not a stolen one. In this case, verification will be made that the phone has not been blacklisted, hence, its IMEI is CLEAN.
                  </p>
                  <p>Moreover, if you're unable to use the phone with most networks like T Mobile, Verizon, Sprint, you can verify if its IMEI has been blacklisted before proceeding to unlock your phone from your network provider</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-5 col-xs-12">
                  <h2>Why Choose Us</h2>
                  <br/>
                  <p>You can expect the following when you use our IMEI check services:</p>
                  <strong>1. Instant Check:</strong>
                  <p>
                    It only takes a few seconds to determine the information of your device when you use our tool. Whether you have a fast or slow network, you'll get the detailed information you need instantly.
                  </p>
                  <strong>2. Highly Secure:</strong>
                  <p>
                    We do not require that you download any software on your phone. Hence, this is a process that safely retrieves your phone's information.
                  </p>
                  <strong>3. No Cost:</strong>
                  <p>
                    Yes, our IMEI checker online tool is free. There are no hidden charges when you use our tool and you get all a full IMEI check.
                  </p>
                  <p>
                    Now you can check sensitive information pertaining to your phone. It's also worth noting that you need to use the best free IMEI checker at all times since it supports more phone brands. The tool will also tend not to go offline every now and then as is often the case with some IMEI check services.
                  </p>
                </div>
              </div>
            </div>
          </section>
          <section id="usp">
            <div className="container">
              <div className="row">
                <div className="col-sm-4 col-xs-12">
                  <div className="text-center">
                    <div className="icon">
                      <i className="fa fa-clock-o fa-2x" />
                    </div>
                    <h3>24 hour unlock delivery</h3>
                    <p className="lead light">Instant unlocking for many mobile networks.</p>
                  </div>
                </div>
                <div className="col-sm-4 col-xs-12">
                  <div className="text-center">
                    <div className="icon">
                      <i className="fa fa-wrench fa-2x"/>
                    </div>
                    <h3>Highly Skilled Tech Team</h3>
                    <p className="lead light">
                      Experienced in all Apple<sup>TM</sup> phone <br/>
                      products.
                    </p>
                  </div>
                </div>
                <div className="col-sm-4 col-xs-12">
                  <div className="text-center">
                    <div className="icon">
                      <i className="fa fa-mobile-phone fa-2x"/>
                    </div>
                    <h3>Your Phone is Safe</h3>
                    <p className="lead light">You keep your device throughout the entire process.</p>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <ListReviewPage />
        </div>
      </div>
    </div>
  );
};

IMEICheck.propTypes = {};

IMEICheck.defaultProps = {};

IMEICheck.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  let rs = await systemApi.getConfig();

  if (context.res && isErrorResponse(rs)) {
    rs = {};
  }

  return {
    props: {
      config: rs,
      productBrand: process.env.PRODUCT_BRAND,
    }, // will be passed to the page component as props
  }
}

export default IMEICheck;
