import Loading from '@/components/common/Loading';
import { IMEI } from '@/constants/system/paramsRoute';
import Product from '@/layout/Product';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import Image from '@/components/common/Image';
import ListReviewPage from '@/components/ListReviewPage';
import systemApi from '@/services/systemApi';
import { classList } from '@/utils/system/ui';
import { getMessage, isErrorResponse } from '@/utils/typeof';

import classes from './style.module.scss';

const ResultChecking = (props) => {
  const imei = props.imei;
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState(null);

  useEffect(async () => {
    setLoading(true);
    const rs = await systemApi.checkingPhoneByImei(imei);
    setLoading(false);

    if (!isErrorResponse(rs) && rs && rs.status !== 'failure') {
      setData(rs);
      setMessage("");
    } else {
      setData(null);
      setMessage(getMessage(rs))
    }
  }, [imei]);

  if (loading) {
    return <Loading isMessage={false}/>;
  }

  if (!data) {
    return (
      <div id="wrapper" className={classes.resultChecking}>
        <Head>
          <title>Phone IMEI Check Service</title>
          <meta
            name="description"
            content={`Result checking phone - ${message || 'Unknown device'}`}
          />
          <meta key="og:title" property="og:title" content={`Phone IMEI Check Service`}/>
          <meta
            key="og:description"
            property="og:description"
            content={`Result checking phone ${message || 'Unknown device'}`}
          />
        </Head>
        <div id="background-img">
          <div className="body-content">
            <section id="checkout">
              <div className="container">
                <div className="panel page-panel" style={{ minHeight: 'auto' }}>
                  <div className="panel-heading">
                    <h1><small>Unknown device</small></h1>
                  </div>
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-md-12">
                        {message || 'Unknown device'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <ListReviewPage />
          </div>
        </div>
      </div>
    );
  }

  return (
    <div id="wrapper" className={classes.resultChecking}>
      <Head>
        <title>{data.model} - Phone IMEI Check Service</title>
        <meta
          name="description"
          content={`Result checking phone ${data.model}`}
        />
        <meta key="og:title" property="og:title" content={`${data.model} - Phone IMEI Check Service`}/>
        <meta
          key="og:description"
          property="og:description"
          content={`Result checking phone ${data.model}`}
        />
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section id="checkout">
            <div className="container">
              <div className="panel page-panel">
                <div className="panel-heading">
                  <h1><small>{data.model}</small></h1>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="order-line">
                        <div className="col-md-3 col-xs-12" style={{ marginBottom: '32px' }}>
                          <Image
                            src={data.image}
                            alt={data.model}
                            className="img-responsive"
                          />
                        </div>
                        <div className={classList(classes.content, 'col-md-9 col-xs-12 no-pad')} style={{ marginBottom: '32px' }}>
                          <div className="text-center">
                            <p style={{ fontSize: '26px', marginBottom: '16px' }}>Information</p>
                          </div>
                          <div className={classList('row', classes.contentWrapper)}>
                            {
                              data.imei &&
                              (
                                <>
                                  <div className="col-xs-6 text-left">
                                    <p className="lead light">
                                      IMEI
                                    </p>
                                  </div>
                                  <div className="col-xs-6">
                                    <p className="lead light">
                                      {data.imei}
                                    </p>
                                  </div>
                                </>
                              )
                            }

                            {
                              data.serial &&
                              (
                                <>
                                  <div className="col-xs-6 text-left">
                                    <p className="lead light">
                                      Serial
                                    </p>
                                  </div>
                                  <div className="col-xs-6">
                                    <p className="lead light">
                                      {data.serial}
                                    </p>
                                  </div>
                                </>
                              )
                            }

                            <div className="col-xs-6 text-left">
                              <p className="lead light">
                                Apple care
                              </p>
                            </div>
                            <div className="col-xs-6">
                              <p className="lead light">
                                {data['apple-care'] || '-'}
                              </p>
                            </div>
                            <div className="col-xs-6 text-left">
                              <p className="lead light">
                                Loaner device
                              </p>
                            </div>
                            <div className="col-xs-6">
                              <p className="lead light">
                                {data['loaner-device'] || '-'}
                              </p>
                            </div>
                            <div className="col-xs-6 text-left">
                              <p className="lead light">
                                Replaced device
                              </p>
                            </div>
                            <div className="col-xs-6">
                              <p className="lead light">
                                {data['replaced-device'] || '-'}
                              </p>
                            </div>

                            <div className="col-xs-6 text-left">
                              <p className="lead light">
                                Activation status
                              </p>
                            </div>
                            <div className="col-xs-6">
                              <p className="lead light">
                                {data['activation-status'] || '-'}
                              </p>
                            </div>

                            <div className="col-xs-6 text-left">
                              <p className="lead light">
                                Refurbished Device
                              </p>
                            </div>
                            <div className="col-xs-6">
                              <p className="lead light">
                                {data['refurbished-device'] || '-'}
                              </p>
                            </div>

                            <div className="col-xs-6 text-left">
                              <p className="lead light">
                                Telephone technical support
                              </p>
                            </div>
                            <div className="col-xs-6">
                              <p className="lead light">
                                {data['telephone-technical-support'] || '-'}
                              </p>
                            </div>

                            <div className="col-xs-6 text-left">
                              <p className="lead light">
                                Repairs and service coverage
                              </p>
                            </div>
                            <div className="col-xs-6">
                              <p className="lead light">
                                {data['repairs-and-service-coverage'] || '-'}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <ListReviewPage />
        </div>
      </div>
    </div>
  );
};

ResultChecking.propTypes = {};

ResultChecking.defaultProps = {};

ResultChecking.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params,
    query
  } = context;
  const imei = query[IMEI];

  if (context.res && !imei) {
    context.res.writeHead(302, { Location: '/' });
    context.res.end();
  }

  return {
    props: {
      imei
    }, // will be passed to the page component as props
  }
}

export default ResultChecking;
