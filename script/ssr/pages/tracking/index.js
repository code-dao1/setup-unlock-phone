import Product from '@/layout/Product';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import validator from 'validator';
import Loading from '@/components/common/Loading';
import { PAGE, PAGE_SIZE } from '@/constants/system/service';
import systemApi from '@/services/systemApi';
import { validateEmail } from '@/utils/inputValidate';
import { isErrorResponse } from '@/utils/typeof';

const OrderTracking = (props) => {
  const history = useHistory();
  const [config, setConfig] = useState(props.config);
  const [errorMessage, setErrorMessage] = useState({});
  const [email, setEmail] = useState();
  const [imei, setImei] = useState();
  const [urlIframe, setUrlIframe] = useState();

  const changeEmail = (e) => setEmail(e.target.value);
  const changeImei = (e) => {
    const newIMEI = e.target.value;
    if (/^[0-9a-z]+$/.test(newIMEI) || !newIMEI) {
      setImei(e.target.value);
    }
  };

  const submit = () => {
    let error = {};

    if (!email) {
      error.email = 'Email is require';
    } else if (!validateEmail(email)) {
      error.email = 'Email invalid';
    }

    if (!imei || !imei.trim()) {
      error.imei = 'Please enter your IMEI number.';
    } else if (/^[0-9]+$/.test(imei) && !validator.isIMEI(imei)) {
      error.imei = 'IMEI invalid.';
    }

    if (Object.keys(error).length > 0) {
      setErrorMessage(error);
    } else if (config) {
      setErrorMessage({});
      const tracking_url = config && config.tracking_url;
      setUrlIframe(`${tracking_url}${imei}`);
    }
  };

  if (!config) return <Loading isMessage={false}/>;

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  return (
    <div id="wrapper">
      <Head>
        <title>Order Tracking - {productBrand}</title>
        <meta
          name="description"
          content="Login to view your Phone activation unlock progress at any point during the process."
        />
        <meta key="og:title" property="og:title" content={`Order Tracking - ${productBrand}`}/>
        <meta
          key="og:description"
          property="og:description"
          content="Login to view your Phone activation unlock progress at any point during the process."
        />
      </Head>
      {
        urlIframe &&
        (
          <iframe style={{ width: '100%', height: '80vh' }} title={'Checking by imei'} src={urlIframe} scrolling="no"/>
        )
      }
      {
        !urlIframe &&
        (
          <div id="background-img">
            <div className="body-content">
              <section id="orderlogin" className="content-page">
                <div className="page-nav affix">
                  <div className="container">
                    <div className="row">
                      <div className="col-xs-6">
                        <h3>Your Order</h3>
                      </div>
                      <div className="col-xs-6">
                        <a href="/support" title="Contact Support" className="pull-right text-black">
                          <small>Contact Support</small>
                        </a>
                        <a
                          href="/frequestly-asked-questions"
                          title="Contact Support"
                          className="pull-right text-black"
                        >
                          <small>FAQ</small>
                        </a>
                      </div>
                    </div>
                    <hr/>
                  </div>
                </div>
                <div className="container">
                  <div className="col-md-6 col-md-offset-3 text-center">
                    <h1>Track your Phone Unlock <small>View your unlock progress.</small></h1>
                    <br/>
                    <div className="row">
                      <div className="col-xs-12 text-red" />
                    </div>
                    <br/>
                    <div>
                      <div className="form-group">
                        <div className="input-group">
                          <input
                            className="form-control form-control-lg"
                            data-val="true"
                            data-val-email="Invalid email address."
                            data-val-required="Email is required."
                            id="Email"
                            name="Email"
                            placeholder="Email address"
                            type="text"
                            value={email}
                            onChange={changeEmail}
                          />
                          <span className="input-group-addon">
                        <i className="fa fa-envelope" />
                      </span>
                        </div>
                        {
                          errorMessage.email &&
                          (
                            <div className="input-group">
                          <span className="field-validation-error">
                            <span
                              className=""
                            >
                              {errorMessage.email}
                            </span>
                          </span>
                            </div>
                          )
                        }
                      </div>
                      <div className="form-group">
                        <input
                          className="form-control form-control-lg"
                          data-val="true"
                          data-val-required="IMEI / Serial Number is required"
                          id="Identifier"
                          name="Identifier"
                          placeholder="IMEI / Serial Number"
                          type="text"
                          value={imei}
                          onChange={changeImei}
                        />
                        {
                          errorMessage.imei &&
                          (
                            <div className="input-group">
                          <span className="field-validation-error">
                            <span
                              className=""
                            >
                              {errorMessage.imei}
                            </span>
                          </span>
                            </div>
                          )
                        }
                      </div>
                      <button className="btn btn-primary" onClick={submit}>Order Tracking</button>
                    </div>
                    <div className="info-panel">
                      <p>Enter the IMEI number and email address used at the time of purchase or use the link in the
                        confirmation email.
                      </p>
                    </div>
                  </div>
                </div>
              </section>
              <section id="usp">
                <div className="container">
                  <div className="row">
                    <div className="col-sm-4 col-xs-12">
                      <div className="text-center">
                        <div className="icon">
                          <i className="fa fa-clock-o fa-2x" />
                        </div>
                        <h3>24 hour unlock delivery</h3>
                        <p className="lead light">Instant unlocking for many mobile networks.</p>
                      </div>
                    </div>
                    <div className="col-sm-4 col-xs-12">
                      <div className="text-center">
                        <div className="icon">
                          <i className="fa fa-wrench fa-2x" />
                        </div>
                        <h3>Highly Skilled Tech Team</h3>
                        <p className="lead light">
                          Experienced in all Apple<sup>TM</sup> Phone <br/>
                          products.
                        </p>
                      </div>
                    </div>
                    <div className="col-sm-4 col-xs-12">
                      <div className="text-center">
                        <div className="icon">
                          <i className="fa fa-mobile-phone fa-2x" />
                        </div>
                        <h3>Your Phone is Safe</h3>
                        <p className="lead light">You keep your device throughout the entire process.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        )
      }
    </div>
  );
};

OrderTracking.propTypes = {};

OrderTracking.defaultProps = {};

OrderTracking.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  const rs = await systemApi.getConfig({
    [PAGE]: 1,
    [PAGE_SIZE]: -1
  });

  if (context.res && isErrorResponse(rs)) {
    context.res.writeHead(302, { Location: '/' });
    context.res.end();
  }

  return {
    props: {
      productBrand: process.env.PRODUCT_BRAND,
      config: rs
    }, // will be passed to the page component as props
  }
}

export default OrderTracking;
