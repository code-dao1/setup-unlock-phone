import Product from '@/layout/Product';
import { extractContent } from '@/utils/string';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router'
import FirstBlog from '@/components/FirstBlog';
import ListBlog from '@/components/ListBlog';
import ListBlogRight from '@/components/ListBlogRight';
import { PAGE, PAGE_SIZE, SEARCH } from '@/constants/system/service';
import blogApi from '@/services/blogApi';
import { getStateFieldByHistory } from '@/utils/history';
import { isErrorResponse } from '@/utils/typeof';
import { publicRuntimeConfig } from '@/constants/system/serverConfig';

import classes from './style.module.scss';

const Blogs = (props) => {
  const dataFromProps = props.blogs || {};
  const router = useRouter();
  const keywordFromProps = props.q;
  const [firstBlog, setFirstBlog] = useState((dataFromProps.items || [])[0]);
  const [keyword, setKeyword] = useState(keywordFromProps || '');

  useEffect(() => {
    let params = {
      [PAGE]: 1,
      [PAGE_SIZE]: 1,
    };

    blogApi.getListBlog(params).then(rs => {
      if (!isErrorResponse(rs)) {
        const {
          items,
          load_more_able,
          page: pageCurr,
          pre_load_able,
          total,
        } = rs;
        if (items && items.length > 0) {
          setFirstBlog(items[0]);
        }
      }
    });
  }, [])

  const changeKeyword = (e) => setKeyword(e.target.value);

  const {
    id,
    content,
    created_at,
    handle,
    image_src,
    image_title,
    title,
    user_name,
  } = firstBlog || {};
  const contentText = extractContent(content) || '';
  const shortText = contentText.substr(0, 200)
  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;
  return (
    <div id="wrapper">
      <Head>
        <title>Welcome to our Blog-{productBrand}</title>
        <meta name="description"
              content={shortText} />
        <meta key="og:title" property="og:title" content={`${title} - ${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content={shortText}/>
        <meta key="og:url" property="og:url" content={`${publicRuntimeConfig.urlProduct}${router.asPath}`}/>
        {
          image_src &&
          (<meta key="og:image" property="og:image" content={image_src}/>)
        }
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section style={{ padding: '56px 0 60px' }}>
            <div className="container">
              <div className="row">
                <div className="col-xs-12 text-center">
                  <h1>
                    Welcome to our Blog
                  </h1>
                  <h3 style={{ marginTop: '15px', fontSize: '26px' }}>
                    {productBrand}
                  </h3>
                  <br/>
                  <br/>
                  <br/>
                </div>
              </div>
              <FirstBlog data={firstBlog}/>
            </div>
          </section>
          <section className={classes.listBlog}>
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <h2>Latest Articles</h2>
                  <br/>
                  <br/>
                </div>
              </div>
              <div className="row">
                <ListBlog ignoreItem={firstBlog} keyword={keyword} data={(dataFromProps.items || []).slice(1)}/>
                <ListBlogRight keyword={keyword} changeKeyword={changeKeyword}/>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

Blogs.propTypes = {};

Blogs.defaultProps = {};

Blogs.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params,
    query
  } = context;

  const blogs = await blogApi.getListBlog({
    [PAGE]: 1,
    [PAGE_SIZE]: 10,
  });

  return {
    props: {
      blogs: blogs,
      productBrand: process.env.PRODUCT_BRAND,
      q: query.q || ''
    }, // will be passed to the page component as props
  }
}

export default Blogs;
