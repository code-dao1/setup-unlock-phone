import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import Product from '@/layout/Product';
import Head from 'next/head';
import React from 'react';
import PropTypes from 'prop-types';
import Image from '@/components/common/Image';
import ListReviewPage from '@/components/ListReviewPage';
import UnbeatableAndTrackUnlock from '@/components/PromoteBrand/UnbeatableAndTrackUnlock';
import SupportService from '@/components/SupportService';

const Support = (props) => {

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  return (
    <div id="wrapper">
      <Head>
        <title>Help &amp; Information-{productBrand}</title>
        <meta name="description"
              content={`Welcome to the ${productBrand} support! If you&apos;re having problems, then you have certainly come to the right place.`} />
        <meta key="og:title" property="og:title" content={`Help &amp; Information-${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content={`Welcome to the ${productBrand} support! If you&apos;re having problems, then you have certainly come to the right place.`}/>
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section id="support-hero">
            <div className="page-nav affix">
              <div className="container">
                <div className="row">
                  <div className="col-sm-6 col-xs-12">
                    <h3>Help &amp; Information</h3>
                  </div>
                  <div className="col-sm-6 hidden-xs">
                    <a
                      href="mailto:support@expressunlock.com"
                      target="_blank"
                      title="Contact Support"
                      className="pull-right text-black"
                    >
                      <small>Contact Support</small>
                    </a>
                    <a href="/frequestly-asked-questions" title="FAQ" className="pull-right text-black">
                      <small>FAQ</small>
                    </a>
                  </div>
                </div>
                <hr/>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <h1 className="text-center">Support <small>We&apos;re here to help you</small></h1>
                  <br/>
                  <br/>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6 col-md-offset-3">
                  <div className="text-center">
                    <p>Welcome to the {publicRuntimeConfig.productBrand} support! If you&apos;re having problems, then you have certainly
                      come to the right place.
                    </p>
                    <br/>
                    <br/>
                    <a
                      href="mailto:support@expressunlock.com"
                      target="_blank"
                      title="Contact us"
                      className="btn btn-primary"
                    ><strong>Contact Support</strong>
                    </a>
                  </div>
                  <div className="form-group hidden">
                    <div className="input-group">
                      <input type="text" className="form-control form-control-lg" placeholder="Search Support"/>
                      <span className="input-group-addon">
                        <button type="button">
                          <i className="fa fa-search" />
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Image
              src={'/media/images/iphone12.jpg'}
              style={{ bottom: 0 }}
              fillMode="contain"
              alt={'image support'}
            />
          </section>
          <SupportService />
          <UnbeatableAndTrackUnlock />
          <ListReviewPage />
        </div>
      </div>
    </div>
  );
};

Support.propTypes = {};

Support.defaultProps = {};

Support.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  return {
    props: {
      productBrand: process.env.PRODUCT_BRAND
    }, // will be passed to the page component as props
  }
}

export default Support;
