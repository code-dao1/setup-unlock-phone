import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import Product from '@/layout/Product';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import PropTypes from 'prop-types';
import UnlockFAQ from '@/components/UnlockFAQ';

const FAQ = (props) => {
  const router = useRouter();
  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  return (
    <div id="wrapper">
      <Head>
        <title>Phone &amp; Unlock FAQs - {productBrand}</title>
        <meta name="description"
              content={`Phone &amp; Unlock FAQs - ${productBrand}`} />
        <meta key="og:title" property="og:title" content={`Phone &amp; Unlock FAQs - ${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content={`Phone &amp; Unlock FAQs - ${productBrand}`}/>
        <meta key="og:url" property="og:url" content={`${publicRuntimeConfig.urlProduct}${router.asPath}`}/>
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section id="support-page" className="content-page">
            <div className="page-nav affix">
              <div className="container">
                <div className="row">
                  <div className="col-sm-6 col-xs-12">
                    <h3>Support</h3>
                  </div>
                  <div className="col-sm-6 hidden-xs">
                    <a href="/support" title="Contact Support" className="pull-right text-black">
                      <small>Contact Support</small>
                    </a>
                    <a
                      href="/frequestly-asked-questions"
                      title="Contact Support"
                      className="pull-right text-black"
                    >
                      <small>FAQ</small>
                    </a>
                  </div>
                </div>
                <hr/>
              </div>
            </div>
            <div className="container">
              <h1 className="heading-sm">Phone &amp; Unlock FAQs - {productBrand}</h1>
              <div className="panel-group" id="accordion">
                <UnlockFAQ />
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

FAQ.propTypes = {};

FAQ.defaultProps = {};

FAQ.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  return {
    props: {
      productBrand: process.env.PRODUCT_BRAND
    }, // will be passed to the page component as props
  }
}

export default FAQ;
