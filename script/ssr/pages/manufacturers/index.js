import { PAGE, PAGE_SIZE } from '@/constants/system/service';
import Product from '@/layout/Product';
import manufacturerApi from '@/services/manufacturerApi';
import Head from 'next/head';
import React from 'react';
import PropTypes from 'prop-types';
import ListManufacturer from '@/components/ListManufacturer';
import ListPhoneOfManufacturer from '@/components/ListPhoneOfManufacturer';
import UnlockStep from '@/components/UnlockStep';

const Manufacturer = (props) => {

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  return (
    <div
      id="wrapper"
    >
      <Head>
        <title>Manufacturer-{productBrand}</title>
        <meta name="description"
              content="We offer an unlocking service for all Smart phone models, our easy and affordable phone unlocking service is compatible with all versions – find your device here." />
        <meta key="og:title" property="og:title" content={`Manufacturer-${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content="We offer an unlocking service for all Smart phone models, our easy and affordable phone unlocking service is compatible with all versions – find your device here."/>
      </Head>
      <div id="background-img">
        <div className="body-content">
          <ListManufacturer data={props.manufacturer && props.manufacturer.items}/>
          <UnlockStep />
          <section id="deviceIndex" style={{ marginTop: '12vh', marginBottom: '3.2rem' }}>
            <div className="row">
              <div className="container">
                <div className="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">
                  <h2 className="text-center">Phone Devices</h2>
                  <br/>
                  <p className="lead light text-center">
                    We are able to unlock any model of Smartphones. Our affordable service and seamless process works well with all phone versions – if you still doubt, check for your device type here.
                  </p>
                  <br/>
                </div>
              </div>
            </div>
          </section>
          <ListPhoneOfManufacturer />
        </div>
      </div>
    </div>
  );
};

Manufacturer.propTypes = {};

Manufacturer.defaultProps = {};

Manufacturer.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  const manufacturer = await manufacturerApi.getListManufacturer({
    [PAGE]: 1,
    [PAGE_SIZE]: -1,
  })

  return {
    props: {
      manufacturer: manufacturer,
      productBrand: process.env.PRODUCT_BRAND
    }, // will be passed to the page component as props
  }
}

export default Manufacturer;
