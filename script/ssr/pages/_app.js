import HeadGeneral from '@/components/HeadGeneral';
import { PAGE, PAGE_SIZE } from '@/constants/system/service';
import blogApi from '@/services/blogApi';
import manufacturerApi from '@/services/manufacturerApi';
import phoneApi from '@/services/phoneApi';
import serviceApi from '@/services/serviceApi';
import systemApi from '@/services/systemApi';
import { createRobots } from '@/utils/system/robots';
import { createSitemap } from '@/utils/system/sitemap';
import { useRouter } from 'next/router';
import React from 'react';
import { Provider } from 'react-redux'
import storeConfig from '../redux/store'
import { PersistGate } from 'redux-persist/integration/react';

import '@/styles/globals.css'
import '@/styles/source/font.css'
import '@/styles/source/layout.css'
import '@/styles/source/font.css'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'perfect-scrollbar/css/perfect-scrollbar.css';

import '@/components/common/Image/style.scss'
import '@/components/common/LoaderBalls/style.scss'
import '@/components/common/ReactImageGallery/style.scss'
import '@/components/common/ReactItemGallery/style.scss'
import '@/components/common/ScrollView/style.scss'
import '@/components/common/Select/style.scss'
import '@/components/common/SInput/style.scss'
import '@/components/common/Loading/style.scss'
import '@/components/common/mobile/AlertMobile/style.scss'
import '@/components/common/mobile/AlertMobile/style.scss'
import '@/components/common/mobile/MenuMobile/style.scss'
import '@/components/common/mobile/ScreenEffectMobile/style.scss'
import '@/components/BannerFeed/style.scss'
import '@/components/FormInputOrder/style.scss'
import '@/components/FormInputUnlockPhone/SelectPhoneDesktop/style.scss'
import '@/components/FormInputUnlockPhone/SelectServiceDesktop/style.scss'
import '@/components/GroupServiceItem/style.scss'
import '@/components/ListGroupService/style.scss'
import '@/components/ListManufacturer/style.scss'
import '@/components/ListServiceVertical/style.scss'
import '@/components/LoaderBalls/style.scss'
import '@/components/ManufacturerItem/style.scss'
import '@/components/ProductDetailInfo/style.scss'
import '@/components/ProductTop/style.scss'
import '@/components/ShareButton/style.scss'

import '@/layout/Product/components/Footer/style.scss'

import '@/pages/blog/style.scss'
import '@/pages/checkout/style.scss'
import '@/pages/manufacturer/style.scss'
import '@/pages/service/style.scss'
import '@/pages/unlock/style.scss'

function MyApp(props) {
  const { Component, pageProps, webConfig, code } = props;
  let initProps = pageProps && pageProps.initialReduxState || {};
  initProps.config = {
    appConfig: webConfig
  }
  const store = storeConfig(initProps)
  const Layout = Component.Layout ? Component.Layout : React.Fragment;
  const router = useRouter();
  const asPath = router.asPath;
  let propsLayout = {};

  if (Component.Layout) {
    propsLayout = {
      webConfig: webConfig
    }
  }

  if (process.browser) {
    return (
      <Provider store={store}>
        <HeadGeneral
          webConfig={webConfig}
          asPath={asPath}
          code={code}
        />
        <PersistGate persistor={store.__PERSISTOR} loading={null}>
          <Layout { ...propsLayout }>
            <Component {...pageProps} webConfig={webConfig} />
          </Layout>
        </PersistGate>
      </Provider>
    );
  } else {
    return (
      <Provider store={store}>
        <HeadGeneral
          webConfig={webConfig}
          asPath={asPath}
          code={code}
        />
        <Layout { ...propsLayout }>
          <Component {...pageProps} webConfig={webConfig} />
        </Layout>
      </Provider>
    );
  }
}

MyApp.getInitialProps = async function (ctx) {
  const { res, req } = ctx.ctx;

  if (req.url.includes('sitemap.xml') && res) {
    const phones = await phoneApi.getListPhone({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }) || {};

    const services = await serviceApi.getListService({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }) || {};

    const blogs = await blogApi.getListBlog({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }) || {};

    const manufacturers = await manufacturerApi.getListManufacturer({
      [PAGE]: 1,
      [PAGE_SIZE]: -1,
    }) || {};

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/xml');
    res.write(createSitemap({
      phones,
      services,
      blogs,
      manufacturers
    }));
    res.end();
    return;
  } else if (req.url.includes('robots.txt') && res) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.write(createRobots());
    res.end();
    return;
  }

  const rs = await systemApi.getScriptByType('global');
  let code = '';

  if (rs && rs.id && rs.code) {
    code = rs.code;
  }
  const rsConfig = await systemApi.getConfigWebsite();
  return { code, webConfig: rsConfig }
}

export default MyApp
