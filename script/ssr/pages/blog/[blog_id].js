import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import Product from '@/layout/Product';
import Checkout from '@/pages/checkout';
import { extractContent } from '@/utils/string';
import Head from 'next/head';
import React, { useEffect, useMemo, useState } from 'react';
import { useRouter } from 'next/router'
import HTMLContent from '@/components/common/HTMLContent';
import Image from '@/components/common/Image';
import Loading from '@/components/common/Loading';
import ListBlogInHome from '@/components/ListBlogInHome';
import ListBlogRight from '@/components/ListBlogRight';
import ShareButton from '@/components/ShareButton';

// import './style.scss'
import { BLOG_ID, PHONE_ID } from '@/constants/system/paramsRoute';
import blogApi from '@/services/blogApi';
import pageLegalsApi from '@/services/pageLegalsApi';
import { formatTime } from '@/utils/formatTime';
import { getParamByRouter } from '@/utils/history';
import { isErrorResponse } from '@/utils/typeof';

const BlogDetail = (props) => {
  const router = useRouter();
  const blogHandle = props.blogHandle;
  const [data, setData] = useState(props.data);
  const [keyword, setKeyword] = useState('');
  useEffect(() => {
    blogApi.getBlogInfo(blogHandle)
      .then(rs => {
        if (!isErrorResponse(rs)) {
          setData(rs);
        }
      })
  }, []);

  const onSearch = () => {
    router.push(`/blogs?q=${keyword}`);
  };

  const changeKeyword = (e) => setKeyword(e.target.value);

  if (!data) {
    return <Loading isMessage={false}/>;
  }

  const {
    id,
    content,
    created_at,
    handle,
    image_src,
    image_title,
    title,
    user_name,
  } = data;
  const contentText = extractContent(content) || '';
  const shortText = contentText.substr(0, 200)

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  return (
    <div id="wrapper">
      <Head>
        <title>{title} - {productBrand}</title>
        <meta name="description"
              content={shortText} />
        <meta key="og:title" property="og:title" content={`${title} - ${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content={shortText}/>
        <meta key="og:url" property="og:url" content={`${publicRuntimeConfig.urlProduct}${router.asPath}`}/>
        {
          image_src &&
          (<meta key="og:image" property="og:image" content={image_src}/>)
        }
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section className="content-page">
            <div className="container">
              <div className="row">
                <div className="col-md-8 col-xs-12" style={{ zIndex: 10 }}>
                  <label className="label label-info" style={{ backgroundColor: 'rgb(0, 122, 255)' }}>Guides</label>
                  <br/>
                  <br/>
                  <h1 className="heading-sm" style={{ marginBottom: '10px' }}>{title}</h1>
                  <p className="text-grey text-uppercase" style={{ marginBottom: '15px' }}>
                    <small>{user_name} &nbsp;•&nbsp; {formatTime(created_at, false)}</small>
                  </p>
                  <br/>
                  <div
                    className="addthis_inline_share_toolbox"
                    style={{ clear: 'both' }}
                  >
                    <ShareButton
                      data={data}
                    />
                  </div>
                  <hr/>
                  <Image
                    src={image_src}
                    alt={title}
                    className="img-responsive"
                  />
                  <br/>
                  <br/>
                  <HTMLContent content={content}/>
                  <br/>
                  <br/>
                  <br/>
                  <div
                    className="addthis_inline_share_toolbox"
                    style={{ clear: 'both' }}
                  >
                    <ShareButton
                      data={data}
                    />
                  </div>
                  <ListBlogInHome/>
                </div>
                <ListBlogRight
                  className={'col-md-3 col-md-offset-1 col-xs-12 col-xs-offset-0'}
                  keyword={keyword}
                  changeKeyword={changeKeyword}
                  onSearch={onSearch}
                />
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

BlogDetail.propTypes = {};

BlogDetail.defaultProps = {};

export async function getServerSideProps(context) {
  const {
    params
  } = context;
  const blogHandle = params[BLOG_ID];

  const rs = await blogApi.getBlogInfo(blogHandle);

  if (context.res && isErrorResponse(rs)) {
    context.res.writeHead(302, { Location: '/' });
    context.res.end();
  }

  return {
    props: {
      data: rs,
      productBrand: process.env.PRODUCT_BRAND,
      blogHandle
    }, // will be passed to the page component as props
  }
}

BlogDetail.Layout = Product;

export default BlogDetail;
