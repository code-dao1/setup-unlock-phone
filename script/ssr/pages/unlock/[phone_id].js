import Product from '@/layout/Product';
import Head from 'next/head';
import React, { useEffect, useMemo, useState } from 'react';
import HTMLContent from '@/components/common/HTMLContent';
import { useRouter } from 'next/router'
import Image from '@/components/common/Image';
import Loading from '@/components/common/Loading';
import FormInputUnlockPhone from '@/components/FormInputUnlockPhone';
import HowToUnlockPhone from '@/components/HowToUnlockPhone';
import ListReviewPage from '@/components/ListReviewPage';
import UnbeatableAndTrackUnlock from '@/components/PromoteBrand/UnbeatableAndTrackUnlock';
import ReasonInPhone from '@/components/ReasonUnlockInPhone';
import StepUnlockInPhone from '@/components/StepUnlockInPhone';
import SuggestServiceCheck from '@/components/SuggestServiceCheck';
import UnlockFAQ from '@/components/UnlockFAQ';

// import './style.scss';
import { PHONE_ID, SERVICE_ID } from '@/constants/system/paramsRoute';
import phoneApi from '@/services/phoneApi';
import { getParamByRouter } from '@/utils/history';
import { isErrorResponse } from '@/utils/typeof';
import { Redirect } from 'react-router';

const TitleFAQ = (props) => {
  const {
    id,
    name,
  } = props.data;
  return (<div className="row">
    <div className="col-xs-12 text-center">
      <h2>Unlock phone FAQ</h2>
      <br/>
      <p className="lead">How to unlock questions answered.</p>
      <br/>
      <br/>
    </div>
  </div>);
}

const PhoneDetail = (props) => {

  const router = useRouter();
  const phoneHandle = props.phoneHandle;

  const [loading, setLoading] = useState(true);
  const [phone, setPhone] = useState(props.data);

  useEffect(() => {
    if (phoneHandle) {
      setLoading(true);
      phoneApi.getPhoneInfo(phoneHandle)
        .then(rs => {
          if (!isErrorResponse(rs) && rs.id) {
            setPhone(rs);
          }
          setLoading(false);
        });
    }
  }, [phoneHandle]);

  if (!phone && loading) {
    return (
      <Loading isMessage={false}/>
    );
  }

  if (!phone) {
    router.push('/');
  }

  const {
    id,
    description,
    seo_description,
    seo_title,
    handle,
    image_src,
    image_title,
    manufacturer_id,
    name,
  } = phone;

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  const titleSeoShow = seo_title || `${name} - ${productBrand}`;
  const descriptionSeoShow = seo_description || `Unlock your phone ${name} quickly and easily with ${productBrand}. Using our secure online unlocking service, all our unlocks have a success rate of 100% without affecting performance or any outstanding warranty on your phone, enjoy peace of mind with a hassle-free service.`;

  return (
    <div id="wrapper">
      <Head>
        <title>{titleSeoShow}</title>
        <meta name="description"
              content={descriptionSeoShow} />
        <meta key="og:title" property="og:title" content={`${titleSeoShow}`}/>
        <meta key="og:description" property="og:description"
              content={`${descriptionSeoShow}`}/>
        {
          image_src &&
          (<meta key="og:image" property="og:image" content={image_src}/>)
        }
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section style={{ padding: '5vh 0', backgroundColor: '#f8f8f8' }}>
            <div className="container">
              <div className="row">
                <div className="col-xs-12 text-center">
                  <h1>{name}</h1>
                  <h3 style={{ marginTop: '15px', fontSize: '26px' }}>
                    From any Network by IMEI
                  </h3>
                  <br/>
                  <br/>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6 col-xs-12">
                  <div className="row">
                    <div className="col-md-10 col-md-offset-1 col-xs-8 col-xs-offset-2">
                      <Image
                        fillMode={'contain'}
                        src={image_src}
                        alt={name}
                        className="img-responsive"
                        style={{ position: 'relative', maxHeight: '45rem', width: '100%' }}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-xs-12">
                  <div className="row">
                    <div className="col-md-11 col-xs-12">
                      <br/>
                      <br/>
                      <HTMLContent content={description} />
                      <br/>
                      <div id="OIPUForm">
                        <label className="control-label text-grey">Let&apos;s get started:</label>
                        <FormInputUnlockPhone defaultPhone={phone} autoChangeUrlByPhone/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <HowToUnlockPhone
            phone={phone}
          />
          <UnlockFAQ
            title={<TitleFAQ data={phone}/>}
          />
          <UnbeatableAndTrackUnlock />
          <ReasonInPhone phone={phone}/>
          <StepUnlockInPhone />
          <ListReviewPage />
          <SuggestServiceCheck phone={phone}/>
        </div>
      </div>
    </div>
  );
};

PhoneDetail.propTypes = {};

PhoneDetail.defaultProps = {};

PhoneDetail.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;
  const phoneHandle = params[PHONE_ID];

  const rs = await phoneApi.getPhoneInfo(phoneHandle);

  if (context.res && isErrorResponse(rs)) {
    context.res.writeHead(302, { Location: '/' });
    context.res.end();
  }

  return {
    props: {
      data: rs,
      productBrand: process.env.PRODUCT_BRAND,
      phoneHandle: phoneHandle
    }, // will be passed to the page component as props
  }
}

export default PhoneDetail;
