import { publicRuntimeConfig } from '@/constants/system/serverConfig';
import { PAGE, PAGE_SIZE } from '@/constants/system/service';
import Product from '@/layout/Product';
import blogApi from '@/services/blogApi';
import phoneApi from '@/services/phoneApi';
import serviceApi from '@/services/serviceApi';
import React, { PureComponent } from 'react';
import BenefitsUnlock from '@/components/BenefitsUnlock';
import BulkUnlock from '@/components/BulkUnlock';
import IntroContent from '@/components/IntroContent';
import ListBlogInHome from '@/components/ListBlogInHome';
import ListDevice from '@/components/ListDevice';
import ListReviewPage from '@/components/ListReviewPage';
import ParallaxIntro from '@/components/ParallaxIntro';
import PromoteBrand from '@/components/PromoteBrand';
import PromoteService from '@/components/PromoteService';
import ShortListService from '@/components/ShortListService';
import SliderPhone from '@/components/SliderPhone';

import UnlockFAQ from '@/components/UnlockFAQ';
import TitleFAQFeed from '@/components/UnlockFAQ/TitleFAQFeed';
import UnlockServiceWork from '@/components/UnlockServiceWork';
import UnlockStep from '@/components/UnlockStep';

class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div style={{ backgroundColor: '#fff' }}>
        <section id={'hero'}>
          <SliderPhone data={this.props.phones}/>
          <ParallaxIntro />
          <IntroContent />
          <UnlockStep />
          <ShortListService data={this.props.services}/>
          <UnlockServiceWork />
          <ListDevice data={this.props.phones && this.props.phones.items}/>
          <UnlockFAQ title={<TitleFAQFeed />}/>
          <PromoteService />
          <BenefitsUnlock />
          <PromoteBrand />
          <ListReviewPage />
          <BulkUnlock />
          <ListBlogInHome data={this.props.blogs}/>
        </section>
      </div>
    );
  }
}

Home.propTypes = {};

Home.defaultProps = {};

Home.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  const phones = await phoneApi.getListPhone({
    [PAGE]: 1,
    [PAGE_SIZE]: 20,
  });

  const services = await serviceApi.getListService({
    [PAGE]: 1,
    [PAGE_SIZE]: 6,
  });

  const blogs = await blogApi.getListBlog({
    [PAGE]: 1,
    [PAGE_SIZE]: 4,
  });

  return {
    props: {
      phones: phones,
      services: services,
      blogs: blogs
    }, // will be passed to the page component as props
  }
}

export default Home;
