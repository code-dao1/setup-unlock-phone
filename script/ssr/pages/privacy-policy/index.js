import { BLOG_ID } from '@/constants/system/paramsRoute';
import Product from '@/layout/Product';
import blogApi from '@/services/blogApi';
import { extractContent } from '@/utils/string';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import HTMLContent from '@/components/common/HTMLContent';
import Loading from '@/components/common/Loading';
import { PRIVACY_POLICY, TERMS_AND_CONDITIONS } from '@/constants/legalPage';
import pageLegalsApi from '@/services/pageLegalsApi';
import { getDateFromTime } from '@/utils/formatTime';
import { isErrorResponse } from '@/utils/typeof';

const PrivacyPolicy = (props) => {
  const [data, setData] = useState(props.data);

  useEffect(() => {
    pageLegalsApi.getLegalPageDetail('privacy-policy')
      .then(rs => {
        if (!isErrorResponse(rs)) {
          setData(rs);
        }
      })
  }, []);

  if (!data) {
    return <Loading isMessage={false}/>;
  }

  const {
    id,
    content,
    handle,
    name,
    updated_at,
  } = data;

  const contentText = extractContent(content) || '';
  const shortText = contentText.substr(0, 200);

  const {
    webConfig = {}
  } = props;

  const productBrand = webConfig.brand_name || props.productBrand;

  return (
    <div id="wrapper">
      <Head>
        <title>{name} - {productBrand}</title>
        <meta name="description"
              content={shortText} />
        <meta key="og:title" property="og:title" content={`${name} - ${productBrand}`}/>
        <meta key="og:description" property="og:description"
              content={shortText}/>
      </Head>
      <div id="background-img">
        <div className="body-content">
          <section id="terms-conditions" className="content-page">
            <div className="page-nav affix">
              <div className="container">
                <div className="row">
                  <div className="col-xs-6">
                    <h3>Privacy</h3>
                  </div>
                  <div className="col-xs-6">
                    <a href="/support" title="Support" className="pull-right text-black">
                      <small>Support</small>
                    </a>
                    <a href="/terms-and-conditions" title="Terms & Conditions" className="pull-right text-black">
                      <small>Terms & Conditions</small>
                    </a>
                  </div>
                </div>
                <hr/>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-8">
                  <h1 className="heading-sm">{name}</h1>
                  <br/>
                  <p>Last Update published on the {getDateFromTime(updated_at)}</p>
                  <br/>
                  <HTMLContent content={content} />
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

PrivacyPolicy.propTypes = {};

PrivacyPolicy.defaultProps = {};

PrivacyPolicy.Layout = Product;

export async function getServerSideProps(context) {
  const {
    params
  } = context;

  const rs = await pageLegalsApi.getLegalPageDetail('privacy-policy');

  return {
    props: {
      data: rs,
      productBrand: process.env.PRODUCT_BRAND
    }, // will be passed to the page component as props
  }
}

export default PrivacyPolicy;
