const path = require('path');

module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  serverRuntimeConfig: {
    // Will only be available on the server side
    mySecret: 'secret',
    secondSecret: process.env.SECOND_SECRET, // Pass through env variables
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
    baseUrl: process.env.BASE_URL,
    urlProduct: process.env.URL_PRODUCT,
    productBrand: process.env.PRODUCT_BRAND,
    // Will be available on both server and client
    staticFolder: '/static',
  },
  plugins: {
    'postcss-preset-env': {},
    'postcss-nested': {},
  },
  webpack: config => {
    config.resolve.mainFields = ["main", "browser", "module"];

    const rules = config.module.rules
      .find((rule) => typeof rule.oneOf === "object")
      .oneOf.filter((rule) => Array.isArray(rule.use));

    rules.forEach((rule) => {
      rule.use.forEach((moduleLoader) => {
        if (
          moduleLoader.loader.includes("css-loader/dist") &&
          typeof moduleLoader.options.modules === "object"
        ) {
          moduleLoader.options = {
            ...moduleLoader.options,
            modules: {
              ...moduleLoader.options.modules,
              exportLocalsConvention: "camelCase", // https://github.com/webpack-contrib/css-loader#exportlocalsconvention
            },
          };
        }
      });
    });

    return config;
  },
}
