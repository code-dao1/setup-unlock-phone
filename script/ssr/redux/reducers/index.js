import config from '@/redux/reducers/system/config';
import { combineReducers } from 'redux';
import modal from './modal';
import modalFormOrder from './modalFormOrder';

import ui from './system/ui';
import sideNav from './sideNav';

export default combineReducers({
  ui,
  config,
  sideNav,
  modal,
  modalFormOrder,
});
