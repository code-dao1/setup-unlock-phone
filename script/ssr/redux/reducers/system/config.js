const initialState = {
  appConfig: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
