import { Ui } from '@/redux/types/system';
import { getCookie, setCookie } from '@/utils/ApplicationStorage/cookieStorage';
import {
  isMobileScreen,
  isTabletScreen,
  isDesktopScreen,
  updateDeviceWidth,
  isTouchDevice
} from '@/utils/system/ui';

const initialState = {
  deviceWidth: updateDeviceWidth(),
  isMobileScreen: isMobileScreen(),
  isTabletScreen: isTabletScreen(),
  isDesktopScreen: isDesktopScreen(),
  isTouchDevice: false,
  isActiveWindow: true,
  isBrowser: !!(process && process.browser),
  aff: '',
  scrollRectBody: {
    height: 0,
    width: 0,
    top: 0,
    left: 0,
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Ui.INIT_SYSTEM:
      const initialStateNew = {
        isBrowser: true,
        deviceWidth: updateDeviceWidth(),
        isMobileScreen: isMobileScreen(),
        isTabletScreen: isTabletScreen(),
        isDesktopScreen: isDesktopScreen(),
        isTouchDevice: isTouchDevice(),
        isActiveWindow: true,
        aff: getCookie('aff'),
        scrollRectBody: {
          height: window.innerHeight,
          width: window.innerWidth,
          top: window.scrollY,
          left: window.scrollX,
        }
      };

      return {
        ...initialStateNew
      }
    case Ui.CHANGE_DIMENSION:
      const deviceWidth = updateDeviceWidth();
      return {
        ...state,
        deviceWidth: deviceWidth,
        isMobileScreen: isMobileScreen(),
        isTabletScreen: isTabletScreen(),
        isDesktopScreen: isDesktopScreen(),
      };
    case Ui.FOCUS_WINDOW:
      return {
        ...state,
        isActiveWindow: true
      };
    case Ui.CHANGE_AFF:
      if (action.payload.aff) {
        setCookie('aff', action.payload.aff, 1)
        return {
          ...state,
          aff: action.payload.aff
        };
      }
      return state;
    case Ui.BLUR_WINDOW:
      return {
        ...state,
        isActiveWindow: false
      };
    case Ui.UPDATE_RECT_WINDOW:
      if (action.payload.data) {
        return {
          ...state,
          scrollRectBody: {
            ...state.scrollRectBody,
            ...action.payload.data
          }
        };
      }
      return state;

    default:
      return state;
  }
};
