import { modalFormOrderPhone } from '@/redux/types/modal';

const initialState = {
  isOpen: false,
};

export default (state = initialState, action) => {
  const payload = action.payload || {};
  switch (action.type) {
    case modalFormOrderPhone.OPEN:
      return {
        isOpen: true,
      };
    case modalFormOrderPhone.CLOSE:
      return {
        isOpen: false,
      };
    default:
      return state;
  }
};
