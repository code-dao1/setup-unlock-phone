import { modal } from '@/redux/types/modal';

const initialState = {
  isOpen: false,
  content: null,
  className: '',
  clickOutSide: true
};

export default (state = initialState, action) => {
  const payload = action.payload || {};
  switch (action.type) {
    case modal.OPEN:
      return {
        ...state,
        ...payload.data,
        isOpen: true,
      };
    case modal.CLOSE:
      return {
        ...state,
        isOpen: false,
      };
    default:
      return state;
  }
};
