import { sideNav } from '@/redux/types/sideNav';

const initialState = {
  isOpenSideNavMobile: false,
  searchText: ''
};

export default (state = initialState, action) => {
  const payload = action.payload || {};
  switch (action.type) {
    case sideNav.OPEN_SIDE_NAV_MOBILE:
      return {
        ...state,
        isOpenSideNavMobile: true,
      };
    case sideNav.CLOSE_SIDE_NAV_MOBILE:
      return {
        ...state,
        isOpenSideNavMobile: false,
      };
    case sideNav.CHANGE_SEARCH:
      return {
        ...state,
        searchText: payload.searchText || ''
      };
    default:
      return state;
  }
};
