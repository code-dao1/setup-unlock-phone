export const modal = {
  OPEN: 'OPEN_MODAL',
  CLOSE: 'CLOSE_MODAL',
};

export const modalFormOrderPhone = {
  OPEN: 'OPEN_MODAL_ORDER_PHONE',
  CLOSE: 'CLOSE_MODAL_ORDER_PHONE',
};
