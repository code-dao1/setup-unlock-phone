import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { persistStore } from 'redux-persist';

import {
  promiseMiddleware,
  localStorageMiddleware,
  APIMiddleware,
  localStorageListener,
  callbackMiddleware
} from './middleware';
import reducers from '../reducers';

const getMiddleware = () => {
  if (process.env.BUILD === 'prod') {
    return applyMiddleware(thunk, promiseMiddleware, localStorageMiddleware);
  }
  // Enable additional logging in non-production environments.
  if (process.env.REACT_APP_SERVER_CONFIG !== 'production') {
    return applyMiddleware(
      thunk,
      promiseMiddleware,
      // createLogger(),
      APIMiddleware,
      localStorageMiddleware,
      callbackMiddleware
    );
  }
  return applyMiddleware(
    thunk,
    promiseMiddleware,
    APIMiddleware,
    localStorageMiddleware,
    callbackMiddleware
  );
};

const createConfigStore = (initialState) => {
  const isClient = typeof window !== 'undefined';

  let store;

  if (isClient) {
    const { persistReducer } = require('redux-persist');
    const storage = require('redux-persist/lib/storage').default;

    const persistConfig = {
      key: 'root',
      storage,
      whitelist: ['user']
    };

    const persistedReducer = persistReducer(persistConfig, reducers);
    store = createStore(
      persistReducer(persistConfig, persistedReducer),
      initialState,
      composeWithDevTools(getMiddleware())
    );

    store.__PERSISTOR = persistStore(store);

    window.addEventListener('storage', localStorageListener(store));

  } else {
    store = createStore(
      reducers,
      initialState,
      composeWithDevTools(getMiddleware())
    );
  }

  return store;
};

export default createConfigStore;
