import uuidv4 from 'uuid/dist/v4';

const localStorageKey = 'redux-store-tab-sync';

function isPromise(v) {
  return v && typeof v.then === 'function';
}

const promiseMiddleware = ({ dispatch }) => next => action => {
  // To create this middleware we used the time() and timeEnd() console methods
  // that record a benchmark with the name provided as a string.
  // We start the timing before running an action, using the action.type as a name.
  // Then, we tell the browser to print the timing after the action is done.
  // This way we can potentially catch poorly performing reducer implementations.
  // console.time(action.type);
  if (isPromise(action.payload)) {
    action.payload.then(
      res => {
        if (typeof res === 'undefined') {
          return;
        }
        let tempAction = action;
        tempAction.payload = res;
        dispatch(tempAction);
      },
      err => {
        // browserHistory.push('/500');
        // history.push('/500');
        let tempAction = action;
        tempAction.payload = err;
        dispatch(action);
      }
    ).catch(error => {
      dispatch({
        type: 'ERROR',
        payload: error
      });
    });
  } else {
    next(action);
  }
  // console.timeEnd(action.type);
};

const callbackMiddleware = ({ dispatch }) => next => action => {
  if (action.callback && typeof action.callback === 'function') {
    action.callback();
  }
  next(action);
};

const APIMiddleware = ({ dispatch }) => next => action => {
  if (action.type !== 'API') {
    return next(action);
  }
  const { payload } = action;

  // return superagent
  //   .get(payload.url)
  //   .use(tokenPlugin)
  //   .then(
  //     function onSuccess(res) {
  //       dispatch({ type: payload.SUCCESS, payload: res.body.result });
  //     },
  //     function onError(err) {
  //       console.log(err);
  //       dispatch({ type: payload.ERROR, payload: 'something happen bad' });
  //     }
  //   );
};

const getCurrentTabID = () => {
  let currentTabID = sessionStorage.getItem('tabID');
  if (!currentTabID) {
    let tabID = uuidv4();
    sessionStorage.setItem('tabID', tabID);
    return tabID;
  }
  return currentTabID;
};

// const OTHER_TAB = 'OTHER_TAB';

const indentifyAction = action => {
  return {
    tabID: getCurrentTabID(),
    action
    // source: 'OTHER_TAB'
  };
};

const localStorageMiddleware = () => next => indentifiedAction => {
  if (!indentifiedAction.tabID) {
    // no tabID, mean new action dispatch from current browser
    let action = indentifiedAction;
    switch (action.type) {
      // mean trial action
      case 'haha':
        localStorage.setItem(
          localStorageKey,
          JSON.stringify(indentifyAction(action))
        );
        return next(action);
      default:
        // mean other action, ex: login, fetch topic ...
        return next(action);
    }
  } else {
    // got tabID, action load from localStorage & from other browser tab
    if (indentifiedAction.tabID !== getCurrentTabID()) {
      return next(indentifiedAction.action);
    }
    // action load from localStorage & from same browser tab
    localStorage.removeItem(localStorageKey);
    return null;
  }
};

const localStorageListener = store => {
  return () => {
    const indentifiedAction = JSON.parse(localStorage.getItem(localStorageKey));

    if (
      indentifiedAction &&
      indentifiedAction.action &&
      indentifiedAction.action.type
    ) {
      if (indentifiedAction.tabID !== getCurrentTabID()) {
        store.dispatch(indentifiedAction);
        localStorage.removeItem(localStorageKey);
      }
    }
  };
};

export {
  promiseMiddleware,
  localStorageMiddleware,
  localStorageListener,
  APIMiddleware,
  callbackMiddleware
};
