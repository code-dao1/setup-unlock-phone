import { modalFormOrderPhone } from '@/redux/types/modal';

export const openModalOrderPhone = () => dispatch => {
  dispatch({
    type: modalFormOrderPhone.OPEN
  });
};

export const closeModalOrderPhone = () => dispatch => {
  dispatch({
    type: modalFormOrderPhone.CLOSE,
  });
};
