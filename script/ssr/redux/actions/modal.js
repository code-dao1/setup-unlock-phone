import { modal } from '@/redux/types/modal';

export const openModal = (data) => dispatch => {
  dispatch({
    type: modal.OPEN,
    payload: {
      data: data
    }
  });
};

export const closeModal = () => dispatch => {
  dispatch({
    type: modal.CLOSE,
  });
};
