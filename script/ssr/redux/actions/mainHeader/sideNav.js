import { sideNav } from '@/redux/types/sideNav';

export const openSideNavMobile = () => dispatch => {
  dispatch({
    type: sideNav.OPEN_SIDE_NAV_MOBILE
  });
};

export const closeSideNavMobile = () => dispatch => {
  dispatch({
    type: sideNav.CLOSE_SIDE_NAV_MOBILE
  });
};

export const changeSearch = (text) => dispatch => {
  dispatch({
    type: sideNav.CHANGE_SEARCH,
    payload: {
      searchText: text
    }
  });
};
