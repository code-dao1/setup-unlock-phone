import { Ui } from '@/redux/types/system';

export const initSystem = () => dispatch => {
  dispatch({
    type: Ui.INIT_SYSTEM
  });
};

export const changeDimension = () => dispatch => {
  dispatch({
    type: Ui.CHANGE_DIMENSION
  });
};

export const changeAff = (aff) => dispatch => {
  dispatch({
    type: Ui.CHANGE_AFF,
    payload: {
      aff
    }
  });
};

export const focusWindow = () => dispatch => {
  dispatch({
    type: Ui.FOCUS_WINDOW
  });
};

export const blurWindow = () => dispatch => {
  dispatch({
    type: Ui.BLUR_WINDOW
  });
};

export const changeScrollBody = (rectBody) => dispatch => {
  dispatch({
    type: Ui.UPDATE_RECT_WINDOW,
    payload: { data: rectBody }
  });
};
