#!/bin/bash
# shellcheck disable=SC2164
cd script
# shellcheck disable=SC2162
sudo rm -r themes
mkdir themes
cp -r frontend/* themes

echo "Nhập domain api frontend-admin gọi lấy resource với format: http://api.domain.vn hoạc https://api.domain.vn"
# shellcheck disable=SC2162
read -p "Nhập domain api " domain

# Check if string is empty using -z. For more 'help test'
if [[ $domain == https:* ]]; then
  sed -i "s/http:\/\/localhost:9090/https:\/\/${domain##*//}/g" themes/*/static/js/main.*.chunk.js
elif [[ $domain == http:* ]]; then
  sed -i "s/http:\/\/localhost:9090/http:\/\/${domain##*//}/g" themes/*/static/js/main.*.chunk.js
else
  printf "Domain api không hợp lệ!"
  exit 1
fi

#start admin
sudo rm -r /usr/share/nginx/html/admin
sudo mkdir -p /usr/share/nginx/html/admin
sudo cp -r themes/theme1/* /usr/share/nginx/html/admin
sudo systemctl restart nginx
