#!/bin/bash
# shellcheck disable=SC2164
cd script
# shellcheck disable=SC2162
portSsr=SSR_PORT
domainWebsite=DOMAIN_WEBSITE
brandNameSsr=BRAND_NAME_SSR
sudo rm ssr/.env

# shellcheck disable=SC2129
echo "SECOND_SECRET=2" >>ssr/.env
echo "LOGO=\"/media/images/system/logo-1x.png\"" >>ssr/.env
echo "URL_PRODUCT=""$domainWebsite" >>ssr/.env
echo "PRODUCT_BRAND=\"""$brandNameSsr""\"" >>ssr/.env

echo "Nhập domain api ssr gọi lấy resource với format: http://api.domain.vn hoạc https://api.domain.vn"
# shellcheck disable=SC2162
read -p "Nhập domain api " domain
if [[ $domain == https:* ]]; then
  echo "$domain"
elif [[ $domain == http:* ]]; then
  echo "$domain"
else
  printf "Domain api không hợp lệ!"
  exit 1
fi

echo "BASE_URL=""$domain" >>ssr/.env

#start ssr
./ssr.sh $portSsr
