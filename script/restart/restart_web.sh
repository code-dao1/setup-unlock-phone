#!/bin/bash
# shellcheck disable=SC2164
cd script
# shellcheck disable=SC2162
sudo rm -r client/html
mkdir -p client/html
cp -r client/build/* client/html

domainWebsite=DOMAIN_WEBSITE
brandNameWebsite=BRAND_NAME_WEBSITE

if [[ $domainWebsite == https:* ]]; then
  sed -i "s/URL_PRODUCT_DOMAIN/https:\/\/${domainWebsite##*//}/g" client/html/static/js/main.*.chunk.js
  sed -i "s/URL_PRODUCT/https:\/\/${domainWebsite##*//}/g" client/html/index.html
elif [[ $domainWebsite == http:* ]]; then
  sed -i "s/URL_PRODUCT_DOMAIN/http:\/\/${domainWebsite##*//}/g" client/html/static/js/main.*.chunk.js
  sed -i "s/URL_PRODUCT/http:\/\/${domainWebsite##*//}/g" client/html/index.html
else
  printf "Domain website không hợp lệ!"
  exit 1
fi

sed -i "s/PRODUCT_BRAND_NAME/$brandNameWebsite/g" client/html/static/js/main.*.chunk.js
sed -i "s/PRODUCT_BRAND/$brandNameWebsite/g" client/html/index.html

echo "Nhập domain api frontend gọi lấy resource với format: http://api.domain.vn hoạc https://api.domain.vn"
# shellcheck disable=SC2162
read -p "Domain api " domain

if [[ $domain == https:* ]]; then
  sed -i "s/http:\/\/localhost:9090/https:\/\/${domain##*//}/g" client/html/static/js/main.*.chunk.js
elif [[ $domain == http:* ]]; then
  sed -i "s/http:\/\/localhost:9090/http:\/\/${domain##*//}/g" client/html/static/js/main.*.chunk.js
else
  printf "Domain api không hợp lệ!"
  exit 1
fi

sudo rm -r /usr/share/nginx/html/client
sudo mkdir -p /usr/share/nginx/html/client
sudo cp -r client/html/* /usr/share/nginx/html/client
sudo systemctl restart nginx