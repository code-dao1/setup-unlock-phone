#!/bin/bash
# shellcheck disable=SC2164
cd script

username=USERNAME_DB
password=PASSWORD_DB
host=HOST_DB
port=PORT_DB
jdbc="jdbc:mysql://$host:$port/unlock_phone"

# shellcheck disable=SC2162
read -p "Nhập port cho api (default: 9090) " portApi
portApi=${portApi:-9090}

echo "Nhập domain api frontend gọi lấy resource với format: http://api.domain.vn hoạc https://api.domain.vn"
# shellcheck disable=SC2162
read -p "Domain api " domain

if [[ $domain == https:* ]]; then
  echo "$domain"
elif [[ $domain == http:* ]]; then
  echo "$domain"
else
  printf "Domain api không hợp lệ!"
  exit 1
fi

sudo docker build -t cmdd/unlock-phone .
echo 'sleep 5s'
sleep 5

sudo docker rm -f unlock-phone
echo 'sleep 2s'
sleep 2

args="-Dspring.datasource.url=$jdbc -Dspring.datasource.jdbc-url=$jdbc -Dspring.datasource.username=$username -Dspring.datasource.password=$password -Dserver.domain=$domain"
sudo docker run -e JAVA_ARGS="$args" -p "$portApi":9090 -v /tmp/projects/unlock_phone:/usr/app/upload-dir -v /usr/share/nginx/html/admin:/usr/app/ui-deploy --name unlock-phone -d cmdd/unlock-phone
