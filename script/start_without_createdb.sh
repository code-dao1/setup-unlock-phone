#!/bin/bash
# create a variable password for database

read -p "username: " username

# Check if string is empty using -z. For more 'help test'
if [[ -z "$username" ]]; then
  printf '%s\n' "usename không hợp lệ"
  exit 1
else
  # If userInput is not empty show what the user typed in and run ls -l
  printf "(database) username %s \n" "$username"
fi

read -p "(database) password: " password

# Check if string is empty using -z. For more 'help test'
if [[ -z "$password" ]]; then
  printf '%s\n' "password không hợp lệ"
  exit 1
else
  # If userInput is not empty show what the user typed in and run ls -l
  printf "password %s \n" "$password"
fi

read -p "(database) host: " host

# Check if string is empty using -z. For more 'help test'
if [[ -z "$host" ]]; then
  printf '%s\n' "host không hợp lệ"
  exit 1
else
  # If userInput is not empty show what the user typed in and run ls -l
  printf "host %s \n" "$host"
fi

read -p "(database) port: " port

# Check if string is empty using -z. For more 'help test'
if [[ -z "$port" ]]; then
  printf '%s\n' "port không hợp lệ"
  exit 1
else
  # If userInput is not empty show what the user typed in and run ls -l
  printf "port %s \n" "$port"
fi

#
#echo 'jdbc-url định dạng: jdbc:mysql://host:port/unlock_phone'
#read -p "(database) jdbc-url: " jdbc
#
## Check if string is empty using -z. For more 'help test'
#if [[ -z "$jdbc" ]]; then
#  printf '%s\n' "jdbc-url không hợp lệ"
#  exit 1
#else
#  # If userInput is not empty show what the user typed in and run ls -l
#  printf "jdbc-url %s \n" "$jdbc"
#fi

sudo mkdir -p /etc/mysql/unlock-phone/conf.d
sudo mkdir -p /var/lib/mysql/unlock-phone

sudo docker rm -f mysql-unlock-phone
sudo docker rm -f unlock-phone

sudo docker network rm unlock-phone-network
sleep 2
sudo docker network create unlock-phone-network

sudo docker build -t cmdd/unlock-phone .

echo 'sleep 10s'
sleep 10
mysql -h $host -P $port --user=$username --password="$password" -e " create database unlock_phone; "

echo 'init database'
mysql -h $host -P $port --user=$username --password="$password" unlock_phone <unlock_phone.sql

echo 'sleep 5s'
sleep 5

jdbc="jdbc:mysql://$host:$port/unlock_phone"

args="-Dspring.datasource.url=$jdbc -Dspring.datasource.jdbc-url=$jdbc -Dspring.datasource.username=$username -Dspring.datasource.password=$password -Dserver.domain=$2"

sudo mkdir -p /tmp/projects/unlock_phone
sudo chmod a+rwx /tmp/projects/unlock_phone

sudo docker run -e JAVA_ARGS="$args" -p $1:9090 -v /tmp/projects/unlock_phone:/usr/app/upload-dir -v /usr/share/nginx/html/admin:/usr/app/ui-deploy --name unlock-phone -d cmdd/unlock-phone

sed -i "s/USERNAME_DB/$username/g" ../restart_api.sh
sed -i "s/PASSWORD_DB/$password/g" ../restart_api.sh
sed -i "s/HOST_DB/$host/g" ../restart_api.sh
sed -i "s/PORT_DB/$port/g" ../restart_api.sh