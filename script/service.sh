#!/bin/bash
# shellcheck disable=SC2162
sudo rm -r themes
sudo rm -r client/html
mkdir themes
mkdir -p client/html
cp -r frontend/* themes
cp -r client/build/* client/html
sudo rm ssr/.env
cp restart/* ../

echo "Nhập thông tin website"
echo "Nhập domain website với format: http://domain.vn hoạc https://domain.vn "
read -p "Nhập domain website: " websiteDomain

if [[ $websiteDomain == https:* ]]; then
  sed -i "s/URL_PRODUCT_DOMAIN/https:\/\/${websiteDomain##*//}/g" client/html/static/js/main.*.chunk.js
  sed -i "s/URL_PRODUCT/https:\/\/${websiteDomain##*//}/g" client/html/index.html
  sed -i "s/DOMAIN_WEBSITE/https:\/\/${websiteDomain##*//}/g" ../restart_web.sh
  sed -i "s/DOMAIN_WEBSITE/https:\/\/${websiteDomain##*//}/g" ../restart_ssr.sh
elif [[ $websiteDomain == http:* ]]; then
  sed -i "s/URL_PRODUCT_DOMAIN/http:\/\/${websiteDomain##*//}/g" client/html/static/js/main.*.chunk.js
  sed -i "s/URL_PRODUCT/http:\/\/${websiteDomain##*//}/g" client/html/index.html
  sed -i "s/DOMAIN_WEBSITE/http:\/\/${websiteDomain##*//}/g" ../restart_web.sh
  sed -i "s/DOMAIN_WEBSITE/http:\/\/${websiteDomain##*//}/g" ../restart_ssr.sh
else
  printf "Domain website không hợp lệ!"
  exit 1
fi

read -p "Nhập tên website: " brandNameWebsite
sed -i "s/PRODUCT_BRAND_NAME/$brandNameWebsite/g" client/html/static/js/main.*.chunk.js
sed -i "s/PRODUCT_BRAND/$brandNameWebsite/g" client/html/index.html

echo "Nhập thông tin ssr"
# ssr
echo "SECOND_SECRET=2" >>ssr/.env
echo "LOGO=\"/media/images/system/logo-1x.png\"" >>ssr/.env
echo "URL_PRODUCT=""$websiteDomain" >>ssr/.env

read -p "Nhập tên website cho ssr: " brandNameSsr
echo "PRODUCT_BRAND_NAME/$brandNameSsr/g"
echo "PRODUCT_BRAND=\"""$brandNameSsr""\"" >>ssr/.env

read -p "Nhập port cho ssr (default: 3001) " portSsr
portSsr=${portSsr:-3001}

echo "Nhập thông tin api"
# api
read -p "Nhập port cho server (default: 9090) " portService
portService=${portService:-9090}

echo "Nhập domain api frontend gọi lấy resource với format: http://api.domain.vn hoạc https://api.domain.vn"
read -p "Nhập domain api " domain

if [[ $domain == https:* ]]; then
  sed -i "s/http:\/\/localhost:9090/https:\/\/${domain##*//}/g" themes/*/static/js/main.*.chunk.js
  sed -i "s/http:\/\/localhost:9090/https:\/\/${domain##*//}/g" client/html/static/js/main.*.chunk.js
elif [[ $domain == http:* ]]; then
  sed -i "s/http:\/\/localhost:9090/http:\/\/${domain##*//}/g" themes/*/static/js/main.*.chunk.js
  sed -i "s/http:\/\/localhost:9090/http:\/\/${domain##*//}/g" client/html/static/js/main.*.chunk.js
else
  printf "Domain api không hợp lệ!"
  exit 1
fi
echo "BASE_URL=""$domain" >>ssr/.env

echo "**Cấu hình mysql**: quá trình cài đặt cho phép chọn 2 chế độ: tạo mới mysql và sử dụng cấu hình mysql có sẵn. Chọn y tạo mới mysql, n sử dụng cấu hình mysql có sẵn"
read -p "Tạo database y/n? " createdb

if [[ "$createdb" == "y" ]]; then
  ./start_with_createdb.sh "$portService" "$domain"
elif [[ "$createdb" == "n" ]]; then
  ./start_without_createdb.sh "$portService" "$domain"
else
  ./service.sh
fi

#start admin
./admin.sh

#start ssr
./ssr.sh $portSsr

# replace value default web
sed -i "s/BRAND_NAME_WEBSITE/$brandNameWebsite/g" ../restart_web.sh
# replace value default ssr
sed -i "s/SSR_PORT/$portSsr/g" ../restart_ssr.sh
sed -i "s/BRAND_NAME_SSR/$brandNameSsr/g" ../restart_ssr.sh
